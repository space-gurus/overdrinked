// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "OverdrinkedGameMode.generated.h"

UCLASS(minimalapi)
class AOverdrinkedGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AOverdrinkedGameMode();
};



