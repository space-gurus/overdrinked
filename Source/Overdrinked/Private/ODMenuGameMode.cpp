// Copyright 2020 Space Gurus


#include "ODMenuGameMode.h"
#include "ODMenuPlayerController.h"
#include "ODLobbyGameState.h"
#include "ODMenuPawn.h"
#include "ODGameInstance.h"
#include "ODPlayerState.h"
#include "Kismet/GameplayStatics.h"

AODMenuGameMode::AODMenuGameMode()
{
	PlayerControllerClass = AODMenuPlayerController::StaticClass();
	DefaultPawnClass = AODMenuPawn::StaticClass();
	GameStateClass = AODLobbyGameState::StaticClass();
	PlayerStateClass = AODPlayerState::StaticClass();
	
	bUseSeamlessTravel = true;
}

void AODMenuGameMode::PostLogin(APlayerController* NewPlayer)
{
	Super::PostLogin(NewPlayer);

	UE_LOG(LogTemp, Warning, TEXT("AODMenuGameMode::PostLogin"));

	HandleNewPlayer(NewPlayer);
}

void AODMenuGameMode::Logout(AController* Exiting)
{
	Super::Logout(Exiting);

	AODMenuPlayerController* ExitingPC = Cast<AODMenuPlayerController>(Exiting);
	if (ExitingPC)
	{
		int32 ExitPlayerNum = -1;
		// Get the player's ID
		AODPlayerState* ExitPlayerState = Cast<AODPlayerState>(ExitingPC->PlayerState);
		if (ExitPlayerState)
		{
			ExitPlayerNum = ExitPlayerState->GetPlayerNum();

			if (ExitPlayerNum != -1)
			{
				NumberOfPlayers--;
				PlayerControllerList.Remove(ExitingPC);

				// We need to shift everyone down
				UE_LOG(LogTemp, Warning, TEXT("Getting lobby game state"));
				AODLobbyGameState* LobbyGameState = Cast<AODLobbyGameState>(UGameplayStatics::GetGameState(GetWorld()));
				if (LobbyGameState)
				{
					LobbyGameState->Server_RemovePlayer(ExitPlayerNum);
					LobbyGameState->SetNumPlayers(NumberOfPlayers);

					UE_LOG(LogTemp, Warning, TEXT("About to shift"));
					for (APlayerState* PlayerState : LobbyGameState->PlayerArray)
					{
						AODPlayerState* ODPlayerState = Cast<AODPlayerState>(PlayerState);
						if (ODPlayerState)
						{
							// We need to shift everyone down
							if (ODPlayerState->GetPlayerNum() > ExitPlayerNum)
							{
								ODPlayerState->SetPlayerNum(ODPlayerState->GetPlayerNum() - 1);
							}
						}
					}

					UE_LOG(LogTemp, Warning, TEXT("Telling %i PCs to HandleLobbyPlayerLeave"), PlayerControllerList.Num());
					// Finally let everyone know to update their UI
					for (AODMenuPlayerController* MenuPC : PlayerControllerList)
					{
						MenuPC->HandleLobbyPlayerLeave(ExitPlayerNum);
					}
				}
			}
		}

	}
}

void AODMenuGameMode::HandleMatchHasStarted()
{
	Super::HandleMatchHasStarted();

	if (bArrivedSeamless)
	{
		// For some reason we need to handle the host separately when traveling seamless
		APlayerController* HostPlayerController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
		if (HostPlayerController)
		{
			UE_LOG(LogTemp, Warning, TEXT("AODMenuGameMode::HandleStartingNewPlayer: Handling the host with %i num players"), NumPlayers);
			HandleNewPlayer(HostPlayerController);
			bHandledHostSeamless = true;
		}
	}
}

void AODMenuGameMode::PostSeamlessTravel()
{
	Super::PostSeamlessTravel();

	bArrivedSeamless = true;
}

void AODMenuGameMode::HandleStartingNewPlayer_Implementation(APlayerController* NewPlayer)
{
	Super::HandleStartingNewPlayer_Implementation(NewPlayer);

	// get the host and see if we're already in a session
	UODGameInstance* ODGameInstance = Cast<UODGameInstance>(UGameplayStatics::GetGameInstance(GetWorld()));
	if (bArrivedSeamless && ODGameInstance && ODGameInstance->IsHost() && ODGameInstance->IsInSession())
	{
		if (!bHandledHostSeamless)
		{

		}

		UE_LOG(LogTemp, Error, TEXT("HANDLE STARTING NEW PLAYER"));
		HandleNewPlayer(NewPlayer);
	}
}

void AODMenuGameMode::HandleNewPlayer(APlayerController* NewPlayer)
{
	NumberOfPlayers++;

	AODMenuPlayerController* NewPlayerController = Cast<AODMenuPlayerController>(NewPlayer);
	if (NewPlayerController)
	{
		NewPlayerController->SetPlayerIndex(NumPlayers);

		UE_LOG(LogTemp, Warning, TEXT("AODMenuGameMode::HandleNewPlayer: Adding player to PlayerControllerList"));
		PlayerControllerList.AddUnique(NewPlayerController);

		// Update the game state
		AODLobbyGameState* LobbyGameState = Cast<AODLobbyGameState>(UGameplayStatics::GetGameState(GetWorld()));
		if (LobbyGameState)
		{
			LobbyGameState->SetNumPlayers(NumberOfPlayers);
			LobbyGameState->Server_AddPlayer(NewPlayerController, NumberOfPlayers);
		}

		AODPlayerState* NewPlayerState = Cast<AODPlayerState>(NewPlayerController->PlayerState);
		if (NewPlayerState)
		{
			NewPlayerState->SetPlayerNum(NumPlayers);
		}

		// Let all the lobby players know to update their UI
		for (AODMenuPlayerController* MenuPC : PlayerControllerList)
		{
			MenuPC->HandleNewLobbyPlayer(NumPlayers, NewPlayerController->GetPlayerName(), 1);
		}
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("AODMenuGameMode::HandleNewPlayer: Could not cast PC to MenuPC"));
	}
}

void AODMenuGameMode::CloseLobby()
{
	UE_LOG(LogOnlineGame, Log, TEXT("AODMenuGameMode::CloseLobby"));
	for (AODMenuPlayerController* MenuPC : PlayerControllerList)
	{
		MenuPC->HandleCloseLobby();
	}
}

AODMenuPlayerController* AODMenuGameMode::GetHostPlayerController()
{
	if (PlayerControllerList.IsValidIndex(0))
	{
		return PlayerControllerList[0];
	}
	else
	{
		return NULL;
	}
}

void AODMenuGameMode::NotifyPlayersOfMapTravel()
{
	for (AODMenuPlayerController* MenuPC : PlayerControllerList)
	{
		MenuPC->PrepareForMapTravel();
	}
}

void AODMenuGameMode::NotifyPlayersOfReadyStatus(int32 PlayerIndex, bool bIsReady)
{
	UE_LOG(LogTemp, Log, TEXT("AODMenuGameMode::NotifyPlayersOfReadyStatus: Telling %i players of ready status change"), PlayerControllerList.Num());
	for (AODMenuPlayerController* MenuPC : PlayerControllerList)
	{
		MenuPC->HandlePlayerReadyStatusUpdate(PlayerIndex, bIsReady);
	}
}

void AODMenuGameMode::NotifyPlayersOfNewMapName(FName MapName)
{
	UE_LOG(LogOnlineGame, Log, TEXT("UODMenuGameMode::NotifyPlayersOfNewMapName"));
	for (AODMenuPlayerController* MenuPC : PlayerControllerList)
	{
		MenuPC->SetLobbyChosenMapName(MapName);
	}
}
