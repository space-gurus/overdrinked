// Copyright 2020 Space Gurus


#include "ODMenuPlayerController.h"
#include "UObject/ConstructorHelpers.h"
#include "ODGameInstance.h"
#include "UI/ODMainMenuWidget.h"
#include "UI/ODLobbyMenuWidget.h"
#include "UI/BaseWidgets/ODWidgetPage.h"
#include "UI/BaseWidgets/ODSwitcherMenu.h"
#include "UI/ODLobbyPage.h"
#include "UI/ODHUDMainMenu.h"
#include "UI/ODMainMenu.h"
#include "Data/LevelData.h"
#include "Actors/ODLobbyPlayer.h"
#include "Actors/ODLobbyManager.h"
#include "ODLobbyGameState.h"
#include "ODLobbyGameMode.h"
#include "ODMenuGameMode.h"
#include "ODMenuPawn.h"
#include "Kismet/GameplayStatics.h"
#include "EngineUtils.h"
#include "GameFramework/PlayerState.h"
#include "Net/UnrealNetwork.h"
#include "ODEnumHelperFunctions.h"
#include "ISwitcherooModule.h"
#include "Engine/DataTable.h"
#include "AkGameplayStatics.h"

AODMenuPlayerController::AODMenuPlayerController()
{
	ConstructorHelpers::FClassFinder<UODMenuWidget> StartPageBP(TEXT("/Game/Blueprints/UI/Menu/WBP_StartPage"));
	if (StartPageBP.Class)
	{
		StartPageBPClass = StartPageBP.Class;
	}

	ConstructorHelpers::FClassFinder<UODMenuWidget> LobbyMenuBP(TEXT("/Game/Blueprints/UI/WBP_LobbyMenu"));
	if (LobbyMenuBP.Class)
	{
		LobbyMenuBPClass = LobbyMenuBP.Class;
	}

	ConstructorHelpers::FClassFinder<UODSwitcherMenu> NewMenuBP(TEXT("/Game/Blueprints/UI/Menu/WBP_MainMenu"));
	if (NewMenuBP.Class)
	{
		MainMenuBPClass = NewMenuBP.Class;
	}

	ConstructorHelpers::FObjectFinder<UDataTable> LevelDataObject(TEXT("/Game/DrinkGame/Data/DT_LevelData"));
	if (LevelDataObject.Succeeded())
	{
		LevelDataTable = LevelDataObject.Object;
	}

	// This HAS to be here
	bAutoManageActiveCameraTarget = false;
}

void AODMenuPlayerController::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AODMenuPlayerController, bIsPlayerReady);
}

void AODMenuPlayerController::BeginPlay()
{
	Super::BeginPlay();

	if (ISwitcherooModule* Switcheroo = ISwitcherooModule::Get())
	{
		Switcheroo->OnDetectedInputDeviceChanged().AddUObject(this, &AODMenuPlayerController::OnDetectedInputDeviceChanged);
	}

	if (UGameplayStatics::GetCurrentLevelName(GetWorld()).Equals("MenuLevel", ESearchCase::IgnoreCase))
	{
		LoadMainMenu();
	}
	else if (UGameplayStatics::GetCurrentLevelName(GetWorld()).Equals("LobbyMenuLevel", ESearchCase::IgnoreCase))
	{
		LoadLobbyMenu();

		TArray<AActor*> FoundActors;
		UGameplayStatics::GetAllActorsOfClass(GetWorld(), AODLobbyManager::StaticClass(), FoundActors);
		if (FoundActors.Num() > 0)
		{
			AODLobbyManager* LobbyManager = Cast<AODLobbyManager>(FoundActors[0]);
			if (LobbyManager)
			{
				if (LobbyManager->GetPlayerViewCamera())
				{
					SetViewTargetWithBlend(LobbyManager->GetPlayerViewCamera(), 0.f, EViewTargetBlendFunction::VTBlend_Linear, 0.f, false);
				}
			}
		}
	}
	else if (UGameplayStatics::GetCurrentLevelName(GetWorld()).Equals("NewMenuLevel", ESearchCase::IgnoreCase))
	{
		// Check if we came here as a new connecting client or we're just returning from somewhere else
		UODGameInstance* ODGameInstance = Cast<UODGameInstance>(UGameplayStatics::GetGameInstance(GetWorld()));
		if (ODGameInstance && ODGameInstance->HasVisitedMainMenu())
		{
			LoadMainMenu();
		}
		else
		{
			// We're arriving initially
			if (IsLocalPlayerController())
			{
				if (StartPageBPClass)
				{
					StartPage = CreateWidget<UODMenuWidget>(this, StartPageBPClass);
					if (StartPage)
					{
						StartPage->SetupWidget();
					}
				}
			}
		}

	}
}

void AODMenuPlayerController::OnDetectedInputDeviceChanged(ESwitcherooInputDevice ActiveDevice)
{
	if (ActiveParentPage)
	{
		ActiveParentPage->HandleDeviceChanged(ActiveDevice);
	}
	
	switch (ActiveDevice)
	{
		case ESwitcherooInputDevice::Gamepad:
			bShowMouseCursor = false;
			SetMouseLocation(0, 0);
			break;
		case ESwitcherooInputDevice::KeyboardMouse:
			bShowMouseCursor = true;
			break;
	}
}

void AODMenuPlayerController::LoadMainMenu()
{
	if (IsLocalPlayerController())
	{
		if (MainMenuBPClass)
		{
			MainMenuWidget = CreateWidget<UODSwitcherMenu>(this, MainMenuBPClass);
			if (MainMenuWidget)
			{
				MainMenuWidget->SetupWidget();
				ActiveParentPage = MainMenuWidget->GetActiveWidgetPage();
				if (ActiveParentPage)
				{
					ActiveParentPage->BecomeActive(true);
				}
			}
		}
	}
}

void AODMenuPlayerController::LoadLobbyMenu()
{
	if (IsLocalPlayerController())
	{
		if (LobbyMenuBPClass)
		{
			LobbyMenuWidget = CreateWidget<UODLobbyMenuWidget>(this, LobbyMenuBPClass);
			if (LobbyMenuWidget)
			{
				LobbyMenuWidget->SetupWidget();
			}
		}
	}
}

void AODMenuPlayerController::TransitionToLobbyMenu()
{
	// Tear down main menu first
	if (MainMenuWidget)
	{
		MainMenuWidget->TearDownWidget();
	}

	LoadLobbyMenu();
}

void AODMenuPlayerController::TransitionFromLobbyToMainMenu()
{
	if (LobbyMenuWidget)
	{
		LobbyMenuWidget->TearDownWidget();
	}

	LoadMainMenu();
}

void AODMenuPlayerController::PrepareForMapTravel_Implementation()
{
	UE_LOG(LogTemp, Warning, TEXT("AODMenuPlayerController::PrepareForMapTravel_Implementation"));
	if (IsLocalPlayerController())
	{
		AODHUDMainMenu* MainHUD = Cast<AODHUDMainMenu>(GetHUD());
		if (MainHUD)
		{
			if (MainHUD->GetMainMenu())
			{
				StopMenuMusic();
				MainHUD->GetMainMenu()->PrepareForMapTravel();
			}
		}
	}
}

void AODMenuPlayerController::Server_TravelToSelectedMap_Implementation()
{
	UODGameInstance* ODGameInstance = Cast<UODGameInstance>(UGameplayStatics::GetGameInstance(GetWorld()));
	AODLobbyGameState* LobbyGameState = Cast<AODLobbyGameState>(UGameplayStatics::GetGameState(GetWorld()));
	AODMenuGameMode* MenuGameMode = Cast<AODMenuGameMode>(UGameplayStatics::GetGameMode(GetWorld()));
	UWorld* ThisWorld = GetWorld();

	if (MenuGameMode)
	{
		MenuGameMode->NotifyPlayersOfMapTravel();
	}

	FString LevelTravelURL = "";

	UE_LOG(LogTemp, Warning, TEXT("Searching for map: %s"), *LobbyGameState->GetChosenMapName().ToString());

	if (LevelDataTable)
	{
		for (FName LevelRowName : LevelDataTable->GetRowNames())
		{
			FLevelData* LevelData = LevelDataTable->FindRow<FLevelData>(LevelRowName, FString(""));
			if (LevelData)
			{
				if (LevelData->LevelName.Equals(LobbyGameState->GetChosenMapName().ToString(), ESearchCase::IgnoreCase))
				{
					LevelTravelURL = LevelData->LevelReferencePath;
				}
			}
		}
	}

	if (ODGameInstance && LobbyGameState && LobbyGameState->AreAllPlayersReady() && MenuGameMode && ThisWorld)
	{
		ODGameInstance->SetPartySize(MenuGameMode->GetNumPlayers());

		//MenuGameMode->NotifyPlayersOfMapTravel();
		UE_LOG(LogTemp, Warning, TEXT("Travelling to URL: %s"), *LevelTravelURL);
		ThisWorld->ServerTravel(LevelTravelURL);
	}
}

void AODMenuPlayerController::Client_HandleHostDisconnect_Implementation()
{
	UWorld* ThisWorld = GetWorld();
	if (ThisWorld)
	{
		UE_LOG(LogTemp, Error, TEXT("Lost connection to host. Going back to main menu."));
	}
}

UODSwitcherMenu* AODMenuPlayerController::GetMainMenuWidget()
{
	return MainMenuWidget;
}

UODLobbyMenuWidget* AODMenuPlayerController::GetLobbyMenuWidget()
{
	return LobbyMenuWidget;
}

void AODMenuPlayerController::SetActivePageWidget(UODWidgetPage* NewPage, bool bTriggerShowPage)
{
	// Handle if we're nested
	bool bIsNested = false;
	if (NewPage && NewPage->HasNestedSwitcherMenu())
	{
		NewPage->GetNestedSwitcherMenu()->GetActiveWidgetPage()->BecomeActive(bTriggerShowPage);
		bIsNested = true;
	}

	ActiveParentPage = NewPage;
	if (ActiveParentPage && !bIsNested)
	{
		ActiveParentPage->BecomeActive(bTriggerShowPage);
	}
}

bool AODMenuPlayerController::IsPlayerReady()
{
	return bIsPlayerReady;
}

void AODMenuPlayerController::Server_ToggleIsPlayerReady_Implementation()
{
	bIsPlayerReady = !bIsPlayerReady;

	// Let the game state know
	AODLobbyGameState* LobbyGameState = Cast<AODLobbyGameState>(UGameplayStatics::GetGameState(GetWorld()));
	if (LobbyGameState)
	{
		LobbyGameState->Server_SetPlayerReadyStatus(this);
	}
}

void AODMenuPlayerController::SetLobbyAvatarReference(AODLobbyPlayer* LobbyPlayer)
{
	if (LobbyPlayer)
	{
		LobbyAvatar = LobbyPlayer;

		// Let the avatar know too
		LobbyAvatar->SetPlayerControllerReference(this);
	}
}

AODLobbyPlayer* AODMenuPlayerController::GetLobbyAvatar()
{
	return LobbyAvatar;
}

FName AODMenuPlayerController::GetPlayerName()
{
	return FName(PlayerState->GetPlayerName());
}

void AODMenuPlayerController::SetLobbyChosenMapName_Implementation(FName MapName)
{
	UE_LOG(LogOnlineGame, Log, TEXT("AODMenuPlayerController::SetLobbyChosenMapName"));
	if (IsLocalPlayerController())
	{
		UE_LOG(LogOnlineGame, Log, TEXT("We are a local controller"));
		AODHUDMainMenu* MainHUD = Cast<AODHUDMainMenu>(GetHUD());
		if (MainHUD)
		{
			UE_LOG(LogOnlineGame, Log, TEXT("Got the HUD"));
			if (MainHUD->GetMainMenu())
			{
				UE_LOG(LogOnlineGame, Log, TEXT("Got the MainMenu"));
				UODLobbyPage* LobbyPage = MainHUD->GetMainMenu()->GetClientLobby();
				if (LobbyPage)
				{
					UE_LOG(LogOnlineGame, Log, TEXT("Got the LobbyPage"));
					LobbyPage->UpdateChosenMapName(MapName);
				}
			}
		}
	}
}

void AODMenuPlayerController::UpdateLobbyFriendsList(TArray<FString> PlayerNames)
{
	if (LobbyMenuWidget)
	{
		LobbyMenuWidget->UpdateFriendsList(PlayerNames);
	}
}

void AODMenuPlayerController::HandleNewLobbyPlayer_Implementation(int32 PlayerIndex, FName PlayerName, int32 PlayerLevel)
{
	AODHUDMainMenu* MainHUD = Cast<AODHUDMainMenu>(GetHUD());
	if (MainHUD)
	{
		if (MainHUD->GetMainMenu())
		{
			UODLobbyPage* LobbyPage = MainHUD->GetMainMenu()->GetActiveLobbyPage();
			if (LobbyPage)
			{
				LobbyPage->SetNewPlayer(PlayerIndex, PlayerName, PlayerLevel);
			}
		}
	}
}

void AODMenuPlayerController::HandleLobbyPlayerLeave_Implementation(int32 PlayerIdx)
{
	AODHUDMainMenu* MainHUD = Cast<AODHUDMainMenu>(GetHUD());
	if (MainHUD)
	{
		if (MainHUD->GetMainMenu())
		{
			UODLobbyPage* LobbyPage = MainHUD->GetMainMenu()->GetActiveLobbyPage();
			if (LobbyPage)
			{
				LobbyPage->RemovePlayer(PlayerIdx);
			}
		}
	}
}

void AODMenuPlayerController::HandlePlayerReadyStatusUpdate_Implementation(int32 PlayerIndex, bool bIsReady)
{
	AODHUDMainMenu* MainHUD = Cast<AODHUDMainMenu>(GetHUD());
	if (MainHUD)
	{
		if (MainHUD->GetMainMenu())
		{
			UODLobbyPage* LobbyPage = MainHUD->GetMainMenu()->GetActiveLobbyPage();
			if (LobbyPage)
			{
				LobbyPage->SetPlayerReadyStatus(PlayerIndex, bIsReady);
			}
		}
	}
}

UODWidgetPage* AODMenuPlayerController::GetActiveWidgetPage()
{
	return ActiveParentPage;
}

void AODMenuPlayerController::SetPlayerIndex_Implementation(int32 Index)
{
	MPPlayerIndex = Index;
}

int32 AODMenuPlayerController::GetPlayerIndex()
{
	return MPPlayerIndex;
}

void AODMenuPlayerController::HandleCloseLobby_Implementation()
{
	if (IsLocalPlayerController())
	{
		UODGameInstance* ODGameInstance = Cast<UODGameInstance>(UGameplayStatics::GetGameInstance(GetWorld()));
		if (ODGameInstance)
		{
			if (ODGameInstance->IsHost())
			{
				AODMenuGameMode* MenuGameMode = Cast<AODMenuGameMode>(UGameplayStatics::GetGameMode(GetWorld()));
				if (MenuGameMode)
				{
					ODGameInstance->HostCloseSession();
				}
			}
			else
			{
				ODGameInstance->ClientCloseSession();
			}
		}
	}

	AODHUDMainMenu* MainHUD = Cast<AODHUDMainMenu>(GetHUD());
	if (MainHUD)
	{
		if (MainHUD->GetMainMenu())
		{
			UODLobbyPage* LobbyPage = MainHUD->GetMainMenu()->GetActiveLobbyPage();
			if (LobbyPage)
			{
				LobbyPage->HandleCloseLobby();
			}
		}
	}
}

void AODMenuPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();

// 	InputComponent->BindAction("NavigateMenuUp", IE_Pressed, this, &AODMenuPlayerController::HandleMenuUp);
// 	InputComponent->BindAction("NavigateMenuDown", IE_Pressed, this, &AODMenuPlayerController::HandleMenuDown);
// 	InputComponent->BindAction("MenuTabLeft", IE_Pressed, this, &AODMenuPlayerController::HandleTabLeft);
// 	InputComponent->BindAction("MenuTabRight", IE_Pressed, this, &AODMenuPlayerController::HandleTabRight);
// 	InputComponent->BindAction("MenuBack", IE_Pressed, this, &AODMenuPlayerController::HandleMenuBack);
}

void AODMenuPlayerController::HandleMenuUp()
{
// 	if (MainMenuWidget)
// 	{
// 		if (!bIsNavigatingWithController)
// 		{
// 			bIsNavigatingWithController = true;
// 			MainMenuWidget->StartNavigatingWithController();
// 		}
// 		else
// 		{
// 			MainMenuWidget->MoveMenuSelectionUp();
// 		}
// 	}
}

void AODMenuPlayerController::HandleMenuDown()
{
// 	if (MainMenuWidget)
// 	{
// 		if (!bIsNavigatingWithController)
// 		{
// 			bIsNavigatingWithController = true;
// 			MainMenuWidget->StartNavigatingWithController();
// 		}
// 		else
// 		{
// 			MainMenuWidget->MoveMenuSelectionDown();
// 		}
// 	}
}

void AODMenuPlayerController::HandleTabLeft()
{
	if (ActiveParentPage && ActiveParentPage->HasNestedSwitcherMenu())
	{
		UODSwitcherMenu* NestedSwitcher = ActiveParentPage->GetNestedSwitcherMenu();
		if (NestedSwitcher && NestedSwitcher->IsTabMenu())
		{
			NestedSwitcher->ChangeTabDirectional(false);
		}
	}
}

void AODMenuPlayerController::HandleTabRight()
{
	if (ActiveParentPage && ActiveParentPage->HasNestedSwitcherMenu())
	{
		UODSwitcherMenu* NestedSwitcher = ActiveParentPage->GetNestedSwitcherMenu();
		if (NestedSwitcher && NestedSwitcher->IsTabMenu())
		{
			NestedSwitcher->ChangeTabDirectional(true);
		}
	}
}

void AODMenuPlayerController::HandleMenuBack()
{
	if (ActiveParentPage)
	{
		ActiveParentPage->HandleBack();
	}
}

void AODMenuPlayerController::OnRep_IsPlayerReady()
{
	if (LobbyMenuWidget)
	{
		LobbyMenuWidget->UpdateReadyStatus();
	}
}

void AODMenuPlayerController::StopMenuMusic()
{
	if (StopMenuMusicEvent)
	{
		UE_LOG(LogTemp, Error, TEXT("AODMenuPlayerController::StopMenuMusic"))
		FOnAkPostEventCallback NullCallback;
		UAkGameplayStatics::PostEvent(StopMenuMusicEvent, this, 0, NullCallback);
	}
}
