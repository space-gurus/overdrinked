// Copyright 2020 Space Gurus


#include "ODEnumHelperFunctions.h"

FName UODEnumHelperFunctions::IngredientEnumToString(EIngredientType IngredientType)
{
	switch (IngredientType)
	{
		case EIngredientType::IT_None:
			return "None";
		case EIngredientType::IT_Shlum:
			return "Shlum";
		case EIngredientType::IT_Fitzky:
			return "Fitzky";
		case EIngredientType::IT_Shocka:
			return "Shocka";
		case EIngredientType::IT_Krin:
			return "Krin";
		case EIngredientType::IT_PurpleDeath:
			return "PurpleDeath";
		case EIngredientType::IT_RedStool:
			return "RedStoolEnergy";
		case EIngredientType::IT_LiquidSun:
			return "LiquidDoubleSun";
		case EIngredientType::IT_Sprixel:
			return "Sprixel";
		case EIngredientType::IT_Zoorp:
			return "ZoorpLite";
		case EIngredientType::IT_HeavyHop:
			return "HeavyHopJPA";
		case EIngredientType::IT_BlackHoleStout:
			return "BlackHoleStout";
		default:
			return "ERROR";
	}
}

EIngredientType UODEnumHelperFunctions::IngredientStringToEnum(FName IngredientName)
{
	if (IngredientName.IsEqual("Shlum", ENameCase::IgnoreCase))
	{
		return EIngredientType::IT_Shlum;
	}
	else if (IngredientName.IsEqual("Fitzky", ENameCase::IgnoreCase))
	{
		return EIngredientType::IT_Fitzky;
	}
	else if (IngredientName.IsEqual("Shocka", ENameCase::IgnoreCase))
	{
		return EIngredientType::IT_Shocka;
	}
	else if (IngredientName.IsEqual("Krin", ENameCase::IgnoreCase))
	{
		return EIngredientType::IT_Krin;
	}
	else if (IngredientName.IsEqual("PurpleDeath", ENameCase::IgnoreCase))
	{
		return EIngredientType::IT_PurpleDeath;
	}
	else if (IngredientName.IsEqual("RedStoolEnergy", ENameCase::IgnoreCase))
	{
		return EIngredientType::IT_RedStool;
	}
	else if (IngredientName.IsEqual("LiquidDoubleSun", ENameCase::IgnoreCase))
	{
		return EIngredientType::IT_LiquidSun;
	}
	else if (IngredientName.IsEqual("Sprixel", ENameCase::IgnoreCase))
	{
		return EIngredientType::IT_Sprixel;
	}
	else if (IngredientName.IsEqual("Zoorp", ENameCase::IgnoreCase))
	{
		return EIngredientType::IT_Zoorp;
	}
	else if (IngredientName.IsEqual("HeavyHop", ENameCase::IgnoreCase))
	{
		return EIngredientType::IT_HeavyHop;
	}
	else if (IngredientName.IsEqual("BlackHoleStout", ENameCase::IgnoreCase))
	{
		return EIngredientType::IT_BlackHoleStout;
	}
	else
	{
		return EIngredientType::IT_None;
	}
}

FName UODEnumHelperFunctions::DrinkEnumToString(EDrink DrinkType)
{
	switch (DrinkType)
	{
		case EDrink::D_None:
			return "None";
		case EDrink::D_ZoorpLite:
			return "ZoorpLite";
		case EDrink::D_HeavyHopJPA:
			return "HeavyHopJPA";
		case EDrink::D_BlackHoleStout:
			return "BlackHoleStout";
		case EDrink::D_FizzleBuzz:
			return "FizzleBuzz";
		case EDrink::D_SexInTheCrater:
			return "SexInTheCrater";
		case EDrink::D_NewFashioned:
			return "NewFashioned";
		case EDrink::D_DinnoriaDomer:
			return "DinnoriaDomer";
		default:
			return "Error";
	}
}

EDrink UODEnumHelperFunctions::DrinkStringToEnum(FName DrinkName)
{
	if (DrinkName.IsEqual("ZoorpLite", ENameCase::IgnoreCase))
	{
		return EDrink::D_ZoorpLite;
	}
	else if (DrinkName.IsEqual("HeavyHopJPA", ENameCase::IgnoreCase))
	{
		return EDrink::D_HeavyHopJPA;
	}
	else if (DrinkName.IsEqual("BlackHoleStout", ENameCase::IgnoreCase))
	{
		return EDrink::D_BlackHoleStout;
	}
	else if (DrinkName.IsEqual("FizzleBuzz", ENameCase::IgnoreCase))
	{
		return EDrink::D_FizzleBuzz;
	}
	else if (DrinkName.IsEqual("SexInTheCrater", ENameCase::IgnoreCase))
	{
		return EDrink::D_SexInTheCrater;
	}
	else if (DrinkName.IsEqual("NewFashioned", ENameCase::IgnoreCase))
	{
		return EDrink::D_NewFashioned;
	}
	else if (DrinkName.IsEqual("DinnoriaDomer", ENameCase::IgnoreCase))
	{
		return EDrink::D_DinnoriaDomer;
	}
	else
	{
		return EDrink::D_None;
	}
}

FName UODEnumHelperFunctions::CustomerEmoteEnumToString(ECustomerEmote Emote)
{
	switch (Emote)
	{
		case ECustomerEmote::CE_Angry:
			return "Angry";
		case ECustomerEmote::CE_Happy:
			return "Happy";
		case ECustomerEmote::CE_Sad:
			return "Sad";
		default:
			return "Error";
	}
}

ECustomerEmote UODEnumHelperFunctions::CustomerEmoteStringToEnum(FName EmoteName)
{
	if (EmoteName.IsEqual("Angry", ENameCase::IgnoreCase))
	{
		return ECustomerEmote::CE_Angry;
	}
	else if (EmoteName.IsEqual("Happy", ENameCase::IgnoreCase))
	{
		return ECustomerEmote::CE_Happy;
	}
	else if (EmoteName.IsEqual("Sad", ENameCase::IgnoreCase))
	{
		return ECustomerEmote::CE_Sad;
	}
	else
	{
		return ECustomerEmote::CE_Sad;
	}
}

float UODEnumHelperFunctions::GetGlassShapeIndex(EGlassType GlassType)
{
	switch (GlassType)
	{
	case EGlassType::GT_Brew:
		return 1.0f;
		break;
	case EGlassType::GT_Mixer:
		return 3.0f;
		break;
	case EGlassType::GT_Shot:
		return 0.0f;
		break;
	case EGlassType::GT_Stemmed:
		return 2.0f;
		break;
	case EGlassType::GT_Blender:
		return 1.0f;
		break;
	default:
		return -1.0f;
		break;
	}
}

FString UODEnumHelperFunctions::ConvertTimeRemainingToClock(int32 TimeRemaining)
{
	if (TimeRemaining <= 0)
	{
		return "02:00 AM";
	}

	int32 Hours = TimeRemaining / 60;
	int32 Seconds = TimeRemaining % 60;

	FString ClockTime;

	switch (Hours)
	{
		case 0:
			if (Seconds > 0)
				ClockTime.Append("01:");
			else if (Seconds == 0)
				ClockTime.Append("02:");
			break;
		case 1:
			if (Seconds > 0)
				ClockTime.Append("12:");
			else if (Seconds == 0)
				ClockTime.Append("01:");
			break;
		case 2:
			if (Seconds > 0)
				ClockTime.Append("11:");
			else if (Seconds == 0)
				ClockTime.Append("12:");
			break;
		case 3:
			if (Seconds > 0)
				ClockTime.Append("10:");
			else if (Seconds == 0)
				ClockTime.Append("11:");
			break;
		case 4:
			if (Seconds > 0)
				ClockTime.Append("09:");
			else if (Seconds == 0)
				ClockTime.Append("10:");
			break;
		case 5:
			if (Seconds > 0)
				ClockTime.Append("08:");
			else if (Seconds == 0)
				ClockTime.Append("09:");
			break;
		case 6:
			if (Seconds > 0)
				ClockTime.Append("07:");
			else if (Seconds == 0)
				ClockTime.Append("08:");
			break;
		case 7:
			if (Seconds > 0)
				ClockTime.Append("06:");
			else if (Seconds == 0)
				ClockTime.Append("07:");
			break;
		case 8:
			if (Seconds > 0)
				ClockTime.Append("05:");
			else if (Seconds == 0)
				ClockTime.Append("06:");
			break;
		case 9:
			if (Seconds > 0)
				ClockTime.Append("04:");
			else if (Seconds == 0)
				ClockTime.Append("05:");
			break;
	}

	if (Seconds == 0)
	{
		ClockTime.Append("00");
	}
	else
	{
		int32 SecondsThisHour = 60 - Seconds;

		if (SecondsThisHour < 10)
		{
			ClockTime.Append("0");
		}
		
		ClockTime.Append(FString::FromInt(SecondsThisHour));
	}

	// Handle AM/PM
	if (TimeRemaining <= 120)
	{
		ClockTime.Append(" AM");
	}
	else
	{
		ClockTime.Append(" PM");
	}

	return ClockTime;
}

FString UODEnumHelperFunctions::ConvertMapNameToURL(FName MapName)
{
	if (MapName.IsEqual("dev_gameplay", ENameCase::IgnoreCase))
	{
		return FString("/Game/DrinkGame/Levels/DevMaps/dev_gameplay");
	}
	else if (MapName.IsEqual("Cycle 1 Level 1", ENameCase::IgnoreCase))
	{
		return FString("/Game/DrinkGame/Levels/Cycle_1/Cycle_1_Level_1");
	}
	else if (MapName.IsEqual("dev_zoo_world1", ENameCase::IgnoreCase))
	{
		return FString("/Game/DrinkGame/Levels/DevMaps/dev_zoo_world1");
	}
	else
	{
		return "";
	}
}

FName UODEnumHelperFunctions::MixMethodEnumToString(EMixMethod MixMethod)
{
	switch (MixMethod)
	{
		case EMixMethod::MM_Blend:
			UE_LOG(LogTemp, Warning, TEXT("MM_Blend"));
			return "Blend";
			break;
		case EMixMethod::MM_Shake:
			UE_LOG(LogTemp, Warning, TEXT("MM_Shaake"));
			return "Shake";
			break;
		case EMixMethod::MM_Straight:
			UE_LOG(LogTemp, Warning, TEXT("MM_Straight"));
			return "Straight";
			break;
		case EMixMethod::MM_None:
			UE_LOG(LogTemp, Warning, TEXT("MM_None"));
			return "None";
			break;
		default:
			UE_LOG(LogTemp, Warning, TEXT("Invalid"));
			return "Invalid";
			break;
	}
}

EMixMethod UODEnumHelperFunctions::MixMethodStringToEnum(FName MixMethodName)
{
	if (MixMethodName.IsEqual("Blend", ENameCase::IgnoreCase))
	{
		return EMixMethod::MM_Blend;
	}
	else if (MixMethodName.IsEqual("Shake", ENameCase::IgnoreCase))
	{
		return EMixMethod::MM_Shake;
	}
	else if (MixMethodName.IsEqual("Straight", ENameCase::IgnoreCase))
	{
		return EMixMethod::MM_Straight;
	}
	else
	{
		return EMixMethod::MM_None;;
	}
}
