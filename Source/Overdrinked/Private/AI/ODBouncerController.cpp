// Copyright 2020 Space Gurus


#include "AI/ODBouncerController.h"
#include "AI/ODBouncer.h"
#include "BehaviorTree/BehaviorTree.h"
#include "BehaviorTree/BlackboardData.h"
#include "BehaviorTree/BlackboardComponent.h"

AODBouncerController::AODBouncerController()
{

}

void AODBouncerController::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);

	if (BehaviorTree)
	{
		UE_LOG(LogTemp, Warning, TEXT("Running Bouncher behavior tree"));
		RunBehaviorTree(BehaviorTree);
	}

	Bouncer = Cast<AODBouncer>(GetPawn());
	if (Bouncer)
	{
		UE_LOG(LogTemp, Warning, TEXT("Bouncer is good in OnPossess"));
		FVector BouncerPost = Bouncer->GetActorLocation();
		SetVectorBlackboardKey("PostLocation", BouncerPost);
		FVector BouncerForward = Bouncer->GetActorForwardVector();
		SetVectorBlackboardKey("PostRotationSpot", BouncerPost + (BouncerForward * 100.f));
	}
}

void AODBouncerController::StartGrabCustomer()
{
}

void AODBouncerController::SetBoolBlackboardKey(FName KeyName, bool val)
{
	GetBlackboardComponent()->SetValueAsBool(KeyName, val);
}

bool AODBouncerController::GetBoolBlackboardKey(FName KeyName)
{
	return GetBlackboardComponent()->GetValueAsBool(KeyName);
}

void AODBouncerController::SetVectorBlackboardKey(FName KeyName, FVector val)
{
	GetBlackboardComponent()->SetValueAsVector(KeyName, val);
}

void AODBouncerController::SetObjectBlackboardKey(FName KeyName, UObject* obj)
{
	GetBlackboardComponent()->SetValueAsObject(KeyName, obj);
}

UObject* AODBouncerController::GetObjectBlackboardKey(FName KeyName)
{
	return GetBlackboardComponent()->GetValueAsObject(KeyName);
}

void AODBouncerController::TryToGrabAI(AODAICharacter* AIToGrab)
{
	if (AIToGrab && Bouncer)
	{
		Bouncer->GrabAI(AIToGrab);
	}
}

void AODBouncerController::TryTossAI(AODAICharacter* AIToToss)
{
	if (AIToToss && Bouncer)
	{
		Bouncer->TossAI(AIToToss);
	}
}
