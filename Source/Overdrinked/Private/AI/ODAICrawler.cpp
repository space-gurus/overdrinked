// Copyright 2020 Space Gurus


#include "AI/ODAICrawler.h"
#include "Net/UnrealNetwork.h"
#include "AI/ODCrawlerController.h"
#include "AI/ODCrawlerManager.h"
#include "PlayerCharacter.h"
#include "Components/SkeletalMeshComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "AkGameplayStatics.h"
#include "AkAudioEvent.h"
#include "Kismet/GameplayStatics.h"

AODAICrawler::AODAICrawler(const FObjectInitializer& OI) : Super(OI)
{
	PrimaryActorTick.bCanEverTick = true;

	PrimaryActorTick.bCanEverTick = false;

	bReplicates = true;

	AIControllerClass = AODCrawlerController::StaticClass();
	AutoPossessAI = EAutoPossessAI::PlacedInWorldOrSpawned;

	GetCharacterMovement()->MaxWalkSpeed = 200.f;

	// For shading look
	GetMesh()->SetRenderCustomDepth(true);
	GetMesh()->SetCustomDepthStencilValue(DefaultCustomDepth);

	// Used for attaching to the player
	PickupAttachPoint = CreateDefaultSubobject<USceneComponent>(TEXT("PickupAttachPoint"));
	PickupAttachPoint->SetupAttachment(RootComponent);
}

void AODAICrawler::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AODAICrawler, bIsPickedUp);
}

void AODAICrawler::BeginPlay()
{
	Super::BeginPlay();
	
	CrawlerController = Cast<AODCrawlerController>(GetController());

	// If we don't spawn with a manager we need to find one
	if (!CrawlerManager)
	{
		TArray<AActor*> FoundActors;
		UGameplayStatics::GetAllActorsOfClass(GetWorld(), AODCrawlerManager::StaticClass(), FoundActors);
		if (FoundActors.Num() > 0)
		{
			CrawlerManager = Cast<AODCrawlerManager>(FoundActors[0]);
		}
	}
}

void AODAICrawler::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
}

void AODAICrawler::PlayerInteraction_Implementation(APlayerCharacter* Player)
{
	if (!Player)
		return;

	if (!Player->IsHoldingObject())
	{
		Player->Server_PickupCrawler(this);
	}
}

void AODAICrawler::BecomeRelevant_Implementation(APlayerCharacter* PlayerTriggered)
{
	if (!PlayerTriggered)
		return;

	if (!PlayerTriggered->IsHoldingObject())
		Highlight();

}

void AODAICrawler::LoseRelevance_Implementation()
{
	EndHighlight();
}

void AODAICrawler::Highlight_Implementation()
{
	GetMesh()->SetCustomDepthStencilValue(HighlightCustomDepth);
}

void AODAICrawler::EndHighlight_Implementation()
{
	GetMesh()->SetCustomDepthStencilValue(DefaultCustomDepth);
}

void AODAICrawler::SetManagerReference(AODCrawlerManager* Manager)
{
	CrawlerManager = Manager;
}

AODCrawlerManager* AODAICrawler::GetManagerReference()
{
	return CrawlerManager;
}

void AODAICrawler::HandlePickup(APlayerCharacter* Player)
{
	PlayerHolding = Player;

	if (CrawlerController)
	{
		CrawlerController->RespondToPickup();
	}

	PlayPickupSound();

	this->Destroy();
}

void AODAICrawler::PlayPickupSound_Implementation()
{
	if (PickupSound)
	{
		FOnAkPostEventCallback NullCallback;
		UAkGameplayStatics::PostEvent(PickupSound, this, 0, NullCallback);
	}
}

