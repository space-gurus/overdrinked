// Copyright 2020 Space Gurus


#include "AI/ODAICharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "Net/UnrealNetwork.h"
#include "Engine/TextureRenderTarget2D.h"
#include "Engine/SkeletalMesh.h"
#include "Materials/Material.h"
#include "Materials/MaterialInstanceDynamic.h"
#include "Components/WidgetComponent.h"
#include "Components/SceneCaptureComponent2D.h"
#include "Components/CapsuleComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "Components/SpotLightComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "ODMultiplayerGameState.h"
#include "ODEnumHelperFunctions.h"
#include "PlayerCharacter.h"
#include "ODPlayerController.h"
#include "AI/ODAIController.h"
#include "AI/ODAIManager.h"
#include "UI/ODCustomerEmoteWidget.h"
#include "UI/ODOrderCardWidget.h"
#include "Actors/ODDrinkGlass.h"
#include "Gameplay/ODDrink.h"
#include "SkeletalMeshMerge.h"
#include "Animation/AnimMontage.h"
#include "AkGameplayStatics.h"
#include "AkAudioEvent.h"
#include "AkComponent.h"
#include "NiagaraFunctionLibrary.h"
#include "NiagaraComponent.h"
#include "Kismet/GameplayStatics.h"
#include "ODHelperFunctions.h"
#include "Data/AlienColorData.h"

AODAICharacter::AODAICharacter(const FObjectInitializer& OI) : Super(OI)
{
	PrimaryActorTick.bCanEverTick = true;
	bReplicates = true;

	AIControllerClass = AODAIController::StaticClass();
	AutoPossessAI = EAutoPossessAI::PlacedInWorldOrSpawned;

	ConstructorHelpers::FClassFinder<UODCustomerEmoteWidget> EmoteWidgetBP(TEXT("/Game/Blueprints/UI/Widgets/WBP_CustomerEmote"));
	if (EmoteWidgetBP.Class)
	{
		EmoteWidgetBPClass = EmoteWidgetBP.Class;
	}
	
	// Load the material for the UI face capture
	ConstructorHelpers::FObjectFinder<UMaterial> FaceCaptureMaterialFinder(TEXT("/Game/DrinkGame/MaterialLibrary/Special/M_AIFaceCapture"));
	if (FaceCaptureMaterialFinder.Succeeded())
	{
		FaceCaptureMaterial = (UMaterial*)FaceCaptureMaterialFinder.Object;
	}

	EmoteWidgetComponent = CreateDefaultSubobject<UWidgetComponent>(TEXT("EmoteWidgetComponent"));
	EmoteWidgetComponent->SetWidgetClass(EmoteWidgetBPClass);
	EmoteWidgetComponent->SetupAttachment(RootComponent);

	FaceCameraComponent = CreateDefaultSubobject<USceneCaptureComponent2D>(TEXT("FaceCameraComponent"));
	FaceCameraComponent->SetupAttachment(RootComponent);
	FaceCameraComponent->bCaptureOnMovement = false;
	FaceCameraComponent->bCaptureEveryFrame = false;

	GetCharacterMovement()->MaxWalkSpeed = 80.f;

	// For shading look
	GetMesh()->SetRenderCustomDepth(true);
	GetMesh()->SetCustomDepthStencilValue(DefaultCustomDepth);

	// Get our data
	ConstructorHelpers::FObjectFinder<UDataTable> AlienColorDataObject(TEXT("/Game/DrinkGame/Data/DT_AlienColors"));
	if (AlienColorDataObject.Succeeded())
	{
		AlienColorTable = AlienColorDataObject.Object;
	}
}

void AODAICharacter::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AODAICharacter, DrinkOrder);
	DOREPLIFETIME(AODAICharacter, bIsActive);
	DOREPLIFETIME(AODAICharacter, bIsWaitingToOrder);
	DOREPLIFETIME(AODAICharacter, bIsWaitingForOrderDelivery);
	DOREPLIFETIME(AODAICharacter, bIsHoldingDrink);
	DOREPLIFETIME(AODAICharacter, bIsFinishedWithDrink);
	DOREPLIFETIME(AODAICharacter, bIsInConversation);
	DOREPLIFETIME(AODAICharacter, bRecentlyFinishedConversation);
	DOREPLIFETIME(AODAICharacter, bIsInBarArea);
	DOREPLIFETIME(AODAICharacter, bIsFinishedWithDrink);
	DOREPLIFETIME(AODAICharacter, LegMeshIndex);
	DOREPLIFETIME(AODAICharacter, HeadMeshIndex);
	DOREPLIFETIME(AODAICharacter, TorsoMeshIndex);
	DOREPLIFETIME(AODAICharacter, Mood);
	DOREPLIFETIME(AODAICharacter, bIsSad);
	DOREPLIFETIME(AODAICharacter, IntoxicationLevel);
	DOREPLIFETIME(AODAICharacter, IntoxicationThreshold);
	DOREPLIFETIME(AODAICharacter, bWantsToFight);
}

void AODAICharacter::CapsuleColliderBeginOverlap(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	APlayerCharacter* PlayerCharacter = Cast<APlayerCharacter>(OtherActor);
	if (PlayerCharacter && bIsWaitingToOrder)
	{
		if (CustomerEmoteWidget && !CustomerEmoteWidget->IsGlowShowing())
		{
			CustomerEmoteWidget->ShowGlow();
		}
	}
}

void AODAICharacter::CapsuleColliderEndOverlap(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	APlayerCharacter* PlayerCharacter = Cast<APlayerCharacter>(OtherActor);
	if (PlayerCharacter && bIsWaitingToOrder)
	{
		if (CustomerEmoteWidget && CustomerEmoteWidget->IsGlowShowing())
		{
			CustomerEmoteWidget->HideGlow();
		}
	}
}

void AODAICharacter::SkeletalMeshBeginOverlap(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{

	AODAICharacter* OtherAI = Cast<AODAICharacter>(OtherActor);
	
	// Are we the one hitting someone else?
	if (OtherAI && bIsAttacking && !bIsBeingHit)
	{
		if (!OtherAI->IsAttacking() && !OtherAI->IsBeingHit())
		{
			OtherAI->HandleHit(this);
		}

		GetWorldTimerManager().ClearTimer(AttackTimerHandle);
		FinishAttack();
	}
}

void AODAICharacter::SkeletalMeshEndOverlap(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{

}

void AODAICharacter::BeginPlay()
{
	Super::BeginPlay();

	MyController = Cast<AODAIController>(GetController());

	// Setup the look of our AI character
	GenerateCharacterVisuals();

	// Save a reference to our widget for later
	CustomerEmoteWidget = Cast<UODCustomerEmoteWidget>(EmoteWidgetComponent->GetUserWidgetObject());

	// Collision
	GetCapsuleComponent()->OnComponentBeginOverlap.AddDynamic(this, &AODAICharacter::CapsuleColliderBeginOverlap);
	GetCapsuleComponent()->OnComponentEndOverlap.AddDynamic(this, &AODAICharacter::CapsuleColliderEndOverlap);
	GetMesh()->OnComponentBeginOverlap.AddDynamic(this, &AODAICharacter::SkeletalMeshBeginOverlap);
	GetMesh()->OnComponentEndOverlap.AddDynamic(this, &AODAICharacter::SkeletalMeshEndOverlap);
}

void AODAICharacter::OnRep_Mood()
{
	if (!CustomerEmoteWidget)
	{
		return;
	}
	
	switch (Mood)
	{
		case EAIMood::AIM_Happy:
			CustomerEmoteWidget->ShowEmoteForTime(ECustomerEmote::CE_Happy, EmoteDisplayTime);
			break;
		case EAIMood::AIM_Sad:
			CustomerEmoteWidget->ShowEmoteForTime(ECustomerEmote::CE_Sad, EmoteDisplayTime);
			break;
		case EAIMood::AIM_Angry:
			
			if (HasAuthority())
			{
				// Randomly generate chance for us to fight or leave
				int32 RandFight = FMath::RandRange(0, 100);
				if (RandFight > 50)
				{
					UE_LOG(LogTemp, Warning, TEXT("%s wants to fight!"), *GetName());
					MyController->SetBoolBlackboardKey("WantsToFight", true);
				}
				else
				{
					UE_LOG(LogTemp, Warning, TEXT("%s wants to leave!"), *GetName());
					MyController->SetBoolBlackboardKey("ShouldLeaveBar", true);
				}
			}

			if (CustomerEmoteWidget->IsEmoteShowing())
			{
				CustomerEmoteWidget->ChangeEmote(ECustomerEmote::CE_Angry);
			}
			else
			{
				CustomerEmoteWidget->ShowEmoteForTime(ECustomerEmote::CE_Angry, EmoteDisplayTime);
			}
			break;
		default:
			break;
	}
}

void AODAICharacter::OnRep_IsWaitingToOrder()
{
	if (bIsWaitingToOrder)
	{
		if (CustomerEmoteWidget)
		{
			CustomerEmoteWidget->ShowEmote(ECustomerEmote::CE_Order);
		}

		PlayOrderAnim();
	}
	else
	{
		if (CustomerEmoteWidget && CustomerEmoteWidget->IsEmoteShowing())
		{
			CustomerEmoteWidget->HideEmote();
			CustomerEmoteWidget->HideGlow();
		}

		if (bIsWaitingForOrderDelivery)
		{
			SetupCustomerSpotlight();
		}
	}
}

void AODAICharacter::OnRep_IsSad()
{
	if (MyController)
	{
		MyController->SetBoolBlackboardKey("IsSad", true);
	}

	if (bIsSad)
	{
		if (CustomerEmoteWidget)
		{
			CustomerEmoteWidget->ShowEmoteForTime(ECustomerEmote::CE_Sad, 10);
		}
	}
}

void AODAICharacter::OnRep_WantsToFight()
{
	GetCharacterMovement()->MaxWalkSpeed = AggressiveSpeed;
}

void AODAICharacter::GenerateDrinkOrder()
{
	if (MyController)
	{
		DrinkOrder = MyController->MakeNewDrinkOrder();

		bIsWaitingToOrder = true;
		bIsActive = true;
		MyController->SetBoolBlackboardKey("WaitingOrderToBeTaken", true);
	}

	if (HasAuthority())
	{
		OnRep_IsWaitingToOrder();
	}
}

void AODAICharacter::PrintCurrentDrinkOrder()
{
	//UE_LOG(LogTemp, Warning, TEXT("ORDER: \nOwner: %s \nDrink: %s"), *DrinkOrder.OrderOwner->GetName(), *UODEnumHelperFunctions::DrinkEnumToString(DrinkOrder.DesiredDrink).ToString());
}

void AODAICharacter::GenerateCharacterVisuals()
{
	// Save a reference to our current anim class
	UAnimInstance* AnimBPInstance = GetMesh()->GetAnimInstance();

	// Load our data
	if (!AlienColorTable)
		return;

	TArray<FName> ColorTableRowNames = AlienColorTable->GetRowNames();
	int32 RowSelected = FMath::RandRange(0, ColorTableRowNames.Num() - 1);

	FAlienColorData* AlienColorData = AlienColorTable->FindRow<FAlienColorData>(ColorTableRowNames[RowSelected], FString(""));
	if (AlienColorData)
	{
		FLinearColor HeadPrimaryColor = AlienColorData->ColorOne;
		FLinearColor TorsoPrimaryColor = AlienColorData->ColorTwo;
		FLinearColor LegPrimaryColor = AlienColorData->ColorThree;
		FLinearColor HeadSecondaryColor = UODHelperFunctions::GetComplementaryColor(HeadPrimaryColor);
		FLinearColor TorsoSecondaryColor = UODHelperFunctions::GetComplementaryColor(TorsoPrimaryColor);
		FLinearColor LegSecondaryColor = UODHelperFunctions::GetComplementaryColor(LegPrimaryColor);
		
		// Only let the server do the generation
		if (HasAuthority())
		{
			HeadMeshIndex = FMath::RandRange(0, HeadMeshOptions.Num() - 1);
			LegMeshIndex = FMath::RandRange(0, LegMeshOptions.Num() - 1);
			TorsoMeshIndex = FMath::RandRange(0, TorsoMeshOptions.Num() - 1);
		}

		// Add our meshes
		if (HeadMeshOptions.IsValidIndex(HeadMeshIndex) && TorsoMeshOptions.IsValidIndex(TorsoMeshIndex) && LegMeshOptions.IsValidIndex(LegMeshIndex))
		{
			MeshMergeParams.MeshesToMerge.Add(HeadMeshOptions[HeadMeshIndex]);
			MeshMergeParams.MeshesToMerge.Add(TorsoMeshOptions[TorsoMeshIndex]);
			MeshMergeParams.MeshesToMerge.Add(LegMeshOptions[LegMeshIndex]);
		}

		MeshMergeParams.bNeedsCpuAccess = true;

		USkeletalMesh* MergedMesh = UODMeshMergeFunctionLibrary::MergeMeshes(MeshMergeParams);
		if (MergedMesh)
		{
			GetMesh()->SetSkeletalMesh(MergedMesh);
			GetMesh()->SetPhysicsAsset(NPCPhysicsAsset);
			GetMesh()->SkeletalMesh->RebuildSocketMap();
			GetMesh()->SkeletalMesh->GetLODInfo(0)->bAllowCPUAccess = true;

			// Setup our dynamic materials for later
			DynamicHeadMaterial = GetMesh()->CreateDynamicMaterialInstance(0);
			if (DynamicHeadMaterial)
			{
				GetMesh()->SetMaterial(0, DynamicHeadMaterial);
				//DynamicHeadMaterial->SetScalarParameterValue(TEXT("ElectrocuteIntensity"), ElectrocuteMaterialEffectValue);
				DynamicHeadMaterial->SetVectorParameterValue(TEXT("SkinPrimary"), HeadPrimaryColor);
			}

			DynamicTorsoMaterial = GetMesh()->CreateDynamicMaterialInstance(2);
			if (DynamicTorsoMaterial)
			{
				GetMesh()->SetMaterial(2, DynamicTorsoMaterial);
				//DynamicTorsoMaterial->SetScalarParameterValue(TEXT("ElectrocuteIntensity"), ElectrocuteMaterialEffectValue);
				DynamicTorsoMaterial->SetVectorParameterValue(TEXT("TorsoPrimary"), TorsoPrimaryColor);
			}

			DynamicLegMaterial = GetMesh()->CreateDynamicMaterialInstance(3);
			if (DynamicLegMaterial)
			{
				GetMesh()->SetMaterial(3, DynamicLegMaterial);
				//DynamicLegMaterial->SetScalarParameterValue(TEXT("ElectrocuteIntensity"), ElectrocuteMaterialEffectValue);
				DynamicLegMaterial->SetVectorParameterValue(TEXT("LegsPrimary"), LegPrimaryColor);
			}

			GetMesh()->SetScalarParameterValueOnMaterials(TEXT("ElectrocuteIntensity"), ElectrocuteMaterialEffectValue);
			GetMesh()->SetVectorParameterValueOnMaterials(TEXT("SkinSecondary"), FVector(HeadPrimaryColor));
			GetMesh()->SetVectorParameterValueOnMaterials(TEXT("TorsoPrimary"), FVector(TorsoPrimaryColor));
			GetMesh()->SetVectorParameterValueOnMaterials(TEXT("LegsPrimary"), FVector(LegPrimaryColor));
			GetMesh()->SetVectorParameterValueOnMaterials(TEXT("SkinPrimary"), FVector(HeadSecondaryColor));
			GetMesh()->SetVectorParameterValueOnMaterials(TEXT("TorsoSecondary"), FVector(TorsoSecondaryColor));
			GetMesh()->SetVectorParameterValueOnMaterials(TEXT("LegsSecondary"), FVector(LegSecondaryColor));
		}
	}

}

float AODAICharacter::GetOrderTimeElapsed()
{
	return GetWorldTimerManager().GetTimerElapsed(OrderTimerHandle);
}

void AODAICharacter::HandleOrderPrepTimerFinished()
{
	// Time ran out while we were waiting for our order
	// We're a little upset about it
	Mood = EAIMood::AIM_Sad;
	DrinkOrder.OrderStatus = EOrderStatus::OS_Expired;

	if (HasAuthority())
	{
		OnRep_Mood();
	}

	IncreaseAggression(FMath::RandRange(20.f, 60.f));

	// Wait until the reaction emote is gone to try and order again
	FTimerHandle EmoteTimerHandle;
	GetWorldTimerManager().SetTimer(EmoteTimerHandle, this, &AODAICharacter::TryToRefreshOrdering, EmoteDisplayTime + 0.5f, false);
	
	AODMultiplayerGameState* MPGameState = Cast<AODMultiplayerGameState>(GetWorld()->GetGameState());
	if (MPGameState)
	{
		MPGameState->Server_RemoveOpenOrder(DrinkOrder, false, this);
	}
}

void AODAICharacter::Client_HandleOrderTimerFinished()
{
}

void AODAICharacter::RagdollAllMeshes()
{
	GetMesh()->SetCollisionResponseToChannel(ECC_Pawn, ECR_Ignore);

	// Do it for each piece of our mesh
	GetMesh()->SetAllBodiesSimulatePhysics(true);
	GetMesh()->SetSimulatePhysics(true);
	GetMesh()->WakeAllRigidBodies();
	GetMesh()->bBlendPhysics = true;
}

void AODAICharacter::TryToRefreshOrdering()
{
	// Decrease our thirst by half and reset some variables so we can try again soon
	ThirstLevel = ThirstLevel / 3;

	bIsWaitingToOrder = false;
	bIsWaitingForOrderDelivery = false;

	if (MyController)
	{
		MyController->SetBoolBlackboardKey("WaitingForOrderDelivery", false);
		MyController->SetBoolBlackboardKey("WantsToOrder", false);
	}
}

void AODAICharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (bIsTakingFaceCapture)
	{
		UTextureRenderTarget2D* RenderTarget2D = FaceCameraComponent->TextureTarget;
		if (RenderTarget2D && FaceCaptureMaterial)
		{
			UMaterialInstanceDynamic* DynamicMaterial = UMaterialInstanceDynamic::Create(FaceCaptureMaterial, this);
			DynamicMaterial->SetTextureParameterValue("FaceCapture", RenderTarget2D);

			if (OrderCardWidget)
			{
				OrderCardWidget->SetFaceCaptureMaterial(DynamicMaterial);

				bIsTakingFaceCapture = false;
			}
		}
	}

	if (bShouldFinishElectrocuteEffect)
	{
		ElectrocuteLerpTime += DeltaTime;
		if (ElectrocuteLerpTime > ElectrocuteMaterialEffectFadeSpeed)
		{
			bShouldFinishElectrocuteEffect = false;
		}

		float ValPerc = ElectrocuteLerpTime / ElectrocuteMaterialEffectFadeSpeed;
		ElectrocuteMaterialEffectValue = FMath::Lerp(1.f, 0.f, ValPerc);

		if (DynamicHeadMaterial && DynamicLegMaterial && DynamicTorsoMaterial)
		{
			DynamicHeadMaterial->SetScalarParameterValue(TEXT("ElectrocuteIntensity"), ElectrocuteMaterialEffectValue);
			DynamicLegMaterial->SetScalarParameterValue(TEXT("ElectrocuteIntensity"), ElectrocuteMaterialEffectValue);
			DynamicTorsoMaterial->SetScalarParameterValue(TEXT("ElectrocuteIntensity"), ElectrocuteMaterialEffectValue);
		}
	}
}

 void AODAICharacter::PlayerInteraction_Implementation(APlayerCharacter* Player)
{
	if (!Player)
		return;

	// Taser should take precedence over everything
	if (Player->IsHoldingTaser() && bIsActive)
	{
		Player->Server_UseTaser(this);
	}
	else if (bIsWaitingToOrder)
	{
		// Try to give order
		AODMultiplayerGameState* ODGameState = Cast<AODMultiplayerGameState>(UGameplayStatics::GetGameState(GetWorld()));
		if (ODGameState && ODGameState->CanHandleNewOrder())
		{
			// Take their order
			AODPlayerController* ODPlayerController = Cast<AODPlayerController>(Player->GetController());
			if (ODPlayerController)
			{
				ODPlayerController->Server_AcceptCustomerOrder(this);
			}
		}
		else
		{
			// Orders are maxed right now
			Player->PlayErrorSound();
		}
	}
	else if (bIsWaitingForOrderDelivery)
	{
		if (Player->IsHoldingDrinkGlass() && CanCustomerAcceptDrink())
		{
			// Take the drink from the player
			AODDrinkGlass* Drink = Cast<AODDrinkGlass>(Player->GetObjectInHand());
			if (Drink)
			{
				Player->Server_DropObject(true, false);
				TakeDrinkFromPlayer(Drink);
			}
		}
		else
		{
			Player->PlayErrorSound();
		}
	}
}

void AODAICharacter::BecomeRelevant_Implementation(APlayerCharacter* PlayerTriggered)
{
	if (!PlayerTriggered)
		return;

	if (PlayerTriggered->IsHoldingTaser() || bIsWaitingToOrder || (CanCustomerAcceptDrink() && PlayerTriggered->IsHoldingDrinkGlass()))
	{
		Highlight();
	}
}

void AODAICharacter::LoseRelevance_Implementation()
{
	EndHighlight();
}

void AODAICharacter::Highlight_Implementation()
{
	GetMesh()->SetCustomDepthStencilValue(HighlightCustomDepth);
}

void AODAICharacter::EndHighlight_Implementation()
{
	GetMesh()->SetCustomDepthStencilValue(DefaultCustomDepth);
}

void AODAICharacter::SetIsInBarArea(bool val)
{
	bIsInBarArea = val;
	
	if (MyController)
	{
		MyController->SetBoolBlackboardKey("IsInBarArea", bIsInBarArea);
	}
}

AODAIController* AODAICharacter::GetODAIController()
{
	return MyController;
}

USceneCaptureComponent2D* AODAICharacter::GetFaceCameraComponent()
{
	return FaceCameraComponent;
}

AODDrinkGlass* AODAICharacter::GetDrinkInHand()
{
	return DrinkInHand;
}

int32 AODAICharacter::GetCustomDepthStencilValue()
{
	return GetMesh()->CustomDepthStencilValue;
}

bool AODAICharacter::IsCustomerActive()
{
	return bIsActive;
}

bool AODAICharacter::IsHoldingDrink()
{
	return bIsHoldingDrink;
}

void AODAICharacter::UpdateWalkSpeed(float NewSpeed)
{
	GetCharacterMovement()->MaxWalkSpeed = NewSpeed;
}

bool AODAICharacter::IsCustomerWaitingToOrder()
{
	return bIsWaitingToOrder;
}

bool AODAICharacter::IsWaitingForOrderDelivery()
{
	return bIsWaitingForOrderDelivery;
}

bool AODAICharacter::IsInBarArea()
{
	return bIsInBarArea;
}

void AODAICharacter::SetBrawlTarget(AODAICharacter* TargetCharacter)
{
	if (TargetCharacter)
	{
		BrawlTarget = TargetCharacter;
		MyController->SetBrawlTargetKey(BrawlTarget);
	}
}

AODAICharacter* AODAICharacter::GetBrawlTarget()
{
	return BrawlTarget;
}

bool AODAICharacter::CanCustomerAcceptDrink()
{
	return bIsHoldingDrink ? false : true;
}

void AODAICharacter::ClearCurrentOrder()
{
	FDrinkOrder NewOrder;
	NewOrder.OrderOwner = this;
	NewOrder.DesiredGlass = EGlassType::GT_Mixer;
	NewOrder.OrderNumber = -1;
	NewOrder.OrderPrepTimer = -1;
	NewOrder.OrderTimer = -1;
	DrinkOrder = NewOrder;

	GetWorldTimerManager().ClearTimer(OrderTimerHandle);
}

FDrinkOrder AODAICharacter::GiveOrder()
{
	Server_StartOrderTimer();

	// Update widget on the server
	OnRep_IsWaitingToOrder();

	return DrinkOrder;
}

void AODAICharacter::Server_StartOrderTimer_Implementation()
{
	GetWorldTimerManager().ClearTimer(OrderTimerHandle);
	GetWorldTimerManager().SetTimer(OrderTimerHandle, this, &AODAICharacter::HandleOrderPrepTimerFinished, DrinkOrder.OrderPrepTimer, false);

	DrinkOrder.OrderStatus = EOrderStatus::OS_Prep;

	bIsWaitingToOrder = false;
	
	if (MyController)
		MyController->SetBoolBlackboardKey("WaitingOrderToBeTaken", false);

	bIsWaitingForOrderDelivery = true;

	if (MyController)
		MyController->SetBoolBlackboardKey("WaitingForOrderDelivery", true);

	// Tell the clients to start their own timers
	Multicast_StartOrderTimer();
}

void AODAICharacter::Multicast_StartOrderTimer_Implementation()
{
	if (!HasAuthority())
	{
		GetWorldTimerManager().ClearTimer(OrderTimerHandle);
		GetWorldTimerManager().SetTimer(OrderTimerHandle, this, &AODAICharacter::Client_HandleOrderTimerFinished, DrinkOrder.OrderPrepTimer, false);
	}
}

void AODAICharacter::PlayElectrocute()
{
	// Do the sound and the anim
	if (TaserElectrocuteSound)
	{
		FOnAkPostEventCallback NullCallback;
		UAkGameplayStatics::PostEvent(TaserElectrocuteSound, this, 0, NullCallback);
	}

	if (ElectrocuteMontage)
	{
		PlayAnimMontage(ElectrocuteMontage);
	}

	// Get the effect going
	if (ElectrocuteEffectSystem)
	{
		ElectrocuteEffectComp = UNiagaraFunctionLibrary::SpawnSystemAttached(ElectrocuteEffectSystem, GetMesh(), "", FVector::ZeroVector, FRotator::ZeroRotator,
			FVector(1.f), EAttachLocation::SnapToTarget, true, ENCPoolMethod::None);
	}

	// Material effect too
	if (DynamicHeadMaterial && DynamicLegMaterial && DynamicTorsoMaterial)
	{
		ElectrocuteMaterialEffectValue = 1.f;
		DynamicHeadMaterial->SetScalarParameterValue(TEXT("ElectrocuteIntensity"), ElectrocuteMaterialEffectValue);
		DynamicLegMaterial->SetScalarParameterValue(TEXT("ElectrocuteIntensity"), ElectrocuteMaterialEffectValue);
		DynamicTorsoMaterial->SetScalarParameterValue(TEXT("ElectrocuteIntensity"), ElectrocuteMaterialEffectValue);
	}

	FTimerHandle ElectrocuteTimerHandle;
	GetWorldTimerManager().SetTimer(ElectrocuteTimerHandle, this, &AODAICharacter::FinishElectrocute, ElectrocuteMontage->GetPlayLength() - 0.2f, false);
}

void AODAICharacter::PlayDrinkAnim_Implementation()
{
	if (NPCTakeDrinkMontage)
	{
		PlayAnimMontage(NPCTakeDrinkMontage);
	}
}

void AODAICharacter::PlayAttackAnim_Implementation()
{
	int AttackAnimIdx = FMath::RandRange(0, AttackAnimations.Num() - 1);
	if (AttackAnimations.IsValidIndex(AttackAnimIdx))
	{
		PlayAnimMontage(AttackAnimations[AttackAnimIdx]);

		bIsAttacking = true;
		GetWorldTimerManager().SetTimer(AttackTimerHandle, this, &AODAICharacter::FinishAttack, 0.75f, false);
	}
}

void AODAICharacter::PlayTalkAnim_Implementation()
{
	if (bIsHoldingDrink)
	{
		int DrinkTalkAnimIdx = FMath::RandRange(0, DrinkTalkAnimations.Num() - 1);
		if (DrinkTalkAnimations.IsValidIndex(DrinkTalkAnimIdx))
		{
			PlayAnimMontage(DrinkTalkAnimations[DrinkTalkAnimIdx]);
		}
	}
	else
	{
		int TalkAnimIdx = FMath::RandRange(0, TalkAnimations.Num() - 1);
		if (TalkAnimations.IsValidIndex(TalkAnimIdx))
		{
			PlayAnimMontage(TalkAnimations[TalkAnimIdx]);
		}
	}
}

void AODAICharacter::PlayIdleAnim_Implementation()
{
	int32 IdleAnimIndex = FMath::RandRange(0, IdleAnimations.Num() - 1);
	Multi_PlayIdleAnim(IdleAnimIndex);
}

void AODAICharacter::Multi_PlayIdleAnim_Implementation(int32 IdleAnimIndex)
{
	if (IdleAnimations.IsValidIndex(IdleAnimIndex))
	{
		PlayAnimMontage(IdleAnimations[IdleAnimIndex]);
	}
}

void AODAICharacter::FinishAttack()
{
	PlayPunchSound();
	bIsAttacking = false;
}

void AODAICharacter::TakeDrinkFromPlayer(AODDrinkGlass* DrinkToTake)
{
	if (DrinkToTake)
	{
		DrinkInHand = DrinkToTake;
		
		bIsHoldingDrink = true;

		if (MyController)
			MyController->SetBoolBlackboardKey("IsHoldingDrink", true);

		// The player dropped it so make sure we pick it up
		DrinkToTake->HandlePickup(this);

		// Were we expecting a drink?
		if (bIsWaitingForOrderDelivery)
		{
			bIsWaitingForOrderDelivery = false;
			if (MyController)
				MyController->SetBoolBlackboardKey("WaitingForOrderDelivery", false);

			EvaluateDrinkOrder();

			DestroyCustomerSpotlight();
		}
		else
		{
			// The player gave the wrong person a drink
			CustomerEmoteWidget->ShowEmoteForTime(ECustomerEmote::CE_Confused, EmoteDisplayTime);
		}
	}
}

void AODAICharacter::DropItem()
{
	if (IsHoldingDrink() && GetDrinkInHand())
	{
		// Are we done with the drink? If so we have some cleanup to do
		if (bIsFinishedWithDrink)
		{
			GetDrinkInHand()->ClearDrink();
			GetDrinkInHand()->MakeDirty();
		}

		// Drop the glass
		GetDrinkInHand()->HandleDrop();


		DrinkInHand = NULL;
		bIsHoldingDrink = false;
	}
}

void AODAICharacter::EvaluateDrinkOrder()
{
	if (DrinkInHand)
	{
		EDrinkScore DrinkScore = UODHelperFunctions::ScoreDrink(DrinkOrder, DrinkInHand);

		switch (DrinkScore)
		{
			case EDrinkScore::DS_A:
				// Really happy
				break;
			case EDrinkScore::DS_B:
				// Pretty happy
				break;
			case EDrinkScore::DS_C:
				// Kinda happy
				break;
			case EDrinkScore::DS_D:
				// Upset
				break;
			default:
				// Really unhappy
				break;
		}

		HandleSuccessfulDelivery();

// 		if ((UODEnumHelperFunctions::DrinkStringToEnum(FName(DrinkInGlass->GetDrinkName())) == DrinkOrder.DesiredDrink) && (DrinkInHand->GetGlassType() == DrinkOrder.DesiredGlass))
// 		{
// 			HandleSuccessfulDelivery();
// 		}
// 		else
// 		{
// 			IncreaseAggression((AggressionLevel / 5) + 10.f);
// 			Mood = EAIMood::AIM_Sad;
// 			if (HasAuthority())
// 			{
// 				OnRep_IsSad();
// 			}
// 		}
	}
}

void AODAICharacter::HandleWaitTimeExpired()
{

}

void AODAICharacter::HandleSuccessfulDelivery()
{
	// Update UI
	Mood = EAIMood::AIM_Happy;
	if (HasAuthority())
	{
		OnRep_Mood();
	}

	AODMultiplayerGameState* MPGameState = Cast<AODMultiplayerGameState>(GetWorld()->GetGameState());
	if (MPGameState)
	{
		MPGameState->Server_RemoveOpenOrder(DrinkOrder, true, this);
	}
	
	// Clear anything related to this order
	ClearCurrentOrder();
}

void AODAICharacter::ChangeEmote_Implementation(ECustomerEmote EmoteToShow)
{
	if (CustomerEmoteWidget)
	{
		CustomerEmoteWidget->ShowEmoteForTime(EmoteToShow, 3.f);
	}
}

void AODAICharacter::TakeFaceCapture(UODOrderCardWidget* OrderWidget)
{
	UTextureRenderTarget2D* RenderTarget2D = NewObject<UTextureRenderTarget2D>();
	if (RenderTarget2D)
	{
		RenderTarget2D->InitAutoFormat(512, 512);
		FaceCameraComponent->TextureTarget = RenderTarget2D;

		FaceCameraComponent->CaptureSceneDeferred();
		bIsTakingFaceCapture = true;

		OrderCardWidget = OrderWidget;
	}
}

void AODAICharacter::SetThirstThreshold(float val)
{
	ThirstThreshold = val;
}

void AODAICharacter::IncreaseThirst(float val)
{
	ThirstLevel += FMath::RoundToInt(val);

	if (ThirstLevel > 100.f)
	{
		ThirstLevel = 100.f;
	}
}

void AODAICharacter::DecreaseThirst(float val)
{
	ThirstLevel -= FMath::RoundToInt(val);

	if (ThirstLevel < 0.f)
	{
		ThirstLevel = 0.f;
	}
}

void AODAICharacter::IncreaseIntoxicationLevel(float val)
{
	IntoxicationLevel += val;

	// Sometimes becoming intoxicated makes us want to fight
	int32 RandFight = -1;
	if (IsIntoxicated())
	{
		RandFight = FMath::RandRange(4, 10);
	}
	else
	{
		RandFight = FMath::RandRange(1, 6);
	}

	if (RandFight >= 5)
	{
		// FIGHT
		bWantsToFight = true;
		if (HasAuthority())
		{
			OnRep_WantsToFight();
		}
	}
}

bool AODAICharacter::IsIntoxicated()
{
	return IntoxicationLevel >= IntoxicationThreshold;
}

void AODAICharacter::SetAggressionThreshold(float val)
{
	AggressionThreshold = val;
}

void AODAICharacter::IncreaseAggression(float val)
{
	AggressionLevel += val;

	if (AggressionLevel >= AggressionThreshold)
	{
		Mood = EAIMood::AIM_Angry;
		if (HasAuthority())
		{
			// Do we want to fight?
			int32 RandFight = FMath::RandRange(0, 1);
			if (RandFight > 0)
			{
				// FIGHT
				bWantsToFight = true;
			}

			OnRep_Mood();
		}
	}
}

void AODAICharacter::ConsumeDrinkInHand()
{
	// Maybe it takes 10 drinks to consume the drink?
	if (bIsHoldingDrink && DrinkInHand)
	{
		float AmountConsumed = DrinkInHand->DecreaseVolume(DrinkInHand->GetMaxVolume() * 0.5f);

		// Drinks make us less thirsty
		// TODO: Make certain drinks hit harder!
		DecreaseThirst(AmountConsumed * 50.f);
		IncreaseIntoxicationLevel(AmountConsumed * 50.f);

		if (DrinkInHand->IsContainerEmpty())
		{
			bIsFinishedWithDrink = true;
		}
	}

	if (HasAuthority())
	{
		PlayDrinkAnim();
	}
}

bool AODAICharacter::WantsToOrderDrink()
{
	if ((ThirstLevel >= ThirstThreshold) && !bIsWaitingToOrder)
	{
		return true;
	}
	else
	{
		return false;
	}
}

void AODAICharacter::SetInConversation(bool val)
{
	bIsInConversation = val;
}

bool AODAICharacter::IsInConversation()
{
	return bIsInConversation;
}

bool AODAICharacter::WantsToFight()
{
	return bWantsToFight;
}

bool AODAICharacter::IsAttacking()
{
	return bIsAttacking;
}

bool AODAICharacter::IsBeingHit()
{
	return bIsBeingHit;
}

void AODAICharacter::SetRecentlyFinishedConversation(bool val)
{
	bRecentlyFinishedConversation = val;
}

bool AODAICharacter::RecentlyFinishedConversation()
{
	return bRecentlyFinishedConversation;
}

void AODAICharacter::HandleTaser_Implementation()
{
	bWantsToFight = false;
	bIsActive = false;

	LoseRelevance();

	// Handle the controller first
	if (HasAuthority() && MyController)
	{
		MyController->HandleTaserLogic();
	}

	PlayElectrocute();

	if (bIsWaitingToOrder)
	{
		CancelOrder();
	}

	// Let everyone know
	if (HasAuthority() && bIsWaitingForOrderDelivery)
	{
		DrinkOrder.OrderStatus = EOrderStatus::OS_Tasered;
		AODMultiplayerGameState* ODGameState = Cast<AODMultiplayerGameState>(UGameplayStatics::GetGameState(GetWorld()));
		if (ODGameState)
		{
			ODGameState->Server_RemoveOpenOrder(DrinkOrder, false, this);
		}
	}
}

void AODAICharacter::HandleBouncerGrab_Implementation()
{
	GetMesh()->SetSimulatePhysics(false);
	GetMesh()->SetAllBodiesSimulatePhysics(false);
	GetMesh()->PutAllRigidBodiesToSleep();

	GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);
	GetCapsuleComponent()->SetCollisionResponseToChannel(ECC_Pawn, ECR_Block);
}

void AODAICharacter::PlayOrderAnim()
{
	if (NPCOrderMontage)
	{
		PlayAnimMontage(NPCOrderMontage);
	}
}

void AODAICharacter::FinishElectrocute()
{
	GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	GetCapsuleComponent()->SetCollisionResponseToAllChannels(ECR_Ignore);

	// Toggle all of our meshes
	RagdollAllMeshes();
	
	// Clear the electrocute
	if (ElectrocuteEffectComp)
	{
		ElectrocuteEffectComp->Deactivate();
	}

	// Spawn our z's
	if (AsleepEffectSystem)
	{
		AsleepEffectComp = UNiagaraFunctionLibrary::SpawnSystemAttached(AsleepEffectSystem, GetMesh(), "Head_Bind", FVector(0.f, 0.f, 15.f), FRotator::ZeroRotator, FVector(2.f), EAttachLocation::SnapToTarget, false, ENCPoolMethod::None);
	}

	// Set a timer for tapering off the material effect
	FTimerHandle ElectrocuteEffectTimerHandle;
	GetWorldTimerManager().SetTimer(ElectrocuteEffectTimerHandle, this, &AODAICharacter::FinishElectrocuteEffect, ElectrocuteMaterialEffectLength, false);
}

void AODAICharacter::FinishElectrocuteEffect()
{
	bShouldFinishElectrocuteEffect = true;
}

TArray<FName> AODAICharacter::GetGrabbableSocketNames()
{
	return GrabbableSocketNames;
}

void AODAICharacter::HandleHit(AODAICharacter* CharacterHitting)
{
	bIsBeingHit = true;

	if (CharacterHitting)
	{
		if (HasAuthority())
		{
			SetBrawlTarget(CharacterHitting);

			if (bIsWaitingToOrder)
			{
				CancelOrder();
			}
		}
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("AODAICharacter::HandleHit: CharacterHitting NULL"));
	}

	bool bParries = FMath::RandBool();

	if (bParries)
	{
		int32 ParryIndx = FMath::RandRange(0, ParryAnimations.Num() - 1);
		if (ParryAnimations.IsValidIndex(ParryIndx))
		{
			PlayAnimMontage(ParryAnimations[ParryIndx]);
			GetWorldTimerManager().SetTimer(HitTimerHandle, this, &AODAICharacter::FinishHandleHit, 0.75f, false);
		}
	}
	else
	{
		int32 HitReactIdx = FMath::RandRange(0, HitReactAnimations.Num() - 1);
		if (HitReactAnimations.IsValidIndex(HitReactIdx))
		{
			PlayAnimMontage(HitReactAnimations[HitReactIdx]);
			GetWorldTimerManager().SetTimer(HitTimerHandle, this, &AODAICharacter::FinishHandleHit, 1.5f, false);
		}
	}

	// Random sound effect
	bool bGrunts = FMath::RandBool();
	if (bGrunts)
	{
		UAkAudioEvent* Grunt = GruntSounds[FMath::RandRange(0, GruntSounds.Num() - 1)];
		if (Grunt)
		{
			FOnAkPostEventCallback NullCallback;
			UAkGameplayStatics::PostEvent(Grunt, this, 0, NullCallback);
		}
	}

}

void AODAICharacter::FinishHandleHit()
{
	bIsBeingHit = false;
	
	if (!bWantsToFight)
	{
		bWantsToFight = true;
		if (HasAuthority())
		{
			OnRep_WantsToFight();
		}
	}
}

void AODAICharacter::CancelOrder()
{
	bIsWaitingToOrder = false;

	if (CustomerEmoteWidget)
	{
		CustomerEmoteWidget->HideEmote();
	}
}

void AODAICharacter::PlayPunchSound()
{
	if (PunchSound)
	{
		FOnAkPostEventCallback NullCallback;
		UAkGameplayStatics::PostEvent(PunchSound, this, 0, NullCallback);
	}
}

void AODAICharacter::SetupCustomerSpotlight()
{
	if (!Spotlight)
		Spotlight = NewObject<USpotLightComponent>(this, USpotLightComponent::StaticClass(), FName("Spotlight"));

	if (Spotlight)
	{
		UE_LOG(LogTemp, Warning, TEXT("Setting up customer spotlight"));
		Spotlight->RegisterComponent();
		Spotlight->AttachTo(RootComponent);

		Spotlight->SetRelativeLocation(FVector(0.f, 0.f, 150.f));
		Spotlight->SetRelativeRotation(FRotator(-90.f, 0.f, 0.f));
		Spotlight->SetLightColor(SpotlightColor);
		Spotlight->SetIntensity(10000.f);
		Spotlight->SetSourceRadius(5.f);
		Spotlight->SetOuterConeAngle(12.f);
		Spotlight->SetCastShadows(false);
	}
}

void AODAICharacter::DestroyCustomerSpotlight()
{
	if (Spotlight)
	{
		Spotlight->DestroyComponent();
	}
}

