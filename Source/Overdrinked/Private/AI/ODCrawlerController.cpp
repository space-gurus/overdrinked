// Copyright 2020 Space Gurus


#include "AI/ODCrawlerController.h"
#include "AI/ODAICrawler.h"
#include "BehaviorTree/BehaviorTree.h"
#include "BehaviorTree/BlackboardData.h"
#include "BehaviorTree/BlackboardComponent.h"

AODCrawlerController::AODCrawlerController()
{

}

void AODCrawlerController::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);

	if (BehaviorTree)
	{
		RunBehaviorTree(BehaviorTree);
	}

	Crawler = Cast<AODAICrawler>(GetPawn());
	if (Crawler)
	{
	}
}

void AODCrawlerController::RespondToPickup()
{
	BrainComponent->StopLogic("Pickup");
}

void AODCrawlerController::RespondToDrop()
{
	BrainComponent->RestartLogic();
}
