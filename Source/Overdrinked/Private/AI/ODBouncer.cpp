// Copyright 2020 Space Gurus


#include "AI/ODBouncer.h"
#include "AI/ODBouncerController.h"
#include "AI/ODAICharacter.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Net/UnrealNetwork.h"
#include "Animation/AnimMontage.h"
#include "Components/SkeletalMeshComponent.h"
#include "AkComponent.h"
#include "PhysicsEngine/PhysicsConstraintComponent.h"
#include "PhysicsEngine/PhysicsHandleComponent.h"
#include "DrawDebugHelpers.h"

AODBouncer::AODBouncer()
{
	PrimaryActorTick.bCanEverTick = false;

	bReplicates = true;

	AIControllerClass = AODBouncerController::StaticClass();
	AutoPossessAI = EAutoPossessAI::PlacedInWorldOrSpawned;

	GetCharacterMovement()->MaxWalkSpeed = 300.f;

	// For shading look
	GetMesh()->SetRenderCustomDepth(true);
	GetMesh()->SetCustomDepthStencilValue(253);

	GrabCubeMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("GrabCubeMesh"));
	GrabCubeMesh->SetupAttachment(GetMesh(), "RightGrabSocket");
	
	GrabConstraintComponent = CreateDefaultSubobject<UPhysicsConstraintComponent>(TEXT("GrabConstraintComponent"));
	GrabConstraintComponent->SetupAttachment(GrabCubeMesh);
}

void AODBouncer::BeginPlay()
{
	Super::BeginPlay();
	
	BouncerController = Cast<AODBouncerController>(GetController());

	PostLocation = GetActorLocation();
}

void AODBouncer::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AODBouncer, bIsAvailable);
	DOREPLIFETIME(AODBouncer, bIsDragging);
}

bool AODBouncer::IsAvailable()
{
	return bIsAvailable;
}

void AODBouncer::AddCustomerToGrabList(AODAICharacter* AICharacter)
{
	if (AICharacter)
	{
		CustomersToGrab.Enqueue(AICharacter);
		QueueSize++;

		// Only do this stuff if we're not already doing something
		if (bIsQueueEmpty)
		{
			bIsQueueEmpty = false;
			bIsAvailable = false;

			BouncerController->SetObjectBlackboardKey("CustomerToGrab", AICharacter);

			FTimerHandle StartTimerHandle;
			GetWorldTimerManager().SetTimer(StartTimerHandle, this, &AODBouncer::DelayStart, FMath::RandRange(0.7f, 1.5f), false);
		}
	}
}

void AODBouncer::GrabAI_Implementation(AODAICharacter* AIToGrab)
{
	PlayAnimMontage(GrabMontage);

	GrabbedAI = AIToGrab;

	// Attach the AI
	FTimerHandle PhysicsGrabTimerHandle;
	// Do the actual grab about halfway through the montage when he is bent over
	GetWorldTimerManager().SetTimer(PhysicsGrabTimerHandle, this, &AODBouncer::TriggerPhysicsGrab, GrabMontage->GetPlayLength() * 0.6f, false);

	// Set a timer to toggle some variables, this is messy
	FTimerHandle GrabTimerHandle;
	GetWorldTimerManager().SetTimer(GrabTimerHandle, this, &AODBouncer::FinishGrab, GrabMontage->GetPlayLength(), false);

}

void AODBouncer::TossAI(AODAICharacter* AIToGrab)
{
	if (AIToGrab)
	{
		AODAICharacter* NextAI;
		CustomersToGrab.Peek(NextAI);
		if (AIToGrab == NextAI)
		{
			if (CustomersToGrab.Pop())
			{
				AIToGrab->DetachFromActor(FDetachmentTransformRules::KeepWorldTransform);
				AIToGrab->Destroy();
				QueueSize--;
			}
		}
		

		// Clean up
		bIsDragging = false;

		if (CustomersToGrab.IsEmpty())
		{
			bIsQueueEmpty = true;
			bIsAvailable = true;
			BouncerController->SetObjectBlackboardKey("CustomerToGrab", NULL);
			BouncerController->SetBoolBlackboardKey("ShouldReturnToPost", true);
			BouncerController->SetBoolBlackboardKey("NeedsToGrabCustomer", false);
		}
		else
		{
			// Go get the next knocked out customer
			AODAICharacter* NextTaseAI;
			CustomersToGrab.Peek(NextTaseAI);
			BouncerController->SetObjectBlackboardKey("CustomerToGrab", NextTaseAI);
			BouncerController->SetBoolBlackboardKey("ShouldReturnToPost", false);
			BouncerController->SetBoolBlackboardKey("NeedsToGrabCustomer", true);
		}
	}
}

void AODBouncer::TriggerPhysicsGrab()
{
	if (GrabbedAI)
	{
		// Find the closest point to grab on the AI
		TArray<FName> AISocketNames = GrabbedAI->GetGrabbableSocketNames();
		USkeletalMeshComponent* AIMesh = GrabbedAI->GetMesh();

		FName ClosestSocketName = "";
		float ClosestDist = 9999.f;
		if (AISocketNames.Num() > 0)
		{
			for (FName AISocketName : AISocketNames)
			{
				FVector SocketPosition = AIMesh->GetSocketTransform(AISocketName, RTS_World).GetLocation();
				float dist = FVector::Dist(GetActorLocation(), SocketPosition);
				if (dist <= ClosestDist)
				{
					ClosestDist = dist;
					ClosestSocketName = AISocketName;
				}
			}
		}

		//UE_LOG(LogTemp, Warning, TEXT("Closest Socket: %s"), *ClosestSocketName.ToString());
		GrabConstraintComponent->SetConstrainedComponents(GrabCubeMesh, "", GrabbedAI->GetMesh(), ClosestSocketName);
		GrabbedAI->HandleBouncerGrab();
	}
}

void AODBouncer::FinishGrab()
{
	bIsDragging = true;
}

FVector AODBouncer::GetPostLocation()
{
	return PostLocation;
}

int32 AODBouncer::GetAIQueueSize()
{
	return QueueSize;
}

void AODBouncer::DelayStart()
{
	if (BouncerController)
	{
		BouncerController->SetBoolBlackboardKey("NeedsToGrabCustomer", true);
	}
}
