// Copyright 2020 Space Gurus


#include "AI/Services/ODBTService_UpdateBehavior.h"
#include "AI/ODAICharacter.h"
#include "AI/ODAIController.h"
#include "AI/ODAIManager.h"
#include "BehaviorTree/BlackboardComponent.h"

UODBTService_UpdateBehavior::UODBTService_UpdateBehavior(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	NodeName = "Update Behavior";

	bNotifyBecomeRelevant = true; // MUST have this
	bNotifyOnSearch = true;

	Interval = 2.f;
	RandomDeviation = 0.f;
}

void UODBTService_UpdateBehavior::OnBecomeRelevant(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	Super::OnBecomeRelevant(OwnerComp, NodeMemory);
}

void UODBTService_UpdateBehavior::TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds)
{
	Super::TickNode(OwnerComp, NodeMemory, DeltaSeconds);

	

	AODAIController* AIController = Cast<AODAIController>(OwnerComp.GetAIOwner());
	if (AIController)
	{
		AODAIManager* AIManager = Cast<AODAIManager>(AIController->GetAIManager());
		if (!AIManager)
			return;

		AODAICharacter* AICharacter = Cast<AODAICharacter>(AIController->GetCharacter());
		if (AICharacter)
		{
			// Update any blackboard keys
			AIController->SetBoolBlackboardKey("IsHoldingDrink", AICharacter->IsHoldingDrink());
			AIController->SetBoolBlackboardKey("WantsToFight", AICharacter->WantsToFight());

			if (AICharacter->WantsToFight())
			{
				// Drop our drink if we want to fight
				AICharacter->DropItem();
			}

			// If we don't have a drink we're getting thirsty
			if (!AICharacter->IsHoldingDrink() && !AIController->IsFiller())
			{
				// Don't start getting thirsty unless we're actually in the bar
				if (AICharacter->IsInBarArea() && AIManager->HasGameStarted())
				{
					AICharacter->IncreaseThirst(FMath::RandRange(MinThirstRange, MaxThirstRange));
				}

				// Thirsty enough to try and order?
				UBlackboardComponent* BlackboardComponent = OwnerComp.GetBlackboardComponent();
				if (AICharacter->WantsToOrderDrink() && !AICharacter->IsCustomerWaitingToOrder() && !AICharacter->IsWaitingForOrderDelivery() && BlackboardComponent)
				{
					BlackboardComponent->SetValueAsBool("WantsToOrder", true);
				}
			}

			// Random chance of leaving if we're a filler
			if (AIController->IsFiller())
			{
				int32 RandomLeave = FMath::RandRange(0, 1000);
				if (RandomLeave <= 5)
				{
					AIController->SetBoolBlackboardKey("ShouldLeaveBar", true);

					// Let the manager know
					if (AIManager)
					{
						AIManager->NotifyOfLeaving(AICharacter);
					}
				}
			}
		}
	}
}
