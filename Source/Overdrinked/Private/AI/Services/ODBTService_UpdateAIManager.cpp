// Copyright 2020 Space Gurus


#include "AI/Services/ODBTService_UpdateAIManager.h"
#include "AI/ODAIManagerController.h"
#include "AI/ODAIManager.h"

UODBTService_UpdateAIManager::UODBTService_UpdateAIManager(const FObjectInitializer& ObjectInitializer)
{
	NodeName = "Update AI Manager";

	bNotifyBecomeRelevant = true; // MUST have this
	bNotifyOnSearch = true;

	Interval = 2.f;
	RandomDeviation = 0.f;
}

void UODBTService_UpdateAIManager::OnBecomeRelevant(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	Super::OnBecomeRelevant(OwnerComp, NodeMemory);
}

void UODBTService_UpdateAIManager::TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds)
{
	Super::TickNode(OwnerComp, NodeMemory, DeltaSeconds);

	AODAIManagerController* AIManagerController = Cast<AODAIManagerController>(OwnerComp.GetAIOwner());
	if (AIManagerController)
	{
		AODAIManager* AIManager = Cast<AODAIManager>(AIManagerController->GetCharacter());
		if (AIManager)
		{
			AIManager->UpdateBrawlLevel();
		}
	}
}
