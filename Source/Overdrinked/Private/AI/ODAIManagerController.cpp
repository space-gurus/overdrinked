// Copyright 2020 Space Gurus


#include "AI/ODAIManagerController.h"
#include "AI/ODAIManager.h"
#include "BehaviorTree/BehaviorTree.h"

AODAIManagerController::AODAIManagerController(const class FObjectInitializer& ObjectInitializer)
{

}

void AODAIManagerController::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);

	AIManager = Cast<AODAIManager>(InPawn);

	if (AIManager && BehaviorTree)
	{
		RunBehaviorTree(BehaviorTree);
	}
}

void AODAIManagerController::Server_CreateNewCustomer_Implementation()
{
	if (AIManager)
	{
		AIManager->SpawnAICustomer();
	}
}
