// Copyright 2020 Space Gurus


#include "AI/ODAIManager.h"
#include "AI/ODAIManagerController.h"
#include "AI/ODAICharacter.h"
#include "AI/ODAIController.h"
#include "AI/ODBouncer.h"
#include "AI/ODCrawlerManager.h"
#include "ODMultiplayerGameMode.h"
#include "ODMultiplayerGameState.h"
#include "Environment/ODAILocation.h"
#include "Environment/ODAISpawnArea.h"
#include "Components/BoxComponent.h"
#include "Kismet/GameplayStatics.h"
#include "AkGameplayStatics.h"
#include "AkRtpc.h"

AODAIManager::AODAIManager(const FObjectInitializer& OI) : Super(OI)
{
	PrimaryActorTick.bCanEverTick = false;
	bReplicates = true;

	AIControllerClass = AODAIManagerController::StaticClass();
	AutoPossessAI = EAutoPossessAI::PlacedInWorldOrSpawned;

	BarBoundingArea = CreateDefaultSubobject<UBoxComponent>(TEXT("BarBoundingArea"));
	BarBoundingArea->AttachTo(RootComponent);

}

void AODAIManager::Server_StartRound_Implementation()
{
	// Let all of the AI know the round has started
	for (AODAICharacter* AICharacter : ActiveAICustomers)
	{
		AODAIController* AIController = Cast<AODAIController>(AICharacter->GetController());
		if (AIController)
		{
			AIController->HandleGameStart();
		}
	}

	bGameStarted = true;
}

void AODAIManager::Server_BeginGameStartCountdown_Implementation()
{
	GetWorldTimerManager().SetTimer(GameStartTimerHandle, this, &AODAIManager::Server_StartRound, 3.f, false);
}

void AODAIManager::Server_SpawnStartingCustomers_Implementation()
{
	for (int i = 0; i < NumStartingAI; i++)
	{
		bool bSpawnSuccess = false;
		int32 FailCounter = 0;

		// Make sure we don't spawn in the same place twice
		while (!bSpawnSuccess && FailCounter < 50)
		{
			FAISpawnProperties NewSpawnProps = GetRandomSpawnLocation();
			if (!PastStartupSpawnLocations.Contains(NewSpawnProps.SpawnLocation))
			{
				SpawnAICustomerAtLocation(NewSpawnProps);
				bSpawnSuccess = true;
				PastStartupSpawnLocations.AddUnique(NewSpawnProps.SpawnLocation);
			}

			FailCounter++;
		}

		if (FailCounter >= 50)
		{
			UE_LOG(LogTemp, Error, TEXT("Failed to find spot"))
		}
	}

	// Cleanup
	PastStartupSpawnLocations.Empty();
}

FVector AODAIManager::GetBarEntrance()
{
	return BarEntrance + GetActorLocation();
}

FVector AODAIManager::GetBarExit()
{
	return BarExit + GetActorLocation();
}

UBoxComponent* AODAIManager::GetBarBoundingArea()
{
	return BarBoundingArea;
}

void AODAIManager::SetupAISpawnAreas()
{
	TArray<AActor*> FoundSpawnAreas;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), AODAISpawnArea::StaticClass(), FoundSpawnAreas);

	for (AActor* FoundActor : FoundSpawnAreas)
	{
		AODAISpawnArea* NewSpawnArea = Cast<AODAISpawnArea>(FoundActor);
		if (NewSpawnArea)
		{
			AISpawnAreas.AddUnique(NewSpawnArea);
		}
	}
}

bool AODAIManager::IsPointInBarArea(FVector PointToCheck)
{
	if (BarBoundingArea)
	{
		FVector BoxMinPoint = BarBoundingArea->GetComponentLocation() - BarBoundingArea->GetScaledBoxExtent();
		FVector BoxMaxPoint = BarBoundingArea->GetComponentLocation() + BarBoundingArea->GetScaledBoxExtent();

		FBox BoundingBox = FBox(BoxMinPoint, BoxMaxPoint);
		if (BoundingBox.IsInsideOrOn(PointToCheck))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	return false;
}

void AODAIManager::FinishCustomerActivity(AODAICharacter* AICharacter)
{
	ActiveAICustomers.Remove(AICharacter);
	AICharacter->Destroy();
}

bool AODAIManager::CanHandleNewOrder()
{
	return CurrentNumOrders < (MaxNumOrders + NumFlexOrders);
}

int32 AODAIManager::GiveOrderPrepTime()
{
	return FMath::RandRange(MinOrderPrepTime, MaxOrderPrepTime);
}

int32 AODAIManager::GiveOrderWaitTime()
{
	return FMath::RandRange(MinOrderWaitTime, MaxOrderWaitTime);
}

// Called when the game starts
void AODAIManager::BeginPlay()
{
	Super::BeginPlay();

	// Let the Game Mode know we exist
	if (HasAuthority())
	{
		AODMultiplayerGameMode* MPGameMode = Cast<AODMultiplayerGameMode>(UGameplayStatics::GetGameMode(GetWorld()));
		if (MPGameMode)
		{
			MPGameMode->SetAIManagerReference(this);
		}
	}
	
	// This must be done before any spawning
	SetupAISpawnAreas();

	Server_SpawnStartingCustomers();
}

void AODAIManager::SpawnAICustomer()
{
	if (AISpawnPositions.IsValidIndex(CurrentSpawnIndex))
	{
		FActorSpawnParameters SpawnParams;
		FVector SpawnPosition = AISpawnPositions[CurrentSpawnIndex] + this->GetActorLocation();
		AODAICharacter* NewAICharacter = GetWorld()->SpawnActor<AODAICharacter>(AIBlueprint, SpawnPosition, FRotator(0.f, 0.f, 0.f), SpawnParams);
		
		if (NewAICharacter)
		{
			// TODO: Setup initial values for AI behavior
			AODAIController* NewAIController = Cast<AODAIController>(NewAICharacter->GetController());
			if (NewAIController)
			{
				// Randomly decide if we're filler or not
				NewAIController->SetFillerStatus(FMath::RandBool());
				
				NewAIController->SetAIManagerReference(this);
				NewAIController->SetVectorBlackboardKey("BarEntrance", BarEntrance);
				NewAIController->SetVectorBlackboardKey("BarExit", GetBarExit());

			}

			GenerateAIAttributes(NewAICharacter, false);
			
			ActiveAICustomers.AddUnique(NewAICharacter);
		}
	}

	IncrementSpawnIndex();
	UpdateAICount();
}

void AODAIManager::SpawnAICustomerAtLocation(FAISpawnProperties SpawnProperties)
{
	FActorSpawnParameters SpawnParams;
	AODAICharacter* NewAICharacter = GetWorld()->SpawnActor<AODAICharacter>(AIBlueprint, SpawnProperties.SpawnLocation, SpawnProperties.SpawnRotation, SpawnParams);

	if (NewAICharacter)
	{
		// TODO: Setup initial values for AI behavior
		AODAIController* NewAIController = Cast<AODAIController>(NewAICharacter->GetController());
		if (NewAIController)
		{
			NewAIController->SetAIManagerReference(this);
			NewAIController->SetVectorBlackboardKey("BarEntrance", BarEntrance);
			NewAIController->SetVectorBlackboardKey("BarExit", GetBarExit());

			// Randomly decide if we're filler or not
			NewAIController->SetFillerStatus(FMath::RandBool());

			// We're spawning at a specific location so we're already in the bar
			NewAIController->SetBoolBlackboardKey("IsInBarArea", true);
			NewAICharacter->SetIsInBarArea(true);

			if (SpawnProperties.AILocation != NULL)
			{
				SpawnProperties.AILocation->StartWithCharacterInBounds();
				NewAIController->SetCurrentAILocation(SpawnProperties.AILocation);
			}
		}

		GenerateAIAttributes(NewAICharacter, true);

		ActiveAICustomers.AddUnique(NewAICharacter);
	}

	UpdateAICount();
}

void AODAIManager::GenerateAIAttributes(AODAICharacter* NewCharacter, bool bIsStarterAI)
{
	float ThirstThreshold = FMath::RandRange(MinThirstThreshold, MaxThirstThreshold);
	NewCharacter->SetThirstThreshold(ThirstThreshold);
	float AggroThreshold = FMath::RandRange(MinAggroThreshold, MaxAggroThreshold);
	NewCharacter->SetAggressionThreshold(AggroThreshold);

	// If we're spawned at the beginning start with some attributes
	if (bIsStarterAI)
	{
		NewCharacter->IncreaseThirst(FMath::RandRange(10.f, ThirstThreshold));
	}
}

FAISpawnProperties AODAIManager::GetRandomSpawnLocation()
{
	// Find an idle location
	TArray<AActor*> FoundLocations;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), AODAILocation::StaticClass(), FoundLocations);

	// Try to find a spot in one of the pre designated locations
	if (FoundLocations.Num() > 0)
	{
		int32 FailCounter = 0;

		while (FailCounter < FoundLocations.Num())
		{
			int32 RandLocIndex = FMath::RandRange(0, FoundLocations.Num() - 1);
			if (FoundLocations.IsValidIndex(RandLocIndex))
			{
				AODAILocation* RandLocation = Cast<AODAILocation>(FoundLocations[RandLocIndex]);
				if (RandLocation && RandLocation->IsLocationFree() && !PastStartupSpawnLocations.Contains(RandLocation->GetActorLocation()))
				{
					if (IsStartingSpawnPointValid(RandLocation->GetActorLocation()))
					{
						FAISpawnProperties NewSpawnProperties;
						NewSpawnProperties.SpawnLocation = RandLocation->GetActorLocation();
						NewSpawnProperties.SpawnRotation = RandLocation->GetActorRotation();
						NewSpawnProperties.AILocation = RandLocation;
						return NewSpawnProperties;
					}
				}
			}

			FailCounter++;
		}
	}

	// Try to find a point in one of the spawn areas instead
	int32 FailCounter = 0;
	while (FailCounter < 50)
	{
		FVector SpawnLocation = FindPointInAISpawnArea();

		if (IsStartingSpawnPointValid(SpawnLocation))
		{
			FAISpawnProperties NewSpawnProperties;
			NewSpawnProperties.SpawnLocation = SpawnLocation;
			NewSpawnProperties.SpawnRotation = FRotator::ZeroRotator;
			NewSpawnProperties.SpawnRotation.Yaw = FMath::RandRange(0.f, 360.f);

			return NewSpawnProperties;
		}

		FailCounter++;
	}

	FAISpawnProperties FailSpawnProps;
	FailSpawnProps.SpawnLocation = FVector::ZeroVector;
	FailSpawnProps.SpawnRotation = FRotator::ZeroRotator;
	return FailSpawnProps;
}

FVector AODAIManager::FindPointInAISpawnArea()
{
	int32 RandIndex = FMath::RandRange(0, AISpawnAreas.Num() - 1);
	if (AISpawnAreas.IsValidIndex(RandIndex))
	{
		return AISpawnAreas[RandIndex]->GetRandomSpawnPoint();
	}

	return FVector::ZeroVector;
}

bool AODAIManager::IsStartingSpawnPointValid(FVector SpawnPoint)
{
	// Edge case check for when we would overlap a capsule
	bool bIsValidSpawnPoint = true;

	for (FVector PastLocation : PastStartupSpawnLocations)
	{
		FBox ContainmentBox = FBox(PastLocation - CapsuleRadius, PastLocation + CapsuleRadius);

		if (ContainmentBox.IsInsideOrOn(SpawnPoint))
		{
			// We can't spawn here because we're inside an AI capsule
			bIsValidSpawnPoint = false;
		}
	}

	return bIsValidSpawnPoint;
}

void AODAIManager::NotifyOfLeaving(AODAICharacter* CharacterLeaving)
{
	// Do we have room to spawn another?
	if (CurrentAICapacity < (MaxAICapacity + CapacityFlexValue))
	{
		SpawnAICustomer();
	}
}

void AODAIManager::HandleAngry()
{

}

void AODAIManager::NotifyOfTaser(AODAICharacter* CharacterDisabled)
{
	// Get the bouncers
	TArray<AActor*> FoundBouncers;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), AODBouncer::StaticClass(), FoundBouncers);

	if (FoundBouncers.Num() > 0 && CharacterDisabled)
	{
		// First try to find an available bouncer
		bool bFoundAvailBouncer = false;
		int32 BouncerIndex = 0;
		while (!bFoundAvailBouncer && BouncerIndex < FoundBouncers.Num())
		{
			if (FoundBouncers.IsValidIndex(BouncerIndex))
			{
				AODBouncer* Bouncer = Cast<AODBouncer>(FoundBouncers[BouncerIndex]);
				if (Bouncer && Bouncer->IsAvailable())
				{
					bFoundAvailBouncer = true;
					Bouncer->AddCustomerToGrabList(CharacterDisabled);
				}
			}

			BouncerIndex++;
		}

		// Otherwise give it to the one with the least to do
		if (!bFoundAvailBouncer)
		{
			int32 SmallestQueue = 999;
			AODBouncer* BestBouncer = NULL;
			for (AActor* FoundActor : FoundBouncers)
			{
				AODBouncer* ThisBouncer = Cast<AODBouncer>(FoundActor);
				if (ThisBouncer && (ThisBouncer->GetAIQueueSize() < SmallestQueue))
				{
					SmallestQueue = ThisBouncer->GetAIQueueSize();
					BestBouncer = ThisBouncer;
				}
			}

			if (BestBouncer)
			{
				BestBouncer->AddCustomerToGrabList(CharacterDisabled);
			}
		}
	}

	NotifyOfLeaving(CharacterDisabled);
}

void AODAIManager::AddOrder()
{
	CurrentNumOrders++;
}

void AODAIManager::RemoveOrder()
{
	CurrentNumOrders = FMath::Clamp(CurrentNumOrders - 1, 0, MaxNumOrders + NumFlexOrders);
}

TArray<AODAICharacter*> AODAIManager::GetActiveAICharacters()
{
	return ActiveAICustomers;
}

bool AODAIManager::HasGameStarted()
{
	return bGameStarted;
}

void AODAIManager::UpdateBrawlLevel()
{
	if (HasAuthority())
	{
		// Only the server should have control of this
		int32 NumBrawlingAI = 0;
		for (AODAICharacter* AICharacter : ActiveAICustomers)
		{
			if (AICharacter && AICharacter->WantsToFight())
			{
				NumBrawlingAI++;
			}
		}

		float BrawlPercentage = (float)NumBrawlingAI / (float)CurrentAICapacity;

		AODMultiplayerGameState* MPGameState = Cast<AODMultiplayerGameState>(UGameplayStatics::GetGameState(GetWorld()));
		if (MPGameState)
		{
			MPGameState->SetBrawlPercentage(BrawlPercentage);
		}
	}
}

void AODAIManager::HandleCrawlerOrder()
{
	TArray<AActor*> FoundActors;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), AODCrawlerManager::StaticClass(), FoundActors);

	if (FoundActors.Num() > 0)
	{
		AODCrawlerManager* CrawlerManager = Cast<AODCrawlerManager>(FoundActors[0]);
		if (CrawlerManager && CrawlerManager->GetNumActiveCrawlers() < CurrentNumOrders)
		{
			CrawlerManager->Server_SpawnCrawler();
		}
	}
}

void AODAIManager::IncrementSpawnIndex()
{
	CurrentSpawnIndex++;
	if (CurrentSpawnIndex > AISpawnPositions.Num() - 1)
	{
		CurrentSpawnIndex = 0;
	}
}

void AODAIManager::UpdateAICount()
{
	CurrentAICapacity = ActiveAICustomers.Num();
	AICapacityProportion = (float)CurrentAICapacity / (float)MaxAICapacity;

	// Let Wwise know
	if (BarPopuplationRTPC)
	{
		UAkGameplayStatics::SetRTPCValue(BarPopuplationRTPC, AICapacityProportion, 500, NULL, FName("bar_population"));
	}
}

