// Copyright 2020 Space Gurus


#include "AI/Decorators/ODBTDec_HoldConversation.h"
#include "AI/ODAIController.h"
#include "Environment/ODAILocation.h"
#include "Environment/ODAIHub.h"

UODBTDec_HoldConversation::UODBTDec_HoldConversation()
{
	NodeName = "Has Someone To Talk To";
	bAllowAbortChildNodes = true;
}

bool UODBTDec_HoldConversation::CalculateRawConditionValue(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) const
{
	AODAIController* AIController = Cast<AODAIController>(OwnerComp.GetAIOwner());
	if (AIController)
	{
		if (AIController->GetCurrentAILocation())
		{
			AODAIHub* CurrentAIHub = AIController->GetCurrentAILocation()->GetConversationHub();
			if (CurrentAIHub)
			{
				// Figure out if anyone is standing near us
				return CurrentAIHub->CanHoldConversation();
			}
		}
	}

	return false;
}
