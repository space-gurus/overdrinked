// Copyright 2020 Space Gurus


#include "AI/ODCrawlerManager.h"
#include "AI/ODAICrawler.h"

AODCrawlerManager::AODCrawlerManager(const FObjectInitializer& OI) : Super(OI)
{
	PrimaryActorTick.bCanEverTick = false;

}

void AODCrawlerManager::BeginPlay()
{
	Super::BeginPlay();
	
}

void AODCrawlerManager::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

int32 AODCrawlerManager::GetNumActiveCrawlers()
{
	return NumActiveCrawlers;
}

void AODCrawlerManager::Server_SpawnCrawler_Implementation()
{
	int32 SpawnPosIndex = FMath::RandRange(0, CrawlerSpawnPositions.Num() - 1);

	if (CrawlerSpawnPositions.IsValidIndex(SpawnPosIndex))
	{
		FActorSpawnParameters SpawnParams;
		AODAICrawler* Crawler = GetWorld()->SpawnActor<AODAICrawler>(CrawlerBP, CrawlerSpawnPositions[SpawnPosIndex], FRotator(0.f), SpawnParams);
		if (Crawler)
		{
			Crawler->SetManagerReference(this);
			ActiveCrawlers.Add(Crawler);
			NumActiveCrawlers++;
		}
	}
}

void AODCrawlerManager::Server_SpawnCrawlerAtPosition_Implementation(FVector SpawnPos)
{
	FActorSpawnParameters SpawnParams;
	SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;
	AODAICrawler* Crawler = GetWorld()->SpawnActor<AODAICrawler>(CrawlerBP, SpawnPos, FRotator(0.f), SpawnParams);
	if (Crawler)
	{
		Crawler->SetManagerReference(this);
		ActiveCrawlers.Add(Crawler);
		NumActiveCrawlers++;
	}
}

