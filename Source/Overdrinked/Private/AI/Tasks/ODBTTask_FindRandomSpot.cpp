// Copyright 2020 Space Gurus


#include "AI/Tasks/ODBTTask_FindRandomSpot.h"
#include "NavigationSystem.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/Blackboard/BlackboardKeyAllTypes.h"
#include "AI/ODAIController.h"
#include "AI/ODAICharacter.h"

EBTNodeResult::Type UODBTTask_FindRandomSpot::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	AODAIController* Controller = Cast<AODAIController>(OwnerComp.GetAIOwner());
	if (Controller)
	{
		AODAICharacter* AICharacter = Cast<AODAICharacter>(Controller->GetPawn());
		if (AICharacter)
		{
			UNavigationSystemV1* NavSystem = UNavigationSystemV1::GetCurrent(GetWorld());
			if (!NavSystem)
			{
				return EBTNodeResult::Failed;
			}

			FNavLocation Result;
			if (!NavSystem->GetRandomReachablePointInRadius(AICharacter->GetActorLocation(), SearchRadius, Result))
			{
				return EBTNodeResult::Failed;
			}

			OwnerComp.GetBlackboardComponent()->SetValueAsVector("Spot", Result);
			return EBTNodeResult::Succeeded;
		}
		else
		{
			return EBTNodeResult::Failed;
		}
	}
	else
	{
		return EBTNodeResult::Failed;
	}


	return EBTNodeResult::Failed;
}
