// Copyright 2020 Space Gurus


#include "AI/Tasks/ODBTTask_StopConversation.h"
#include "AI/ODAIController.h"
#include "AI/ODAICharacter.h"

UODBTTask_StopConversation::UODBTTask_StopConversation()
{
	NodeName = "Stop Conversation";
}

EBTNodeResult::Type UODBTTask_StopConversation::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	AODAIController* AIController = Cast<AODAIController>(OwnerComp.GetAIOwner());
	if (AIController)
	{
		AODAICharacter* AICharacter = Cast<AODAICharacter>(AIController->GetPawn());
		if (AICharacter)
		{
			// If we just finished our conversation

			if (AICharacter->IsInConversation())
			{
				AIController->FinishConversation();
				return EBTNodeResult::Succeeded;
			}
		}
	}

	return EBTNodeResult::Succeeded;
}
