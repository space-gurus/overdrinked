// Copyright 2020 Space Gurus


#include "AI/Tasks/ODBTTask_StartConversation.h"
#include "AI/ODAIController.h"
#include "AI/ODAICharacter.h"

UODBTTask_StartConversation::UODBTTask_StartConversation()
{
	bNotifyTaskFinished = true;
}

EBTNodeResult::Type UODBTTask_StartConversation::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	AIController = Cast<AODAIController>(OwnerComp.GetAIOwner());
	if (AIController)
	{
		AICharacter = Cast<AODAICharacter>(AIController->GetPawn());
		if (AICharacter)
		{
			// If we just finished our conversation
			
			if (!AICharacter->IsInConversation())
			{
				AIController->StartConversation();
				return EBTNodeResult::Succeeded;
			}
		}
	}

	return EBTNodeResult::Failed;
}

