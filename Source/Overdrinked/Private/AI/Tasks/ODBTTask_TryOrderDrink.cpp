// Copyright 2020 Space Gurus


#include "AI/Tasks/ODBTTask_TryOrderDrink.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "AI/ODAIController.h"

EBTNodeResult::Type UODBTTask_TryOrderDrink::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	AODAIController* AIController = Cast<AODAIController>(OwnerComp.GetAIOwner());
	if (AIController)
	{
		AIController->Server_PlaceNewOrder();
		
		UBlackboardComponent* BlackboardComponent = OwnerComp.GetBlackboardComponent();
		if (BlackboardComponent)
		{
			BlackboardComponent->SetValueAsBool("WantsToOrder", false);
			return EBTNodeResult::Succeeded;
		}
	}

	return EBTNodeResult::Failed;
}
