// Copyright 2020 Space Gurus


#include "AI/Tasks/ODBTTask_FindCrawlerMove.h"
#include "NavigationSystem.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/Blackboard/BlackboardKeyAllTypes.h"
#include "AI/ODAICrawler.h"
#include "AI/ODCrawlerController.h"

EBTNodeResult::Type UODBTTask_FindCrawlerMove::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	AODCrawlerController* Controller = Cast<AODCrawlerController>(OwnerComp.GetAIOwner());
	if (Controller)
	{
		AODAICrawler* AICharacter = Cast<AODAICrawler>(Controller->GetPawn());
		if (AICharacter)
		{
			UNavigationSystemV1* NavSystem = UNavigationSystemV1::GetCurrent(GetWorld());
			if (!NavSystem)
			{
				return EBTNodeResult::Failed;
			}

			FNavLocation Result;
			if (!NavSystem->GetRandomReachablePointInRadius(AICharacter->GetActorLocation(), SearchRadius, Result))
			{
				return EBTNodeResult::Failed;
			}

			OwnerComp.GetBlackboardComponent()->SetValueAsVector("MoveSpot", Result);
			return EBTNodeResult::Succeeded;
		}
		else
		{
			return EBTNodeResult::Failed;
		}
	}
	else
	{
		return EBTNodeResult::Failed;
	}

	return EBTNodeResult::Failed;
}
