// Copyright 2020 Space Gurus


#include "AI/Tasks/ODBTTask_Attack.h"
#include "AI/ODAIController.h"

EBTNodeResult::Type UODBTTask_Attack::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	AODAIController* AIController = Cast<AODAIController>(OwnerComp.GetAIOwner());
	if (!AIController)
	{
		return EBTNodeResult::Failed;
	}

	AIController->StartAttack();
	return EBTNodeResult::Succeeded;
}
