// Copyright 2020 Space Gurus


#include "AI/Tasks/ODBTTask_TakeDrink.h"
#include "AI/ODAIController.h"

EBTNodeResult::Type UODBTTask_TakeDrink::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	AODAIController* AIController = Cast<AODAIController>(OwnerComp.GetAIOwner());
	if (AIController && AIController->CanTakeDrink())
	{
		AIController->Server_TakeDrink();
		return EBTNodeResult::Succeeded;
	}

	return EBTNodeResult::Failed;
}
