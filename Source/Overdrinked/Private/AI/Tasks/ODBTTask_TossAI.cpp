// Copyright 2020 Space Gurus


#include "AI/Tasks/ODBTTask_TossAI.h"
#include "AI/ODBouncerController.h"
#include "AI/ODAICharacter.h"

UODBTTask_TossAI::UODBTTask_TossAI()
{
	NodeName = "Toss AI";
}

EBTNodeResult::Type UODBTTask_TossAI::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	AODBouncerController* BouncerController = Cast<AODBouncerController>(OwnerComp.GetAIOwner());
	if (!BouncerController)
	{
		return EBTNodeResult::Failed;
	}

	AODAICharacter* AIToToss = Cast<AODAICharacter>(BouncerController->GetObjectBlackboardKey("CustomerToGrab"));
	if (!AIToToss)
	{
		return EBTNodeResult::Failed;
	}

	BouncerController->TryTossAI(AIToToss);

	// Check if we need to return to base
	return EBTNodeResult::Succeeded;
}
