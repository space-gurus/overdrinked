// Copyright 2020 Space Gurus


#include "AI/Tasks/ODBTTask_FindBrawlTarget.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "AI/ODAIController.h"
#include "AI/ODAIManager.h"
#include "AI/ODAICharacter.h"

EBTNodeResult::Type UODBTTask_FindBrawlTarget::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	AODAIController* AIController = Cast<AODAIController>(OwnerComp.GetAIOwner());
	if (!AIController)
	{
		return EBTNodeResult::Failed;
	}

	AODAIManager* AIManager = AIController->GetAIManager();
	if (!AIManager)
	{
		return EBTNodeResult::Failed;
	}

	AODAICharacter* ThisAI = Cast<AODAICharacter>(AIController->GetPawn());
	if (!ThisAI)
	{
		return EBTNodeResult::Failed;
	}

	// Do we already have a target? If we do, just keep that one
	if (ThisAI->GetBrawlTarget())
	{
		return EBTNodeResult::Succeeded;
	}
	else
	{
		// Find someone new
		TArray<AODAICharacter*> ActiveAI = AIManager->GetActiveAICharacters();
		for (AODAICharacter* AICharacter : ActiveAI)
		{
			// Don't select ourselves
			if (AICharacter && (AICharacter != AIController->GetPawn()))
			{
				if (FVector::Dist(AICharacter->GetActorLocation(), AIController->GetPawn()->GetActorLocation()) <= SearchRadius)
				{
					UBlackboardComponent* BlackboardComponent = OwnerComp.GetBlackboardComponent();
					if (BlackboardComponent)
					{
						BlackboardComponent->SetValueAsObject("BrawlTarget", AICharacter);
						return EBTNodeResult::Succeeded;
					}
				}
			}
		}
	}

	UE_LOG(LogTemp, Error, TEXT("UODBTTask_FindBrawlTarget Returning FindBrawlTarget FAIL"));
	return EBTNodeResult::Failed;
}
