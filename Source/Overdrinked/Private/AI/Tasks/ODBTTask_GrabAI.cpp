// Copyright 2020 Space Gurus


#include "AI/Tasks/ODBTTask_GrabAI.h"
#include "AI/ODBouncer.h"
#include "AI/ODBouncerController.h"
#include "AI/ODAICharacter.h"
#include "AI/ODAIManager.h"
#include "Kismet/GameplayStatics.h"

UODBTTask_GrabAI::UODBTTask_GrabAI()
{
	NodeName = "Try To Grab AI";
}

EBTNodeResult::Type UODBTTask_GrabAI::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	AODBouncerController* BouncerController = Cast<AODBouncerController>(OwnerComp.GetAIOwner());
	if (!BouncerController)
	{
		return EBTNodeResult::Failed;
	}

	AODAICharacter* AIToGrab = Cast<AODAICharacter>(BouncerController->GetObjectBlackboardKey("CustomerToGrab"));
	if (!AIToGrab)
	{
		UE_LOG(LogTemp, Error, TEXT("AIToGrab is NULL in ODBTTask_GrabAI"));
		return EBTNodeResult::Failed;
	}

	BouncerController->TryToGrabAI(AIToGrab);

	// Make sure we know where to go from here
	TArray<AActor*> FoundAIManagers;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), AODAIManager::StaticClass(), FoundAIManagers);
	if (FoundAIManagers.Num() > 0)
	{
		AODAIManager* AIManager = Cast<AODAIManager>(FoundAIManagers[0]);
		FVector BarExit = AIManager->GetBarExit();
		BouncerController->SetVectorBlackboardKey("BarExit", BarExit);
	}

	return EBTNodeResult::Succeeded;
}
