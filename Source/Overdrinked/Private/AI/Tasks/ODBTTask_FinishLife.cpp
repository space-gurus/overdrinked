// Copyright 2020 Space Gurus


#include "AI/Tasks/ODBTTask_FinishLife.h"
#include "AI/ODAIController.h"

EBTNodeResult::Type UODBTTask_FinishLife::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	AODAIController* AIController = Cast<AODAIController>(OwnerComp.GetAIOwner());
	if (AIController)
	{
		AIController->Server_FinishLife();
		return EBTNodeResult::Succeeded;
	}
	
	return EBTNodeResult::Failed;
}
