// Copyright 2020 Space Gurus


#include "AI/Tasks/ODBTTask_CreateNewCustomer.h"
#include "AI/ODAIManagerController.h"

EBTNodeResult::Type UODBTTask_CreateNewCustomer::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	AODAIManagerController* ManagerController = Cast<AODAIManagerController>(OwnerComp.GetAIOwner());
	if (ManagerController)
	{
		ManagerController->Server_CreateNewCustomer();
		return EBTNodeResult::Succeeded;
	}

	return EBTNodeResult::Failed;
}
