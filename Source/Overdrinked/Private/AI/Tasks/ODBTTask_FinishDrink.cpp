// Copyright 2020 Space Gurus


#include "AI/Tasks/ODBTTask_FinishDrink.h"
#include "AI/ODAIController.h"

EBTNodeResult::Type UODBTTask_FinishDrink::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	AODAIController* AIController = Cast<AODAIController>(OwnerComp.GetAIOwner());
	if (AIController && AIController->CanDropItem())
	{
		AIController->Server_DropGlass();
		//AIController->SetBoolBlackboardKey("ShouldLeaveBar", true);
		return EBTNodeResult::Succeeded;
	}

	return EBTNodeResult::Failed;
}
