// Copyright 2020 Space Gurus


#include "AI/Tasks/ODBTTask_IdleAI.h"
#include "AI/ODAIController.h"
#include "AI/ODAICharacter.h"

EBTNodeResult::Type UODBTTask_IdleAI::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	AODAIController* AIController = Cast<AODAIController>(OwnerComp.GetAIOwner());
	if (!AIController)
		return EBTNodeResult::Failed;

	AODAICharacter* AICharacter = Cast<AODAICharacter>(AIController->GetPawn());
	if (!AICharacter)
		return EBTNodeResult::Failed;

	if (AICharacter->HasAuthority())
	{
		bool bPlayAnim = FMath::RandRange(0.f, 1.f) < IdleAnimChance;

		if (bPlayAnim)
		{
			AICharacter->PlayIdleAnim();
			return EBTNodeResult::Succeeded;
		}
	}

	return EBTNodeResult::Succeeded;
}
