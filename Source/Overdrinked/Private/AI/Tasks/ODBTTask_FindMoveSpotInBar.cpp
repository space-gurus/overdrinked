// Copyright 2020 Space Gurus


#include "AI/Tasks/ODBTTask_FindMoveSpotInBar.h"
#include "NavigationSystem.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/Blackboard/BlackboardKeyAllTypes.h"
#include "Kismet/GameplayStatics.h"
#include "Components/BoxComponent.h"
#include "AI/ODAIController.h"
#include "AI/ODAICharacter.h"
#include "AI/ODAIManager.h"
#include "Environment/ODAILocation.h"
#include "DrawDebugHelpers.h"

EBTNodeResult::Type UODBTTask_FindMoveSpotInBar::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	AODAIController* AIController = Cast<AODAIController>(OwnerComp.GetAIOwner());
	if (!AIController)
	{
		return EBTNodeResult::Failed;
	}
	
	AODAIManager* AIManager = AIController->GetAIManager();
	if (!AIManager)
	{
		return EBTNodeResult::Failed;
	}
	
	UNavigationSystemV1* NavSystem = UNavigationSystemV1::GetCurrent(GetWorld());
	if (!NavSystem)
	{
		return EBTNodeResult::Failed;
	}

	AODAICharacter* AICharacter = Cast<AODAICharacter>(AIController->GetPawn());
	if (!AICharacter)
	{
		return EBTNodeResult::Failed;
	}

	// First figure out if we should move to an idle spot or just a random spot
	// If we're holding a drink we probably want to idle while we drink it
	float DrinkWeight = 0.f;
	if (AICharacter->IsHoldingDrink())
	{
		DrinkWeight += 0.5;
	}

	float IdleDecisionWeight = FMath::RandRange(0.f, 0.8f) + DrinkWeight;

	if (IdleDecisionWeight >= 0.2f)
	{
		// Find an idle location
		TArray<AActor*> FoundLocations;
		UGameplayStatics::GetAllActorsOfClass(GetWorld(), AODAILocation::StaticClass(), FoundLocations);

		if (FoundLocations.Num() > 0)
		{
			int32 FailCounter = 0;

			while (FailCounter < FoundLocations.Num())
			{
				int32 RandLocIndex = FMath::RandRange(0, FoundLocations.Num() - 1);
				if (FoundLocations.IsValidIndex(RandLocIndex))
				{
					AODAILocation* RandLocation = Cast<AODAILocation>(FoundLocations[RandLocIndex]);
					if (RandLocation && RandLocation->IsLocationFree() && RandLocation->IsAvailableForMove())
					{
						FVector MoveLocation = RandLocation->GetActorLocation();
						// Let the location know so other AI can't move there
						RandLocation->SetIsAvailableForMove(false);

						OwnerComp.GetBlackboardComponent()->SetValueAsVector("Spot", MoveLocation);
						
						// Get some position in front of where we're going to face that direction
						FVector ToFace = MoveLocation + (RandLocation->GetActorForwardVector() * 100.f);
						AIController->SetVectorBlackboardKey("ItemToFace", ToFace);

						return EBTNodeResult::Succeeded;
					}
				}

				FailCounter++;
			}
		}
	}
	
	// If we got this far we don't really care so move to a random spot
	FVector MoveLocation = AIController->GetCharacter()->GetActorLocation();
	int32 FailCounter = 0;

	// Keep trying to find a spot that's contained within the bar
	while (FailCounter < 100)
	{
		FNavLocation Result;

		if (NavSystem->GetRandomReachablePointInRadius(AICharacter->GetActorLocation(), 2000.f, Result))
		{
			// Check if it's contained in the bar area
			if (AIManager->IsPointInBarArea(Result.Location))
			{
				OwnerComp.GetBlackboardComponent()->SetValueAsVector("Spot", Result.Location);
				AIController->SetBoolBlackboardKey("MovingToTable", false);
				AIController->SetVectorBlackboardKey("ItemToFace", Result.Location);
				return EBTNodeResult::Succeeded;
			}
		}

		FailCounter++;
	}

	// Break out and fail after 50 tries
	return EBTNodeResult::Failed;

}
