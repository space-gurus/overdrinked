// Copyright 2020 Space Gurus


#include "AI/Tasks/ODBTTask_Talk.h"
#include "AI/ODAIController.h"

UODBTTask_Talk::UODBTTask_Talk()
{
	NodeName = "Talk";
}

EBTNodeResult::Type UODBTTask_Talk::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	AODAIController* AIController = Cast<AODAIController>(OwnerComp.GetAIOwner());
	if (AIController)
	{
		AIController->StartTalk();
		return EBTNodeResult::Succeeded;
	}

	return EBTNodeResult::Failed;
}
