// Copyright 2020 Space Gurus


#include "AI/Tasks/ODBTTask_FindBarEntrance.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "AI/ODAIManager.h"
#include "AI/ODAIController.h"
#include "EngineUtils.h"

EBTNodeResult::Type UODBTTask_FindBarEntrance::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	AODAIController* AIController = Cast<AODAIController>(OwnerComp.GetAIOwner());
	if (!AIController)
	{
		return EBTNodeResult::Failed;
	}

	AODAIManager* AIManager = AIController->GetAIManager();
	if (!AIManager)
	{
		return EBTNodeResult::Failed;
	}

	UBlackboardComponent* BlackboardComponent = OwnerComp.GetBlackboardComponent();
	if (BlackboardComponent && AIManager)
	{
		BlackboardComponent->ClearValue("BarEntrance");
		BlackboardComponent->SetValueAsVector("BarEntrance", AIManager->GetBarEntrance());

		return EBTNodeResult::Succeeded;
	}
	else
	{
		return EBTNodeResult::Failed;
	}

	return EBTNodeResult::Failed;
}
