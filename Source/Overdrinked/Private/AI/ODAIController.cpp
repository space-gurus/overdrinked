// Copyright 2020 Space Gurus


#include "AI/ODAIController.h"
#include "AI/ODAICharacter.h"
#include "AI/ODAIManager.h"
#include "Actors/ODDrinkGlass.h"
#include "Gameplay/ODDrink.h"
#include "Data/LevelConstraintData.h"
#include "Data/DrinkData.h"
#include "Perception/AIPerceptionComponent.h"
#include "Perception/AISenseConfig_Sight.h"
#include "BehaviorTree/BehaviorTree.h"
#include "BehaviorTree/BlackboardData.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "Engine/DataTable.h"
#include "Kismet/GameplayStatics.h"
#include "ODEnumHelperFunctions.h"

AODAIController::AODAIController(const class FObjectInitializer& ObjectInitializer)
{
	// Sight
	SightConfiguration = CreateDefaultSubobject<UAISenseConfig_Sight>(TEXT("SightConfiguration"));
	SightConfiguration->DetectionByAffiliation.bDetectNeutrals = true;
	// Perception
	AIPerceptionComponent = CreateDefaultSubobject<UAIPerceptionComponent>(TEXT("AIPerceptionComponent"));
	AIPerceptionComponent->ConfigureSense(*SightConfiguration);
	SetPerceptionComponent(*AIPerceptionComponent);

	// Load necessary data
	ConstructorHelpers::FObjectFinder<UDataTable> LevelConstraintDataObject(TEXT("/Game/DrinkGame/Data/DT_LevelConstraints"));
	if (LevelConstraintDataObject.Succeeded())
	{
		LevelConstraintsTable = LevelConstraintDataObject.Object;
	}

	ConstructorHelpers::FObjectFinder<UDataTable> DrinkDataObject(TEXT("/Game/DrinkGame/Data/DT_Drinks"));
	if (DrinkDataObject.Succeeded())
	{
		DrinksTable = DrinkDataObject.Object;
	}
}

void AODAIController::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);

	// Run the behavior tree
	AICharacter = Cast<AODAICharacter>(InPawn);

	if (AICharacter && !bIsFiller && ActiveBehaviorTree)
	{
		RunBehaviorTree(ActiveBehaviorTree);
	}
	else if (AICharacter && bIsFiller && FillerBehaviorTree)
	{
		RunBehaviorTree(FillerBehaviorTree);
	}
}

void AODAIController::HandleGameStart()
{
	SetBoolBlackboardKey("GameStarted", true);

	if (AIManager)
	{
		SetVectorBlackboardKey("BarEntrance", AIManager->GetBarEntrance());
		SetVectorBlackboardKey("BarExit", AIManager->GetBarExit());
	}
}

void AODAIController::Server_PlaceNewOrder_Implementation()
{
	if (AIManager && AIManager->CanHandleNewOrder())
	{
		if (AICharacter)
		{
			AICharacter->GenerateDrinkOrder();
		}
	}
}

void AODAIController::Server_TakeDrink_Implementation()
{
	if (AICharacter && AICharacter->IsHoldingDrink())
	{
		AICharacter->ConsumeDrinkInHand();
	}
}

void AODAIController::Server_DropGlass_Implementation()
{
	if (AICharacter)
	{
		AICharacter->DropItem();
	}
}

void AODAIController::Server_FinishLife_Implementation()
{
	if (AIManager)
	{
		AIManager->FinishCustomerActivity(AICharacter);
	}
}

void AODAIController::SetBoolBlackboardKey(FName KeyName, bool val)
{
	GetBlackboardComponent()->SetValueAsBool(KeyName, val);
}

void AODAIController::SetVectorBlackboardKey(FName KeyName, FVector val)
{
	GetBlackboardComponent()->SetValueAsVector(KeyName, val);
}

void AODAIController::SetBrawlTargetKey(AODAICharacter* TargetCharacter)
{
	GetBlackboardComponent()->SetValueAsObject("BrawlTarget", TargetCharacter);
}

bool AODAIController::CanTakeDrink()
{
	if (AICharacter && AICharacter->IsHoldingDrink())
	{
		AODDrinkGlass* DrinkInHand = AICharacter->GetDrinkInHand();
		if (DrinkInHand)
		{
			if (DrinkInHand->GetCurrentVolume() > 0.f)
			{
				return true;
			}
		}
	}

	return false;
}

bool AODAIController::CanDropItem()
{
	if (AICharacter && AICharacter->IsHoldingDrink())
	{
		return true;
	}
	else
	{
		return false;
	}
}

void AODAIController::SetAIManagerReference(AODAIManager* Manager)
{
	if (Manager)
	{
		AIManager = Manager;
		
		// Initialize some necessary blackboard keys
		SetVectorBlackboardKey("BarEntrance", AIManager->GetBarEntrance());
		SetVectorBlackboardKey("BarExit", AIManager->GetBarExit());

		// Were we spawned after the game started?
		SetBoolBlackboardKey("GameStarted", AIManager->HasGameStarted());
	}
}

AODAIManager* AODAIController::GetAIManager()
{
	return AIManager;
}

FDrinkOrder AODAIController::MakeNewDrinkOrder()
{
	FDrinkOrder NewOrder;
	NewOrder.OrderOwner = AICharacter;
	NewOrder.DesiredGlass = PickRandomGlass();

	TMap<EIngredientType, int32> DesiredIngredients = PickRandomIngredients(NewOrder.DesiredGlass);
	for (TPair<EIngredientType, int32> Ingredient : DesiredIngredients)
	{
		NewOrder.DesiredIngredients.Add(Ingredient.Key);
		NewOrder.DesiredIngredientAmounts.Add(Ingredient.Value);
	}

	TMap<EGarnishType, int32> DesiredGarnishes = PickRandomGarnishes();
	for (TPair<EGarnishType, int32> Garnish : DesiredGarnishes)
	{
		NewOrder.DesiredGarnishes.Add(Garnish.Key);
		NewOrder.DesiredGarnishAmounts.Add(Garnish.Value);
	}

	NewOrder.DesiredMixMethod = PickRandomMixMethod();
	NewOrder.OrderTimer = AIManager->GiveOrderWaitTime();
	NewOrder.OrderPrepTimer = AIManager->GiveOrderPrepTime();
	NewOrder.OrderNumber = FMath::RandRange(1, 100000);

	return NewOrder;
}

EGlassType AODAIController::PickRandomGlass()
{
	if (LevelConstraintsTable)
	{
		// Look up the constraints for our current level
		FLevelConstraintData* LevelConstraintData = LevelConstraintsTable->FindRow<FLevelConstraintData>(FName(UGameplayStatics::GetCurrentLevelName(GetWorld())), FString(""));
		if (LevelConstraintData)
		{
			int32 RandGlassIndex = FMath::RandRange(0, LevelConstraintData->AvailableGlasses.Num() - 1);
			if (LevelConstraintData->AvailableGlasses.IsValidIndex(RandGlassIndex))
			{
				return LevelConstraintData->AvailableGlasses[RandGlassIndex];
			}
		}
	}

	return EGlassType::GT_Brew;
}

TMap<EIngredientType, int32> AODAIController::PickRandomIngredients(EGlassType GlassType)
{
	TMap<EIngredientType, int32> IngredientsToOrder;

	int32 TotalVolume = 0;
	switch (GlassType)
	{
		case EGlassType::GT_Brew:
			TotalVolume = 10;
			break;
		case EGlassType::GT_Shot:
			TotalVolume = 3;
			break;
		case EGlassType::GT_Stemmed:
			TotalVolume = 5;
			break;
		default:
			break;
	}

	if (LevelConstraintsTable)
	{
		FLevelConstraintData* LevelConstraintData = LevelConstraintsTable->FindRow<FLevelConstraintData>(FName(UGameplayStatics::GetCurrentLevelName(GetWorld())), FString(""));
		if (LevelConstraintData)
		{
			// A quick fix for if we have a Brew glass. Should probably not be hard coded
			if (GlassType == EGlassType::GT_Brew && LevelConstraintData->AvailableIngredients.Num() < 3)
			{
				TotalVolume = 5;
			}

			int32 NumIngredients = FMath::RandRange(2, LevelConstraintData->AvailableIngredients.Num()); // We want to have at least 2 ingredients

			// First do some math to figure out how much of each ingredient we need to add
			TArray<int32> IngredientAmounts;
			int32 CurrentSum = 0;
			for (int32 i = NumIngredients - 1; i > 0; --i)
			{
				int32 RandNum = FMath::RandRange(1, ((TotalVolume - CurrentSum) - i));
				CurrentSum += RandNum;
				IngredientAmounts.Add(RandNum);
			}
			IngredientAmounts.Add(TotalVolume - CurrentSum);


			int32 NumIngredientsAdded = 0;
			int32 RandIndex = 0;
			while (NumIngredientsAdded < NumIngredients)
			{
				// Try to grab a random ingredient from the ones available
				RandIndex = FMath::RandRange(0, LevelConstraintData->AvailableIngredients.Num() - 1);
				if (LevelConstraintData->AvailableIngredients.IsValidIndex(RandIndex))
				{
					// If our map of stuff to order does not already contain this ingredient, add it
					if (!IngredientsToOrder.Contains(LevelConstraintData->AvailableIngredients[RandIndex]))
					{
						IngredientsToOrder.Add(LevelConstraintData->AvailableIngredients[RandIndex], IngredientAmounts[NumIngredientsAdded]);
						NumIngredientsAdded++;
					}
				}
			}
		}
	}

	return IngredientsToOrder;
}

int32 AODAIController::RandomNumBetween(int32 Min, int32 Max)
{
	return FMath::FloorToInt(FMath::RandRange(0.f, 1.f) * (Max - Min + 1) + Min);
}

TMap<EGarnishType, int32> AODAIController::PickRandomGarnishes()
{
	TMap<EGarnishType, int32> GarnishesToOrder;

	if (LevelConstraintsTable)
	{
		FLevelConstraintData* LevelConstraintData = LevelConstraintsTable->FindRow<FLevelConstraintData>(FName(UGameplayStatics::GetCurrentLevelName(GetWorld())), FString(""));
		if (LevelConstraintData)
		{
			int32 NumGarnishes = FMath::RandRange(0, LevelConstraintData->AvailableGarnishes.Num());
			int32 RandIndex = 0;
			int32 NumGarnishesAdded = 0;
			while (NumGarnishesAdded < NumGarnishes)
			{
				RandIndex = FMath::RandRange(0, LevelConstraintData->AvailableGarnishes.Num() - 1);
				if (LevelConstraintData->AvailableGarnishes.IsValidIndex(RandIndex))
				{
					EGarnishType GarnishType = LevelConstraintData->AvailableGarnishes[RandIndex];
					// If our map of garnishes to order doesn't contain this garnish, add it
					if (!GarnishesToOrder.Contains(GarnishType))
					{
						GarnishesToOrder.Add(GarnishType, 1); // Only allow 1 for now
						NumGarnishesAdded++;
					}
				}
			}
		}
	}

	return GarnishesToOrder;
}

EMixMethod AODAIController::PickRandomMixMethod()
{
	if (LevelConstraintsTable)
	{
		// Look up the constraints for our current level
		FLevelConstraintData* LevelConstraintData = LevelConstraintsTable->FindRow<FLevelConstraintData>(FName(UGameplayStatics::GetCurrentLevelName(GetWorld())), FString(""));
		if (LevelConstraintData)
		{
			int32 RandMixIndex = FMath::RandRange(0, LevelConstraintData->AvailableMixMethods.Num() - 1);
			if (LevelConstraintData->AvailableMixMethods.IsValidIndex(RandMixIndex))
			{
				return LevelConstraintData->AvailableMixMethods[RandMixIndex];
			}
		}
	}

	return EMixMethod::MM_None;
}

void AODAIController::StartConversation()
{
	if (AICharacter)
	{
		AICharacter->SetInConversation(true);
		SetBoolBlackboardKey("IsInConversation", true);

		GetWorldTimerManager().ClearTimer(ConversationTimerHandle);
		GetWorldTimerManager().SetTimer(ConversationTimerHandle, this, &AODAIController::FinishConversation, FMath::RandRange(1, 3) * 3.f, false);
	}
}

void AODAIController::FinishConversation()
{
	if (AICharacter)
	{
		AICharacter->SetInConversation(false);
		SetBoolBlackboardKey("IsInConversation", false);
	}
}

void AODAIController::SetCurrentAILocation(AODAILocation* NewLocation)
{
	CurrentAILocation = NewLocation;
}

AODAILocation* AODAIController::GetCurrentAILocation()
{
	return CurrentAILocation;
}

void AODAIController::SetFillerStatus(bool val)
{
	bIsFiller = val;

	if (AICharacter && !bIsFiller && ActiveBehaviorTree)
	{
		BrainComponent->StopLogic("Switching Tree");
		RunBehaviorTree(ActiveBehaviorTree);
	}
	else if (AICharacter && bIsFiller && FillerBehaviorTree)
	{
		BrainComponent->StopLogic("Switching Tree");
		RunBehaviorTree(FillerBehaviorTree);
	}
}

bool AODAIController::IsFiller()
{
	return bIsFiller;
}

void AODAIController::HandleTaserLogic()
{
	BrainComponent->StopLogic("Taser");

	// Let the manager know we need to be cleaned up
	if (AIManager)
	{
		AIManager->NotifyOfTaser(AICharacter);
	}
}

void AODAIController::StartAttack()
{
	if (AICharacter)
	{
		AICharacter->PlayAttackAnim();
		

	}
}

void AODAIController::StartTalk()
{
	if (AICharacter)
	{
		AICharacter->PlayTalkAnim();
	}
}
