// Copyright 2020 Space Gurus


#include "ODLobbyGameState.h"
#include "Net/UnrealNetwork.h"
#include "ODMenuPlayerController.h"
#include "ODMenuGameMode.h"
#include "Actors/ODLobbyManager.h"
#include "Actors/ODLobbyPlayer.h"
#include "ODPlayerState.h"
#include "Kismet/GameplayStatics.h"

AODLobbyGameState::AODLobbyGameState()
{
	bReplicates = true;
}

void AODLobbyGameState::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AODLobbyGameState, NumPlayers);
	DOREPLIFETIME(AODLobbyGameState, NumReadyPlayers);
	DOREPLIFETIME(AODLobbyGameState, ChosenMapName);
	DOREPLIFETIME(AODLobbyGameState, ReadyStatuses);
}

void AODLobbyGameState::BeginPlay()
{
	Super::BeginPlay();

	for (int32 i = 0; i < 4; i++)
	{
		ReadyStatuses.Add(false);
	}
}

void AODLobbyGameState::SetNumPlayers(int32 NumOfPlayers)
{
	NumPlayers = NumOfPlayers;
}

int32 AODLobbyGameState::GetNumPlayers()
{
	return NumPlayers;
}

void AODLobbyGameState::SetNumReadyPlayers()
{

}

int32 AODLobbyGameState::GetNumReadyPlayers()
{
	return NumReadyPlayers;
}

bool AODLobbyGameState::AreAllPlayersReady()
{
	return NumPlayers == NumReadyPlayers;
}

void AODLobbyGameState::SetChosenMapName(FName MapName)
{
	ChosenMapName = MapName;

	AODMenuGameMode* MenuGameMode = Cast<AODMenuGameMode>(UGameplayStatics::GetGameMode(GetWorld()));
	if (MenuGameMode)
	{
		MenuGameMode->NotifyPlayersOfNewMapName(MapName);
	}
}

void AODLobbyGameState::Server_SetPlayerReadyStatus_Implementation(APlayerController* PlayerController)
{
	AODMenuPlayerController* MenuPC = Cast<AODMenuPlayerController>(PlayerController);
	if (MenuPC)
	{
		AODPlayerState* PlayerState = Cast<AODPlayerState>(PlayerController->PlayerState);
		if (PlayerState)
		{
			int32 PlayerIndx = PlayerState->GetPlayerNum();
			if (ReadyStatuses.IsValidIndex(PlayerIndx - 1))
			{
				ReadyStatuses[PlayerIndx - 1] = !ReadyStatuses[PlayerIndx - 1];
			}

			// Update it in the UI for everyone
			AODMenuGameMode* MenuGameMode = Cast<AODMenuGameMode>(UGameplayStatics::GetGameMode(GetWorld()));
			if (MenuGameMode)
			{
				MenuGameMode->NotifyPlayersOfReadyStatus(PlayerIndx, ReadyStatuses[PlayerIndx - 1]);
			}
		}

	}

	// Update our number of ready players
	int32 ReadyCounter = 0;
	
	for (bool ReadyStatus : ReadyStatuses)
	{
		if (ReadyStatus)
		{
			ReadyCounter++;
		}
	}

	NumReadyPlayers = ReadyCounter;
}

void AODLobbyGameState::Server_AddPlayer_Implementation(AODMenuPlayerController* MenuPlayerController, int32 PlayerIndex)
{
	if (ReadyStatuses.IsValidIndex(MenuPlayerController->GetPlayerIndex() - 1))
	{
		ReadyStatuses[MenuPlayerController->GetPlayerIndex() - 1] = false;
	}
}

void AODLobbyGameState::Server_RemovePlayer_Implementation(int32 PlayerIndex)
{
	if (ReadyStatuses.IsValidIndex(PlayerIndex - 1))
	{
		ReadyStatuses[PlayerIndex - 1] = false;
	}
}

void AODLobbyGameState::SetLobbyManagerReference(AODLobbyManager* NewManager)
{
	if (NewManager)
	{
		LobbyManager = NewManager;
	}
}

FName AODLobbyGameState::GetChosenMapName()
{
	return ChosenMapName;
}

bool AODLobbyGameState::GetPlayerReadyStatus(int32 PlayerIndex)
{
	// Player state's index is not zero indexed
	if (ReadyStatuses.IsValidIndex(PlayerIndex - 1))
	{
		return ReadyStatuses[PlayerIndex - 1];
	}

	return false;
}

void AODLobbyGameState::OnRep_NumPlayers()
{

}

void AODLobbyGameState::OnRep_NumReadyPlayers()
{

}

void AODLobbyGameState::OnRep_ChosenMapName()
{
	
}
