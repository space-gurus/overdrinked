// Copyright 2020 Space Gurus


#include "UI/ODMainMenuWidget.h"
#include "Components/WidgetSwitcher.h"
#include "Components/Button.h"

void UODMainMenuWidget::LoadMainMenu()
{
	if (MainMenuSwitcher && MainMenuWidget)
	{
		MainMenuSwitcher->SetActiveWidget(MainMenuWidget);
		UpdateVisibleButtons();
	}
}

void UODMainMenuWidget::LoadHostMenu()
{
	if (MainMenuSwitcher && HostMenuWidget)
	{
		MainMenuSwitcher->SetActiveWidget(HostMenuWidget);
		UpdateVisibleButtons();
	}
}

void UODMainMenuWidget::LoadJoinMenu()
{
	if (MainMenuSwitcher && JoinMenuWidget)
	{
		MainMenuSwitcher->SetActiveWidget(JoinMenuWidget);
		UpdateVisibleButtons();
	}
	
}

void UODMainMenuWidget::UpdateVisibleButtons()
{
	if (MainMenuSwitcher)
	{
		// Clear them first
		VisibleButtons.Empty();

		// Populate with all buttons in the current visible widget
		UPanelWidget* PanelWidget = Cast<UPanelWidget>(MainMenuSwitcher->GetActiveWidget());
		if (PanelWidget)
		{
			for (UWidget* ChildWidget : PanelWidget->GetAllChildren())
			{
				UButton* PossibleButton = Cast<UButton>(ChildWidget);
				if (PossibleButton)
				{
					// This widget is a button so add it to visible
					VisibleButtons.Add(PossibleButton);
				}
			}
		}
	}
}

void UODMainMenuWidget::StartNavigatingWithController()
{
	// Probably the first time so we need to update our buttons first
	UpdateVisibleButtons();

	if (VisibleButtons.Num() > 0)
	{
		ButtonHighlightIndex = 0;
		HighlightCurrentButtonForControllerSelection();
	}
}

void UODMainMenuWidget::MoveMenuSelectionUp()
{
	if (VisibleButtons.Num() > 0)
	{
		ButtonHighlightIndex++;
		if (ButtonHighlightIndex > (VisibleButtons.Num() - 1))
		{
			ButtonHighlightIndex = 0;
		}
	}
	HighlightCurrentButtonForControllerSelection();
}

void UODMainMenuWidget::MoveMenuSelectionDown()
{
	if (VisibleButtons.Num() > 0)
	{
		ButtonHighlightIndex--;
		if (ButtonHighlightIndex < 0)
		{
			ButtonHighlightIndex = VisibleButtons.Num() - 1;
		}
	}
	HighlightCurrentButtonForControllerSelection();
}

void UODMainMenuWidget::HighlightCurrentButtonForControllerSelection()
{
	if (VisibleButtons.IsValidIndex(ButtonHighlightIndex))
	{
		VisibleButtons[ButtonHighlightIndex]->SetBackgroundColor(FLinearColor(0.352941f, 1.f, 0.960784f, 1.f));
		VisibleButtons[ButtonHighlightIndex]->SetKeyboardFocus();
	}
}
