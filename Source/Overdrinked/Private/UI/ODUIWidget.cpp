// Copyright 2020 Space Gurus


#include "UI/ODUIWidget.h"

void UODUIWidget::SetupWidget()
{
	Super::SetupWidget();

	this->AddToViewport();

	APlayerController* playerController = GetWorld()->GetFirstPlayerController();
	if (playerController)
	{
		FInputModeGameOnly InputModeData;
		InputModeData.SetConsumeCaptureMouseDown(true);
		playerController->SetInputMode(InputModeData);
		playerController->bShowMouseCursor = false;
	}
}

void UODUIWidget::TearDownWidget()
{
	Super::TearDownWidget();

	this->RemoveFromViewport();
	APlayerController* playerController = GetWorld()->GetFirstPlayerController();

	FInputModeGameOnly InputModeData;
	if (playerController)
	{
		playerController->SetInputMode(InputModeData);
		playerController->bShowMouseCursor = false;
	}
}
