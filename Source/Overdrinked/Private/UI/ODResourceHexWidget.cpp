// Copyright 2020 Space Gurus


#include "UI/ODResourceHexWidget.h"

EGarnishType UODResourceHexWidget::GetGarnishType()
{
	return GarnishType;
}

EIngredientType UODResourceHexWidget::GetIngredientType()
{
	return IngredientType;
}

bool UODResourceHexWidget::IsBeingUsed()
{
	return bIsBeingUsed;
}

void UODResourceHexWidget::SetIsBeingUsed(bool val)
{
	bIsBeingUsed = val;
}

void UODResourceHexWidget::HideAndReset()
{
	SetIsBeingUsed(false);
	GarnishType = EGarnishType::GT_None;
	IngredientType = EIngredientType::IT_None;
}
