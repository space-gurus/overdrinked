// Copyright 2020 Space Gurus


#include "UI/ODMenuWidget.h"

void UODMenuWidget::SetupWidget()
{
	Super::SetupWidget();

	this->AddToViewport();

	APlayerController* playerController = GetWorld()->GetFirstPlayerController();
	if (playerController)
	{
		UE_LOG(LogTemp, Warning, TEXT("SetupWidget"));
		FInputModeGameAndUI InputModeData;
		InputModeData.SetWidgetToFocus(this->TakeWidget());
		InputModeData.SetLockMouseToViewportBehavior(EMouseLockMode::DoNotLock);
		playerController->SetInputMode(InputModeData);
		playerController->bShowMouseCursor = true;
	}
}

void UODMenuWidget::TearDownWidget()
{
	Super::TearDownWidget();

	this->RemoveFromViewport();
	APlayerController* playerController = GetWorld()->GetFirstPlayerController();

	FInputModeGameOnly InputModeData;
	if (playerController)
	{
		playerController->SetInputMode(InputModeData);
		playerController->bShowMouseCursor = false;
	}
}
