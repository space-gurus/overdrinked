// Copyright 2020 Space Gurus


#include "UI/ODPourHUD.h"
#include "Engine/DataTable.h"
#include "Components/Image.h"
#include "Components/TextBlock.h"
#include "Data/IngredientData.h"
#include "ODEnumHelperFunctions.h"

UODPourHUD::UODPourHUD(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	ConstructorHelpers::FObjectFinder<UDataTable> IngredientDataObject(TEXT("/Game/DrinkGame/Data/DT_Ingredients"));
	if (IngredientDataObject.Succeeded())
	{
		IngredientData = IngredientDataObject.Object;
	}
}

void UODPourHUD::SetIngredients(TArray<EIngredientType> Ingredients)
{
	UE_LOG(LogTemp, Warning, TEXT("SetIngredients()"));
	// Setup only if it's the first time
	if (IngredientImages.Num() == 0)
	{
		IngredientImages.Add(IngredientOne);
		IngredientImageToText.Add(IngredientOne, IngredientOneText);
		IngredientImages.Add(IngredientTwo);
		IngredientImageToText.Add(IngredientTwo, IngredientTwoText);
		IngredientImages.Add(IngredientThree);
		IngredientImageToText.Add(IngredientThree, IngredientThreeText);
		IngredientImages.Add(IngredientFour);
		IngredientImageToText.Add(IngredientFour, IngredientFourText);
		
		for (int32 IngredientIndex = 0; IngredientIndex < IngredientImages.Num(); ++IngredientIndex)
		{
			if (Ingredients.IsValidIndex(IngredientIndex))
			{
				// It exists on the dispenser so it should have an icon/text
				// Setup dynamic material instance
				UMaterialInterface* IngredientMaterialInterface = Cast<UMaterialInterface>(IngredientImages[IngredientIndex]->Brush.GetResourceObject());
				if (IngredientMaterialInterface)
				{
					UMaterialInstanceDynamic* DynamicIngredientMaterial = UMaterialInstanceDynamic::Create(IngredientMaterialInterface, this);
					if (DynamicIngredientMaterial)
					{
						// Set the reference
						DynamicIngredientMaterials.Add(DynamicIngredientMaterial);
						IngredientImages[IngredientIndex]->Brush.SetResourceObject(DynamicIngredientMaterial);
						IngredientImages[IngredientIndex]->SetVisibility(ESlateVisibility::Visible);

						// Find the image we need to set
						FIngredientData* IngredientRow = IngredientData->FindRow<FIngredientData>(FName(UODEnumHelperFunctions::IngredientEnumToString(Ingredients[IngredientIndex])), FString(""));
						if (IngredientRow)
						{
							DynamicIngredientMaterial->SetTextureParameterValue(TEXT("TextureParameter"), IngredientRow->Icon);
						}

						// Now set the text
						IngredientImageToText[IngredientImages[IngredientIndex]]->SetText(FText::FromName(UODEnumHelperFunctions::IngredientEnumToString(Ingredients[IngredientIndex])));
					}
				}
			}
			else
			{
				// Should be blank
				IngredientImages[IngredientIndex]->SetVisibility(ESlateVisibility::Hidden);
				IngredientImageToText[IngredientImages[IngredientIndex]]->SetText(FText::FromString(""));
			}
		}
	}
	else
	{
		// Otherwise just adjust everything
		for (int32 IngredientIndex = 0; IngredientIndex < IngredientImages.Num(); ++IngredientIndex)
		{
			if (Ingredients.IsValidIndex(IngredientIndex))
			{
				// It exists on the dispenser so it should have an icon/text
				
				// If we haven't made it before, make one and save the reference
				if (!DynamicIngredientMaterials.IsValidIndex(IngredientIndex))
				{
					UMaterialInterface* IngredientMaterialInterface = Cast<UMaterialInterface>(IngredientImages[IngredientIndex]->Brush.GetResourceObject());
					if (IngredientMaterialInterface)
					{
						UMaterialInstanceDynamic* DynamicIngredientMaterial = UMaterialInstanceDynamic::Create(IngredientMaterialInterface, this);
						if (DynamicIngredientMaterial)
						{
							// Set the reference
							DynamicIngredientMaterials.Add(DynamicIngredientMaterial);
							IngredientImages[IngredientIndex]->Brush.SetResourceObject(DynamicIngredientMaterial);
						}
					}
				}
				// Find the image we need to set
				FIngredientData* IngredientRow = IngredientData->FindRow<FIngredientData>(FName(UODEnumHelperFunctions::IngredientEnumToString(Ingredients[IngredientIndex])), FString(""));
				if (IngredientRow)
				{
					DynamicIngredientMaterials[IngredientIndex]->SetTextureParameterValue(TEXT("TextureParameter"), IngredientRow->Icon);
				}
				IngredientImages[IngredientIndex]->SetVisibility(ESlateVisibility::Visible);

				// Now set the text
				IngredientImageToText[IngredientImages[IngredientIndex]]->SetText(FText::FromName(UODEnumHelperFunctions::IngredientEnumToString(Ingredients[IngredientIndex])));
			}
			else
			{
				// Should be blank
				IngredientImages[IngredientIndex]->SetVisibility(ESlateVisibility::Hidden);
				IngredientImageToText[IngredientImages[IngredientIndex]]->SetText(FText::FromString(""));
			}
		}
	}

}
