// Copyright 2020 Space Gurus


#include "UI/ODUserWidget.h"

void UODUserWidget::SetupWidget()
{
	bIsWidgetActive = true;
}

void UODUserWidget::TearDownWidget()
{
	bIsWidgetActive = false;
}

bool UODUserWidget::IsWidgetActive()
{
	return bIsWidgetActive;
}
