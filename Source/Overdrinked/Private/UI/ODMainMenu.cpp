// Copyright 2020 Space Gurus


#include "UI/ODMainMenu.h"
#include "UI/ODLobbyPage.h"

UODLobbyPage* UODMainMenu::GetHostLobby()
{
	return HostLobbyPage;
}

UODLobbyPage* UODMainMenu::GetClientLobby()
{
	return ClientLobbyPage;
}

UODLobbyPage* UODMainMenu::GetActiveLobbyPage()
{
	if (HostLobbyPage)
	{
		return HostLobbyPage;
	}

	if (ClientLobbyPage)
	{
		return ClientLobbyPage;
	}

	return NULL;
}
