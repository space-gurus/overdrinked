// Copyright 2020 Space Gurus


#include "UI/ODMixerWidget.h"
#include "Materials/MaterialInterface.h"
#include "Materials/MaterialInstanceDynamic.h"
#include "Engine/DataTable.h"
#include "Components/Image.h"
#include "Styling/SlateColor.h"

#include "Actors/ODBevContainer.h"
#include "Gameplay/ODIngredient.h"
#include "Gameplay/ODDrink.h"
#include "Gameplay/ODContainerContents.h"
#include "Data/DrinkData.h"
#include "Data/IngredientData.h"
#include "ODEnumHelperFunctions.h"

UODMixerWidget::UODMixerWidget(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	ConstructorHelpers::FObjectFinder<UDataTable> DrinkDataObject(TEXT("/Game/DrinkGame/Data/DT_Drinks"));
	if (DrinkDataObject.Succeeded())
	{
		DrinkDataTable = DrinkDataObject.Object;
	}

	ConstructorHelpers::FObjectFinder<UDataTable> IngredientDataObject(TEXT("/Game/DrinkGame/Data/DT_Ingredients"));
	if (IngredientDataObject.Succeeded())
	{
		IngredientDataTable = IngredientDataObject.Object;
	}
}

void UODMixerWidget::InitialSetup(AODBevContainer* DrinkMixer)
{
	// Keep a reference to the mixer so variables can be automatically updated
// 	DrinkMixer->SetWidgetReference(this);
// 	DrinkContainerReference = DrinkMixer;

	SetAlignmentInViewport(FVector2D(0.5, 0.5));

	// Setup calculations for size
	MixerMaxVolume = DrinkMixer->GetMaxVolume();
	CurrentMaterialIndex = 1;

	// Save the reference of this mixer material
	MixerMaterialInterface = Cast<UMaterialInterface>(MixerImage->Brush.GetResourceObject());
	ResetMixerMaterial();

	// Set the overlay image
	SetGlassOverlayImage(DrinkContainerReference->GetGlassType());

	// Get initial values
	AdjustFill();

	// Show what you got
	SetVisibility(ESlateVisibility::Visible);
}

void UODMixerWidget::AdjustFill()
{
	if (DrinkContainerReference->GetCurrentVolume() == 0.f)
	{
		ClearIngredients();
	}
	else
	{
		TArray<EIngredientType> IngredientTypes = DrinkContainerReference->GetContainerContents().IngredientTypes;
		TArray<float> IngredientAmounts = DrinkContainerReference->GetContainerContents().IngredientAmounts;

		if (IngredientTypes.Num() == IngredientAmounts.Num())
		{
			for (int32 IngIndex = 0; IngIndex < IngredientTypes.Num(); ++IngIndex)
			{
				EIngredientType IngredientType = IngredientTypes[IngIndex];
				float Amount = IngredientAmounts[IngIndex];

				if (CurrentMaterialIndex <= 5)
				{
					if (IngredientToMaterialIndex.Contains(IngredientType))
					{
						// Build parameter strings based on saved index reference
						FString IngredientColorString = "";
						IngredientColorString.Append(BaseIngredientString);
						IngredientColorString.Append(FString::FromInt(IngredientToMaterialIndex[IngredientType]));
						IngredientColorString.Append(ColorString);
						FString IngredientLevelString = "";
						IngredientLevelString.Append(BaseIngredientString);
						IngredientLevelString.Append(FString::FromInt(IngredientToMaterialIndex[IngredientType]));
						IngredientLevelString.Append(LevelString);

						// Calculate our level
						float IngredientLevel = Amount / MixerMaxVolume;

						// Set necessary parameters in the material
						if (DynamicMixerMaterial)
						{
							// Only have to update the level
							DynamicMixerMaterial->SetScalarParameterValue(FName(IngredientLevelString), IngredientLevel);
						}
					}
					else
					{
						// It's a new ingredient for this mixer
						// Build parameter strings
						FString IngredientColorString = "";
						IngredientColorString.Append(BaseIngredientString);
						IngredientColorString.Append(FString::FromInt(CurrentMaterialIndex));
						IngredientColorString.Append(ColorString);
						FString IngredientLevelString = "";
						IngredientLevelString.Append(BaseIngredientString);
						IngredientLevelString.Append(FString::FromInt(CurrentMaterialIndex));
						IngredientLevelString.Append(LevelString);

						// Calculate our level
						float IngredientLevel = Amount / MixerMaxVolume;

						// Set necessary parameters in the material
						if (DynamicMixerMaterial)
						{
							// Get color from data table
							if (IngredientDataTable)
							{
								FIngredientData* IngredientData = IngredientDataTable->FindRow<FIngredientData>(UODEnumHelperFunctions::IngredientEnumToString(IngredientType), FString(""));
								if (IngredientData)
								{
									DynamicMixerMaterial->SetVectorParameterValue(FName(IngredientColorString), IngredientData->Color.ReinterpretAsLinear());
								}
							}
							DynamicMixerMaterial->SetScalarParameterValue(FName(IngredientLevelString), IngredientLevel);
						}

						//PrintVectorParameter(DynamicMixerMaterial, FName(IngredientColorString));

						// Keep the reference and move our index forward
						IngredientToMaterialIndex.Add(IngredientType, CurrentMaterialIndex);
						++CurrentMaterialIndex;
					}
				}
			}
		}
	}
}

void UODMixerWidget::ClearIngredients()
{
	// Zero out the material
	for (int MatIndex = 1; MatIndex <= 5; ++MatIndex)
	{
		FString IngredientLevelString = "";
		IngredientLevelString.Append(BaseIngredientString);
		IngredientLevelString.Append(FString::FromInt(MatIndex));
		IngredientLevelString.Append(LevelString);

		if (DynamicMixerMaterial)
		{
			FName NameVersion = FName(IngredientLevelString);
			DynamicMixerMaterial->SetScalarParameterValue(FName(IngredientLevelString), 0.f);
		}
	}

	// Clear and reset necessary variables
	CurrentMaterialIndex = 1;
	IngredientToMaterialIndex.Empty();
	ResetMixerMaterial();
}

void UODMixerWidget::DisplayFinishedDrink()
{
	// Set the color of our liquid
	// TODO: Make this less janky
	if (CurrentDrinkReference)
	{
		// Find the data for the current drink
		if (DrinkDataTable)
		{
			FDrinkData* DrinkData = DrinkDataTable->FindRow<FDrinkData>(FName(CurrentDrinkReference->GetDrinkName()), FString(""));
			if (DrinkData)
			{
				UMaterialInstance* DrinkMaterialInterface = DrinkData->Aesthetic2D.MaterialInstance;
				if (DrinkMaterialInterface)
				{
					// Setup our dynamic material for the mixer
					UMaterialInstanceDynamic* DynamicDrinkMaterial = UMaterialInstanceDynamic::Create(DrinkMaterialInterface, this);
					if (DynamicDrinkMaterial)
					{
						// Set the properties
						DynamicDrinkMaterial->SetVectorParameterValue(TEXT("DrinkColor"), DrinkData->BaseColor.ReinterpretAsLinear());
						DynamicDrinkMaterial->SetVectorParameterValue(TEXT("FoamColor"), DrinkData->Aesthetic2D.FoamColor.ReinterpretAsLinear());
						DynamicDrinkMaterial->SetScalarParameterValue(TEXT("FoamPercent"), DrinkData->Aesthetic2D.FoamPercent);
						float FillLevel = DrinkContainerReference->GetCurrentVolume() / DrinkContainerReference->GetMaxVolume();
						DynamicDrinkMaterial->SetScalarParameterValue(TEXT("FillLevel"), FillLevel);
						
						SetMaterialShape(DynamicDrinkMaterial);

						// Set the image
						DynamicMixerMaterial = DynamicDrinkMaterial;
						MixerImage->Brush.SetResourceObject(DynamicMixerMaterial);
					}
				}
			}
		}
	}
	
}

void UODMixerWidget::ResetMixerMaterial()
{
	// Initialize Material Instance
	if (MixerMaterialInterface)
	{
		// Setup our dynamic material for the mixer
		DynamicMixerMaterial = UMaterialInstanceDynamic::Create(MixerMaterialInterface, this);
		if (DynamicMixerMaterial)
		{
			MixerImage->Brush.SetResourceObject(DynamicMixerMaterial);
			DynamicMixerMaterial->ClearParameterValues();

			// Set the shape based on the type of glass
			SetMaterialShape(DynamicMixerMaterial);
		}
	}

	// Zero out the material
	for (int MatIndex = 1; MatIndex <= 5; ++MatIndex)
	{
		FString IngredientLevelString = "";
		IngredientLevelString.Append(BaseIngredientString);
		IngredientLevelString.Append(FString::FromInt(MatIndex));
		IngredientLevelString.Append(LevelString);

		if (DynamicMixerMaterial)
		{
			FName NameVersion = FName(IngredientLevelString);
			DynamicMixerMaterial->SetScalarParameterValue(FName(IngredientLevelString), 0.f);
		}
	}
}

void UODMixerWidget::SetMaterialShape(UMaterialInstanceDynamic* MaterialToSet)
{
	switch (DrinkContainerReference->GetGlassType())
	{
	case EGlassType::GT_Brew:
		MaterialToSet->SetScalarParameterValue(TEXT("DrinkIndex"), 1.0f);
		break;
	case EGlassType::GT_Mixer:
		MaterialToSet->SetScalarParameterValue(TEXT("DrinkIndex"), 3.0f);
		break;
	case EGlassType::GT_Shot:
		MaterialToSet->SetScalarParameterValue(TEXT("DrinkIndex"), 0.0f);
		break;
	case EGlassType::GT_Stemmed:
		MaterialToSet->SetScalarParameterValue(TEXT("DrinkIndex"), 2.0f);
		break;
	}
}
