// Copyright 2020 Space Gurus


#include "UI/ODSliderQTE.h"
#include "Components/ProgressBar.h"
#include "Environment/ODQTEStation.h"

void UODSliderQTE::NativeTick(const FGeometry& MyGeometry, float InDeltaTime)
{
	Super::NativeTick(MyGeometry, InDeltaTime);

	if (bIsQTEActive)
	{
		if (bIsProgressBarFinished)
		{
			float NewProgressPercent = FMath::FInterpConstantTo(Progress, 0.f, InDeltaTime, QTESpeed);
			SetProgress(NewProgressPercent);
			Progress = NewProgressPercent;
			
			if (Progress <= 0.001f)
			{
				bIsProgressBarFinished = false;
			}
		}
		else
		{
			float NewProgressPercent = FMath::FInterpConstantTo(Progress, 1.f, InDeltaTime, QTESpeed);
			SetProgress(NewProgressPercent);
			Progress = NewProgressPercent;

			if (Progress >= 0.99f)
			{
				bIsProgressBarFinished = true;
			}
		}
	}
}

void UODSliderQTE::InteractWithQTE()
{
	Super::InteractWithQTE();

	TryPlayerHit();
}

void UODSliderQTE::ActivateQTE(AODQTEStation* NewQTEStation)
{
	Super::ActivateQTE(NewQTEStation);
}

void UODSliderQTE::DeactivateQTE()
{
	Super::DeactivateQTE();

	SetProgress(0.f);
	PlayerSuccessCount = 0;
	QTESpeed = 1.f;
}

void UODSliderQTE::TryPlayerHit()
{
	if (IsInSuccessRange())
	{
		PlayerSuccessCount++;
		QTESpeed += 0.5f;

		if (PlayerSuccessCount == SuccessesNeeded)
		{
			bPlayerSuccess = true;
			DeactivateQTE();
		}
	}
}

bool UODSliderQTE::IsInSuccessRange()
{
	return (Progress >= SuccessRangeStart) && (Progress <= (SuccessRangeStart + SuccessRangeSize));
}

bool UODSliderQTE::IsQTEActive()
{
	return bIsQTEActive;
}

int32 UODSliderQTE::GetPlayerSuccessCount()
{
	return PlayerSuccessCount;
}
