// Copyright 2020 Space Gurus


#include "UI/ODPourHUDSimple.h"
#include "Materials/MaterialInstanceDynamic.h"
#include "Components/Image.h"
#include "Styling/SlateColor.h"

void UODPourHUDSimple::InitialSetup(TArray<FColor> IngredientColors)
{
	// Setup image references
	IngredientImages.Add(IngredientOne);
	IngredientImages.Add(IngredientTwo);
	IngredientImages.Add(IngredientThree);
	IngredientImages.Add(IngredientFour);

	for (int32 i = 0; i < IngredientImages.Num(); ++i)
	{
		if (IngredientColors.IsValidIndex(i))
		{
			// Setup dynamic material instance
			UMaterialInterface* IngredientMaterialInterface = Cast<UMaterialInterface>(IngredientImages[i]->Brush.GetResourceObject());
			if (IngredientMaterialInterface)
			{
				UMaterialInstanceDynamic* DynamicIngredientMaterial = UMaterialInstanceDynamic::Create(IngredientMaterialInterface, this);
				if (DynamicIngredientMaterial)
				{
					IngredientImages[i]->Brush.SetResourceObject(DynamicIngredientMaterial);
					// Set the color
					DynamicIngredientMaterial->SetVectorParameterValue(TEXT("IngredientColor"), IngredientColors[i].ReinterpretAsLinear());
				}
			}
		}
		else
		{
			// Set blank
			IngredientImages[i]->SetVisibility(ESlateVisibility::Hidden);
		}
	}
}

bool UODPourHUDSimple::IsWidgetOpen()
{
	return bIsWidgetOpen;
}
