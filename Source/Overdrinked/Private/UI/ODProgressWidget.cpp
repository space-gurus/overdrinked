// Copyright 2020 Space Gurus


#include "UI/ODProgressWidget.h"
#include "Components/ProgressBar.h"

void UODProgressWidget::SetProgress(float val)
{
	Progress->SetPercent(FMath::Clamp(val, 0.f, 1.f));
}
