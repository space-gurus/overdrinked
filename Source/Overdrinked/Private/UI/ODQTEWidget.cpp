// Copyright 2020 Space Gurus


#include "UI/ODQTEWidget.h"
#include "Environment/ODQTEStation.h"

void UODQTEWidget::NativeTick(const FGeometry& MyGeometry, float InDeltaTime)
{
	Super::NativeTick(MyGeometry, InDeltaTime);
}

void UODQTEWidget::InteractWithQTE()
{

}

void UODQTEWidget::ActivateQTE(AODQTEStation* NewQTEStation)
{
	QTEStation = NewQTEStation;
	bIsQTEActive = true;
}

void UODQTEWidget::DeactivateQTE()
{
	bIsQTEActive = false;

	if (QTEStation)
	{
		QTEStation->HandleQTEFinish(bPlayerSuccess);
	}

	bPlayerSuccess = false;
}
