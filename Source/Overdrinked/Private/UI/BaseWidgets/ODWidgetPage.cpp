// Copyright 2020 Space Gurus


#include "UI/BaseWidgets/ODWidgetPage.h"
#include "Components/CanvasPanel.h"
#include "UI/BaseWidgets/ODButton.h"
#include "ISwitcherooModule.h"
#include "SwitcherooTypes.h"
#include "Blueprint/WidgetBlueprintLibrary.h"

void UODWidgetPage::NativeOnInitialized()
{
	Super::NativeOnInitialized();

	TArray<UUserWidget*> FoundButtons;
	UWidgetBlueprintLibrary::GetAllWidgetsOfClass(GetWorld(), FoundButtons, UODButton::StaticClass(), false);
	for (UWidget* Widget : FoundButtons)
	{
		UODButton* ODButton = Cast<UODButton>(Widget);
		if (ODButton)
		{
			ButtonList.Add(ODButton);
			ODButton->SetParentPageReference(this);

			if (ODButton->IsStartingFocus())
			{
				StartingFocusButton = ODButton;

				// Only set active focus if we're using a controller
				if (ISwitcherooModule* Switcheroo = ISwitcherooModule::Get())
				{
					if (Switcheroo->GetDetectedInputDevice() == ESwitcherooInputDevice::Gamepad)
					{
						StartingFocusButton->SetActiveFocus();
					}
				}
			}
		}
	}

	if (StartingFocusButton)
	{
		FocusButtonIndex = ButtonList.IndexOfByKey(StartingFocusButton);
	}
}

void UODWidgetPage::BecomeActive(bool bTriggerShowPage)
{
	if (bTriggerShowPage)
	{
		ShowPage();
	}

	// Only set active focus if we're using a controller
	if (ISwitcherooModule* Switcheroo = ISwitcherooModule::Get())
	{
		if ((Switcheroo->GetDetectedInputDevice() == ESwitcherooInputDevice::Gamepad) && StartingFocusButton)
		{
			StartingFocusButton->SetActiveFocus();
		}
	}
}

void UODWidgetPage::MoveSelectionForward()
{
	if (FocusButtonIndex < ButtonList.Num() - 1)
	{
		FocusButtonIndex++;
		ButtonList[FocusButtonIndex]->SetActiveFocus();
		ClearUnfocusedButtons();
	}
	else
	{
		ButtonList[FocusButtonIndex]->SetActiveFocus();
		ClearUnfocusedButtons();
	}
}

void UODWidgetPage::MoveSelectionBackward()
{
	if (FocusButtonIndex > 0)
	{
		FocusButtonIndex--;
		ButtonList[FocusButtonIndex]->SetActiveFocus();
		ClearUnfocusedButtons();
	}
	else
	{
		ButtonList[FocusButtonIndex]->SetActiveFocus();
		ClearUnfocusedButtons();
	}
}

void UODWidgetPage::HandleDeviceChanged(ESwitcherooInputDevice ActiveDevice)
{
	switch (ActiveDevice)
	{
		case ESwitcherooInputDevice::Gamepad:
			if (StartingFocusButton)
			{
				if (ButtonList.IsValidIndex(FocusButtonIndex))
				{
					ButtonList[FocusButtonIndex]->SetActiveFocus();
				}
				ClearUnfocusedButtons();
			}
			break;
		case ESwitcherooInputDevice::KeyboardMouse:
			ClearAllFocus();
			break;
	}
}

void UODWidgetPage::SetFocusedButton(UODButton* NewFocus)
{
	FocusButtonIndex = ButtonList.IndexOfByKey(NewFocus);
}

bool UODWidgetPage::HasNestedSwitcherMenu()
{
	return NestedSwitcherMenu ? true : false;
}

UODSwitcherMenu* UODWidgetPage::GetNestedSwitcherMenu()
{
	return NestedSwitcherMenu;
}

void UODWidgetPage::ClearUnfocusedButtons()
{
	for (int32 ButtonIdx = 0; ButtonIdx < ButtonList.Num(); ++ButtonIdx)
	{
		if (ButtonIdx != FocusButtonIndex)
		{
			ButtonList[ButtonIdx]->LoseActiveFocus();
		}
	}
}

void UODWidgetPage::ClearAllFocus()
{
	for (UODButton* ODButton : ButtonList)
	{
		ODButton->LoseActiveFocus();
	}
}
