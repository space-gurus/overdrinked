// Copyright 2020 Space Gurus


#include "UI/BaseWidgets/ODButton.h"

void UODButton::NativePreConstruct()
{
	Super::NativePreConstruct();

	bIsFocusable = true;
}

void UODButton::SetParentPageReference(UODWidgetPage* NewParent)
{
	ParentPage = NewParent;
}

void UODButton::SetActiveFocus()
{
// 	bIsHovered = true;
// 	bIsFocused = true;
	SetFocus();
	SetKeyboardFocus();
	
	//SetUserFocus(GetOwningPlayer());
}

void UODButton::LoseActiveFocus()
{
	bIsHovered = false;
	bIsFocused = false;
}

bool UODButton::IsStartingFocus()
{
	return bIsStartingFocus;
}

