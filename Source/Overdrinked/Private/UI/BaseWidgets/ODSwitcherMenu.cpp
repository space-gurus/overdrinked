// Copyright 2020 Space Gurus


#include "UI/BaseWidgets/ODSwitcherMenu.h"
#include "Components/WidgetSwitcher.h"
#include "UI/BaseWidgets/ODWidgetPage.h"
#include "UI/BaseWidgets/ODTabButton.h"
#include "Blueprint/WidgetBlueprintLibrary.h"

void UODSwitcherMenu::NativeOnInitialized()
{
	Super::NativeOnInitialized();

	TArray<UUserWidget*> FoundTabs;
	UWidgetBlueprintLibrary::GetAllWidgetsOfClass(GetWorld(), FoundTabs, UODTabButton::StaticClass(), false);
	for (UWidget* Widget : FoundTabs)
	{
		UODTabButton* ODTab = Cast<UODTabButton>(Widget);
		if (ODTab)
		{
			SwitcherTabs.Add(ODTab);
			if (ODTab->IsStartingTab())
			{
				ODTab->SetTabActiveStatus(true);
				ActiveTabButton = ODTab;
				ActiveTabIndex = ActiveTabButton->GetSwitcherIndex();
			}
		}
	}
}

UODWidgetPage* UODSwitcherMenu::GetActiveWidgetPage()
{
	UODWidgetPage* WidgetPage = Cast<UODWidgetPage>(MenuSwitcher->GetActiveWidget());
	return WidgetPage;
}

bool UODSwitcherMenu::IsTabMenu()
{
	return bIsTabMenu;
}

void UODSwitcherMenu::ChangeTabDirectional(bool bIsRight)
{
	UODWidgetPage* NewWidgetPage = NULL;

	if (bIsRight)
	{
		if (MenuSwitcher && (ActiveTabIndex < MenuSwitcher->GetNumWidgets() - 1))
		{
			// Move forward in our widgets and let the widgets know to transition
			UODWidgetPage* CurrentWidgetPage = Cast<UODWidgetPage>(MenuSwitcher->GetActiveWidget());
			if (CurrentWidgetPage)
			{
				CurrentWidgetPage->HandleTabSwitch(false);
			}

			ActiveTabIndex++;

			MenuSwitcher->SetActiveWidgetIndex(ActiveTabIndex);

			NewWidgetPage = Cast<UODWidgetPage>(MenuSwitcher->GetWidgetAtIndex(ActiveTabIndex));
			if (NewWidgetPage)
			{
				CurrentWidgetPage->HandleTabSwitch(true);
			}
		}
	}
	else
	{
		if (MenuSwitcher && (ActiveTabIndex > 0))
		{
			// Move forward in our widgets and let the widgets know to transition
			UODWidgetPage* CurrentWidgetPage = Cast<UODWidgetPage>(MenuSwitcher->GetActiveWidget());
			if (CurrentWidgetPage)
			{
				CurrentWidgetPage->HandleTabSwitch(false);
			}

			ActiveTabIndex--;

			MenuSwitcher->SetActiveWidgetIndex(ActiveTabIndex);

			NewWidgetPage = Cast<UODWidgetPage>(MenuSwitcher->GetWidgetAtIndex(ActiveTabIndex));
			if (NewWidgetPage)
			{
				CurrentWidgetPage->HandleTabSwitch(true);
			}
		}
	}

	// Now we need to set that widget as active (for controllers)
	if (NewWidgetPage)
	{
		NewWidgetPage->BecomeActive(true);
	}

	ClearInactiveTabButtons();
}

void UODSwitcherMenu::ChangeTab(int32 TabIndex)
{
	UE_LOG(LogTemp, Warning, TEXT("TabIndex is ChangeTab: %i"), TabIndex);
	if (SwitcherTabs.IsValidIndex(TabIndex))
	{
		UODWidgetPage* CurrentWidgetPage = Cast<UODWidgetPage>(MenuSwitcher->GetActiveWidget());
		if (CurrentWidgetPage)
		{
			CurrentWidgetPage->HandleTabSwitch(false);
		}

		ActiveTabIndex = TabIndex;

		MenuSwitcher->SetActiveWidgetIndex(ActiveTabIndex);

		UODWidgetPage* NewWidgetPage = Cast<UODWidgetPage>(MenuSwitcher->GetWidgetAtIndex(ActiveTabIndex));
		if (NewWidgetPage)
		{
			CurrentWidgetPage->HandleTabSwitch(true);
		}
	}

	ClearInactiveTabButtons();
}

void UODSwitcherMenu::ClearInactiveTabButtons()
{
	SwitcherTabs[ActiveTabIndex]->SetTabActiveStatus(true);

	for (int32 TabIndex = 0; TabIndex < SwitcherTabs.Num(); ++TabIndex)
	{
		if (SwitcherTabs[TabIndex]->GetSwitcherIndex() != ActiveTabIndex)
		{
			SwitcherTabs[TabIndex]->SetTabActiveStatus(false);
		}
	}
}

