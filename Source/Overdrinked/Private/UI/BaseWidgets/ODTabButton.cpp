// Copyright 2020 Space Gurus


#include "UI/BaseWidgets/ODTabButton.h"

void UODTabButton::SetTabActiveStatus(bool bActive)
{
	bIsActive = bActive;

	if (!bIsActive)
	{
		bIsHovered = false;
	}
}

bool UODTabButton::IsStartingTab()
{
	return bIsStartingTab;
}

int32 UODTabButton::GetSwitcherIndex()
{
	return SwitcherIndex;
}
