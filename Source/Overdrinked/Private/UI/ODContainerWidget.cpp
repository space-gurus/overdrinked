// Copyright 2020 Space Gurus


#include "UI/ODContainerWidget.h"
#include "Materials/MaterialInterface.h"
#include "Materials/MaterialInstanceDynamic.h"
#include "Engine/DataTable.h"
#include "Components/Image.h"
#include "Styling/SlateColor.h"
#include "Data/IngredientData.h"
#include "Actors/ODBevContainer.h"
#include "Gameplay/ODContainerContents.h"
#include "UI/ODResourceHexWidget.h"
#include "ODEnumHelperFunctions.h"
#include "ODHelperFunctions.h"
#include "ODPlayerController.h"

UODContainerWidget::UODContainerWidget(const FObjectInitializer& OI) : Super(OI)
{
	ConstructorHelpers::FObjectFinder<UDataTable> IngredientDataObject(TEXT("/Game/DrinkGame/Data/DT_Ingredients"));
	if (IngredientDataObject.Succeeded())
	{
		IngredientDataTable = IngredientDataObject.Object;
	}

	bIsWidgetActive = false;
	SetVisibility(ESlateVisibility::Hidden);
}

void UODContainerWidget::NativeTick(const FGeometry& MyGeometry, float InDeltaTime)
{
	// Only update our position if we're active
	AODPlayerController* ODPlayerController = Cast<AODPlayerController>(GetOwningPlayer());
	if (ODPlayerController && ODPlayerController->GetPawn())
	{
		// Set our position to the right of the player
		FVector2D PlayerScreenLocation;
		ODPlayerController->ProjectWorldLocationToScreen(ODPlayerController->GetPawn()->GetActorLocation(), PlayerScreenLocation);

		this->SetPositionInViewport(PlayerScreenLocation + ViewportOffset);
	}
}

void UODContainerWidget::SetNewContainer(AODBevContainer* NewContainer)
{
	if (!NewContainer)
		return;

	//UE_LOG(LogTemp, Warning, TEXT("UODContainerWidget::SetNewContainer"));
	// Keep a reference to the mixer so variables can be automatically updated
	NewContainer->SetWidgetReference(this);
	DrinkContainerReference = NewContainer;

	if (DrinkContainerReference->GetContainerContents().GarnishAmounts.Num() > 0)
	{
		UE_LOG(LogTemp, Warning, TEXT("%s() Garnish Amounts: %i"), *FString(__FUNCTION__), DrinkContainerReference->GetContainerContents().GarnishAmounts[0]);
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("%s() No Garnishes"), *FString(__FUNCTION__));
	}
	// Setup calculations for size
	ContainerMaxVolume = NewContainer->GetMaxVolume();
	CurrentMaterialIndex = 1;

	// Setup our hexes if they aren't already
	if (ResourceHexes.Num() == 0)
	{
		ResourceHexes.Add(ResourceHex1);
		ResourceHexes.Add(ResourceHex2);	
		ResourceHexes.Add(ResourceHex3);
		ResourceHexes.Add(ResourceHex4);
		ResourceHexes.Add(ResourceHex5);
	}

	// Save the reference of this mixer material if needed
// 	if (!MixerMaterialInterface)
// 	{
// 		MixerMaterialInterface = Cast<UMaterialInterface>(FillImage->Brush.GetResourceObject());
// 		SetupMixerMaterial();
// 	}
	SetFillMaterial(DrinkContainerReference->GetGlassType());
	if (DynamicMixerMaterial)
	{
		//UE_LOG(LogTemp, Log, TEXT("UODContainerWidget::SetNewContainer: DynamicMixerMaterial good after BP Event"));
		DynamicMixerMaterial->ClearParameterValues();
		ClearIngredients();
	}

	// Set the overlay image
	SetGlassOverlayImage(DrinkContainerReference->GetGlassType());
	SetFillMaterialShape(DynamicMixerMaterial);

	// Get initial values
	AdjustContents();

	// Show what you got
	ShowContainer();
}

void UODContainerWidget::AdjustContents()
{
	if (DrinkContainerReference->GetCurrentVolume() == 0.f && DrinkContainerReference->GetContainerContents().GarnishAmounts.Num() == 0)
	{
		ClearIngredients();
	}
	else
	{
		TArray<EIngredientType> IngredientTypes = DrinkContainerReference->GetContainerContents().IngredientTypes;
		TArray<float> IngredientAmounts = DrinkContainerReference->GetContainerContents().IngredientAmounts;

		// Has this drink been mixed up? If so, display differently
		if (DrinkContainerReference->GetContainerContents().bIsMixed)
		{
			TArray<int32> AmountsAsInts = UODHelperFunctions::ConvertFloatArrayToInt(IngredientAmounts);
			FLinearColor BlendedLiquidColor = UODHelperFunctions::BuildDrinkColor(IngredientTypes, AmountsAsInts, IngredientDataTable);
			
			// Treat it as a one ingredient drink
			FString IngredientColorString = "";
			IngredientColorString.Append(BaseIngredientString);
			IngredientColorString.Append(FString::FromInt(1));
			IngredientColorString.Append(ColorString);
			FString IngredientLevelString = "";
			IngredientLevelString.Append(BaseIngredientString);
			IngredientLevelString.Append(FString::FromInt(1));
			IngredientLevelString.Append(LevelString);

			float TotalAmount = 0.f;
			for (float Amount : IngredientAmounts)
			{
				TotalAmount += Amount;
			}

			// Calculate our level
			float FillLevel = DrinkContainerReference->GetCurrentVolume() / ContainerMaxVolume;
			// Set necessary parameters in the material
			if (DynamicMixerMaterial)
			{
				// Zero out the material
				for (int MatIndex = 1; MatIndex <= 5; ++MatIndex)
				{
					FString ThisLevelString = "";
					ThisLevelString.Append(BaseIngredientString);
					ThisLevelString.Append(FString::FromInt(MatIndex));
					ThisLevelString.Append(LevelString);

					DynamicMixerMaterial->SetScalarParameterValue(FName(ThisLevelString), 0.f);
				}

				// Only have to update the level and color
				DynamicMixerMaterial->SetScalarParameterValue(FName(IngredientLevelString), FillLevel);
				DynamicMixerMaterial->SetVectorParameterValue(FName(IngredientColorString), BlendedLiquidColor);
			}

			// Still want to do hexes so player can see amounts
			// First clear them out
			for (UODResourceHexWidget* Hex : ResourceHexes)
			{
				Hex->SetResourceAmount(0);
				Hex->SetIsBeingUsed(false);
			}

			// Then repopulate
			for (int32 IngIndex = 0; IngIndex < IngredientTypes.Num(); ++IngIndex)
			{
				EIngredientType IngredientType = IngredientTypes[IngIndex];
				float Amount = IngredientAmounts[IngIndex];
				bool bFoundHex = false;
				int32 HexIndex = 0;
				while (!bFoundHex && HexIndex < ResourceHexes.Num())
				{
					if (ResourceHexes.IsValidIndex(HexIndex))
					{
						// Find the first available hex
						if (!ResourceHexes[HexIndex]->IsBeingUsed())
						{
							ResourceHexes[HexIndex]->SetResourceAsIngredient(IngredientType);
							ResourceHexes[HexIndex]->SetResourceAmount(Amount);
							bFoundHex = true;
						}
					}

					HexIndex++;
				}
			}
		}
		else
		{
			// Has not been mixed up
			if (IngredientTypes.Num() == IngredientAmounts.Num())
			{
				for (int32 IngIndex = 0; IngIndex < IngredientTypes.Num(); ++IngIndex)
				{
					EIngredientType IngredientType = IngredientTypes[IngIndex];
					float Amount = IngredientAmounts[IngIndex];

					if (CurrentMaterialIndex <= 5)
					{
						if (IngredientToMaterialIndex.Contains(IngredientType))
						{
							// Build parameter strings based on saved index reference
							FString IngredientColorString = "";
							IngredientColorString.Append(BaseIngredientString);
							IngredientColorString.Append(FString::FromInt(IngredientToMaterialIndex[IngredientType]));
							IngredientColorString.Append(ColorString);
							FString IngredientLevelString = "";
							IngredientLevelString.Append(BaseIngredientString);
							IngredientLevelString.Append(FString::FromInt(IngredientToMaterialIndex[IngredientType]));
							IngredientLevelString.Append(LevelString);

							// Calculate our level
							float IngredientLevel = Amount / ContainerMaxVolume;

							// Set necessary parameters in the material
							if (DynamicMixerMaterial)
							{
								// Only have to update the level
								DynamicMixerMaterial->SetScalarParameterValue(FName(IngredientLevelString), IngredientLevel);
							}

							// We should already have this hex
							for (UODResourceHexWidget* ResourceHex : ResourceHexes)
							{
								if (ResourceHex->GetIngredientType() == IngredientType)
								{
									ResourceHex->SetResourceAmount(FMath::FloorToInt(Amount));
								}
							}
						}
						else
						{
							
							// It's a new ingredient for this mixer
							// Build parameter strings
							FString IngredientColorString = "";
							IngredientColorString.Append(BaseIngredientString);
							IngredientColorString.Append(FString::FromInt(CurrentMaterialIndex));
							IngredientColorString.Append(ColorString);
							FString IngredientLevelString = "";
							IngredientLevelString.Append(BaseIngredientString);
							IngredientLevelString.Append(FString::FromInt(CurrentMaterialIndex));
							IngredientLevelString.Append(LevelString);

							// Calculate our level
							float IngredientLevel = Amount / ContainerMaxVolume;

							// Set necessary parameters in the material
							if (DynamicMixerMaterial)
							{
								// Get color from data table
								if (IngredientDataTable)
								{
									FIngredientData* IngredientData = IngredientDataTable->FindRow<FIngredientData>(UODEnumHelperFunctions::IngredientEnumToString(IngredientType), FString(""));
									if (IngredientData)
									{
										DynamicMixerMaterial->SetVectorParameterValue(FName(IngredientColorString), IngredientData->Color.ReinterpretAsLinear());
									}
								}
								DynamicMixerMaterial->SetScalarParameterValue(FName(IngredientLevelString), IngredientLevel);
							}

							// Set up our resource hex
							bool bFoundHex = false;
							int32 HexIndex = 0;
							while (!bFoundHex && HexIndex < ResourceHexes.Num())
							{
								if (ResourceHexes.IsValidIndex(HexIndex))
								{
									// Find the first available hex
									if (!ResourceHexes[HexIndex]->IsBeingUsed())
									{
										ResourceHexes[HexIndex]->SetResourceAsIngredient(IngredientType);
										ResourceHexes[HexIndex]->SetResourceAmount(Amount);
										bFoundHex = true;
									}
								}

								HexIndex++;
							}

							// Keep the reference and move our index forward
							IngredientToMaterialIndex.Add(IngredientType, CurrentMaterialIndex);
							++CurrentMaterialIndex;
						}
					}
				}
			}
		}

		// Now do the garnishes
		TArray<EGarnishType> GarnishTypes = DrinkContainerReference->GetContainerContents().GarnishTypes;
		TArray<int32> GarnishAmounts = DrinkContainerReference->GetContainerContents().GarnishAmounts;

		if (GarnishAmounts.Num() == GarnishTypes.Num())
		{
			for (int32 GarIndex = 0; GarIndex < GarnishTypes.Num(); ++GarIndex)
			{
				EGarnishType GarnishType = GarnishTypes[GarIndex];
				int32 Amount = GarnishAmounts[GarIndex];

				bool bAlreadyContained = false;
				int32 HexIndex = -1;
				for (int32 i = 0; i < ResourceHexes.Num(); ++i)
				{
					if (ResourceHexes[i]->GetGarnishType() == GarnishType)
					{
						bAlreadyContained = true;
						HexIndex = i;
					}
				}

				if (bAlreadyContained && ResourceHexes.IsValidIndex(HexIndex))
				{
					ResourceHexes[HexIndex]->SetResourceAmount(Amount);
				}
				else
				{
					// Make a new hex
					bool bFoundHex = false;
					int32 NewHexIndex = 0;
					while (!bFoundHex && NewHexIndex < ResourceHexes.Num())
					{
						if (ResourceHexes.IsValidIndex(NewHexIndex))
						{
							// Find the first available hex
							if (!ResourceHexes[NewHexIndex]->IsBeingUsed())
							{
								ResourceHexes[NewHexIndex]->SetResourceAsGarnish(GarnishType);
								ResourceHexes[NewHexIndex]->SetResourceAmount(Amount);
								bFoundHex = true;
							}
						}

						NewHexIndex++;
					}
				}
			}
		}

		// Show active hexes
		for (UODResourceHexWidget* ResourceHex : ResourceHexes)
		{
			if (ResourceHex->IsBeingUsed())
			{
				ResourceHex->SetVisibility(ESlateVisibility::Visible);
			}
			else
			{
				ResourceHex->SetVisibility(ESlateVisibility::Hidden);
			}
		}
	}
}

void UODContainerWidget::ClearIngredients()
{
	// Zero out the material
	for (int MatIndex = 1; MatIndex <= 5; ++MatIndex)
	{
		FString IngredientLevelString = "";
		IngredientLevelString.Append(BaseIngredientString);
		IngredientLevelString.Append(FString::FromInt(MatIndex));
		IngredientLevelString.Append(LevelString);

		if (DynamicMixerMaterial)
		{
			FName NameVersion = FName(IngredientLevelString);
			DynamicMixerMaterial->SetScalarParameterValue(FName(IngredientLevelString), 0.f);
		}
	}

	// Clear hexes
	for (UODResourceHexWidget* Hex : ResourceHexes)
	{
		Hex->HideAndReset();
		Hex->SetVisibility(ESlateVisibility::Hidden);
	}

	// Clear and reset necessary variables
	CurrentMaterialIndex = 1;
	IngredientToMaterialIndex.Empty();
}

void UODContainerWidget::SetFillMaterialShape(UMaterialInstanceDynamic* MaterialToSet)
{
	if (MaterialToSet)
	{
		switch (DrinkContainerReference->GetGlassType())
		{
		case EGlassType::GT_Brew:
			MaterialToSet->SetScalarParameterValue(TEXT("DrinkIndex"), 1.0f);
			break;
		case EGlassType::GT_Mixer:
			MaterialToSet->SetScalarParameterValue(TEXT("DrinkIndex"), 3.0f);
			break;
		case EGlassType::GT_Shot:
			MaterialToSet->SetScalarParameterValue(TEXT("DrinkIndex"), 0.0f);
			break;
		case EGlassType::GT_Stemmed:
			MaterialToSet->SetScalarParameterValue(TEXT("DrinkIndex"), 2.0f);
			break;
		case EGlassType::GT_Blender:
			MaterialToSet->SetScalarParameterValue(TEXT("DrinkIndex"), 1.0f);
			break;
		}
	}
}

void UODContainerWidget::SetupMixerMaterial()
{
	// Initialize Material Instance
	if (MixerMaterialInterface)
	{
		// Setup our dynamic material for the mixer
		if (!DynamicMixerMaterial)
		{
			DynamicMixerMaterial = UMaterialInstanceDynamic::Create(MixerMaterialInterface, this);
			//FillImage->Brush.SetResourceObject(DynamicMixerMaterial);
			DynamicMixerMaterial->ClearParameterValues();
			SetFillMaterialShape(DynamicMixerMaterial);
			ClearIngredients();
		}
	}
}
