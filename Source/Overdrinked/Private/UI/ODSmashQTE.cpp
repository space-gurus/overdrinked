// Copyright 2020 Space Gurus


#include "UI/ODSmashQTE.h"
#include "Materials/MaterialInstanceDynamic.h"

void UODSmashQTE::NativeTick(const FGeometry& MyGeometry, float InDeltaTime)
{
	Super::NativeTick(MyGeometry, InDeltaTime);

	if (bIsQTEActive)
	{
		if (!bIsProgressBarFull)
		{
			// Constantly decrease to try to keep the player from filling the bar
			float NewProgressPercent = FMath::FInterpConstantTo(Progress, 0.f, InDeltaTime, ProgressDecreaseSpeed);
			SetProgress(NewProgressPercent);
			Progress = NewProgressPercent;
		}
	}
}

void UODSmashQTE::InteractWithQTE()
{
	Super::InteractWithQTE();

	PlayerHit();
}

void UODSmashQTE::PlayerHit()
{
	float NewProgressPercent = Progress + PlayerHitProgress;
	SetProgress(NewProgressPercent);
	Progress = NewProgressPercent;

	if (NewProgressPercent >= 0.99f)
	{
		bPlayerSuccess = true;
		DeactivateQTE();
	}
}

void UODSmashQTE::DeactivateQTE()
{
	Super::DeactivateQTE();

	Progress = 0.f;
	SetProgress(Progress);
}
