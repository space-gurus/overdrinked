// Copyright 2020 Space Gurus


#include "UI/ODCustomerEmoteWidget.h"

bool UODCustomerEmoteWidget::IsGlowShowing()
{
	return bIsGlowShowing;
}

bool UODCustomerEmoteWidget::IsEmoteShowing()
{
	return bIsEmoteShowing;
}
