// Copyright 2020 Space Gurus


#include "ODLobbyGameMode.h"
#include "ODMenuPlayerController.h"
#include "ODLobbyGameState.h"
#include "ODGameInstance.h"
#include "ODMenuPawn.h"
#include "ODPlayerState.h"
#include "Kismet/GameplayStatics.h"

AODLobbyGameMode::AODLobbyGameMode()
{
	PlayerControllerClass = AODMenuPlayerController::StaticClass();
	DefaultPawnClass = AODMenuPawn::StaticClass();
	GameStateClass = AODLobbyGameState::StaticClass();
	PlayerStateClass = AODPlayerState::StaticClass();

	bUseSeamlessTravel = true;
}

void AODLobbyGameMode::PostLogin(APlayerController* NewPlayer)
{
	Super::PostLogin(NewPlayer);

	UE_LOG(LogTemp, Warning, TEXT("PostLogin"));

	NumberOfPlayers++;

	AODMenuPlayerController* NewPlayerController = Cast<AODMenuPlayerController>(NewPlayer);
	if (NewPlayerController)
	{
		UODGameInstance* ODGameInstance = Cast<UODGameInstance>(UGameplayStatics::GetGameInstance(GetWorld()));
		if (ODGameInstance)
		{
			ODGameInstance->PrintCurrentSessionState();
		}

		PlayerControllerList.AddUnique(NewPlayerController);
		
		// Update the game state
		AODLobbyGameState* LobbyGameState = Cast<AODLobbyGameState>(UGameplayStatics::GetGameState(GetWorld()));
		if (LobbyGameState)
		{
			LobbyGameState->SetNumPlayers(NumberOfPlayers);
			LobbyGameState->Server_AddPlayer(NewPlayerController, NumberOfPlayers);
		}

		AODPlayerState* NewPlayerState = Cast<AODPlayerState>(NewPlayerController->PlayerState);
		if (NewPlayerState)
		{
			UE_LOG(LogTemp, Warning, TEXT("Got the Player State in the LobbyGameMode"));
			NewPlayerState->SetPlayerNum(NumPlayers);
		}
	}
}

void AODLobbyGameMode::Logout(AController* Exiting)
{
	Super::Logout(Exiting);
// 
// 	AODMenuPlayerController* ExitingPC = Cast<AODMenuPlayerController>(Exiting);
// 	if (ExitingPC)
// 	{
// 		NumberOfPlayers--;
// 
// 		PlayerControllerList.Remove(ExitingPC);
// 		
// 		AODLobbyGameState* LobbyGameState = Cast<AODLobbyGameState>(UGameplayStatics::GetGameState(GetWorld()));
// 		if (LobbyGameState)
// 		{
// 			LobbyGameState->Server_RemovePlayer(1);
// 			LobbyGameState->SetNumPlayers(NumberOfPlayers);
// 		}
// 	}
}

AODMenuPlayerController* AODLobbyGameMode::GetHostPlayerController()
{
	if (PlayerControllerList.IsValidIndex(0))
	{
		return PlayerControllerList[0];
	}
	else
	{
		return NULL;
	}
}

void AODLobbyGameMode::NotifyPlayersOfMapTravel()
{
	for (AODMenuPlayerController* PlayerController : PlayerControllerList)
	{
		PlayerController->PrepareForMapTravel();
	}
}
