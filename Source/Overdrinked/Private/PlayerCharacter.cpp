// Copyright 2020 Space Gurus


#include "PlayerCharacter.h"
#include "ODGameInstance.h"
#include "ODPlayerController.h"
#include "ODMultiplayerGameState.h"
#include "ODPlayerState.h"
#include "ODHelperFunctions.h"
#include "Actors/PickupActor.h"
#include "Actors/ODBevContainer.h"
#include "Actors/ODTaser.h"
#include "Actors/ODInteractInterface.h"
#include "Environment/ODCounter.h"
#include "Environment/ODBarArea.h"
#include "Environment/ODWasteBin.h"
#include "Environment/ODGlassRack.h"
#include "Environment/ODChopStation.h"
#include "Environment/ODDummyPickup.h"
#include "Actors/ODBlender.h"
#include "Actors/ODIngredientDispenser.h"
#include "Actors/ODDrinkMixer.h"
#include "Actors/ODDrinkGlass.h"
#include "Actors/ODGarnish.h"
#include "Actors/ODIce.h"
#include "Actors/ODSkelPickupActor.h"
#include "Gameplay/ODDrink.h"
#include "UI/ODMixerWidget.h"
#include "UI/ODPlayerIDWidget.h"
#include "UI/ODContainerWidget.h"
#include "AI/ODAICharacter.h"
#include "AI/ODAICrawler.h"
#include "AI/ODCrawlerManager.h"
#include "Components/CapsuleComponent.h"
#include "Components/WidgetComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Components/BoxComponent.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "NiagaraComponent.h"
#include "Net/UnrealNetwork.h"
#include "Animation/AnimMontage.h"
#include "DrawDebugHelpers.h"
#include "AkGameplayStatics.h"
#include "AkAudioEvent.h"
#include "AkComponent.h"

// Sets default values
APlayerCharacter::APlayerCharacter(const FObjectInitializer& OI) : Super(OI)
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	bReplicates = true;
	
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// For outlined look
	GetMesh()->SetRenderCustomDepth(true);
	GetMesh()->SetCustomDepthStencilValue(255);

	// Components
	PickupCollider = CreateDefaultSubobject<UBoxComponent>(TEXT("PickupCollider"));
	PickupCollider->SetupAttachment(RootComponent);

	CounterCollider = CreateDefaultSubobject<UBoxComponent>(TEXT("CounterCollider"));
	CounterCollider->SetupAttachment(RootComponent);

	CameraSpringArm = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraSpringArm"));
	CameraSpringArm->SetupAttachment(RootComponent);
	CameraSpringArm->TargetArmLength = 800.f;
	CameraSpringArm->SetRelativeRotation(FRotator(-50.f, 0.f, 0.f), false);
	CameraSpringArm->bDoCollisionTest = false;
	CameraSpringArm->bEnableCameraLag = true;
	CameraSpringArm->CameraLagSpeed = 2.f;
	CameraSpringArm->bInheritPitch = false;
	CameraSpringArm->bInheritRoll = false;
	CameraSpringArm->bInheritYaw = false;

	PlayerCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("PlayerCamera"));
	PlayerCamera->SetupAttachment(CameraSpringArm);

	TrailComponent = CreateDefaultSubobject<UNiagaraComponent>(TEXT("PlayerTrail"));
	TrailComponent->SetupAttachment(GetMesh());

	AkComponent = CreateDefaultSubobject<UAkComponent>(TEXT("AkComponent"));
	AkComponent->SetupAttachment(GetMesh());

	PlayerIdentifierWidgetComp = CreateDefaultSubobject<UWidgetComponent>(TEXT("PlayerIdentifierWidget"));
	PlayerIdentifierWidgetComp->SetupAttachment(RootComponent);
	PlayerIdentifierWidgetComp->SetWidgetSpace(EWidgetSpace::Screen);
	PlayerIdentifierWidgetComp->SetDrawAtDesiredSize(true);

	ContainerWidgetComponent = CreateDefaultSubobject<UWidgetComponent>(TEXT("ContainerWidgetComponent"));
	ContainerWidgetComponent->SetupAttachment(RootComponent);
	ContainerWidgetComponent->SetWidgetSpace(EWidgetSpace::Screen);
	ContainerWidgetComponent->SetDrawAtDesiredSize(true);

	// Movement
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;
	GetCharacterMovement()->bOrientRotationToMovement = true;
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f);
	GetCharacterMovement()->MaxWalkSpeed = 400.f;
	GetCharacterMovement()->GroundFriction = 1.5f;
	DashAcceleration = 5000.f;
	DashSpeed = 1000.f;
	DashTime = 0.2f;
	DashCooldownTime = 1.f;
}

void APlayerCharacter::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	// Variables to replicate
	DOREPLIFETIME(APlayerCharacter, ObjectInHand);
	DOREPLIFETIME(APlayerCharacter, bIsHoldingObject);
	DOREPLIFETIME(APlayerCharacter, ShakeAxis);
	DOREPLIFETIME(APlayerCharacter, bIsShaking);
	DOREPLIFETIME(APlayerCharacter, bCanDash);
	DOREPLIFETIME(APlayerCharacter, bIsInRangeOfGlassRack);
	DOREPLIFETIME(APlayerCharacter, bIsWashing);
	DOREPLIFETIME(APlayerCharacter, bIsPouring);
	DOREPLIFETIME(APlayerCharacter, bIsDashing);
}

// Called when the game starts or when spawned
void APlayerCharacter::BeginPlay()
{
	Super::BeginPlay();
	
	GetCapsuleComponent()->OnComponentBeginOverlap.AddDynamic(this, &APlayerCharacter::CapsuleColliderBeginOverlap);
	GetCapsuleComponent()->OnComponentEndOverlap.AddDynamic(this, &APlayerCharacter::CapsuleColliderEndOverlap);
	PickupCollider->OnComponentBeginOverlap.AddDynamic(this, &APlayerCharacter::PickupColliderBeginOverlap);
	PickupCollider->OnComponentEndOverlap.AddDynamic(this, &APlayerCharacter::PickupColliderEndOverlap);
	CounterCollider->OnComponentBeginOverlap.AddDynamic(this, &APlayerCharacter::CounterColliderBeginOverlap);
	CounterCollider->OnComponentEndOverlap.AddDynamic(this, &APlayerCharacter::CounterColliderEndOverlap);

	ODPlayerController = Cast<AODPlayerController>(this->GetController());

	PlayerIDWidget = Cast<UODPlayerIDWidget>(PlayerIdentifierWidgetComp->GetUserWidgetObject());
}

FString APlayerCharacter::GetRoleEnumText(ENetRole role)
{
	switch (role)
	{
	case ROLE_None:
		return "None";
	case ROLE_SimulatedProxy:
		return "SimulatedProxy";
	case ROLE_AutonomousProxy:
		return "AutonomousProxy";
	case ROLE_Authority:
		return "Authority";
	default:
		return "ERROR";
	}
}

// Called every frame
void APlayerCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (bIsInBarArea)
	{
		CameraSpringArm->SetWorldLocation(CameraLockPosition, false);
	}

	// This is just to show what role the actor is
// 	UODGameInstance* ThisGameInstance = Cast<UODGameInstance>(GetGameInstance());
// 	if (ThisGameInstance->GetDrawRoleCards())
// 	{
// 		if (!bIsHoldingObject)
// 		{
// 			DrawDebugString(GetWorld(), FVector(0, 0, 100), GetRoleEnumText(GetLocalRole()), this, FColor::White, DeltaTime, false, 1.f);
// 		}
// 		else
// 		{
// 			DrawDebugString(GetWorld(), FVector(0, 0, 100), GetRoleEnumText(GetLocalRole()), this, FColor::Red, DeltaTime, false, 1.f);
// 		}
// 		
// 	}

	// Do we need to update our closest interact actor regularly?
	if (InteractActorsInRange.Num() >= 2)
	{
		UpdateClosestInteractActor();
	}

	// Update constant niagara effects
	if (TrailComponent)
	{
		if (bIsDashing)
		{
			TrailComponent->SetFloatParameter(TEXT("SpawnRate"), DashSpawnRate);
			TrailComponent->SetFloatParameter(TEXT("Scale"), DashScale);
		}
		else
		{
			if (GetMovementComponent()->Velocity.IsNearlyZero(0.1f))
			{
				TrailComponent->SetFloatParameter(TEXT("SpawnRate"), IdleSpawnRate);
				TrailComponent->SetFloatParameter(TEXT("Scale"), IdleScale);
			}
			else
			{
				TrailComponent->SetFloatParameter(TEXT("SpawnRate"), MovingSpawnRate);
				TrailComponent->SetFloatParameter(TEXT("Scale"), MovingScale);
			}
		}
	}
}

// Called to bind functionality to input
void APlayerCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	check(PlayerInputComponent);
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}

void APlayerCharacter::SetupPlayerIDWidget_Implementation()
{
	if (HasAuthority())
	{
		UE_LOG(LogTemp, Error, TEXT("SetupPlayerIdWidget ON THE SERVER %s"), *GetName());
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("SetupPlayerIdWidget ON THE CLIENT %s"), *GetName());
	}

	AODPlayerState* ODPlayerState = Cast<AODPlayerState>(GetPlayerState());
	if (ODPlayerState)
	{
		if (HasAuthority())
		{
			UE_LOG(LogTemp, Error, TEXT("Got the ODPlayerState in SetupPlayerIdWidget ON THE SERVER %s"), *GetName());
		}
		else
		{
			UE_LOG(LogTemp, Error, TEXT("Got the ODPlayerState in SetupPlayerIdWidget ON THE CLIENT %s"), *GetName());
		}
	}

	if (ODPlayerState && PlayerIDWidget)
	{
		PlayerIDWidget->SetPlayer(ODPlayerState->GetPlayerName(), ODPlayerState->GetPlayerNum());
	}
}

void APlayerCharacter::MoveForward(float val)
{
	if ((Controller != NULL) && (val != 0.0f))
	{
		// Find out which way is forward
		const FRotator rot = Controller->GetControlRotation();
		const FRotator yawRot(0, rot.Yaw, 0);

		// Get forward vector
		const FVector dir = FRotationMatrix(yawRot).GetUnitAxis(EAxis::X);
		AddMovementInput(dir, val);
	}
}

void APlayerCharacter::MoveRight(float val)
{
	if ((Controller != NULL) && (val != 0.0f))
	{
		// Find out which way is right
		const FRotator rot = Controller->GetControlRotation();
		const FRotator yawRot(0, rot.Yaw, 0);

		// Get right vector 
		const FVector dir = FRotationMatrix(yawRot).GetUnitAxis(EAxis::Y);
		AddMovementInput(dir, val);
	}
}

void APlayerCharacter::Server_StartDash_Implementation()
{
	GetCharacterMovement()->MaxWalkSpeed = DashSpeed;
	GetCharacterMovement()->MaxAcceleration = DashAcceleration;

	//GetWorldTimerManager().SetTimer(DashTimerHandle, this, &APlayerCharacter::EndDash, DashTime, false);
	// Set the dash on cooldowm
	bCanDash = false;
	GetWorldTimerManager().SetTimer(DashCooldownTimerHandle, this, &APlayerCharacter::RefreshDash, DashCooldownTime, false);

	int32 NumHands = 0;
	if (IsHoldingObject())
	{
		NumHands++;

		AODIce* Ice = Cast<AODIce>(ObjectInHand);
		if (Ice)
			NumHands++;
	}

	// Play animation for everyone
	NetPlayDashAnimation(NumHands);
}

void APlayerCharacter::EndDash()
{
	GetCharacterMovement()->MaxWalkSpeed = 400.f;
	GetCharacterMovement()->MaxAcceleration = 2048.f;
	Client_SetWalkSpeed();
	//GetWorldTimerManager().ClearTimer(DashTimerHandle);
}

void APlayerCharacter::SetClientDashSpeed()
{
	GetCharacterMovement()->MaxWalkSpeed = DashSpeed;
	GetCharacterMovement()->MaxAcceleration = DashAcceleration;
}

void APlayerCharacter::Client_SetWalkSpeed_Implementation()
{
	GetCharacterMovement()->MaxWalkSpeed = 400.f;
	GetCharacterMovement()->MaxAcceleration = 2048.f;
}

bool APlayerCharacter::HasObjectInRange()
{
	return ObjectsInRange.Num() > 0;
}

bool APlayerCharacter::HasCounterInRange()
{
	return ClosestCounter ? true : false;
}

AODCounter* APlayerCharacter::GetClosestCounter()
{
	return ClosestCounter;
}

bool APlayerCharacter::CanCharacterDash()
{
	return bCanDash && !GetCharacterMovement()->Velocity.IsZero();
}

void APlayerCharacter::NetPlayDashAnimation_Implementation(int32 NumHandsInUse)
{
	// Play the dash anim
	PlayDashAnimation(NumHandsInUse);

	bIsDashing = true;

	FTimerHandle DashTimerHandle;
	GetWorldTimerManager().SetTimer(DashTimerHandle, this, &APlayerCharacter::FinishDash, DashEffectTime, false);
}

void APlayerCharacter::FinishDash()
{
	bIsDashing = false;
}

void APlayerCharacter::Server_Interact_Implementation()
{
	// Handle if it's a chop station ( we need to be holding a crawler)
// 	AODChopStation* ChopStation = Cast<AODChopStation>(GetClosestInteractObject());
// 	if (ChopStation)
// 	{
// 		if (!IsHoldingCrawler())
// 		{
// 			return;
// 		}
// 	}
	if (ClosestInteractActor)
	{
		IODInteractInterface::Execute_PlayerInteraction(ClosestInteractActor, this);
		return;
	}
}

void APlayerCharacter::Interact()
{
	if (ClosestInteractActor)
	{
		Server_InteractSpecificObject(ClosestInteractActor);
	}
}

void APlayerCharacter::DashForward()
{
	if (!HasAuthority())
		Server_DashForward();

	FVector ForwardVec = GetActorForwardVector();
	ForwardVec.Z = 100.f;
	ForwardVec.X = ForwardVec.X * DashMultiplier;
	ForwardVec.Y = ForwardVec.Y * DashMultiplier;
	GetCharacterMovement()->Launch(ForwardVec);
}

void APlayerCharacter::Server_DashForward_Implementation()
{
	FVector ForwardVec = GetActorForwardVector();
	ForwardVec.Z = 100.f;
	ForwardVec.X = ForwardVec.X * DashMultiplier;
	ForwardVec.Y = ForwardVec.Y * DashMultiplier;
	GetCharacterMovement()->Launch(ForwardVec);

	int32 NumHands = 0;
	if (IsHoldingObject())
	{
		NumHands++;

		AODIce* Ice = Cast<AODIce>(ObjectInHand);
		if (Ice)
			NumHands++;
	}

	NetPlayDashAnimation(NumHands);
}

void APlayerCharacter::Server_InteractSpecificObject_Implementation(AActor* SpecificObject)
{
	IODInteractInterface* InteractObj = Cast<IODInteractInterface>(SpecificObject);
	if (InteractObj)
	{
		IODInteractInterface::Execute_PlayerInteraction(SpecificObject, this);
		return;
	}
}

void APlayerCharacter::HandleObjectPickup(AActor* PickupObject)
{
	bIsHoldingObject = true;
	ObjectInHand = PickupObject;

	// Remove it from the list of things we can currently interact with
	if (InteractActorsInRange.Contains(PickupObject))
	{
		InteractActorsInRange.Remove(PickupObject);
	}

	UpdateClosestInteractActor();
	PlayPickupSound();

	// Remind the server
	if (HasAuthority())
	{
		OnRep_ObjectInHand();
	}
}

void APlayerCharacter::Server_PickupObjectInRange_Implementation()
{
// 	if (ObjectInHand)
// 	{
// 		// We already have something in our hand
// 		return;
// 	}
// 
// 	// Prioritize finding the closest object
// 	AActor* closestPickupActor = NULL;
// 	float closestDistance = 10000.f;
// 	for (AActor* pickupObject : ObjectsInRange)
// 	{
// 		if (this->GetDistanceTo(pickupObject) < closestDistance)
// 		{
// 			closestDistance = this->GetDistanceTo(pickupObject);
// 			closestPickupActor = pickupObject;
// 		}
// 	}
// 
// 	if (closestPickupActor)
// 	{
// 		// Can only be picked up if someone else isn't holding it already
// 		AODBevContainer* BevContainer = Cast<AODBevContainer>(closestPickupActor);
// 		if (BevContainer && BevContainer->GetIsBeingHeld())
// 		{
// 			UE_LOG(LogTemp, Warning, TEXT("Can't touch this"));
// 			return;
// 		}
// 
// 		// Attach the actor to our player
// 		ObjectInHand = closestPickupActor;
// 		if (ObjectInHand)
// 		{
// 			AODPickupActor* PickupActor = Cast<AODPickupActor>(ObjectInHand);
// 			if (PickupActor)
// 			{
// 				ObjectsInRange.Remove(ObjectInHand);
// 				PickupActor->HandlePickup(this);
// 				bIsHoldingObject = true;
// 				UpdateClosestPickupObject();
// 				
// 				AODPlayerController* ThisPlayerController = Cast<AODPlayerController>(GetController());
// 				if (ThisPlayerController && BevContainer)
// 				{
// 					ThisPlayerController->ShowContainerWidget(BevContainer);
// 				}		
// 				
// 				// Remove it from our interactable objects if needed
// 				if (InteractableBevContainersInRange.Contains(BevContainer))
// 				{
// 					InteractableBevContainersInRange.Remove(BevContainer);
// 				}
// 
// 				if (GetController()->IsLocalPlayerController())
// 				{
// 					OnRep_ObjectInHand();
// 				}
// 			}
// 
// 			// This is bad, needs refactor
// 			AODGarnish* GarnishActor = Cast<AODGarnish>(ObjectInHand);
// 			if (GarnishActor)
// 			{
// 				GarnishActor->HandlePickup(this);
// 				bIsHoldingObject = true;
// 				ObjectsInRange.Remove(ObjectInHand);
// 				UpdateClosestPickupObject();
// 			}
// 
// 			return;
// 		}
// 		else
// 		{
// 			return;
// 		}
// 	}
// 
// 	// Try to pickup something off the closest counter top
// 	if (ClosestCounter && ClosestCounter->GetIsCounterFull())
// 	{
// 		TArray<AActor*> attachedActors;
// 		ClosestCounter->GetAttachedActors(attachedActors, true);
// 			
// 		if (attachedActors.Num() > 0)
// 		{
// 			AODBevContainer* containerToPickup = Cast<AODBevContainer>(attachedActors[0]);
// 			if (containerToPickup)
// 			{
// 				ObjectInHand = attachedActors[0];
// 				bIsHoldingObject = true;
// 				containerToPickup->HandlePickup(this);
// 				return;
// 			}
// 		}
// 	}
	
	// If we got this far we probably haven't picked anything up!
}

void APlayerCharacter::Server_PickupSpecificObject_Implementation(AODPickupActor* ActorToPickup, bool bShouldPlaySound)
{
	if (!IsHoldingObject())
	{
		if (ActorToPickup)
		{
			ActorToPickup->HandlePickup(this);
			bIsHoldingObject = true;
			ObjectInHand = ActorToPickup;

			// Handle updating UI based on role
			// This checks if the server is running it for the player who is the server
			AODPlayerController* ThisPlayerController = Cast<AODPlayerController>(GetController());
			AODBevContainer* BevContainer = Cast<AODBevContainer>(ActorToPickup);
			if (ThisPlayerController && BevContainer)
			{
				ThisPlayerController->ShowContainerWidget(BevContainer);
			}

			// Remove it from the list of things we can currently interact with
			if (InteractActorsInRange.Contains(ActorToPickup))
			{
				InteractActorsInRange.Remove(ActorToPickup);
			}

			// Sound stuff
			if (bShouldPlaySound)
			{
				PlayPickupSound();
			}

			// Remind the server
			if (GetController()->IsLocalPlayerController())
			{
				OnRep_ObjectInHand();
			}
		}
	}
}

void APlayerCharacter::Server_PickupSpecificGarnish_Implementation(AODGarnish* GarnishToPickup)
{
	if (!IsHoldingObject())
	{
		if (GarnishToPickup)
		{
			GarnishToPickup->HandlePickup(this);
			bIsHoldingObject = true;
			ObjectInHand = GarnishToPickup;
		}
	}
}

void APlayerCharacter::Server_PickupCrawler_Implementation(AODAICrawler* CrawlerToPickup)
{
	if (CrawlerToPickup)
	{
		// Save for later if we don't have it already
		if (!CrawlerManager)
			CrawlerManager = CrawlerToPickup->GetManagerReference();

		// This kills the bug
		CrawlerToPickup->HandlePickup(this);

		// Spawn the CrawlerPickup object and hold that instead
		FActorSpawnParameters SpawnParams;
		AODSkelPickupActor* CrawlerPickup = GetWorld()->SpawnActor<AODSkelPickupActor>(CrawlerPickupBP, GetActorLocation(), FRotator(0.f), SpawnParams);
		if (CrawlerPickup)
		{
			CrawlerPickup->HandlePickup(this);
			ObjectInHand = CrawlerPickup;
		}

		bIsHoldingObject = true;
		CrawlerInRange = NULL;
	}
}

int32 APlayerCharacter::GetCustomDepthStencilValue()
{
	return GetMesh()->CustomDepthStencilValue;
}

void APlayerCharacter::Server_DropObject_Implementation(bool bWillBeAttaching, bool bShouldPlaySound)
{
	if (ObjectInHand)
	{
		if (IsHoldingBevContainer())
		{
			AODPlayerController* ThisPlayerController = Cast<AODPlayerController>(GetController());
			if (ThisPlayerController)
			{
				if (ThisPlayerController->IsLocalController())
				{
					// Server
					ThisPlayerController->ClearContainerWidget();
				}
				else
				{
					ThisPlayerController->ClearContainerWidget(); // TODO: Handle for client
				}
			}
		}

		// Drop the object
		AODPickupActor* PickupActor = Cast<AODPickupActor>(ObjectInHand);
		if (PickupActor)
		{
			PickupActor->HandleDrop();
		}

		AODSkelPickupActor* CrawlerPickup = Cast<AODSkelPickupActor>(ObjectInHand);
		if (CrawlerPickup)
		{
			// Kill the fake in our hand and spawn a new one
			CrawlerPickup->HandleDrop();

			if (CrawlerManager)
			{
				FVector PlacementLocation = GetActorLocation();
				FVector PlayerForwardVector = GetActorForwardVector();
				PlayerForwardVector *= 40.f;
				PlacementLocation += PlayerForwardVector;
				PlacementLocation.Z += 50.f;
				CrawlerManager->Server_SpawnCrawlerAtPosition(PlacementLocation);
			}
		}

		// Handle updating UI based on role
		// This checks if the server is running it for the player who is the server
		AODPlayerController* ThisPlayerController = Cast<AODPlayerController>(GetController());
		if (ThisPlayerController)
		{
			ThisPlayerController->HideContainerWidget();
		}

		Client_FinishDrop(bWillBeAttaching, ObjectInHand, bShouldPlaySound);
		
		ObjectInHand = NULL;
		bIsHoldingObject = false;

		if (HasAuthority())
		{
			OnRep_ObjectInHand();
		}
	}
}

void APlayerCharacter::Server_DropObjectAndDestroy_Implementation()
{
	if (ObjectInHand)
	{
		// Handle updating UI based on role
		// This checks if the server is running it for the player who is the server
		AODPlayerController* ThisPlayerController = Cast<AODPlayerController>(GetController());
		AODBevContainer* BevContainer = Cast<AODBevContainer>(ObjectInHand);
		if (ThisPlayerController && BevContainer)
		{
			ThisPlayerController->HideContainerWidget();
		}

		ObjectInHand->Destroy();
		ObjectInHand = NULL;
		bIsHoldingObject = false;
	}
}

void APlayerCharacter::Client_FinishDrop_Implementation(bool bWillBeAttaching, AActor* ObjectBeingDropped, bool bShouldPlaySound)
{
	AODSkelPickupActor* SkelPickupActor = Cast<AODSkelPickupActor>(ObjectBeingDropped);

	// Temp fix: Skel Pickup actor should not be added because it's going to be immediately destroyed
	if (!bWillBeAttaching && !SkelPickupActor)
	{
		InteractActorsInRange.AddUnique(ObjectBeingDropped);
	}

	if (bShouldPlaySound)
		PlayDropSound();
}

void APlayerCharacter::Server_GetGlassFromRack_Implementation()
{
	if (GlassRackInRange)
	{
		AODDrinkGlass* GlassToGrab = GlassRackInRange->GetTopGlass();

		if (GlassToGrab)
		{
			// Attach the actor to our player
			ObjectInHand = GlassToGrab;

			AODPickupActor* PickupActor = Cast<AODPickupActor>(ObjectInHand);
			if (PickupActor)
			{
				PickupActor->HandlePickup(this);
				bIsHoldingObject = true;

				// Handle updating UI based on role
				// This checks if the server is running it for the player who is the server
				AODPlayerController* ThisPlayerController = Cast<AODPlayerController>(GetController());
				AODBevContainer* BevContainer = Cast<AODBevContainer>(ObjectInHand);
				if (ThisPlayerController && BevContainer)
				{
					ThisPlayerController->ShowContainerWidget(BevContainer);

					// Remove it from our interactable objects if needed
					if (InteractableBevContainersInRange.Contains(BevContainer))
					{
						InteractableBevContainersInRange.Remove(BevContainer);
					}
				}
			}
		}
	}
}

void APlayerCharacter::Server_GetIngredientsFromDispenser_Implementation(AODIngredientDispenser* DispenserToUse)
{
	if (DispenserToUse)
	{
		if (ObjectInHand)
		{
			DispenserBeingUsed = DispenserToUse;

			// Make sure what we're holding can contain an ingredient
			AODBevContainer* BevContainer = Cast<AODBevContainer>(ObjectInHand);
			if (BevContainer && IsHoldingPourableContainer())
			{
				// Get initial level for fill sound later
				int32 CurrentFillLevel = FMath::FloorToInt(BevContainer->GetCurrentVolume());

				TPair<EIngredientType, float> IngredientToAdd = DispenserToUse->DispenseIngredient();
				BevContainer->Server_AddIngredient(IngredientToAdd.Key, IngredientToAdd.Value);

				// Play the sound if necessary
				if (!DispenserToUse->IsPlayingDispenseLoopSound())
				{
					DispenserToUse->StartDispenseSound();
				}

				AODPlayerController* ThisPlayerController = Cast<AODPlayerController>(GetController());
				if (ThisPlayerController)
				{
					ThisPlayerController->Client_RefreshMixerWidget();

					// Do we need to play a sound?
					int32 NewFillLevel = FMath::FloorToInt(BevContainer->GetCurrentVolume());
					if (NewFillLevel > CurrentFillLevel)
					{
						ThisPlayerController->Client_HandleFill(NewFillLevel);
					}
				}
			}
		}
	}
}

void APlayerCharacter::Server_StopDispensingIngredients_Implementation()
{
	if (DispenserBeingUsed)
	{
		if (DispenserBeingUsed->IsPlayingDispenseLoopSound())
		{
			DispenserBeingUsed->FinishDispenseSound();
		}
	}
}

void APlayerCharacter::Server_AddGarnishToContainer_Implementation(AODGarnish* GarnishToAdd, AODDrinkGlass* ToGlass)
{
	if (!GarnishToAdd || !ToGlass)
		return;

	// Add the garnish to the target glass
	ToGlass->Server_AddGarnish(GarnishToAdd->GetGarnishType(), 1);
	PlayAddGarnishSound(ToGlass->IsContainerEmpty());

	// Drop the object and destroy it
	Server_DropObjectAndDestroy();

	if (ODPlayerController)
	{
		ODPlayerController->Client_HandleAddGarnish();
	}
}

void APlayerCharacter::CapsuleColliderBeginOverlap(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor)
	{
		// If it's a camera zone we need to focus on it
		AODBarArea* barArea = Cast<AODBarArea>(OtherActor);
		if (barArea)
		{
			CameraLockPosition = barArea->GetActorLocation();
			bIsInBarArea = true;
		}
	}
}

void APlayerCharacter::CapsuleColliderEndOverlap(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (OtherActor)
	{
		// If it's a camera zone we need to focus back on our character
		AODBarArea* barArea = Cast<AODBarArea>(OtherActor);
		if (barArea)
		{
			CameraSpringArm->SetRelativeLocation(FVector(0, 0, 0), false);
			bIsInBarArea = false;
			CameraLockPosition = FVector(0, 0, 0);
		}
	}
}

void APlayerCharacter::PickupColliderBeginOverlap(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	AddInteractActorInRange(OtherActor);
}

void APlayerCharacter::PickupColliderEndOverlap(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	RemoveInteractActorInRange(OtherActor);
}

void APlayerCharacter::CounterColliderBeginOverlap(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	AddInteractActorInRange(OtherActor);
}

void APlayerCharacter::CounterColliderEndOverlap(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	RemoveInteractActorInRange(OtherActor);
}

void APlayerCharacter::AddInteractActorInRange(AActor* InteractActor)
{
	// If the actor is a pickup actor on a counter it should be ignored
	AODPickupActor* PickupActor = Cast<AODPickupActor>(InteractActor);
	if (PickupActor && PickupActor->IsAttached())
		return;

	// If it's an AI who is knocked out it should be ignored
	AODAICharacter* AICharacter = Cast<AODAICharacter>(InteractActor);
	if (AICharacter && !AICharacter->IsCustomerActive())
		return;

	if (InteractActor->Implements<UODInteractInterface>() && !InteractActorsInRange.Contains(InteractActor))
	{
		//UE_LOG(LogTemp, Warning, TEXT("Adding %s to the interact actors in range."), *InteractActor->GetName());
		InteractActorsInRange.AddUnique(InteractActor);
		UpdateClosestInteractActor();
	}
}

void APlayerCharacter::RemoveInteractActorInRange(AActor* InteractActor)
{
	// If the actor is a pickup actor attached to something else it should be ignored
	AODPickupActor* PickupActor = Cast<AODPickupActor>(InteractActor);
	if (PickupActor && PickupActor->IsAttached())
		return;

	if (InteractActor && InteractActor->Implements<UODInteractInterface>() && InteractActorsInRange.Contains(InteractActor))
	{
		IODInteractInterface::Execute_LoseRelevance(InteractActor);
		InteractActorsInRange.Remove(InteractActor);
		UpdateClosestInteractActor();
	}
}

void APlayerCharacter::UpdateClosestInteractActor()
{
	if (ClosestInteractActor && (InteractActorsInRange.Num() == 0))
	{
// 		if (!HasAuthority())
// 			UE_LOG(LogTemp, Warning, TEXT("CLIENT APlayerCharacter::UpdateClosestInteractActor(): No actors in range"));

		ClosestInteractActor = NULL;
	}
	else if (InteractActorsInRange.Num() == 1)
	{
// 		if (!HasAuthority())
// 			UE_LOG(LogTemp, Warning, TEXT("CLIENT APlayerCharacter::UpdateClosestInteractActor(): 1 actor in range"));

		ClosestInteractActor = InteractActorsInRange[0];

		// TODO: Do some stuff related to becoming the closest (highlighting or whatever)
		if (ClosestInteractActor->Implements<UODInteractInterface>())
		{
			IODInteractInterface::Execute_BecomeRelevant(ClosestInteractActor, this);
		}
	}
	else if (ClosestInteractActor)
	{
// 		if (!HasAuthority())
// 			UE_LOG(LogTemp, Warning, TEXT("CLIENT APlayerCharacter::UpdateClosestInteractActor(): ClosestInteractActor valid so recalculating"));

		// Find the best counter based on distance and the dot product (with dot product taking precedence)
		float ClosestDistance = MAX_int32;
		int32 ClosestIndex = -1;

		for (int32 i = 0; i < InteractActorsInRange.Num(); ++i)
		{
			AActor* InteractActor = InteractActorsInRange[i];
			float DistToActor = FVector::Dist(GetActorLocation(), InteractActor->GetActorLocation());

			if (DistToActor <= ClosestDistance)
			{
				if (ClosestDistance - DistToActor <= 40.f)
				{
					// If we're really close, check the angle between the two closest ones
					float ClosestAngle = UODHelperFunctions::AngleBetweenActors(this, InteractActorsInRange[ClosestIndex]);
					float ThisAngle = UODHelperFunctions::AngleBetweenActors(this, InteractActor);

					// Only update the new counter if our dot product is better
					if (ThisAngle < ClosestAngle)
					{
						// Tell the old one to handle not being closest anymore
						if (ClosestInteractActor->Implements<UODInteractInterface>())
						{
							IODInteractInterface::Execute_LoseRelevance(ClosestInteractActor);
						}

						ClosestDistance = DistToActor;
						ClosestInteractActor = InteractActor;
						ClosestIndex = i;
					}
				}
				else
				{
					// Tell the old one to handle not being closest anymore
					if (ClosestInteractActor->Implements<UODInteractInterface>())
					{
						IODInteractInterface::Execute_LoseRelevance(ClosestInteractActor);
					}

					// We're just basing this on distance
					ClosestDistance = DistToActor;
					ClosestInteractActor = InteractActor;
					ClosestIndex = i;
				}
			}
		}

		// Tell this new actor to handle being the closest now
		if (ClosestInteractActor->Implements<UODInteractInterface>())
		{
			IODInteractInterface::Execute_BecomeRelevant(ClosestInteractActor, this);
		}
	}
	else
	{
// 		if (!HasAuthority())
// 			UE_LOG(LogTemp, Warning, TEXT("CLIENT APlayerCharacter::UpdateClosestInteractActor(): Just setting to NULL"));

		ClosestInteractActor = NULL;
	}
}

void APlayerCharacter::UpdateClosestPickupObject()
{
	if (ClosestPickupActor && (ObjectsInRange.Num() == 0))
	{
		ClosestPickupActor = NULL;
	}
	else if (ClosestPickupActor)
	{
		float ClosestDistance = 99999.f;
		for (AActor* OtherObject : ObjectsInRange)
		{
			if (OtherObject)
			{
				float DistToObject = FVector::Dist(GetActorLocation(), OtherObject->GetActorLocation());
				if (DistToObject <= ClosestDistance)
				{
					// Clear the old close counter if we need to
					AODPickupActor* PickupActor = Cast<AODPickupActor>(OtherObject);
					if (PickupActor && PickupActor->IsHighlighted())
					{
						PickupActor->EndHighlight();
					}

					AODGarnish* GarnishActor = Cast<AODGarnish>(OtherObject);
					if (GarnishActor && GarnishActor->IsHighlighted())
					{
						GarnishActor->EndHighlight();
					}

					ClosestDistance = DistToObject;
					ClosestPickupActor = OtherObject;
				}
			}
		}

		AODPickupActor* PickupActor = Cast<AODPickupActor>(ClosestPickupActor);
		if (PickupActor && !PickupActor->IsHighlighted())
		{
			UE_LOG(LogTemp, Warning, TEXT("APlayerCharacter::UpdateClosestPickupObject(): Telling pickup object to highlight"));
			if (!IsHoldingObject())
				PickupActor->ShowPickupHighlight();
		}

// 		AODGarnish* GarnishActor = Cast<AODGarnish>(ClosestPickupActor);
// 		if (GarnishActor && !GarnishActor->IsHighlighted())
// 		{
// 			GarnishActor->ShowHighlight();
// 		}
	}
	else if ((ClosestPickupActor == NULL) & (ObjectsInRange.Num() == 1))
	{
		ClosestPickupActor = ObjectsInRange[0];
		
		AODPickupActor* PickupActor = Cast<AODPickupActor>(ClosestPickupActor);
		if (PickupActor && !PickupActor->IsHighlighted())
		{
			UE_LOG(LogTemp, Warning, TEXT("APlayerCharacter::UpdateClosestPickupObject(): Telling pickup object to highlight"));
			if (!IsHoldingObject())
				PickupActor->ShowPickupHighlight();
		}

// 		AODGarnish* GarnishActor = Cast<AODGarnish>(ClosestPickupActor);
// 		if (GarnishActor && !GarnishActor->IsHighlighted())
// 		{
// 			GarnishActor->ShowHighlight();
// 		}
	}
	else
	{
		ClosestPickupActor = NULL;
	}
}

void APlayerCharacter::UpdateClosestInteractableContainer()
{
	if (!IsHoldingBevContainer() && !IsHoldingGarnish())
		return;

	if (ClosestInteractableContainer && (InteractableBevContainersInRange.Num() == 0))
	{
		ClosestInteractableContainer = NULL;
	}
	else if (ClosestInteractableContainer)
	{
		float ClosestDistance = 99999.f;
		for (AODBevContainer* OtherContainer : InteractableBevContainersInRange)
		{
			if (OtherContainer)
			{
				float DistToObject = FVector::Dist(GetActorLocation(), OtherContainer->GetActorLocation());
				if (DistToObject <= ClosestDistance)
				{
					// Clear the old close counter if we need to
					if (OtherContainer && OtherContainer->IsHighlighted())
					{
						OtherContainer->EndHighlight();
					}

					ClosestDistance = DistToObject;
					ClosestInteractableContainer = OtherContainer;
				}
			}
		}

		if (ClosestInteractableContainer && !ClosestInteractableContainer->IsHighlighted())
		{
			UE_LOG(LogTemp, Warning, TEXT("APlayerCharacter::UpdateClosestInteractableContainer(): Telling interactable container to highlight"));
			ClosestInteractableContainer->ShowInteractHighlight();
		}
	}
	else if ((ClosestInteractableContainer == NULL) & (InteractableBevContainersInRange.Num() == 1))
	{
		ClosestInteractableContainer = InteractableBevContainersInRange[0];

		if (ClosestInteractableContainer && !ClosestInteractableContainer->IsHighlighted())
		{
			UE_LOG(LogTemp, Warning, TEXT("APlayerCharacter::UpdateClosestInteractableContainer(): Telling interactable container to highlight"));
			ClosestInteractableContainer->ShowInteractHighlight();
		}
	}
	else
	{
		ClosestInteractableContainer = NULL;
	}
}

void APlayerCharacter::UpdateClosestCounter()
{
	if (ClosestCounter && (CountersInRange.Num() == 0))
	{
		ClosestCounter = NULL;
	}
	else if (ClosestCounter)
	{
		// Find the best counter based on distance and the dot product (with dot product taking precedence)
		float closestDistance = 99999.f;
		int32 ClosestIndex = -1;

		for (int32 i = 0; i < CountersInRange.Num(); ++i)
		{
			AODCounter* counter = CountersInRange[i];
			FVector CounterAttachPoint = counter->GetDrinkAttachWorldLocation();
			float DistToCounter = FVector::Dist(GetActorLocation(), CounterAttachPoint);

			if (DistToCounter <= closestDistance)
			{
				if (closestDistance - DistToCounter <= 40.f)
				{
					// If we're really close, check the dot product between the two closest ones
					FVector ClosestCounterForward = CountersInRange[ClosestIndex]->GetCounterForwardVector() * -1.f;
					float ClosestDotResult = 1.f - FVector::DotProduct(GetActorForwardVector(), ClosestCounterForward); // We want to minimize this so subtract 1

					FVector CounterForward = counter->GetCounterForwardVector() * -1.f;
					float DotResult = 1.f - FVector::DotProduct(GetActorForwardVector(), CounterForward); // We want to minimize this so subtract 1

					// Only update the new counter if our dot product is better
					if (DotResult < ClosestDotResult)
					{
						if (ClosestCounter->IsHighlighted())
						{
							ClosestCounter->EndHighlight();
						}

						closestDistance = DistToCounter;
						ClosestCounter = counter;
						ClosestIndex = i;
					}

				}
				else
				{
					if (ClosestCounter->IsHighlighted())
					{
						ClosestCounter->EndHighlight();
					}

					// We're just basing this on distance
					closestDistance = DistToCounter;
					ClosestCounter = counter;
					ClosestIndex = i;
				}
			}
		}

		if (!ClosestCounter->IsHighlighted())
		{
			ClosestCounter->Highlight();
		}
	}
	else if ((ClosestCounter == NULL) & (CountersInRange.Num() == 1))
	{
		ClosestCounter = CountersInRange[0];
		ClosestCounter->Highlight();
	}
	else
	{
		ClosestCounter = NULL;
	}
}

void APlayerCharacter::OnRep_ObjectInHand()
{
	AODPlayerController* ThisPlayerController = Cast<AODPlayerController>(GetController());
	
	if (ObjectInHand)
	{
		// Remove it from our interactable objects if needed
		AODBevContainer* BevContainer = Cast<AODBevContainer>(ObjectInHand);
		if (BevContainer)
		{
			if (InteractableBevContainersInRange.Contains(BevContainer))
			{
				InteractableBevContainersInRange.Remove(BevContainer);
			}

			if (ThisPlayerController)
			{
				ThisPlayerController->ShowContainerWidget(BevContainer);
			}
		}
	}
	else
	{
		// We probably just dropped something if this is NULL
		if (ThisPlayerController && ThisPlayerController->IsLocalPlayerController())
		{
			ThisPlayerController->HideContainerWidget();
			UpdateClosestInteractActor();
		}
	}
}

void APlayerCharacter::RefreshDash()
{
	bCanDash = true;
}

bool APlayerCharacter::IsHoldingObject()
{
	return ObjectInHand ? true : false;
}

bool APlayerCharacter::IsHoldingBevContainer()
{
	AODBevContainer* BevContainer = Cast<AODBevContainer>(ObjectInHand);
	return BevContainer ? true : false;
}

bool APlayerCharacter::IsHoldingDrinkMixer()
{
	if (ObjectInHand)
	{
		AODDrinkMixer* DrinkMixer = Cast<AODDrinkMixer>(ObjectInHand);
		if (DrinkMixer)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	return false;
}

bool APlayerCharacter::IsHoldingIce()
{
	if (ObjectInHand)
	{
		AODIce* Ice = Cast<AODIce>(ObjectInHand);
		return Ice ? true : false;
	}

	return false;
}

bool APlayerCharacter::IsHoldingBlender()
{
	AODBlender* Blender = Cast<AODBlender>(ObjectInHand);
	return Blender ? true : false;
}

bool APlayerCharacter::IsHoldingPourableContainer()
{
	if (ObjectInHand)
	{
		AODBevContainer* BevContainer = Cast<AODBevContainer>(ObjectInHand);
		if (BevContainer)
		{
			switch (BevContainer->GetGlassType())
			{
				case EGlassType::GT_Brew:
					return true;
				case EGlassType::GT_Mixer:
					return true;
				case EGlassType::GT_Shot:
					return true;
				case EGlassType::GT_Stemmed:
					return false;
				case EGlassType::GT_Blender:
					return true;
				default:
					return false;
			}
		}
	}
	return false;
}

bool APlayerCharacter::IsHoldingDrinkGlass()
{
	if (ObjectInHand)
	{
		AODDrinkGlass* DrinkGlass = Cast<AODDrinkGlass>(ObjectInHand);
		if (DrinkGlass)
		{
			return true;
		}
	}
	return false;
}

bool APlayerCharacter::IsWashing()
{
	return bIsWashing;
}

bool APlayerCharacter::IsHoldingTaser()
{
	AODTaser* Taser = Cast<AODTaser>(ObjectInHand);
	return Taser ? true : false;
}

bool APlayerCharacter::IsHoldingCrawler()
{
	AODSkelPickupActor* AICrawler = Cast<AODSkelPickupActor>(ObjectInHand);
	return AICrawler ? true : false;
}

bool APlayerCharacter::GetIsInRangeOfDispenser()
{
	return bIsInRangeOfDispenser;
}

bool APlayerCharacter::HasInteractableContainerInRange()
{
	return (InteractableBevContainersInRange.Num() > 0) ? true : false;
}

bool APlayerCharacter::IsHoldingGarnish()
{
	AODGarnish* Garnish = Cast<AODGarnish>(ObjectInHand);
	if (Garnish)
	{
		return true;
	}
	return false;
}

bool APlayerCharacter::IsHoldingAggressiveObject()
{
	AODTaser* Taser = Cast<AODTaser>(ObjectInHand);
	AODDummyPickup* DummyPickup = Cast<AODDummyPickup>(ObjectInHand);
	if (Taser || DummyPickup)
	{
		return true;
	}
	else
	{
		return false;
	}
}

bool APlayerCharacter::HasDrinkMixerInRange()
{
	AODDrinkMixer* MixerInRange = Cast<AODDrinkMixer>(GetClosestInteractableBevContainer());
	if (MixerInRange)
	{
		UE_LOG(LogTemp, Warning, TEXT("Has a Drink mixer in range"));
		return true;
	}
	return false;
}

AODIngredientDispenser* APlayerCharacter::GetDispenserInRange()
{
	return (DispenserInRange && bIsHoldingObject) ? DispenserInRange : NULL;
}

AODWasteBin* APlayerCharacter::GetWasteBinInRange()
{
	return WasteBinInRange;
}

AODGlassRack* APlayerCharacter::GetGlassRackInRange()
{
	return GlassRackInRange;
}

AActor* APlayerCharacter::GetObjectInHand()
{
	return ObjectInHand;
}

AActor* APlayerCharacter::GetClosestInteractObject()
{
	if (InteractActorsInRange.Num() == 0)
	{
		return NULL;
	}
	else
	{
		float closestDistance = 99999.f;
		AActor* ClosestInteractObj = InteractActorsInRange[0];
		for (AActor* InteractObj : InteractActorsInRange)
		{
			if (GetDistanceTo(InteractObj) <= GetDistanceTo(ClosestInteractObj))
			{
				closestDistance = GetDistanceTo(InteractObj);
				ClosestInteractObj = InteractObj;
			}
		}
		return ClosestInteractObj;
	}
}

AODBevContainer* APlayerCharacter::GetClosestInteractableBevContainer()
{
	return ClosestInteractableContainer;
}

AODAICharacter* APlayerCharacter::GetClosestCustomerInRange()
{
	if (AICharactersInRange.Num() == 0)
	{
		return NULL;
	}
	else
	{
		float ClosestDistance = 99999.f;
		AODAICharacter* ClosestAICharacter = AICharactersInRange[0];
		for (AODAICharacter* AICharacter : AICharactersInRange)
		{
			float DistToNext = GetDistanceTo(AICharacter);
			if (DistToNext <= GetDistanceTo(ClosestAICharacter))
			{
				ClosestDistance = DistToNext;
				ClosestAICharacter = AICharacter;
			}
		}
		return ClosestAICharacter;
	}
}

AODAICrawler* APlayerCharacter::GetCrawlerInRange()
{
	return CrawlerInRange;
}

bool APlayerCharacter::IsInRangeOfCustomer()
{
	return (AICharactersInRange.Num() > 0) ? true : false;
}

bool APlayerCharacter::CanInteract()
{
	return ClosestInteractActor ? true : false;
}

void APlayerCharacter::Server_MixDrinkInHand_Implementation()
{
	AODDrinkMixer* DrinkMixerInHand = Cast<AODDrinkMixer>(ObjectInHand);
	if (DrinkMixerInHand && !DrinkMixerInHand->HasBeenFullyShaken())
	{
		// If we made a drink we need to update UI
		if (DrinkMixerInHand->ShakeMixer())
		{
			// Update for the server, the client will update after OnRep_CurrentDrink
			AODPlayerController* ThisPlayerController = Cast<AODPlayerController>(GetController());
			if (ThisPlayerController)
			{
				if (ThisPlayerController->IsLocalController())
				{
					ThisPlayerController->SetDrinkInWidget();
				}
			}
		}
	}
}

void APlayerCharacter::Server_SetShakeAxis_Implementation(float val)
{
	ShakeAxis = FMath::Clamp(val, -1.0f, 1.0f);
}

float APlayerCharacter::GetShakeAxis()
{
	return ShakeAxis;
}

bool APlayerCharacter::GetIsShaking()
{
	return bIsShaking;
}

void APlayerCharacter::Server_SetIsShaking_Implementation(bool val)
{
	bIsShaking = val;
}

bool APlayerCharacter::IsPouring()
{
	return bIsPouring;
}

void APlayerCharacter::Server_SetIsPouring_Implementation(bool val)
{
	bIsPouring = val;
}

void APlayerCharacter::Server_PourContentsInHand_Implementation()
{
	if (IsHoldingObject())
	{
		AODBevContainer* BevContainer = Cast<AODBevContainer>(ObjectInHand);
		
		if (WasteBinInRange && BevContainer)
		{
            if (!BevContainer->IsContainerEmpty())
            {
                PlayWasteSound();
            }

			BevContainer->Dump();
           
			return;
		}

		if (BevContainer)
		{
			if (HasInteractableContainerInRange())
			{
				AODBevContainer* InteractableContainer = GetClosestInteractableBevContainer();
				if (InteractableContainer && BevContainer)
				{
					if (!InteractableContainer->IsContainerEmpty())
					{
						// We can't pour on top of something else
						if (ODPlayerController)
						{
							ODPlayerController->Client_HandlePlayerError();
							return;
						}
					}

					UE_LOG(LogTemp, Warning, TEXT("APlayerCharacter::Server_PourContentsInHand got this far"));
					BevContainer->Server_TransferContents(InteractableContainer);

					// Drop the one we're using and grab the new one
					Server_DropObject(false, false);
					Server_PickupSpecificObject(InteractableContainer, false);

					// Update widget UI
					if (ODPlayerController)
					{
						ODPlayerController->Client_HandlePour();

						if (ODPlayerController->IsLocalController())
						{
							// Server
							ODPlayerController->RefreshMixerWidget();
						}
						else
						{
							ODPlayerController->Client_ClearMixerWidget();
						}
					}
				}
			}
		}
	}
}

void APlayerCharacter::Server_PourIntoContainer_Implementation(AODBevContainer* OtherContainer)
{
	if (IsHoldingBevContainer())
	{
		AODBevContainer* BevContainer = Cast<AODBevContainer>(ObjectInHand);
		if (BevContainer && OtherContainer)
		{
			BevContainer->Server_TransferContents(OtherContainer);

			// Drop the one we're using and grab the new one
			Server_DropObject(false, false);
			Server_PickupSpecificObject(OtherContainer, false);

			PlayPourSound();

			// Update widget UI
			if (ODPlayerController)
			{
				if (ODPlayerController->IsLocalController())
				{
					// Server
					ODPlayerController->RefreshMixerWidget();
				}
				else
				{
					ODPlayerController->Client_ClearMixerWidget();
				}
			}
		}
	}
}

void APlayerCharacter::Server_DumpContainerInHand_Implementation()
{
	if (IsHoldingBevContainer())
	{
		AODBevContainer* BevContainer = Cast<AODBevContainer>(ObjectInHand);
		if (BevContainer)
		{
			BevContainer->Dump();
			PlayWasteSound();
		}
	}
}

void APlayerCharacter::PlayPourSound_Implementation()
{
	if (PourDrinkSound)
	{
		FOnAkPostEventCallback NullCallback;
		UAkGameplayStatics::PostEvent(PourDrinkSound, this, 0, NullCallback);
	}
}

void APlayerCharacter::PlayPickupSound_Implementation()
{
	if (PickupSound)
	{
		FOnAkPostEventCallback NullCallback;
		UAkGameplayStatics::PostEvent(PickupSound, this, 0, NullCallback);
	}
}

void APlayerCharacter::PlayDropSound_Implementation()
{
	if (DropSound)
	{
		FOnAkPostEventCallback NullCallback;
		UAkGameplayStatics::PostEvent(DropSound, this, 0, NullCallback);
	}
}

void APlayerCharacter::PlayShowOrdersSound()
{
    if (ShowOrdersSound)
    {
        FOnAkPostEventCallback NullCallback;
        UAkGameplayStatics::PostEvent(ShowOrdersSound, this, 0, NullCallback);
    }
}

void APlayerCharacter::PlayHideOrdersSound()
{
    if (ShowOrdersSound)
    {
        FOnAkPostEventCallback NullCallback;
        UAkGameplayStatics::PostEvent(HideOrdersSound, this, 0, NullCallback);
    }
}

void APlayerCharacter::PlayErrorSound_Implementation()
{
	if (ErrorSound)
	{
		FOnAkPostEventCallback NullCallback;
		UAkGameplayStatics::PostEvent(ErrorSound, this, 0, NullCallback);
	}
}

void APlayerCharacter::PlayWasteSound_Implementation()
{
    if (WasteSound)
    {
        FOnAkPostEventCallback NullCallback;
        UAkGameplayStatics::PostEvent(WasteSound, this, 0, NullCallback);
    }
}

void APlayerCharacter::PlayWashSound()
{
    if (WashSound)
    {
        FOnAkPostEventCallback NullCallback;
        UAkGameplayStatics::PostEvent(WashSound, this, 0, NullCallback);
    }
}

void APlayerCharacter::PlayWashSoundStop()
{
    if (WashSoundStop)
    {
        FOnAkPostEventCallback NullCallback;
        UAkGameplayStatics::PostEvent(WashSoundStop, this, 0, NullCallback);
    }
}

void APlayerCharacter::PlayAddGarnishSound_Implementation(bool bIsGlassEmpty)
{
	FOnAkPostEventCallback NullCallback;
	if (bIsGlassEmpty)
	{
		if (AddGarnishEmptyGlassSound)
		{
			UAkGameplayStatics::PostEvent(AddGarnishEmptyGlassSound, this, 0, NullCallback);
		}
	}
	else
	{
		UAkGameplayStatics::PostEvent(AddGarnishSound, this, 0, NullCallback);
	}
}

void APlayerCharacter::PlayFillLevelSound(int32 FillLevel)
{
	if (FillMeterSound)
	{
		UAkGameplayStatics::SetRTPCValue(NULL, FillLevel, 1, this, "fill_counter_pitch");
		FOnAkPostEventCallback NullCallback;
		UAkGameplayStatics::PostEvent(FillMeterSound, this, 0, NullCallback);
	}
}

void APlayerCharacter::StartWashing()
{
	bIsWashing = true;

	Multi_PlayWashMontage();
   
    if (WashSound)
    {
        PlayWashSound();
    }
}

void APlayerCharacter::Server_FinishWashing_Implementation()
{
	bIsWashing = false;

	Multi_FinishWashMontage();
    PlayWashSoundStop();
}

void APlayerCharacter::Server_PlayAttackAnim_Implementation()
{
	Multi_PlayAttackAnim();
}

void APlayerCharacter::Multi_PlayAttackAnim_Implementation()
{
	if (AttackMontage)
	{
		PlayAnimMontage(AttackMontage);
	}
}

void APlayerCharacter::Multi_PlayWashMontage_Implementation()
{
	if (WashMontage)
	{
		PlayAnimMontage(WashMontage);
	}
}

void APlayerCharacter::Multi_FinishWashMontage_Implementation()
{
	if (WashMontage)
	{
		StopAnimMontage(WashMontage);
	}
}

void APlayerCharacter::ToggleShakeSound(bool bShouldBePlaying)
{
	if (bShouldBePlaying)
	{
		if (!bIsPlayingShakeSound)
		{
			// Should only play the sound if there's something in the container to make noise
			if (ObjectInHand)
			{
				AODDrinkMixer* DrinkMixer = Cast<AODDrinkMixer>(ObjectInHand);
				if (DrinkMixer && DrinkMixer->GetCurrentVolume() > 0.f)
				{
					FOnAkPostEventCallback NullCallback;
					ShakeSoundID = UAkGameplayStatics::PostEvent(ShakeSound, this, 0, NullCallback);
					bIsPlayingShakeSound = true;
				}
			}
		}
	}
	else
	{
		if (bIsPlayingShakeSound)
		{
			UAkGameplayStatics::ExecuteActionOnPlayingID(AkActionOnEventType::Stop, ShakeSoundID, 500);
			bIsPlayingShakeSound = false;
		}
	}
}

void APlayerCharacter::UpdateShakeSpeed(float speed)
{
	ShakeSpeed = FMath::Clamp(speed, 0.f, 3.0f);
}

void APlayerCharacter::Server_UseTaser_Implementation(AODAICharacter* CharacterToTase)
{
	UE_LOG(LogTemp, Warning, TEXT("APlayerCharacter::Server_UseTaser()"));
	AODTaser* Taser = Cast<AODTaser>(ObjectInHand);
	if (Taser)
	{
		FTimerHandle TaserTimerHandle;
		FTimerDelegate TaserTimerDelegate = FTimerDelegate::CreateUObject(this, &APlayerCharacter::TaserDelay, CharacterToTase);
		GetWorldTimerManager().SetTimer(TaserTimerHandle, TaserTimerDelegate, 0.25f, false);

		Multi_PlayAttackAnim();
	}
}

void APlayerCharacter::TaserDelay_Implementation(AODAICharacter* CharacterToTase)
{
	UE_LOG(LogTemp, Warning, TEXT("APlayerCharacter::TaserDelay()"));
	AODTaser* Taser = Cast<AODTaser>(ObjectInHand);
	if (Taser)
	{
		Taser->UseTaser();
	}

	if (HasAuthority())
	{
		if (CharacterToTase)
		{
			CharacterToTase->HandleTaser();
		}
	}

	RemoveInteractActorInRange(CharacterToTase);
	UpdateClosestInteractActor();
}
