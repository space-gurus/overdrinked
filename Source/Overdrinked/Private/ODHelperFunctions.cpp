// Copyright 2020 Space Gurus


#include "ODHelperFunctions.h"
#include "Engine/DataTable.h"
#include "ODEnumHelperFunctions.h"
#include "Data/IngredientData.h"
#include "Actors/ODDrinkGlass.h"
#include "Gameplay/ODMixMethod.h"

FLinearColor UODHelperFunctions::BuildDrinkColor(TArray<EIngredientType> IngredientTypes, TArray<int32> IngredientAmounts, UDataTable* IngredientDataTable)
{
	FLinearColor FinalColor = FLinearColor(0, 0, 0);
	float NormalizeColorTotal = 0.f;

	int32 TotalAmount = 0;

	if (IngredientDataTable)
	{
		for (int32 i = 0; i < IngredientTypes.Num(); ++i)
		{
			FIngredientData* IngredientData = IngredientDataTable->FindRow<FIngredientData>(UODEnumHelperFunctions::IngredientEnumToString(IngredientTypes[i]), FString(""));
			if (IngredientData)
			{
				FLinearColor ThisColor = IngredientData->Color.ReinterpretAsLinear();

				NormalizeColorTotal += ThisColor.R;
				NormalizeColorTotal += ThisColor.G;
				NormalizeColorTotal += ThisColor.B;

				int32 IngredientAmount = IngredientAmounts[i];

				ThisColor.R = ThisColor.R * IngredientAmount;
				ThisColor.G = ThisColor.G * IngredientAmount;
				ThisColor.B = ThisColor.B * IngredientAmount;

				TotalAmount += IngredientAmount;
				FinalColor += ThisColor;
			}

		}
	}

	NormalizeColorTotal = NormalizeColorTotal * TotalAmount;
	FinalColor.R = FinalColor.R / NormalizeColorTotal;
	FinalColor.G = FinalColor.G / NormalizeColorTotal;
	FinalColor.B = FinalColor.B / NormalizeColorTotal;
	FinalColor.A = 1.0f; // why
	return FinalColor;
}

EDrinkScore UODHelperFunctions::ScoreDrink(FDrinkOrder DrinkOrder, AODDrinkGlass* Drink)
{
	int32 PossibleScore = 5; // This is the target goal to be populated first (+2 for glass and +3 for mix method)
	for (EIngredientType Ingredient : DrinkOrder.DesiredIngredients)
	{
		PossibleScore += DrinkOrder.DesiredIngredientAmounts[DrinkOrder.DesiredIngredients.IndexOfByKey(Ingredient)];
	}

	for (EGarnishType Garnish : DrinkOrder.DesiredGarnishes)
	{
		PossibleScore += DrinkOrder.DesiredGarnishAmounts[DrinkOrder.DesiredGarnishes.IndexOfByKey(Garnish)];
	}

	UE_LOG(LogTemp, Warning, TEXT("Possible score: %i"), PossibleScore);

	int32 TotalScore = 0;
	FContainerContents DrinkContents = Drink->GetContainerContents();
	// Ingredients
	for (int IngIndex = 0; IngIndex < DrinkContents.IngredientTypes.Num(); ++IngIndex)
	{
		EIngredientType IngredientType = DrinkContents.IngredientTypes[IngIndex];
		int32 IngredientAmount = DrinkContents.IngredientAmounts[IngIndex];

		if (DrinkOrder.DesiredIngredients.Contains(IngredientType))
		{
			// +1 for each correct ingredient amount
			if (IngredientAmount == DrinkOrder.DesiredIngredientAmounts[DrinkOrder.DesiredIngredients.IndexOfByKey(IngredientType)])
			{
				TotalScore += IngredientAmount;
			}
			else
			{
				int32 ScoreDiff = FMath::Abs(DrinkOrder.DesiredIngredientAmounts[DrinkOrder.DesiredIngredients.IndexOfByKey(IngredientType)] - IngredientAmount);
				TotalScore += (IngredientAmount - ScoreDiff);
			}
		}
		else
		{
// We aren't even supposed to have this ingredient (penalty)
TotalScore -= 2;
		}
	}

	// Garnishes
	for (int GarIndex = 0; GarIndex < DrinkContents.GarnishTypes.Num(); ++GarIndex)
	{
		EGarnishType GarnishType = DrinkContents.GarnishTypes[GarIndex];
		int32 GarnishAmount = DrinkContents.GarnishAmounts[GarIndex];

		if (DrinkOrder.DesiredGarnishes.Contains(GarnishType))
		{
			// +1 for each correct garnish
			if (GarnishAmount == DrinkOrder.DesiredGarnishAmounts[DrinkOrder.DesiredGarnishes.IndexOfByKey(GarnishType)])
			{
				TotalScore += GarnishAmount;
			}
			else
			{
				int32 ScoreDiff = FMath::Abs(DrinkOrder.DesiredGarnishAmounts[DrinkOrder.DesiredGarnishes.IndexOfByKey(GarnishType)] - GarnishAmount);
				TotalScore += (GarnishAmount - ScoreDiff);
			}
		}
		else
		{
			// We aren't even supposed to have this garnish (penalty)
			TotalScore -= 2;
		}
	}

	// Mix method

	if (DrinkOrder.DesiredMixMethod == DrinkContents.MixMethod)
	{
		TotalScore += 3;
	}
	else
	{
		TotalScore -= 3;
	}

	// Glass
	if (DrinkOrder.DesiredGlass == Drink->GetGlassType())
	{
		TotalScore += 2;
	}
	else
	{
		TotalScore -= 2;
	}

	UE_LOG(LogTemp, Warning, TEXT("Total score: %i"), TotalScore);

	// Now normalize and grade the drink
	float FinalDrinkValue = (float)TotalScore / PossibleScore;
	UE_LOG(LogTemp, Warning, TEXT("Final Drink Value: %f"), FinalDrinkValue);

	if (FinalDrinkValue >= 0.95f && FinalDrinkValue <= 1.f)
	{
		return EDrinkScore::DS_A;
	}
	else if (FinalDrinkValue >= 0.8f && FinalDrinkValue < 0.95f)
	{
		return EDrinkScore::DS_B;
	}
	else if (FinalDrinkValue >= 0.7f && FinalDrinkValue < 0.8f)
	{
		return EDrinkScore::DS_C;
	}
	else if (FinalDrinkValue >= 0.6f && FinalDrinkValue < 0.7f)
	{
		return EDrinkScore::DS_D;
	}
	else
	{
		return EDrinkScore::DS_F;
	}
}

TArray<int32> UODHelperFunctions::ConvertFloatArrayToInt(TArray<float> InArray)
{
	TArray<int32> OutArray;
	for (float val : InArray)
	{
		OutArray.Add(FMath::RoundToInt(val));
	}

	return OutArray;
}

float UODHelperFunctions::AngleBetweenActors(AActor* ActorA, AActor* ActorB)
{
	// First normalize
	FVector ForwardA = ActorA->GetActorForwardVector();
	if (ForwardA.Normalize())
	{
		// Find the vector to the other actor and normalize that
		FVector VecToB = ActorB->GetActorLocation() - ActorA->GetActorLocation();
		if (VecToB.Normalize())
		{
			// Do the math
			float DotProduct = FVector::DotProduct(ForwardA, VecToB);
			float AngleResult = FMath::Acos(DotProduct);
			return AngleResult;
		}
	}
	return -1.f;
}

FLinearColor UODHelperFunctions::GetComplementaryColor(FLinearColor InColor)
{
	return FLinearColor(255 - InColor.R, 255 - InColor.G, 255 - InColor.B);
}
