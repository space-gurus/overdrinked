// Copyright 2020 Space Gurus


#include "ODPlayerState.h"
#include "Net/UnrealNetwork.h"

int32 AODPlayerState::GetPlayerNum()
{
	return PlayerNum;
}

void AODPlayerState::SetPlayerNum(int32 PNum)
{
	PlayerNum = PNum;
}

void AODPlayerState::BeginPlay()
{
	Super::BeginPlay();

	if (HasAuthority())
	{
		UE_LOG(LogTemp, Warning, TEXT("SERVER ODPlayerState BeginPlay() for %s"), *GetName());
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("CLIENT ODPlayerState BeginPlay() for %s"), *GetName());
	}

	UE_LOG(LogTemp, Warning, TEXT("PlayerName %s"), *GetPlayerName());
}

void AODPlayerState::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AODPlayerState, PlayerNum);
	DOREPLIFETIME(AODPlayerState, bIsPlayerReady);
}

void AODPlayerState::CopyProperties(APlayerState* PlayerState)
{
	Super::CopyProperties(PlayerState);

	UE_LOG(LogTemp, Warning, TEXT("CopyProperties()"));

	if (PlayerState)
	{
		AODPlayerState* OtherPlayerState = Cast<AODPlayerState>(PlayerState);
		if (OtherPlayerState)
		{
			OtherPlayerState->SetPlayerNum(PlayerNum);
		}
	}
}

void AODPlayerState::OverrideWith(APlayerState* PlayerState)
{
	Super::OverrideWith(PlayerState);

	UE_LOG(LogTemp, Warning, TEXT("OverrideWith()"));

	if (PlayerState)
	{
		AODPlayerState* OtherPlayerState = Cast<AODPlayerState>(PlayerState);
		if (OtherPlayerState)
		{
			PlayerNum = OtherPlayerState->GetPlayerNum();
		}
	}
}
