// Copyright 2020 Space Gurus


#include "ODMenuPawn.h"

AODMenuPawn::AODMenuPawn()
{
	PrimaryActorTick.bCanEverTick = false;

}

void AODMenuPawn::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called to bind functionality to input
void AODMenuPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

