// Copyright 2020 Space Gurus


#include "ODMultiplayerGameState.h"
#include "Net/UnrealNetwork.h"
#include "Kismet/GameplayStatics.h"
#include "ODMultiplayerGameMode.h"
#include "ODPlayerController.h"
#include "AI/ODAICharacter.h"
#include "AI/ODAIManager.h"
#include "UI/ODInGameHUD.h"
#include "Actors/ODDrinkGlass.h"
#include "Gameplay/ODDrink.h"
#include "Environment/ODGlobalSoundActor.h"
#include "ODHelperFunctions.h"
#include "ODEnumHelperFunctions.h"

AODMultiplayerGameState::AODMultiplayerGameState()
{
	bReplicates = true;
}

void AODMultiplayerGameState::BeginPlay()
{
	Super::BeginPlay();
}

void AODMultiplayerGameState::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AODMultiplayerGameState, TeamPoints);
	DOREPLIFETIME(AODMultiplayerGameState, MaxNumOrders);
	DOREPLIFETIME(AODMultiplayerGameState, NumActiveOrders);
	DOREPLIFETIME(AODMultiplayerGameState, OpenOrders);
	DOREPLIFETIME(AODMultiplayerGameState, CompletedOrders);
	DOREPLIFETIME(AODMultiplayerGameState, TotalLevelTime);
	DOREPLIFETIME(AODMultiplayerGameState, TimeRemainingInSeconds);
	DOREPLIFETIME(AODMultiplayerGameState, StarThresholds);
	DOREPLIFETIME(AODMultiplayerGameState, ThreeStarThreshold);
	DOREPLIFETIME(AODMultiplayerGameState, BrawlPercentage);
}

int32 AODMultiplayerGameState::GetTimeRemainingInSeconds()
{
	return TimeRemainingInSeconds;
}

float AODMultiplayerGameState::GetFractionTimeRemaining()
{
	if (TotalLevelTime > 0)
	{
		return (float)((TotalLevelTime - TimeRemainingInSeconds) / (float)TotalLevelTime);
	}
	else
	{
		return 0;
	}
}

bool AODMultiplayerGameState::CanHandleNewOrder()
{
	return NumActiveOrders < MaxNumOrders;
}

void AODMultiplayerGameState::SetGlobalSoundActor(AODGlobalSoundActor* SoundActor)
{
	if (SoundActor)
	{
		GlobalSoundActor = SoundActor;
	}
}

void AODMultiplayerGameState::StartLevelTimer_Implementation(int32 LevelLengthInMinutes)
{
	AODMultiplayerGameMode* MPGameMode = Cast<AODMultiplayerGameMode>(UGameplayStatics::GetGameMode(GetWorld()));
	if (MPGameMode)
	{
		// Setup our time
		TotalLevelTime = LevelLengthInMinutes * 60;
		TotalLevelTime += 3.0f; // Add 3 seconds for the countdown
		TimeRemainingInSeconds = TotalLevelTime;

		// get some necessary variables
		StarThresholds = MPGameMode->GetStarThresholds();
		if (StarThresholds.IsValidIndex(2))
		{
			ThreeStarThreshold = StarThresholds[2];
		}

		GetWorldTimerManager().ClearTimer(ServerLevelTimerHandle);
		GetWorldTimerManager().SetTimer(ServerLevelTimerHandle, this, &AODMultiplayerGameState::DecreaseTimeRemaining, MPGameMode->GetGameSpeed(), false);

		FTimerHandle CountdownTimerHandle;
		GetWorldTimerManager().SetTimer(CountdownTimerHandle, this, &AODMultiplayerGameState::GameStarted, 3.f, false);
	}
}

void AODMultiplayerGameState::OnRep_NumActiveOrders()
{
}

void AODMultiplayerGameState::OnRep_TeamPoints()
{
	AODPlayerController* LocalPC = Cast<AODPlayerController>(UGameplayStatics::GetPlayerController(GetWorld(), 0));
	if (LocalPC)
	{
		UODInGameHUD* InGameHUD = LocalPC->GetInGameHUD();
		if (InGameHUD)
		{
			InGameHUD->SetTips(TeamPoints);
		}
	}
}

int32 AODMultiplayerGameState::GetTeamPoints()
{
	return TeamPoints;
}

int32 AODMultiplayerGameState::GetNumActiveOrders()
{
	return NumActiveOrders;
}

TArray<FDrinkOrder> AODMultiplayerGameState::GetOpenOrders()
{
	return OpenOrders;
}

void AODMultiplayerGameState::Server_AddOpenOrder_Implementation(FDrinkOrder DrinkOrder)
{
	for (int i = 0; i < DrinkOrder.DesiredIngredients.Num(); ++i)
	{
		UE_LOG(LogTemp, Log, TEXT("AODMultiplayerGameState::Server_AddOpenOrder(): %i of %s"), DrinkOrder.DesiredIngredientAmounts[i],
			*UODEnumHelperFunctions::IngredientEnumToString(DrinkOrder.DesiredIngredients[i]).ToString());
	}

 	OpenOrders.Add(DrinkOrder);
 	NumActiveOrders++;

	// Notify the clients to update their UI
	// AddOpenOrder() always runs on the server so shouldn't have a problem getting GameMode
	AODMultiplayerGameMode* MPGameMode = Cast<AODMultiplayerGameMode>(UGameplayStatics::GetGameMode(GetWorld()));
	if (MPGameMode)
	{
		MPGameMode->NotifyPlayersOfNewOrder(DrinkOrder);

		if (GlobalSoundActor)
		{
			GlobalSoundActor->PlayTakeOrder();
		}
	}

	UpdateAIManager(true);
}

 void AODMultiplayerGameState::Server_RemoveOpenOrder_Implementation(FDrinkOrder DrinkOrder, bool bWasDelivered, AODAICharacter* Customer)
 {
	OpenOrders.Remove(DrinkOrder);
	NumActiveOrders--;

	if (bWasDelivered)
	{
		// Was it to the right customer?
		if (DrinkOrder.OrderOwner == Customer)
		{
			CompletedOrders.Add(DrinkOrder);
			TPair<int32, EOrderStatus> DrinkResults = CalculatePoints(DrinkOrder, Customer);
			DrinkOrder.Tip = DrinkResults.Key;
			DrinkOrder.OrderStatus = DrinkResults.Value;

			if (GlobalSoundActor)
			{
				GlobalSoundActor->PlayDrinkDeliveryNoise(EFeedbackType::FT_Great);

				// Random chance of playing a voice line

			}
		}
		else
		{
			// Wrong person
			DrinkOrder.OrderStatus = EOrderStatus::OS_WrongCustomer;
			DrinkOrder.Tip = 0;
			FailedOrders.Add(DrinkOrder);
		}
	}
	else
	{
		// They ran out of time
		if (DrinkOrder.OrderStatus != EOrderStatus::OS_Tasered)
		{
			DrinkOrder.OrderStatus = EOrderStatus::OS_Expired;
		}
		DrinkOrder.Tip = 0;
		FailedOrders.Add(DrinkOrder);
	}

	 // Notify the clients to update their UI
	 // RemoveOpenOrder() always runs on the server so shouldn't have a problem getting GameMode
	 AODMultiplayerGameMode* MPGameMode = Cast<AODMultiplayerGameMode>(UGameplayStatics::GetGameMode(GetWorld()));
	 if (MPGameMode)
	 {
		 MPGameMode->NotifyPlayersOfClosedOrder(DrinkOrder);
	 }

	 UpdateAIManager(false);
 }

 bool AODMultiplayerGameState::HasGameStarted()
 {
	 return bHasGameStarted;
 }

 void AODMultiplayerGameState::GameStarted()
 {
	 bHasGameStarted = true;
 }

 void AODMultiplayerGameState::SetBrawlPercentage(float BrawlPerc)
 {
	 BrawlPercentage = BrawlPerc;

	 AODMultiplayerGameMode* MPGameMode = Cast<AODMultiplayerGameMode>(UGameplayStatics::GetGameMode(GetWorld()));
	 if (MPGameMode)
	 {
		 MPGameMode->HandleBrawlLevelUpdate(BrawlPercentage);
	 }
 }

 void AODMultiplayerGameState::DecreaseTimeRemaining()
 {
	 AODMultiplayerGameMode* MPGameMode = Cast<AODMultiplayerGameMode>(UGameplayStatics::GetGameMode(GetWorld()));
	 
	 if (HasAuthority() && MPGameMode)
	 {
		 if (!MPGameMode->IsGameTimePaused())
			TimeRemainingInSeconds--;

		 if (TimeRemainingInSeconds <= 0)
		 {
			 MPGameMode->HandleLevelEnd();
		 }
		 else
		 {
			 GetWorldTimerManager().ClearTimer(ServerLevelTimerHandle);
			 GetWorldTimerManager().SetTimer(ServerLevelTimerHandle, this, &AODMultiplayerGameState::DecreaseTimeRemaining, MPGameMode->GetGameSpeed(), false);
		 }
	 }
 }

TPair<int32, EOrderStatus> AODMultiplayerGameState::CalculatePoints(FDrinkOrder DrinkOrder, AODAICharacter* Customer)
{
	TPair<int32, EOrderStatus> OrderPair;
	int32 PointsToAdd = 0;
	OrderPair.Key = 0;
	OrderPair.Value = EOrderStatus::OS_DeliveredBad;

	if (Customer)
	{
		AODDrinkGlass* CustomerDrink = Customer->GetDrinkInHand();
		if (CustomerDrink)
		{
			EDrinkScore DrinkScore = UODHelperFunctions::ScoreDrink(DrinkOrder, CustomerDrink);

			// This double enum thing is so bad, fix me later
			switch (DrinkScore)
			{
				case EDrinkScore::DS_A:
					PointsToAdd += 8;
					OrderPair.Value = EOrderStatus::OS_DeliveredPerfect;
					break;
				case EDrinkScore::DS_B:
					PointsToAdd += 6;
					OrderPair.Value = EOrderStatus::OS_DeliveredGreat;
					break;
				case EDrinkScore::DS_C:
					PointsToAdd += 3;
					OrderPair.Value = EOrderStatus::OS_DeliveredMediocre;
					break;
				case EDrinkScore::DS_D:
					PointsToAdd += 2;
					OrderPair.Value = EOrderStatus::OS_DeliveredBad;
					break;
				case EDrinkScore::DS_F:
					PointsToAdd += 1;
					OrderPair.Value = EOrderStatus::OS_DeliveredBad;
					break;
				default:
					break;
			}
		}

		// Time bonus
		float TimeLeftOnOrder = DrinkOrder.OrderPrepTimer - Customer->GetOrderTimeElapsed();
		int32 TimeLeftBonusPoints = (int32)((TimeLeftOnOrder / DrinkOrder.OrderPrepTimer) * 2);
		PointsToAdd += TimeLeftBonusPoints;
	}

	TeamPoints += PointsToAdd;
	if (HasAuthority())
	{
		OnRep_TeamPoints();
	}

	OrderPair.Key = PointsToAdd;
	return OrderPair;
 }

 void AODMultiplayerGameState::UpdateAIManager(bool bOrderAdded)
 {
	 // Notify the AI manager that there's a new order
	 TArray<AActor*> FoundActors;
	 UGameplayStatics::GetAllActorsOfClass(GetWorld(), AODAIManager::StaticClass(), FoundActors);
	 if (FoundActors.Num() > 0)
	 {
		 AODAIManager* AIManager = Cast<AODAIManager>(FoundActors[0]);
		 if (AIManager)
		 {
			 if (bOrderAdded)
			 {
				 AIManager->AddOrder();
			 }
			 else
			 {
				 AIManager->RemoveOrder();
			 }
		 }
	 }
 }
