// Copyright 2020 Space Gurus


#include "Actors/ODBlender.h"
#include "Net/UnrealNetwork.h"
#include "Materials/MaterialInstanceDynamic.h"
#include "ODHelperFunctions.h"
#include "PlayerCharacter.h"

AODBlender::AODBlender(const FObjectInitializer& OI) : Super(OI)
{
	bReplicates = true;

	// Defaults
	MaxVolume = 12.f;
	GlassType = EGlassType::GT_Blender;

	ContainerContents.bIsMixed = false;
}

void AODBlender::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AODBlender, BlendProgress);
}

void AODBlender::BeginPlay()
{
	Super::BeginPlay();

	// Setup the material instances and start the container as empty
	GlassMaterialDynamic = StaticMesh->CreateDynamicMaterialInstance(0);
	if (GlassMaterialDynamic)
	{
		StaticMesh->SetMaterial(0, GlassMaterialDynamic);
		GlassMaterialDynamic->SetScalarParameterValue(TEXT("DrinkFillLevel"), 0.f);
	}

	FillMaterialDynamic = StaticMesh->CreateDynamicMaterialInstance(1);
	if (FillMaterialDynamic)
	{
		StaticMesh->SetMaterial(1, FillMaterialDynamic);
		FillMaterialDynamic->SetScalarParameterValue(TEXT("DrinkFillLevel"), 0.f);
	}
}

void AODBlender::Server_AddIngredient_Implementation(EIngredientType IngredientToAdd, float Amount)
{
	Super::Server_AddIngredient_Implementation(IngredientToAdd, Amount);

	OnRep_CurrentVolume();

	BlendProgress = 0.f;
	ContainerContents.bIsMixed = false;
	ContainerContents.MixMethod = EMixMethod::MM_Straight;
}

void AODBlender::Server_AddContainerContents_Implementation(FContainerContents NewContents, bool bIsAddition)
{
	Super::Server_AddContainerContents_Implementation(NewContents, bIsAddition);

	// Pouring back into a blender resets the mix
	BlendProgress = 0.f;
	ContainerContents.bIsMixed = false;
	ContainerContents.MixMethod = EMixMethod::MM_Straight;
}

void AODBlender::PlayerInteraction_Implementation(APlayerCharacter* Player)
{
	if (!Player)
		return;

	if (Player->IsHoldingGarnish())
	{
		Player->PlayErrorSound();
		return;
	}
	else
	{
		Super::PlayerInteraction_Implementation(Player);
	}
}

void AODBlender::BecomeRelevant_Implementation(APlayerCharacter* PlayerTriggered)
{
	Super::BecomeRelevant_Implementation(PlayerTriggered);

	if (PlayerTriggered && PlayerTriggered->IsHoldingGarnish())
		EndHighlight();
}

bool AODBlender::CanBeBlended()
{
	return CurrentVolume > 0;
}

float AODBlender::GetBlendProgress()
{
	return BlendProgress;
}

void AODBlender::SetBlendProgress(float val)
{
	BlendProgress = val;

	if (BlendProgress >= 1.f)
	{
		ContainerContents.bIsMixed = true;
		ContainerContents.MixMethod = EMixMethod::MM_Blend;
		UE_LOG(LogTemp, Warning, TEXT("Should now be BLENDED"));
	}
}

float AODBlender::GetOverflowProgress()
{
	return OverflowProgress;
}

void AODBlender::SetOverflowProgress(float val)
{
	OverflowProgress = val;

	if (OverflowProgress >= 1.f)
	{
		// Handle a full overflow
		ContainerContents.bIsMixed = false;
		ContainerContents.MixMethod = EMixMethod::MM_Straight;
		OverflowProgress = 0.f;
		BlendProgress = 0.f;
		Dump();
	}
}

void AODBlender::AdjustLiquidMaterial()
{
	MaterialFillAmount = FMath::Clamp(CurrentVolume / MaxVolume, 0.f, 1.f);

	if (!FillMaterialDynamic)
	{
		UE_LOG(LogTemp, Error, TEXT("No GlassMaterialDynamic in AdjustLiquidMateril"));
	}

	if (!FillMaterialDynamic)
	{
		UE_LOG(LogTemp, Error, TEXT("No FillMaterialDynamic in AdjustLiquidMateril"));
	}
	
	if (!IngredientDataTable)
	{
		UE_LOG(LogTemp, Error, TEXT("No IngredientDataTable in AdjustLiquidMateril"));
	}

	if (GlassMaterialDynamic && FillMaterialDynamic && IngredientDataTable)
	{
		// If there's a drink we need to pull the properties
		TArray<int32> RoundedIngredients;
		for (float i : ContainerContents.IngredientAmounts)
		{
			RoundedIngredients.Add(FMath::Clamp(FMath::RoundToInt(i), 1, 10));
		}

		FLinearColor NewDrinkColor = UODHelperFunctions::BuildDrinkColor(ContainerContents.IngredientTypes, RoundedIngredients, IngredientDataTable);
		LiquidColor = NewDrinkColor;

		GlassMaterialDynamic->SetVectorParameterValue(TEXT("DrinkColor"), NewDrinkColor);
		GlassMaterialDynamic->SetScalarParameterValue(TEXT("DrinkFillLevel"), MaterialFillAmount);
		FillMaterialDynamic->SetVectorParameterValue(TEXT("DrinkColor"), NewDrinkColor);
		FillMaterialDynamic->SetScalarParameterValue(TEXT("DrinkFillLevel"), MaterialFillAmount);
	}
}

void AODBlender::OnRep_CurrentVolume()
{
	Super::OnRep_CurrentVolume();

	AdjustLiquidMaterial();
}

