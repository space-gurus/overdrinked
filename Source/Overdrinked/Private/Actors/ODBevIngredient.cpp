// Copyright 2020 Space Gurus

#include "Actors/ODBevIngredient.h"

// Sets default values
AODBevIngredient::AODBevIngredient()
{
	PrimaryActorTick.bCanEverTick = false;

}

// Called when the game starts or when spawned
void AODBevIngredient::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AODBevIngredient::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

FString AODBevIngredient::GetIngredientType()
{
	return IngredientType;
}

