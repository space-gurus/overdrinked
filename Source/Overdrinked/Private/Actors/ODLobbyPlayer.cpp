// Copyright 2020 Space Gurus


#include "Actors/ODLobbyPlayer.h"
#include "Components/SkeletalMeshComponent.h"
#include "Components/WidgetComponent.h"
#include "UI/ODLobbyPlayerWidget.h"
#include "Net/UnrealNetwork.h"
#include "Actors/ODLobbyManager.h"
#include "ODMenuPlayerController.h"

AODLobbyPlayer::AODLobbyPlayer(const FObjectInitializer& OI) : Super(OI)
{
	PrimaryActorTick.bCanEverTick = false;
	bReplicates = true;

// 	ConstructorHelpers::FClassFinder<UODLobbyPlayerWidget> LobbyWidgetBP(TEXT("/Game/Blueprints/UI/Widgets/WBP_LobbyPlayerWidget"));
// 	if (LobbyWidgetBP.Class)
// 	{
// 		LobbyWidgetBPClass = LobbyWidgetBP.Class;
// 	}

	SkeletalMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("SkeletalMesh"));
	RootComponent = SkeletalMesh;

	LobbyPlayerWidgetComponent = CreateDefaultSubobject<UWidgetComponent>(TEXT("LobbyPlayerWidget"));
	LobbyPlayerWidgetComponent->SetWidgetClass(LobbyWidgetBPClass);
	LobbyPlayerWidgetComponent->SetupAttachment(RootComponent);
	LobbyPlayerWidgetComponent->SetWidgetSpace(EWidgetSpace::Screen);
}

void AODLobbyPlayer::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AODLobbyPlayer, AttachedPlayerName);
}

void AODLobbyPlayer::SetPlayerControllerReference(AODMenuPlayerController* NewPlayerController)
{
	if (NewPlayerController)
	{
		AttachedPlayerController = NewPlayerController;

		// Set the name for this player
	}
}

void AODLobbyPlayer::SetLobbyManagerReference(AODLobbyManager* NewManager)
{
	LobbyManager = NewManager;
}

void AODLobbyPlayer::UpdateReadyStatus_Implementation(bool val)
{
	if (LobbyPlayerWidget)
	{
		LobbyPlayerWidget->SetReadyStatus(val);
	}
}

void AODLobbyPlayer::UpdatePlayerName(FName PlayerName)
{
	if (LobbyPlayerWidget)
	{
		LobbyPlayerWidget->SetPlayerName(PlayerName);
	}
}

void AODLobbyPlayer::SetPlayerCameraView(AODMenuPlayerController* PlayerController)
{

}

void AODLobbyPlayer::OnRep_AttachedPlayerName()
{
	UpdatePlayerName(AttachedPlayerName);	
}

void AODLobbyPlayer::SetAttachedPlayerName(FName PlayerName)
{
	AttachedPlayerName = PlayerName;
}

void AODLobbyPlayer::BeginPlay()
{
	Super::BeginPlay();


	// Save a reference to our widget for later
	LobbyPlayerWidget = Cast<UODLobbyPlayerWidget>(LobbyPlayerWidgetComponent->GetUserWidgetObject());
	UpdatePlayerName(AttachedPlayerName);
}