// Copyright 2020 Space Gurus


#include "Actors/ODDrinkMixer.h"
#include "Net/UnrealNetwork.h"
#include "Components/TextRenderComponent.h"
#include "Gameplay/ODDrink.h"
#include "Gameplay/ODIngredient.h"
#include "ODEnumHelperFunctions.h"
#include "UI/ODMixerWidget.h"
#include "UI/ODContainerWidget.h"

AODDrinkMixer::AODDrinkMixer(const FObjectInitializer& OI) : Super(OI)
{
	bReplicates = true;

	// Defaults
	MaxVolume = 12.f;
	GlassType = EGlassType::GT_Mixer;

	ContainerContents.bIsMixed = false;
}

void AODDrinkMixer::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
}

void AODDrinkMixer::Server_AddIngredient_Implementation(EIngredientType IngredientToAdd, float Amount)
{
	Super::Server_AddIngredient_Implementation(IngredientToAdd, Amount);
}

void AODDrinkMixer::BeginPlay()
{
	Super::BeginPlay();
}

bool AODDrinkMixer::ShakeMixer()
{
	if (CurrentVolume > 0)
	{
		// Shake it up!
		ShakeAmount++;

		if (ShakeAmount >= FullyShakenThreshold)
		{
			UE_LOG(LogTemp, Warning, TEXT("FULLY SHAKEN"));
			bHasBeenFullyShaken = true;
			ContainerContents.bIsMixed = true;
			ContainerContents.MixMethod = EMixMethod::MM_Shake;
			return true;
		}
		else
		{
			return false;
		}
	}
	return false;
}

bool AODDrinkMixer::HasBeenFullyShaken()
{
	return bHasBeenFullyShaken;
}

void AODDrinkMixer::UpdateMixerWidget()
{
	if (ContainerWidget)
	{
		ContainerWidget->AdjustContents();
	}
}

void AODDrinkMixer::ResetShake()
{
	bHasBeenFullyShaken = false;
	ShakeAmount = 0;
}

void AODDrinkMixer::StirMixer()
{
	UE_LOG(LogTemp, Warning, TEXT("Stirring %s"), *this->GetName());
}


