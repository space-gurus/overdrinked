// Copyright 2020 Space Gurus


#include "Actors/ODSkelPickupActor.h"
#include "Net/UnrealNetwork.h"
#include "Components/StaticMeshComponent.h"
#include "PlayerCharacter.h"

AODSkelPickupActor::AODSkelPickupActor(const FObjectInitializer& OI) : Super(OI)
{
	PrimaryActorTick.bCanEverTick = false;
	bReplicates = true;

	SkeletalMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("IceMesh"));
	RootComponent = SkeletalMesh;

	SkeletalMesh->SetRenderCustomDepth(true);
	SkeletalMesh->SetCustomDepthStencilValue(DefaultCustomDepth);
}

void AODSkelPickupActor::BeginPlay()
{
	Super::BeginPlay();
	
}

void AODSkelPickupActor::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
}

void AODSkelPickupActor::HandlePickup_Implementation(AActor* ActorPickingUp)
{
	// It's the player
	APlayerCharacter* player = Cast<APlayerCharacter>(ActorPickingUp);
	if (player)
	{
		// Attach to the player
		
		this->AttachToComponent(player->GetMesh(), FAttachmentTransformRules::SnapToTargetNotIncludingScale, TEXT("RightPickupSocket"));
		SetActorRelativeRotation(RotationOffset);

		// For shading look
		SkeletalMesh->SetCustomDepthStencilValue(player->GetCustomDepthStencilValue());
	}
}

void AODSkelPickupActor::HandleDrop_Implementation()
{
	this->DetachFromActor(FDetachmentTransformRules::KeepWorldTransform);
	this->Destroy();
}
