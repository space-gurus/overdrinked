// Copyright 2020 Space Gurus


#include "Actors/ODIce.h"
#include "Net/UnrealNetwork.h"
#include "Components/SkeletalMeshComponent.h"
#include "NiagaraComponent.h"
#include "Kismet/GameplayStatics.h"
#include "ODMultiplayerGameMode.h"

AODIce::AODIce(const FObjectInitializer& OI) : Super(OI)
{
	PrimaryActorTick.bCanEverTick = true;
	bReplicates = true;

	IceMistEffect = CreateDefaultSubobject<UNiagaraComponent>(TEXT("IceMistEffect"));
	IceMistEffect->SetupAttachment(RootComponent);
}

void AODIce::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AODIce, IceMeltTime);
	DOREPLIFETIME(AODIce, IceMeltSpeed);
	DOREPLIFETIME(AODIce, bShouldMelt);
}

void AODIce::BeginPlay()
{
	Super::BeginPlay();

	if (HasAuthority())
	{
		// Ask the game mode how fast we should melt
		AODMultiplayerGameMode* MPGameMode = Cast<AODMultiplayerGameMode>(UGameplayStatics::GetGameMode(GetWorld()));
		if (MPGameMode)
		{
			IceMeltSpeed = MPGameMode->GetIceMeltTime();
			UE_LOG(LogTemp, Warning, TEXT("AODIce::BeginPlay(): GameMode told us IceMeltSpeed: %f"), IceMeltSpeed);
		}
	}

	//StartMelting();
	OriginalScale = StaticMesh->GetRelativeScale3D();
}

void AODIce::Tick(float DeltaTime)
{
	if (bShouldMelt)
	{
		IceMeltTime += DeltaTime;

		if (IceMeltTime > IceMeltSpeed)
		{
			bShouldMelt = false;
			
			FTimerHandle FinishMeltTimerHandle;
			GetWorldTimerManager().SetTimer(FinishMeltTimerHandle, this, &AODIce::FinishMelting, 3.0f, false);
		}

		float ValPerc = IceMeltTime / IceMeltSpeed;
		StaticMesh->SetWorldScale3D(FMath::Lerp(OriginalScale, FVector(0.f), ValPerc));
	}
}

void AODIce::StartMelting()
{
	bShouldMelt = true;
}

void AODIce::FinishMelting()
{
	this->Destroy();
}

void AODIce::HandlePickup_Implementation(AActor* ActorPickingUp)
{
	Super::HandlePickup_Implementation(ActorPickingUp);

	bShouldMelt = false;

	//StaticMesh->SetMorphTarget("IceMeltable001", 0.f);
}

void AODIce::HandleDrop_Implementation()
{
	Super::HandleDrop_Implementation();

	//StaticMesh->SetMorphTarget("IceMeltable001", IceMeltTime);

	bShouldMelt = true;
}