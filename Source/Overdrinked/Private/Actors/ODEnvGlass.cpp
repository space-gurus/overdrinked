// Copyright 2020 Space Gurus


#include "Actors/ODEnvGlass.h"

AODEnvGlass::AODEnvGlass()
{
	PrimaryActorTick.bCanEverTick = false;
	bReplicates = true;

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
	RootComponent = StaticMesh;
}

void AODEnvGlass::BeginPlay()
{
	Super::BeginPlay();
	
}

void AODEnvGlass::SetGlassMesh(EGlassType GlassType)
{
	switch (GlassType)
	{
		case EGlassType::GT_Brew:
			StaticMesh->SetStaticMesh(BrewGlassMesh);
			SetActorScale3D(FVector(1.75f,1.75f,1.75f));
			break;
		case EGlassType::GT_Shot:
			StaticMesh->SetStaticMesh(ShotGlassMesh);
			SetActorScale3D(FVector(1.75f, 1.75f, 1.75f));
			break;
		case EGlassType::GT_Stemmed:
			StaticMesh->SetStaticMesh(StemmedGlassMesh);
			SetActorScale3D(FVector(1.25f, 1.25f, 1.25f));
			break;
		default:
			break;
	}
}

