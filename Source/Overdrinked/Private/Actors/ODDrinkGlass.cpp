// Copyright 2020 Space Gurus


#include "Actors/ODDrinkGlass.h"
#include "Net/UnrealNetwork.h"
#include "Materials/MaterialInstanceDynamic.h"
#include "Engine/DataTable.h"
#include "Data/DrinkData.h"
#include "Gameplay/ODDrink.h"
#include "Actors/ODGarnish.h"
#include "UI/ODMixerWidget.h"
#include "UI/ODContainerWidget.h"
#include "PlayerCharacter.h"
#include "ODPlayerController.h"
#include "NiagaraFunctionLibrary.h"
#include "NiagaraComponent.h"
#include "ODHelperFunctions.h"

AODDrinkGlass::AODDrinkGlass(const FObjectInitializer& OI) : Super(OI)
{
	bReplicates = true;

}

void AODDrinkGlass::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AODDrinkGlass, bIsGlassDirty);
}

void AODDrinkGlass::BeginPlay()
{
	Super::BeginPlay();

	// Setup the material instances and start the container as empty
	GlassMaterialDynamic = StaticMesh->CreateDynamicMaterialInstance(0);
	if (GlassMaterialDynamic)
	{
		StaticMesh->SetMaterial(0, GlassMaterialDynamic);
		GlassMaterialDynamic->SetScalarParameterValue(TEXT("DrinkFillLevel"), 0.f);
	}

	FillMaterialDynamic = StaticMesh->CreateDynamicMaterialInstance(1);
	if (FillMaterialDynamic)
	{
		StaticMesh->SetMaterial(1, FillMaterialDynamic);
		FillMaterialDynamic->SetScalarParameterValue(TEXT("DrinkFillLevel"), 0.f);
	}
}

void AODDrinkGlass::AdjustLiquidMaterial()
{
	MaterialFillAmount = FMath::Clamp(CurrentVolume / MaxVolume, 0.f, 1.f);

	if (GlassMaterialDynamic && FillMaterialDynamic && IngredientDataTable)
	{
		// If there's a drink we need to pull the properties
		TArray<int32> RoundedIngredients;
		for (float i : ContainerContents.IngredientAmounts)
		{
			RoundedIngredients.Add(FMath::Clamp(FMath::RoundToInt(i), 1, 10));
		}

		FLinearColor NewDrinkColor = UODHelperFunctions::BuildDrinkColor(ContainerContents.IngredientTypes, RoundedIngredients, IngredientDataTable);
		LiquidColor = NewDrinkColor;

		GlassMaterialDynamic->SetVectorParameterValue(TEXT("DrinkColor"), NewDrinkColor);
		GlassMaterialDynamic->SetScalarParameterValue(TEXT("DrinkFillLevel"), MaterialFillAmount);
		FillMaterialDynamic->SetVectorParameterValue(TEXT("DrinkColor"), NewDrinkColor);
		FillMaterialDynamic->SetScalarParameterValue(TEXT("DrinkFillLevel"), MaterialFillAmount);
	}
}

void AODDrinkGlass::SetFillMaterialProperties()
{
}

void AODDrinkGlass::OnRep_CurrentVolume()
{
	Super::OnRep_CurrentVolume();

	// Anytime our volume changes we should probably update the widget
	if (ContainerWidget && bIsBeingHeldByPlayer)
	{
		ContainerWidget->AdjustContents();
	}

	AdjustLiquidMaterial();
}

float AODDrinkGlass::DecreaseVolume(float Amount)
{
	if (CurrentVolume > 0.f)
	{
		float ActualDecreasedAmount = 0.f;

		// We were able to consume the entire amount
		if (CurrentVolume - Amount >= 0.f)
		{
			ActualDecreasedAmount = Amount;
			CurrentVolume -= Amount;
		}
		else
		{
			ActualDecreasedAmount = CurrentVolume;
			CurrentVolume = 0.f;
		}

		// Update on the server if necessary
		if (HasAuthority())
		{
			OnRep_CurrentVolume();
		}

		return ActualDecreasedAmount;
	}

	return 0.f; // Default to zero
}

void AODDrinkGlass::ClearDrink()
{
	Dump();
}

void AODDrinkGlass::PlayerInteraction_Implementation(APlayerCharacter* Player)
{
	if (!Player)
		return;

	if (Player->IsHoldingGarnish())
	{
		AODGarnish* GarnishInHand = Cast<AODGarnish>(Player->GetObjectInHand());
		if (GarnishInHand)
		{
			Player->Server_AddGarnishToContainer(GarnishInHand, this);
		}
	}
	else
	{
		Super::PlayerInteraction_Implementation(Player);
	}
}

void AODDrinkGlass::Server_AddIngredient_Implementation(EIngredientType IngredientToAdd, float Amount)
{
	Super::Server_AddIngredient_Implementation(IngredientToAdd, Amount);

	OnRep_CurrentVolume();
}

void AODDrinkGlass::OnRep_IsHoldingDrink()
{

}

bool AODDrinkGlass::IsGlassDirty()
{
	return bIsGlassDirty;
}

void AODDrinkGlass::MakeDirty()
{
	bIsGlassDirty = true;
	if (DirtyEffect)
	{
		DirtyNiagara = UNiagaraFunctionLibrary::SpawnSystemAttached(DirtyEffect, StaticMesh, "", FVector(0.f, 0.f, 20.f), FRotator::ZeroRotator, FVector(0.5f, 0.5f, 0.5f), EAttachLocation::SnapToTarget, false, ENCPoolMethod::None);
	}
}

void AODDrinkGlass::MakeClean()
{
	bIsGlassDirty = false;
	if (DirtyNiagara)
	{
		DirtyNiagara->DestroyComponent();
	}
}
