// Copyright 2020 Space Gurus


#include "Actors/ODTaser.h"
#include "PlayerCharacter.h"
#include "Components/WidgetComponent.h"
#include "NiagaraFunctionLibrary.h"
#include "NiagaraComponent.h"
#include "AkAudioEvent.h"
#include "AkGameplayStatics.h"
#include "Net/UnrealNetwork.h"

AODTaser::AODTaser(const FObjectInitializer& OI) : Super(OI)
{
	bReplicates = true;

	TaserEffectSpawnLoc = CreateDefaultSubobject<USceneComponent>(TEXT("TaserEffectSpawnLoc"));
	TaserEffectSpawnLoc->SetupAttachment(RootComponent);

	HelperWidget = CreateDefaultSubobject<UWidgetComponent>(TEXT("HelperWidget"));
	HelperWidget->SetupAttachment(RootComponent);
}

void AODTaser::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AODTaser, bIsBeingHeld);
}

void AODTaser::HandlePickup(AActor* ActorPickingUp)
{
	Super::HandlePickup(ActorPickingUp);

	bIsBeingHeld = true;
	
	if (HasAuthority())
	{
		OnRep_IsBeingHeld();
	}
}

void AODTaser::HandleDrop()
{
	Super::HandleDrop();

	bIsBeingHeld = false;
	if (HasAuthority())
	{
		OnRep_IsBeingHeld();
	}
}

void AODTaser::UseTaser_Implementation()
{
	if (TaserNS)
	{
		FVector SpawnLoc = GetActorLocation() + TaserEffectSpawnLoc->GetRelativeLocation();
		UNiagaraFunctionLibrary::SpawnSystemAttached(TaserNS, TaserEffectSpawnLoc, "", FVector(0.f), FRotator(0.f), EAttachLocation::SnapToTarget, true);
	}

	if (TaserFireSound)
	{
		FOnAkPostEventCallback NullCallback;
		UAkGameplayStatics::PostEvent(TaserFireSound, this, 0, NullCallback);
	}
}

void AODTaser::OnRep_IsBeingHeld()
{
	HelperWidget->SetVisibility(!bIsBeingHeld);
}
