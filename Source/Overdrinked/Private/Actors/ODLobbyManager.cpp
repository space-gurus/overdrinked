// Copyright 2020 Space Gurus


#include "Actors/ODLobbyManager.h"
#include "Kismet/GameplayStatics.h"
#include "ODLobbyGameState.h"
#include "ODLobbyGameMode.h"
#include "ODMenuPlayerController.h"
#include "Actors/ODLobbyPlayer.h"

AODLobbyManager::AODLobbyManager()
{
	PrimaryActorTick.bCanEverTick = false;
}

void AODLobbyManager::SpawnPlayerAvatar(AODMenuPlayerController* NewPlayerController)
{
	if (NewPlayerController && LobbyPlayerBlueprint && NumOfPlayers < 4 && PlayerPositions.IsValidIndex(NumOfPlayers))
	{
		FActorSpawnParameters SpawnParams;
		FVector SpawnPosition = PlayerPositions[NumOfPlayers] + this->GetActorLocation();
		AODLobbyPlayer* NewLobbyAvatar = GetWorld()->SpawnActor<AODLobbyPlayer>(LobbyPlayerBlueprint, SpawnPosition, FRotator(0.f, 0.f, 0.f), SpawnParams);

		if (NewLobbyAvatar)
		{
			NumOfPlayers++;

			NewPlayerController->SetLobbyAvatarReference(NewLobbyAvatar);
			NewLobbyAvatar->SetAttachedPlayerName(NewPlayerController->GetPlayerName());
			NewLobbyAvatar->UpdatePlayerName(NewPlayerController->GetPlayerName());
			NewLobbyAvatar->SetLobbyManagerReference(this);
		}
	}
}

void AODLobbyManager::RemovePlayerAvatar(AODMenuPlayerController* NewPlayerController)
{
	if (NewPlayerController)
	{
		NewPlayerController->GetLobbyAvatar()->Destroy();
		NumOfPlayers--;
	}
}

AActor* AODLobbyManager::GetPlayerViewCamera()
{
	return PlayerViewCamera;
}

// Called when the game starts or when spawned
void AODLobbyManager::BeginPlay()
{
	Super::BeginPlay();

	// Tell the game state we exist
	AODLobbyGameState* LobbyGameState = Cast<AODLobbyGameState>(UGameplayStatics::GetGameState(GetWorld()));
	if (LobbyGameState)
	{
		LobbyGameState->SetLobbyManagerReference(this);

		// We need to spawn the first player
		if (HasAuthority())
		{
			AODLobbyGameMode* LobbyGameMode = Cast<AODLobbyGameMode>(UGameplayStatics::GetGameMode(GetWorld()));
			if (LobbyGameMode)
			{
				SpawnPlayerAvatar(LobbyGameMode->GetHostPlayerController());
			}
		}
	}
}
