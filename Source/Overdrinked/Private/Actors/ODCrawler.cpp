// Copyright 2020 Space Gurus


#include "Actors/ODCrawler.h"
#include "Net/UnrealNetwork.h"

AODCrawler::AODCrawler(const FObjectInitializer& OI) : Super(OI)
{
	PrimaryActorTick.bCanEverTick = true;
	bReplicates = true;
}

void AODCrawler::HandlePickup_Implementation(AActor* ActorPickingUp)
{
	Super::HandlePickup_Implementation(ActorPickingUp);
}

void AODCrawler::HandleDrop_Implementation()
{
	Super::HandleDrop_Implementation();
}

void AODCrawler::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
}

void AODCrawler::BeginPlay()
{
	Super::BeginPlay();
}
