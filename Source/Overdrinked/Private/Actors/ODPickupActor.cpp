// Copyright 2020 Space Gurus


#include "Actors/ODPickupActor.h"
#include "Net/UnrealNetwork.h"
#include "Materials/MaterialInstanceDynamic.h"
#include "Components/StaticMeshComponent.h"
#include "Components/BoxComponent.h"
#include "PlayerCharacter.h"
#include "Environment/ODCounter.h"
#include "DrawDebugHelpers.h"

AODPickupActor::AODPickupActor(const FObjectInitializer& OI) : Super(OI)
{
	PrimaryActorTick.bCanEverTick = false;
	bReplicates = true;

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
	RootComponent = StaticMesh;

	StaticMesh->SetRenderCustomDepth(true);
	StaticMesh->SetCustomDepthStencilValue(DefaultCustomDepth);

	// Defaults
	// Highlight yellow
	PickupHighlightColor = FColor(255.f, 255.f, 0.f);
	// Highlight green
	InteractHighlightColor = FColor(0.f, 255.f, 17.f);
}

void AODPickupActor::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AODPickupActor, ExteriorDynamicMaterial);
	DOREPLIFETIME(AODPickupActor, bIsAttached);
}

void AODPickupActor::BeginPlay()
{
	Super::BeginPlay();
	
	// Setup the exterior material for highlighting
	ExteriorDynamicMaterial = UMaterialInstanceDynamic::Create(StaticMesh->GetMaterial(0), this);
	if (StaticMesh && ExteriorDynamicMaterial)
	{
		StaticMesh->SetMaterial(0, ExteriorDynamicMaterial);
		ExteriorDynamicMaterial->SetScalarParameterValue(TEXT("HighlightIntensity"), 0.f);
	}

	StaticMesh->OnComponentBeginOverlap.AddDynamic(this, &AODPickupActor::MeshColliderBeginOverlap);
	StaticMesh->OnComponentEndOverlap.AddDynamic(this, &AODPickupActor::MeshColliderEndOverlap);

	// Are we starting locked to a counter?
	if (ParentCounter)
	{
		bIsAttached = true;
	}
}

void AODPickupActor::MeshColliderBeginOverlap(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{

}

void AODPickupActor::MeshColliderEndOverlap(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{

}

void AODPickupActor::ShowPickupHighlight()
{
	if (!bIsPickupHighlighted && !bIsInteractHighlighted)
	{
		bIsPickupHighlighted = true;
		StaticMesh->SetCustomDepthStencilValue(240);
	}
}

void AODPickupActor::ShowInteractHighlight()
{
	// This highlight will overwrite the other
	if (bIsPickupHighlighted)
		bIsPickupHighlighted = false;

	bIsInteractHighlighted = true;
	StaticMesh->SetCustomDepthStencilValue(242);
}

void AODPickupActor::EndHighlight()
{
	bIsPickupHighlighted = false;
	bIsInteractHighlighted = false;
	StaticMesh->SetCustomDepthStencilValue(DefaultCustomDepth);
}

bool AODPickupActor::IsHighlighted()
{
	return bIsPickupHighlighted;
}

bool AODPickupActor::IsAttached()
{
	return bIsAttached;
}

void AODPickupActor::SetIsAttached(bool val)
{
	bIsAttached = val;
}

void AODPickupActor::HandlePickup_Implementation(AActor* ActorPickingUp)
{
	LoseRelevance();

	// It's the player
	APlayerCharacter* Player = Cast<APlayerCharacter>(ActorPickingUp);
	if (Player)
	{
		// Handle the counter
		if (bIsAttached)
		{
			if (ParentCounter)
			{
				ParentCounter->RemoveAttachedObject();
				ParentCounter = NULL;
				DetachFromActor(FDetachmentTransformRules::KeepWorldTransform);
			}
			else
			{
				ParentCounter = Cast<AODCounter>(this->GetAttachParentActor());
				if (ParentCounter)
				{
					ParentCounter->RemoveAttachedObject();
					ParentCounter = NULL;
					DetachFromActor(FDetachmentTransformRules::KeepWorldTransform);
				}
			}
		}
		// Attach to the player and let them know to handle the new object
		AttachToComponent(Player->GetMesh(), FAttachmentTransformRules::SnapToTargetNotIncludingScale, TEXT("RightPickupSocket"));
		Player->HandleObjectPickup(this);

		SetActorRelativeLocation(PickupPositionOffset);
		SetActorRelativeRotation(PickupRotationOffset);

		bIsBeingHeldByPlayer = true;

		// For shading look
		StaticMesh->SetCustomDepthStencilValue(Player->GetCustomDepthStencilValue());
	}

	// Mesh settings
	StaticMesh->SetSimulatePhysics(false);

	bIsAttached = true;
}

void AODPickupActor::HandleDrop_Implementation()
{
	// Make it stand upright
	SetActorRotation(CounterAttachRotationOffset);

	// Snap to the floor
	FVector PlacementLocation = GetActorLocation();
	FVector PlayerForwardVector = FVector(0.f);

	if (GetAttachParentActor()) // Null check because sometimes this crashes on clients for some reason
		PlayerForwardVector = GetAttachParentActor()->GetActorForwardVector();
	
	FHitResult TraceResult;
	FVector DownVector = FVector(0.f, 0.f, -200.f) + GetActorLocation();
	bool bHit =	GetWorld()->LineTraceSingleByObjectType(TraceResult, GetActorLocation(), DownVector, FCollisionObjectQueryParams::AllStaticObjects);

	PlayerForwardVector *= 40.f;
	PlacementLocation += PlayerForwardVector;

	if (bHit)
	{
		PlacementLocation.Z = TraceResult.Location.Z + 5.f;
	}
	else
	{
		PlacementLocation.Z = 0.f;
	}

	SetActorLocation(PlacementLocation);

	this->DetachFromActor(FDetachmentTransformRules::KeepWorldTransform);
	bIsBeingHeldByPlayer = false;
	bIsAttached = false;

	// Mesh settings
	StaticMesh->SetSimulatePhysics(false);

	// For shading look
	StaticMesh->SetCustomDepthStencilValue(DefaultCustomDepth);
}

void AODPickupActor::PlayerInteraction_Implementation(APlayerCharacter* Player)
{
	if (Player && !Player->IsHoldingObject())
	{
		HandlePickup(Player);
	}
}

void AODPickupActor::BecomeRelevant_Implementation(APlayerCharacter* PlayerTriggered)
{
	if (PlayerTriggered && !PlayerTriggered->IsHoldingObject())
		ShowPickupHighlight();
}

void AODPickupActor::LoseRelevance_Implementation()
{
	EndHighlight();
}

void AODPickupActor::AttachToCounter(AODCounter* CounterToAttach)
{
	if (CounterToAttach)
	{
		StaticMesh->SetSimulatePhysics(false);
		this->AttachToComponent(CounterToAttach->GetDrinkAttachComponent(), FAttachmentTransformRules::SnapToTargetNotIncludingScale);
		ParentCounter = CounterToAttach;
		bIsAttached = true;

		SetActorRelativeRotation(CounterAttachRotationOffset);
		SetActorRelativeLocation(CounterAttachPositionOffset);
	}
}

// Called every frame
void AODPickupActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

