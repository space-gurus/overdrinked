// Copyright 2020 Space Gurus


#include "Actors/ODIngredientDispenser.h"
#include "UObject/ConstructorHelpers.h"
#include "Engine/DataTable.h"
#include "Components/StaticMeshComponent.h"
#include "Components/ArrowComponent.h"
#include "Components/WidgetComponent.h"
#include "Gameplay/ODIngredient.h"
#include "Actors/ODBevContainer.h"
#include "ODEnumHelperFunctions.h"
#include "UI/ODPourHUDSimple.h"
#include "Data/IngredientData.h"
#include "PlayerCharacter.h"
#include "ODPlayerController.h"
#include "AkGameplayStatics.h"
#include "AkAudioEvent.h"

AODIngredientDispenser::AODIngredientDispenser(const FObjectInitializer& OI) : Super(OI)
{
	PrimaryActorTick.bCanEverTick = false;
	bReplicates = true;

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
	RootComponent = StaticMesh;
	StaticMesh->SetRenderCustomDepth(true);

	ForwardArrow = CreateDefaultSubobject<UArrowComponent>(TEXT("ForwardArrow"));
	ForwardArrow->SetupAttachment(RootComponent);

	WidgetComponent = CreateDefaultSubobject<UWidgetComponent>(TEXT("WidgetComponent"));
	WidgetComponent->SetWidgetSpace(EWidgetSpace::World);
	WidgetComponent->SetVisibility(false);

	// Ingredient Reference
	ConstructorHelpers::FObjectFinder<UDataTable> IngredientDataObject(TEXT("/Game/DrinkGame/Data/DT_Ingredients"));
	if (IngredientDataObject.Succeeded())
	{
		IngredientDataTable = IngredientDataObject.Object;
	}
}

void AODIngredientDispenser::BeginPlay()
{
	Super::BeginPlay();

	// Overlap Setup
	StaticMesh->OnComponentBeginOverlap.AddDynamic(this, &AODIngredientDispenser::MeshBeginOverlap);
	StaticMesh->OnComponentEndOverlap.AddDynamic(this, &AODIngredientDispenser::MeshEndOverlap);
}

void AODIngredientDispenser::MeshBeginOverlap(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{

}

void AODIngredientDispenser::MeshEndOverlap(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{

}

void AODIngredientDispenser::PlayerInteraction_Implementation(APlayerCharacter* Player)
{
	UE_LOG(LogTemp, Warning, TEXT("AODIngredientDispenser::PlayerInteraction()"));
	if (Player && Player->IsHoldingBevContainer())
	{
		if (!Player->IsHoldingPourableContainer())
		{
			Player->PlayErrorSound();
			return;
		}

		AODBevContainer* BevContainer = Cast<AODBevContainer>(Player->GetObjectInHand());
		if (BevContainer && (BevContainer->GetCurrentVolume() < BevContainer->GetMaxVolume()))
		{
			AODPlayerController* ODPlayerController = Cast<AODPlayerController>(Player->GetController());
			if (ODPlayerController)
			{
				ODPlayerController->StartPouring(this);
			}
		}
		else
		{
			Player->PlayErrorSound();
		}
	}
}

void AODIngredientDispenser::BecomeRelevant_Implementation(APlayerCharacter* PlayerTriggered)
{
	ShowHighlight();
}

void AODIngredientDispenser::LoseRelevance_Implementation()
{
	EndHighlight();
}

void AODIngredientDispenser::ShowHighlight()
{
	StaticMesh->SetCustomDepthStencilValue(HighlightCustomDepth);
}

void AODIngredientDispenser::EndHighlight()
{
	StaticMesh->SetCustomDepthStencilValue(DefaultCustomDepth);
}

TPair<EIngredientType, float> AODIngredientDispenser::DispenseIngredient()
{
	TPair<EIngredientType, float> PairToReturn;
	PairToReturn.Key = IngredientType;
	PairToReturn.Value = DispenseAmount;
	return PairToReturn;
}

float AODIngredientDispenser::GetDispenseAmount()
{
	return DispenseAmount;
}

EIngredientType AODIngredientDispenser::GetIngredientType()
{
	return IngredientType;
}

bool AODIngredientDispenser::IsPlayingDispenseLoopSound()
{
	return bIsPlayingDispenseLoopSound;
}

void AODIngredientDispenser::PourWidgetSetup()
{

}

void AODIngredientDispenser::DispenseSoundTimerUp()
{
	if (DispenseEndSound)
	{
		// Stop the looping sound
		UAkGameplayStatics::ExecuteActionOnPlayingID(AkActionOnEventType::Stop, DispensePlayingID, 500);
		
		// Play the finishing sound
		FOnAkPostEventCallback NullCallback;
		DispensePlayingID = UAkGameplayStatics::PostEvent(DispenseEndSound, this, 0, NullCallback);
	}
}

void AODIngredientDispenser::StartDispenseSound_Implementation()
{
	if (DispenseSound)
	{
		// If we're not already playing it
		FOnAkPostEventCallback NullCallback;
		DispensePlayingID = UAkGameplayStatics::PostEvent(DispenseSound, this, 0, NullCallback);

		bIsPlayingDispenseLoopSound = true;

		// Cancel the timer if we have one
		GetWorldTimerManager().ClearTimer(DispenseSoundTimerHandle);
	}
}

void AODIngredientDispenser::FinishDispenseSound_Implementation()
{
	if (DispenseEndSound)
	{
		bIsPlayingDispenseLoopSound = false;
		// This sets a timer to only finish playing the sound if the player actually stops for a while
		// this is to combat someone pouring back to back
		GetWorldTimerManager().SetTimer(DispenseSoundTimerHandle, this, &AODIngredientDispenser::DispenseSoundTimerUp, 0.2f, false);
	}
}

