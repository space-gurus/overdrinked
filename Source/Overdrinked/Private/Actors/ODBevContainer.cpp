// Copyright 2020 Space Gurus

#include "Actors/ODBevContainer.h"

#include "Components/WidgetComponent.h"
#include "Net/UnrealNetwork.h"
#include "UObject/ConstructorHelpers.h"
#include "Engine/DataTable.h"
#include "ODGameInstance.h"
#include "ODEnumHelperFunctions.h"
#include "Data/IngredientData.h"
#include "Gameplay/ODIngredient.h"
#include "UI/ODMixerWidget.h"
#include "UI/ODContainerWidget.h"
#include "PlayerCharacter.h"
#include "AI/ODAICharacter.h"
#include "ODPlayerController.h"
#include "Gameplay/ODDrink.h"
#include "Environment/ODCounter.h"
#include "Environment/ODWasteBin.h"
#include "DrawDebugHelpers.h"

// Sets default values
AODBevContainer::AODBevContainer(const FObjectInitializer& OI) : Super(OI)
{
	PrimaryActorTick.bCanEverTick = false;
	bReplicates = true;
	SetReplicateMovement(true);
	
	// Components
	StaticMesh->SetSimulatePhysics(false);

	// Ingredient Reference
	ConstructorHelpers::FObjectFinder<UDataTable> IngredientDataObject(TEXT("/Game/DrinkGame/Data/DT_Ingredients"));
	if (IngredientDataObject.Succeeded())
	{
		IngredientDataTable = IngredientDataObject.Object;
	}

	ContainerContents.MixMethod = EMixMethod::MM_Straight;
}

void AODBevContainer::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AODBevContainer, ContainerContents);
	DOREPLIFETIME(AODBevContainer, MaxVolume);
	DOREPLIFETIME(AODBevContainer, CurrentVolume);
	DOREPLIFETIME(AODBevContainer, MaterialFillAmount);
	DOREPLIFETIME(AODBevContainer, LiquidColor);
}

// Called when the game starts or when spawned
void AODBevContainer::BeginPlay()
{
	Super::BeginPlay();

}

void AODBevContainer::OnRep_CurrentVolume()
{
	// Anytime our volume changes we should probably update the widget
	if (ContainerWidget)
	{
		ContainerWidget->AdjustContents();
	}
}

void AODBevContainer::PrintIngredients()
{
	UE_LOG(LogTemp, Warning, TEXT("ALL INGREDIENTS IN %s ------"), *this->GetName());
	for (int32 IngIndex = 0; IngIndex < ContainerContents.IngredientTypes.Num(); ++IngIndex)
	{
		if (ContainerContents.IngredientAmounts.IsValidIndex(IngIndex))
		{
			UE_LOG(LogTemp, Warning, TEXT("%s : %f"), *(UODEnumHelperFunctions::IngredientEnumToString(ContainerContents.IngredientTypes[IngIndex]).ToString()), ContainerContents.IngredientAmounts[IngIndex]);
		}
	}
	UE_LOG(LogTemp, Error, TEXT("%s 's Current volume: %f"), *GetName(), CurrentVolume);
}

FString AODBevContainer::GetRoleEnumText(ENetRole role)
{
	switch (role)
	{
	case ROLE_None:
		return "None";
	case ROLE_SimulatedProxy:
		return "SimulatedProxy";
	case ROLE_AutonomousProxy:
		return "AutonomousProxy";
	case ROLE_Authority:
		return "Authority";
	default:
		return "ERROR";
	}
}

// Called every frame
void AODBevContainer::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

// 	UODGameInstance* ThisGameInstance = Cast<UODGameInstance>(GetGameInstance());
// 	if (ThisGameInstance->GetDrawRoleCards())
// 	{
// 		DrawDebugString(GetWorld(), FVector(0, 0, 100), GetRoleEnumText(GetLocalRole()), this, FColor::White, DeltaTime, false, 1.f);
// 	}
}

void AODBevContainer::MeshColliderBeginOverlap(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	Super::MeshColliderBeginOverlap(OverlappedComp, OtherActor, OtherComp, OtherBodyIndex, bFromSweep, SweepResult);

	// First check to see if what we're overlapping is a counter
	AODCounter* counter;
	counter = Cast<AODCounter>(OtherActor);
	if (counter)
	{
		// It's definitely a counter so now see if it's the closest one
		if (ClosestCounter)
		{
			// We already have a counter nearby so get the closest one
			if (this->GetDistanceTo(OtherActor) < this->GetDistanceTo(ClosestCounter))
			{
				// Only update counter if the new one is closer
				ClosestCounter = counter;
			}
		}
		else
		{
			ClosestCounter = counter;
		}
	}
}

void AODBevContainer::MeshColliderEndOverlap(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	Super::MeshColliderEndOverlap(OverlappedComp, OtherActor, OtherComp, OtherBodyIndex);

	// First check to see if what we're stopping overlapping is a counter
	AODCounter* counter;
	counter = Cast<AODCounter>(OtherActor);
	if (counter)
	{
		if (counter == ClosestCounter)
		{
			ClosestCounter = NULL;
		}
	}
}

void AODBevContainer::PlayerInteraction_Implementation(APlayerCharacter* Player)
{
	if (Player && !Player->IsHoldingObject())
	{
		// Just pickup the object
		Super::PlayerInteraction_Implementation(Player);
	}
	else if (Player && Player->IsHoldingBevContainer())
	{
		AODBevContainer* OtherContainer = Cast<AODBevContainer>(Player->GetObjectInHand());
		if (OtherContainer)
		{
			if (!OtherContainer->IsContainerEmpty())
			{
				// Pour into this container
				Player->Server_PourIntoContainer(this);
			}
			else
			{
				// Can't do that
				Player->PlayErrorSound();
				return;
			}
		}
	}
}

void AODBevContainer::BecomeRelevant_Implementation(APlayerCharacter* PlayerTriggered)
{
	if (!PlayerTriggered)
		return;

	if (PlayerTriggered->IsHoldingBevContainer() || PlayerTriggered->IsHoldingGarnish())
	{
		ShowInteractHighlight();
	}
	else
	{
		ShowPickupHighlight();
	}
}

void AODBevContainer::ShowPickupHighlight()
{
	if (!bIsPickupHighlighted && !bIsInteractHighlighted)
	{
		bIsPickupHighlighted = true;
		StaticMesh->SetCustomDepthStencilValue(241);
	}
}

void AODBevContainer::EndHighlight()
{
	bIsPickupHighlighted = false;
	bIsInteractHighlighted = false;
	StaticMesh->SetCustomDepthStencilValue(DefaultCustomDepth);
}

FContainerContents AODBevContainer::GetContainerContents()
{
	return ContainerContents;
}

EGlassType AODBevContainer::GetGlassType()
{
	return GlassType;
}

bool AODBevContainer::IsContainerEmpty()
{
	return CurrentVolume < 0.01f;
}

void AODBevContainer::SetWidgetReference(UODContainerWidget* NewContainerWidget)
{
	if (NewContainerWidget)
	{
		ContainerWidget = NewContainerWidget;
	}
}

UODContainerWidget* AODBevContainer::GetMixerWidget()
{
	return ContainerWidget;
}

void AODBevContainer::Server_AddIngredient_Implementation(EIngredientType IngredientToAdd, float Amount)
{
	// First check if we can even pour into this
	if (CurrentVolume < MaxVolume)
	{
		if (ContainerContents.IngredientTypes.Contains(IngredientToAdd))
		{
			// We already have some of this ingredient, so just add it in
			int32 IngredientIndex = ContainerContents.IngredientTypes.IndexOfByKey(IngredientToAdd);
			if (ContainerContents.IngredientAmounts.IsValidIndex(IngredientIndex))
			{
				ContainerContents.IngredientAmounts[IngredientIndex] += Amount;
			}
		}
		else
		{
			// We don't have it yet so create a new entry for it
			ContainerContents.IngredientTypes.Add(IngredientToAdd);
			ContainerContents.IngredientAmounts.Add(Amount);
		}

		// Add it to our total volume, but don't go over the max
		CurrentVolume = FMath::Clamp(CurrentVolume + Amount, 0.f, MaxVolume);

		// Adjust UI as needed
		if (ContainerWidget)
		{
			ContainerWidget->AdjustContents();
		}
	}
	else
	{
		// The container is full
	}
}

void AODBevContainer::Server_RemoveIngredient_Implementation(EIngredientType IngredientToRemove)
{
	if (ContainerContents.IngredientTypes.Contains(IngredientToRemove))
	{
		int32 IngredientIndex = ContainerContents.IngredientTypes.IndexOfByKey(IngredientToRemove);
		if (ContainerContents.IngredientAmounts.IsValidIndex(IngredientIndex))
		{
			CurrentVolume -= ContainerContents.IngredientAmounts[IngredientIndex];

			// Clean up
			ContainerContents.IngredientAmounts.RemoveAt(IngredientIndex);
			ContainerContents.IngredientTypes.Remove(IngredientToRemove);
		}
	}
}

void AODBevContainer::Server_AddGarnish_Implementation(EGarnishType GarnishToAdd, int32 Amount)
{
	if (ContainerContents.GarnishTypes.Contains(GarnishToAdd))
	{
		int32 GarnishIndex = ContainerContents.GarnishTypes.IndexOfByKey(GarnishToAdd);
		if (ContainerContents.GarnishAmounts.IsValidIndex(GarnishIndex))
		{
			ContainerContents.GarnishAmounts[GarnishIndex] += Amount;
		}
	}
	else
	{
		ContainerContents.GarnishTypes.Add(GarnishToAdd);
		ContainerContents.GarnishAmounts.Add(Amount);
	}

	// Adjust UI as needed
// 	if (MixerWidget)
// 	{
// 		MixerWidget->AdjustFill();
// 	}
}

void AODBevContainer::Server_RemoveGarnish_Implementation(EGarnishType GarnishToRemove)
{
	if (ContainerContents.GarnishTypes.Contains(GarnishToRemove))
	{
		int32 GarnishIndex = ContainerContents.GarnishTypes.IndexOfByKey(GarnishToRemove);
		if (ContainerContents.GarnishAmounts.IsValidIndex(GarnishIndex))
		{
			// Clean up
			ContainerContents.GarnishAmounts.RemoveAt(GarnishIndex);
			ContainerContents.GarnishTypes.Remove(GarnishToRemove);
		}
	}
}

void AODBevContainer::Server_TransferContents_Implementation(AODBevContainer* OtherContainer)
{
	// Move all current ingredients and whatever drink it's holding to the specified container
	if (OtherContainer && OtherContainer->IsContainerEmpty())
	{
		// Transfer everything
		OtherContainer->Server_AddContainerContents(ContainerContents, true);
	}

	// Now clear the ingredients
	Dump();
}

void AODBevContainer::Server_AddContainerContents_Implementation(FContainerContents NewContents, bool bIsAddition)
{
	// Are we just adding these ingredients on top of what we already have?
	if (bIsAddition)
	{
		for (int32 i = 0; i < NewContents.IngredientTypes.Num(); i++)
		{
			Server_AddIngredient(NewContents.IngredientTypes[i], NewContents.IngredientAmounts[i]);
		}

		for (int32 i = 0; i < NewContents.GarnishTypes.Num(); i++)
		{
			Server_AddGarnish(NewContents.GarnishTypes[i], NewContents.GarnishAmounts[i]);
		}

		if (NewContents.bIsMixed)
		{
			ContainerContents.bIsMixed = true;
			ContainerContents.MixMethod = NewContents.MixMethod;
		}
	}
	else
	{
		ContainerContents.IngredientAmounts.Empty();
		ContainerContents.IngredientAmounts = NewContents.IngredientAmounts;
		ContainerContents.IngredientTypes.Empty();
		ContainerContents.IngredientTypes = NewContents.IngredientTypes;
		ContainerContents.GarnishAmounts.Empty();
		ContainerContents.GarnishAmounts = NewContents.GarnishAmounts;
		ContainerContents.GarnishTypes.Empty();
		ContainerContents.GarnishTypes = NewContents.GarnishTypes;

		ContainerContents.MixMethod = NewContents.MixMethod;
		ContainerContents.bIsMixed = NewContents.bIsMixed;
	}

	CurrentVolume = 0.f;
	for (float Amount : ContainerContents.IngredientAmounts)
	{
		CurrentVolume += Amount;
	}

	OnRep_CurrentVolume();
}

void AODBevContainer::Dump()
{
	RemoveAllContents();
	OnRep_CurrentVolume();
}

void AODBevContainer::RemoveAllContents_Implementation()
{
	ContainerContents.IngredientTypes.Empty();
	ContainerContents.IngredientAmounts.Empty();
	ContainerContents.GarnishTypes.Empty();
	ContainerContents.GarnishAmounts.Empty();
	ContainerContents.bIsMixed = false;

	CurrentVolume = 0.f;
}

void AODBevContainer::HandlePickup(AActor* ActorPickingUp)
{
	Super::HandlePickup(ActorPickingUp);

	// It's an AI actor
	AODAICharacter* AICharacter = Cast<AODAICharacter>(ActorPickingUp);
	if (AICharacter)
	{
		this->AttachToComponent(AICharacter->GetMesh(), FAttachmentTransformRules::SnapToTargetNotIncludingScale, TEXT("LeftPickupSocket"));

		bIsBeingHeldByAI = true;
	}


	switch (GlassType)
	{
		case EGlassType::GT_Mixer:
			ScaleMesh(1.5f);
			break;
		case EGlassType::GT_Blender:
			ScaleMesh(1.f);
			break;
		case EGlassType::GT_Brew:
			ScaleMesh(1.f);
			break;
		case EGlassType::GT_Stemmed:
			ScaleMesh(1.5f);
			break;
		default:
			ScaleMesh(1.25f);
	}

	//PrintIngredients();
}

void AODBevContainer::HandleDrop()
{
	Super::HandleDrop();

	bIsBeingHeldByAI = false;
	
	if (GlassType != EGlassType::GT_Blender)
	{
		ScaleMesh(2.f);
	}
}

void AODBevContainer::ScaleMesh_Implementation(float NewScale)
{
	StaticMesh->SetWorldScale3D(FVector(NewScale, NewScale, NewScale));
}

bool AODBevContainer::GetIsBeingHeld()
{
	return (bIsBeingHeldByPlayer || bIsBeingHeldByAI);
}

bool AODBevContainer::IsBeingHeldByPlayer()
{
	return bIsBeingHeldByPlayer;
}

bool AODBevContainer::IsBeingHeldByAI()
{
	return bIsBeingHeldByAI;
}

bool AODBevContainer::GetIsLockedOnGridSpace()
{
	return bIsAttached;
}

float AODBevContainer::GetMaxVolume()
{
	return MaxVolume;
}

float AODBevContainer::GetCurrentVolume()
{
	return CurrentVolume;
}

FLinearColor AODBevContainer::GetLiquidColor()
{
	return LiquidColor;
}

void AODBevContainer::TryToPourContents(AODBevContainer* OtherContainer)
{
	// First check if it's just a waste bin
	AODWasteBin* WasteBin = Cast<AODWasteBin>(OtherContainer);
	if (WasteBin)
	{
		RemoveAllContents();
	}
}