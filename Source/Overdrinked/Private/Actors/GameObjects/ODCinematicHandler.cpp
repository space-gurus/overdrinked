// Copyright 2020 Space Gurus


#include "Actors/GameObjects/ODCinematicHandler.h"
#include "LevelSequencePlayer.h"
#include "LevelSequenceActor.h"
#include "LevelSequence.h"

AODCinematicHandler::AODCinematicHandler()
{
}

// Called when the game starts or when spawned
void AODCinematicHandler::BeginPlay()
{
	Super::BeginPlay();
	
	FMovieSceneSequencePlaybackSettings PlaybackSettings;
	FadeInSequencePlayer = ULevelSequencePlayer::CreateLevelSequencePlayer(GetWorld(), FadeInSequence, PlaybackSettings, LevelSequenceActor);

	if (FadeInSequencePlayer)
	{
		FadeInSequencePlayer->Play();
	}
}


