// Copyright 2020 Space Gurus


#include "Actors/ODGarnish.h"
#include "Net/UnrealNetwork.h"
#include "Components/SkeletalMeshComponent.h"
#include "Components/BoxComponent.h"
#include "PlayerCharacter.h"
#include "Environment/ODCounter.h"

AODGarnish::AODGarnish(const FObjectInitializer& OI) : Super(OI)
{
	PrimaryActorTick.bCanEverTick = true;
	bReplicates = true;

	PickupCollider = CreateDefaultSubobject<UBoxComponent>(TEXT("PickupCollider"));
	PickupCollider->SetupAttachment(RootComponent);

	// Defaults
	// Highlight yellow
	PickupHighlightColor = FColor(255.f, 255.f, 0.f);
}

void AODGarnish::BeginPlay()
{
	Super::BeginPlay();

}

void AODGarnish::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
}

void AODGarnish::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}


EGarnishType AODGarnish::GetGarnishType()
{
	return GarnishType;
}

