// Copyright 2020 Space Gurus


#include "Gameplay/ODIngredient.h"
#include "Net/UnrealNetwork.h"

// Sets default values
AODIngredient::AODIngredient()
{
	bReplicates = true;

	USceneComponent* SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("SceneComponent"));
	RootComponent = SceneComponent;
}

void AODIngredient::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AODIngredient, Name);
	DOREPLIFETIME(AODIngredient, Amount);
	DOREPLIFETIME(AODIngredient, Color);
}


