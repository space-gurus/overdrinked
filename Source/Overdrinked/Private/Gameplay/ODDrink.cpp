// Copyright 2020 Space Gurus


#include "Gameplay/ODDrink.h"

#include "UObject/ConstructorHelpers.h"
#include "Net/UnrealNetwork.h"
#include "Engine/DataTable.h"

#include "ODEnumHelperFunctions.h"
#include "Data/DrinkData.h"
#include "Gameplay/ODIngredient.h"

AODDrink::AODDrink()
{
	bReplicates = true;

	USceneComponent* SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("SceneCpmponent"));
	RootComponent = SceneComponent;

	ConstructorHelpers::FObjectFinder<UDataTable> RecipeDataObject(TEXT("/Game/DrinkGame/Data/DT_Recipes"));
	if (RecipeDataObject.Succeeded())
	{
		RecipeData = RecipeDataObject.Object;
	}

	MixMethod = EMixMethod::MM_None;
}

void AODDrink::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	
	DOREPLIFETIME(AODDrink, DrinkName);
	DOREPLIFETIME(AODDrink, DrinkRating);
	DOREPLIFETIME(AODDrink, RecipeVariance);
	DOREPLIFETIME(AODDrink, TargetRecipeVolume);
	DOREPLIFETIME(AODDrink, ActualDrinkVolume);
	DOREPLIFETIME(AODDrink, MixMethod);
}

FString AODDrink::GetDrinkName()
{
	return DrinkName;
}

float AODDrink::GetRecipeVariance()
{
	return RecipeVariance;
}

float AODDrink::GetTargetRecipeVolume()
{
	return TargetRecipeVolume;
}

EDrinkScore AODDrink::GetDrinkRating()
{
	return DrinkRating;
}

void AODDrink::BuildDrink(TArray<AODIngredient*> ProvidedIngredients)
{
	// Create a map to compare to the recipe
	TMap<EIngredientType, float> ProvidedIngredientMap;
	for (AODIngredient* Ingredient : ProvidedIngredients)
	{
		// Only add non empty ingredients
		if (Ingredient->Amount > 0)
		{
			ProvidedIngredientMap.Add(UODEnumHelperFunctions::IngredientStringToEnum(Ingredient->Name), Ingredient->Amount);
		}
	}

	// Try to build a drink with the assigned ingredients
	if (RecipeData)
	{
		TMap<EIngredientType, float> TargetRecipe;
		FString TargetDrinkName;
		bool bMadeDrink = false;

		// Get each row from the table
		// These are our recipes to follow
		for (auto RowData : RecipeData->GetRowMap())
		{
			bool bHasAllIngredients = true;
			FDrinkData* DrinkRecipeData = reinterpret_cast<FDrinkData*>(RowData.Value);
			
			for (auto RecipeIngredient : DrinkRecipeData->IngredientList)
			{
				if (!ProvidedIngredientMap.Contains(RecipeIngredient.Key))
				{
					// If we're missing an ingredient
					bHasAllIngredients = false;
					break;
				}
			}

			if (bHasAllIngredients)
			{
				// We found the drink we're trying to make
				TargetRecipe.Empty();
				TargetRecipe = DrinkRecipeData->IngredientList;
				TargetDrinkName = RowData.Key.ToString();

				break;
			}
		}

		if (TargetRecipe.Num() > 0)
		{
			// Reset variance
			RecipeVariance = 0.f;
			TargetRecipeVolume = 0.f;
			ActualDrinkVolume = 0.f;

			// Now check for recipe variance
			for (TPair<EIngredientType, float> RecipeIngredient : TargetRecipe)
			{
				if (ProvidedIngredientMap.Contains(RecipeIngredient.Key))
				{
					float RecipeIngredientAmount = RecipeIngredient.Value;
					float MixerIngredientAmount = ProvidedIngredientMap[RecipeIngredient.Key];
					float IngredientVariance = FMath::Abs(RecipeIngredientAmount - MixerIngredientAmount);

					TargetRecipeVolume += RecipeIngredientAmount;
					ActualDrinkVolume += MixerIngredientAmount;

					if (IngredientVariance > IngredientVarianceThreshold)
					{
						RecipeVariance += IngredientVariance;
					}
					else
					{
						// Fail making the drink
					}
				}
			}

			if (RecipeVariance <= RecipeVarianceThreshold)
			{
				DrinkName = TargetDrinkName;
				AssignDrinkRating();
			}
		}
		else
		{
			// Fail
		}
	}
}

void AODDrink::AssignDrinkRating()
{
	float TotalScore = 0.f;
	float VolumeVariance = FMath::Abs(TargetRecipeVolume - ActualDrinkVolume);

	TotalScore = VolumeVariance + RecipeVariance;

	if (TotalScore >= 0.f && TotalScore < 1.f)
	{
		DrinkRating = EDrinkScore::DS_A;
	}
	else if (TotalScore >= 1.f && TotalScore < 2.f)
	{
		DrinkRating = EDrinkScore::DS_B;
	}
	else if (TotalScore >= 2.f && TotalScore < 3.f)
	{
		DrinkRating = EDrinkScore::DS_C;
	}
	else if (TotalScore >= 3.f && TotalScore < 4.f)
	{
		DrinkRating = EDrinkScore::DS_D;
	}
	else
	{
		DrinkRating = EDrinkScore::DS_F;
	}
}
