// Copyright 2020 Space Gurus


#include "Gameplay/ODProgressActor.h"
#include "Components/WidgetComponent.h"

// Sets default values
AODProgressActor::AODProgressActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	bReplicates = true;

	WidgetComponent = CreateDefaultSubobject<UWidgetComponent>(TEXT("WidgetComponent"));
}

// Called when the game starts or when spawned
void AODProgressActor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AODProgressActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

