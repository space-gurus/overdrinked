// Copyright 2020 Space Gurus


#include "ODGameInstance.h"
#include "OnlineSubsystem.h"
#include "OnlineSessionSettings.h"
#include "OnlineSubsystemSteam.h"
#include "Interfaces/OnlineFriendsInterface.h"
#include "Interfaces/OnlineIdentityInterface.h"
#include "Net/UnrealNetwork.h"
#include "UObject/ConstructorHelpers.h"
#include "Kismet/GameplayStatics.h"
#include "GameFramework/PlayerState.h"
#include "ODMenuPlayerController.h"
#include "ODPlayerController.h"
#include "ODMultiplayerGameMode.h"

#include "ODLobbyGameState.h"
#include "UI/ODMenuWidget.h"
#include "UI/ODMainMenuWidget.h"
#include "UI/ODOnlineJoinPage.h"
#include "ISwitcherooModule.h"

const static FName SERVER_NAME_SETTINGS_KEY = TEXT("LobbyName");
const static FName GAME_NAME_SETTINGS_KEY = TEXT("GameName");

UODGameInstance::UODGameInstance(const FObjectInitializer& ObjectInitializer)
{
}

void UODGameInstance::Init()
{
	IOnlineSubsystem* OnlineSub = IOnlineSubsystem::Get();
	if (OnlineSub)
	{
		UE_LOG(LogTemp, Warning, TEXT("Interface: %s"), *OnlineSub->GetSubsystemName().ToString());
		auto SteamSubsystem = OnlineSub->Get(OnlineSub->GetSubsystemName());
		
		if (SteamSubsystem)
		{
			UE_LOG(LogTemp, Warning, TEXT("Got the SteamSubsystem"));

			FOnlineSubsystemSteam* OnlineSubsystemSteam = static_cast<FOnlineSubsystemSteam*>(SteamSubsystem);
			if (OnlineSubsystemSteam)
			{
				UE_LOG(LogTemp, Warning, TEXT("Casted the steam subsystem"));
			}
		}

		OnlineSub->AddOnConnectionStatusChangedDelegate_Handle(FOnConnectionStatusChangedDelegate::CreateUObject(this, &UODGameInstance::HandleNetworkConnectionStatusChanged));

		SessionInterface = OnlineSub->GetSessionInterface();
		if (SessionInterface)
		{
			SessionInterface->OnCreateSessionCompleteDelegates.AddUObject(this, &UODGameInstance::OnCreateSessionComplete);
			SessionInterface->OnDestroySessionCompleteDelegates.AddUObject(this, &UODGameInstance::OnDestroySessionComplete);
			SessionInterface->OnFindSessionsCompleteDelegates.AddUObject(this, &UODGameInstance::OnFindSessionsComplete);
			SessionInterface->OnJoinSessionCompleteDelegates.AddUObject(this, &UODGameInstance::OnJoinSessionComplete);
			SessionInterface->OnSessionUserInviteAcceptedDelegates.AddUObject(this, &UODGameInstance::OnSessionUserInviteAccepted);
			SessionInterface->OnEndSessionCompleteDelegates.AddUObject(this, &UODGameInstance::OnEndSessionComplete);

			SessionInterface->AddOnSessionFailureDelegate_Handle(FOnSessionFailureDelegate::CreateUObject(this, &UODGameInstance::HandleSessionFailure));
		}

		FriendsInterface = OnlineSub->GetFriendsInterface();
		if (FriendsInterface)
		{
			OnReadFriendsListCompleteDelegate = FOnReadFriendsListComplete::CreateUObject(this, &UODGameInstance::OnReadFriendsListComplete);
		}

		OnlineIdentityInterface = OnlineSub->GetIdentityInterface();
		if (!OnlineIdentityInterface)
		{
			UE_LOG(LogOnlineGame, Error, TEXT("Failed to retrieve the Online Identity Interface"));
		}
		
		// Handle network errors
		GetEngine()->OnNetworkFailure().AddUObject(this, &UODGameInstance::HandleNetworkFailure);
	}

	if (ISwitcherooModule* Switcheroo = ISwitcherooModule::Get())
	{
		Switcheroo->EnableInputDetection();
		Switcheroo->OnDetectedInputDeviceChanged().AddUObject(this, &UODGameInstance::OnDetectedInputDeviceChanged);
	}
}

void UODGameInstance::OnDetectedInputDeviceChanged(ESwitcherooInputDevice ActiveDevice)
{
	switch (ActiveDevice)
	{
	case ESwitcherooInputDevice::KeyboardMouse:
		UE_LOG(LogTemp, Warning, TEXT("Using Keyboard"));
		break;
	case ESwitcherooInputDevice::Gamepad:
		UE_LOG(LogTemp, Warning, TEXT("Using Gamepad"));
		break;
	}
}

void UODGameInstance::ToggleRoleCards()
{
	bDrawRoleCards = !bDrawRoleCards;
}

bool UODGameInstance::GetDrawRoleCards()
{
	return bDrawRoleCards;
}

void UODGameInstance::Host()
{
	UE_LOG(LogOnlineGame, Log, TEXT("UODGameInstance::Host"));
	if (SessionInterface)
	{
		auto ExistingSession = SessionInterface->GetNamedSession(TrueLobbyName);
		if (ExistingSession)
		{
			SessionInterface->DestroySession(TrueLobbyName);
			UE_LOG(LogOnlineGame, Log, TEXT("UODGameInstance::Host destroyed an existing sesion: %s"), *TrueLobbyName.ToString());
		}
		else
		{
			UE_LOG(LogTemp, Error, TEXT("About to CreateSession"));
			TrueLobbyName = GenerateRandomLobbyName();
			CreateSession();
		}
	}

}

bool UODGameInstance::Join(int32 LobbyIndex)
{
	if (!SessionInterface || !SessionSearch)
	{
		return false;
	}

	if (FoundLobbyIndex >= 0)
	{
		// Return the result of the join
		UE_LOG(LogTemp, Warning, TEXT("JOINING SESSION IN Join"));
		return SessionInterface->JoinSession(0, DesiredLobbyName, SessionSearch->SearchResults[LobbyIndex]);
	}

	return false;
}

FString UODGameInstance::GetTrueLobbyname()
{
	return TrueLobbyName.ToString();
}

FServerData UODGameInstance::GetLobbyData()
{
	return LobbyData;
}

void UODGameInstance::PrintCurrentSessionState()
{
	if (SessionInterface)
	{
		UE_LOG(LogTemp, Error, TEXT("Session status: %s"), EOnlineSessionState::ToString(SessionInterface->GetSessionState(TrueLobbyName)));
		
		bool Presencebool = SessionInterface->GetSessionSettings(TrueLobbyName)->bAllowJoinViaPresence;
		if (Presencebool)
		{
			UE_LOG(LogTemp, Warning, TEXT("bAllowJoinViaPrecense = %s"), (Presencebool ? TEXT("true") : TEXT("false")));
		}

		int32 NumPubConn = SessionInterface->GetSessionSettings(TrueLobbyName)->NumPublicConnections;
		UE_LOG(LogTemp, Warning, TEXT("NumPubConnections: %i"), NumPubConn);
		
	}
}

FName UODGameInstance::GenerateRandomLobbyName()
{
	const FString PossibleCharacters = "ABCDEFGHJKLMNPQRTUVWXYZ2346789";
	FString NewLobbyName;

	for (int32 i = 0; i < LobbyNameLength; ++i)
	{
		NewLobbyName.AppendChar(PossibleCharacters[FMath::RandRange(0, PossibleCharacters.Len() - 1)]);
	}

	return FName(NewLobbyName);
}

void UODGameInstance::OnCreateSessionComplete(FName SessionName, bool Success)
{
	UE_LOG(LogOnlineGame, Log, TEXT("UODGameInstance::OnCreateSessionComplete"));
	if (!Success)
	{
		UE_LOG(LogTemp, Warning, TEXT("Could not create session"));
		return;
	}

	UWorld* ThisWorld = GetWorld();
	if (ThisWorld)
	{
		// We are now hosting
		bIsHost = true;
		bIsInSession = true;

		UE_LOG(LogOnlineGame, Warning, TEXT("LobbyName: %s"), *TrueLobbyName.ToString());
	}
}

void UODGameInstance::OnDestroySessionComplete(FName SessionName, bool Success)
{
	if (Success)
	{
		UE_LOG(LogOnlineGame, Log, TEXT("UODGameInstance::OnDestroySessionComplete is Successful"));
		SessionCleanup();
	}
}

void UODGameInstance::OnEndSessionComplete(FName InSessionName, bool bWasSuccessful)
{
	UE_LOG(LogOnlineGame, Log, TEXT("UODGameInstance::OnEndSessionComplete"));

	if (bWasSuccessful)
	{
		UE_LOG(LogOnlineGame, Log, TEXT("UODGameInstace::OnEndSessionComplete Successful"));
	}
}

void UODGameInstance::OnFindSessionsComplete(bool Success)
{
	if (Success && SessionSearch)
	{
		// Look for the desired lobby
		FoundLobbyIndex = -1; // Set for failure

		UE_LOG(LogTemp, Error, TEXT("Found %i lobbies"), SessionSearch->SearchResults.Num());

		for (int32 i = 0; i < SessionSearch->SearchResults.Num(); ++i)
		{
			FString LobbyName;
			FString ThisGameName;
			// Retrieve the name of this server and check for a match
			if (SessionSearch->SearchResults[i].Session.SessionSettings.Get(SERVER_NAME_SETTINGS_KEY, LobbyName))
			{
				UE_LOG(LogTemp, Warning, TEXT("DesiredLobbyName: %s   Checking: %s"), *DesiredLobbyName.ToString(), *LobbyName);
				// Successfully got the name, check for match
				if (LobbyName.Equals(DesiredLobbyName.ToString(), ESearchCase::IgnoreCase))
				{
					UE_LOG(LogTemp, Warning, TEXT("Found matching lobby!"), *LobbyName);
					FoundLobbyIndex = i;
					TrueLobbyName = DesiredLobbyName;

					// Stop the search now
					i = SessionSearch->SearchResults.Num();
				}
			}
		}
		
		// FoundLobbyIndex should now hold the index of the correct session (if it's a valid index), so join that one
		if (FoundLobbyIndex >= 0)
		{
			SearchSucceedDelegate.Broadcast();
			Join(FoundLobbyIndex);
		}
		else
		{
			SearchFailDelegate.Broadcast();
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("OnFindSessionsComplete() Failed"));
	}
}

void UODGameInstance::OnJoinSessionComplete(FName SessionName, EOnJoinSessionCompleteResult::Type Result)
{
	UE_LOG(LogTemp, Error, TEXT("JoinSessionComplete() SessionName: %s"), *SessionName.ToString());
	if (SessionInterface)
	{
		FString Address;
		if (!SessionInterface->GetResolvedConnectString(SessionName, Address))
		{
			UE_LOG(LogTemp, Warning, TEXT("Could not get connect string"));
			return;
		}

		APlayerController* MenuPlayerController = GetFirstLocalPlayerController();
		if (MenuPlayerController)
		{
			bIsHost = false;
			bIsInSession = true;

			// Seamless travel to the lobby
			MenuPlayerController->ClientTravel(Address, ETravelType::TRAVEL_Absolute, true);
		}

		UE_LOG(LogTemp, Error, TEXT("Session status: %s"), EOnlineSessionState::ToString(SessionInterface->GetSessionState(TrueLobbyName)));
	}


}

void UODGameInstance::OnReadFriendsListComplete(int32 LocalUserNum, bool bWasSuccessful, const FString& ListName, const FString& ErrorStr)
{
	if (bWasSuccessful)
	{
		if (FriendsInterface)
		{
			// Clear the cache and refresh it
			FriendsList.Empty();
			FriendNames.Empty();
			FriendsInterface->GetFriendsList(0, EFriendsLists::ToString(EFriendsLists::InGamePlayers), FriendsList);


			// Create a table to save for later
			for (int i = 0; i < FriendsList.Num(); ++i)
			{
				FriendNames.Add(FriendsList[i]->GetDisplayName());
			}

			// Tell the UI to update
			UE_LOG(LogOnlineGame, Log, TEXT("About to fire friends delegate"));
			FriendsUpdateDelegate.Broadcast();
		}
	}
	else
	{
		UE_LOG(LogOnlineGame, Warning, TEXT("Friends list read fail"));
	}
}

void UODGameInstance::OnSessionUserInviteAccepted(const bool bWasSuccess, const int32 ControllerId, TSharedPtr<const FUniqueNetId> UserId, const FOnlineSessionSearchResult& InviteResult)
{
	if (bWasSuccess)
	{
		if (SessionInterface)
		{
			FString JoinLobbyName;
			InviteResult.Session.SessionSettings.Get(SERVER_NAME_SETTINGS_KEY, JoinLobbyName);

			InviteAcceptedCompleteDelegate.Broadcast();
			SessionInterface->JoinSession(0, FName(JoinLobbyName), InviteResult);
		}
	}
}

void UODGameInstance::HandleNetworkFailure(UWorld* World, UNetDriver* NetDriver, ENetworkFailure::Type FailureType, const FString& ErrorString)
{
	//UE_LOG(LogOnlineGame, Log, TEXT("UODGameInstance::HandleNetworkFailure: %s"), *ENetworkFailure::ToString(FailureType));
	// Travel back to the main menu
	if (FailureType == ENetworkFailure::FailureReceived || FailureType == ENetworkFailure::ConnectionLost)
	{
		APlayerController* PlayerController = GetFirstLocalPlayerController(GetWorld());
		check(PlayerController);

		bIsInSession = false;
		bIsHost = false;

		bWasDisconnected = true;

		PlayerController->ClientTravel("/Game/DrinkGame/Levels/DevMaps/NewMenulevel?listen", ETravelType::TRAVEL_Absolute);
	}
}

void UODGameInstance::HandleNetworkConnectionStatusChanged(const FString& ServiceName, EOnlineServerConnectionStatus::Type LastConnectionStatus, EOnlineServerConnectionStatus::Type ConnectionStatus)
{
	UE_LOG(LogOnlineGame, Log, TEXT("UODGameInstance::HandleNetworkConnectionStatusChanged: %s"), EOnlineServerConnectionStatus::ToString(ConnectionStatus));
	
	if (ConnectionStatus != EOnlineServerConnectionStatus::Normal && ConnectionStatus != EOnlineServerConnectionStatus::Connected)
	{
		UE_LOG(LogOnlineGame, Log, TEXT("UODGameInstance::HandleNetworkConnectionStatusChanged: Going to main menu"));

	}
}

void UODGameInstance::HandleSessionFailure(const FUniqueNetId& NetId, ESessionFailure::Type FailureType)
{
	UE_LOG(LogOnlineGame, Log, TEXT("UODGameInstance::HandleSessionFailure: %u"), (uint32)FailureType);

}

void UODGameInstance::TryJoinLobby(FString LobbyToSearchFor)
{
	DesiredLobbyName = FName(LobbyToSearchFor);

	SessionSearch = MakeShareable(new FOnlineSessionSearch());
	if (SessionSearch)
	{
		SessionSearch->MaxSearchResults = 20000;
		SessionSearch->QuerySettings.Set(SEARCH_PRESENCE, true, EOnlineComparisonOp::Equals);
		//SessionSearch->QuerySettings.Set(GAME_NAME_SETTINGS_KEY, FString(TEXT("BotsBoozeBrawls")), EOnlineComparisonOp::Equals);
		SessionSearch->bIsLanQuery = false;
		SessionInterface->FindSessions(0, SessionSearch.ToSharedRef());

		UE_LOG(LogTemp, Warning, TEXT("Got here in TryJoinLobby"));

		// Toggle necessary menu items
		SearchBeginDelegate.Broadcast();
	}
}

void UODGameInstance::CreateSession()
{
	if (SessionInterface)
	{
		FOnlineSessionSettings SessionSettings;

		// Are we using steam?
		if (IOnlineSubsystem::Get()->GetSubsystemName() == "NULL")
		{
			SessionSettings.bIsLANMatch = true;
		}
		else
		{
			SessionSettings.bIsLANMatch = false;
		}

		SessionSettings.NumPublicConnections = 4;
		SessionSettings.NumPrivateConnections = 0;
		SessionSettings.bShouldAdvertise = true;
		SessionSettings.bUsesPresence = true;
		SessionSettings.bAllowInvites = true;
		SessionSettings.bAllowJoinViaPresence = true;
		SessionSettings.bAllowJoinInProgress = true;
		SessionSettings.Set(SERVER_NAME_SETTINGS_KEY, TrueLobbyName.ToString(), EOnlineDataAdvertisementType::ViaOnlineService);
		SessionSettings.Set(GAME_NAME_SETTINGS_KEY, FString(TEXT("BotsBoozeBrawls")), EOnlineDataAdvertisementType::ViaOnlineService);

		LobbyData.Name = TrueLobbyName;
		LobbyData.CurrentPlayers = 1;
		LobbyData.MaxPlayers = 4;

		SessionInterface->CreateSession(0, TrueLobbyName, SessionSettings);
	}
}

void UODGameInstance::CloseExistingSession()
{
	UE_LOG(LogTemp, Warning, TEXT("CloseSession()"));
	if (SessionInterface)
	{
		UE_LOG(LogTemp, Warning, TEXT("SessionInterface good"));
		auto ExistingSession = SessionInterface->GetNamedSession(TrueLobbyName);
		if (ExistingSession)
		{
			UE_LOG(LogTemp, Warning, TEXT("Destroying session"));
			SessionInterface->DestroySession(TrueLobbyName);
		}
	}
}

void UODGameInstance::ClientCloseSession()
{
	UE_LOG(LogOnlineGame, Log, TEXT("UODGameInstance::ClientCloseSession"));
	
	if (SessionInterface)
	{
		SessionInterface->DestroySession(TrueLobbyName);
	}
}

void UODGameInstance::HostCloseSession()
{
	UE_LOG(LogOnlineGame, Log, TEXT("UODGameInstance::HostCloseSession"));

	if (SessionInterface)
	{
		UE_LOG(LogOnlineGame, Log, TEXT("Ending Session for Session: %s"), *TrueLobbyName.ToString());

		if (OnlineIdentityInterface)
		{
			AODLobbyGameState* LobbyGameState = Cast<AODLobbyGameState>(UGameplayStatics::GetGameState(GetWorld()));
			if (LobbyGameState)
			{
				for (APlayerState* PlayerState : LobbyGameState->PlayerArray)
				{
					TSharedPtr<const FUniqueNetId> PlayerID = PlayerState->GetUniqueId().GetUniqueNetId();
					if (PlayerID)
					{
						UE_LOG(LogOnlineGame, Log, TEXT("Unregistering %s"), *PlayerID->ToString());
						SessionInterface->UnregisterPlayer(TrueLobbyName, *PlayerID);
					}
				}

			}
		}
		SessionInterface->DestroySession(TrueLobbyName);
	}
}

void UODGameInstance::SessionCleanup()
{
	// Just do some cleanup for next time
	DesiredLobbyName = "";
	TrueLobbyName = "";
	bIsHost = false;
	bIsInSession = false;
	bWasDisconnected = false;
}

void UODGameInstance::ReturnToMainMenu()
{
	Super::ReturnToMainMenu();

	SessionCleanup();
}

void UODGameInstance::KickPlayer(int32 PlayerToKick)
{
	if (SessionInterface)
	{
		//SessionInterface->UnregisterPlayer(TrueLobbyName, PlayerToKick);
	}
}

void UODGameInstance::RefreshFriendsList()
{
	if (FriendsInterface)
	{
		FriendsInterface->ReadFriendsList(0, EFriendsLists::ToString(EFriendsLists::InGamePlayers), OnReadFriendsListCompleteDelegate);
	}
}

void UODGameInstance::InviteFriend(FString FriendName)
{
	if (FriendNames.Contains(FriendName))
	{
		int32 FriendIndex = FriendNames.IndexOfByKey(FriendName);

		if (FriendsList.IsValidIndex(FriendIndex))
		{
			if (SessionInterface)
			{
				if (SessionInterface->SendSessionInviteToFriend(0, TrueLobbyName, *FriendsList[FriendIndex]->GetUserId()))
				{
					UE_LOG(LogTemp, Warning, TEXT("SendSessionInviteToFriend succeeded"));
				}
				else
				{
					UE_LOG(LogTemp, Warning, TEXT("SendSessionInviteToFriend failed"));
				}
			}
		}
	}
}

void UODGameInstance::SetPartySize(int32 PartySize)
{
	PartyMemberCount = PartySize;
}

int32 UODGameInstance::GetPartySize()
{
	return PartyMemberCount;
}

bool UODGameInstance::HasVisitedMainMenu()
{
	return bHasVisitedMainMenu;
}

bool UODGameInstance::IsInSession()
{
	return bIsInSession;
}

bool UODGameInstance::IsHost()
{
	return bIsHost;
}

TArray<FString> UODGameInstance::GetFriendNames()
{
	return FriendNames;
}

void UODGameInstance::HostReloadLevel()
{
	// Get the server's player controller
	AODPlayerController* HostPC = Cast<AODPlayerController>(UGameplayStatics::GetPlayerController(GetWorld(), 0));
	if (HostPC)
	{
		GetWorld()->ServerTravel(GetWorld()->GetName());
	}
}

void UODGameInstance::HostMovePartyToLobby()
{
	AODMultiplayerGameMode* MPGameMode = Cast<AODMultiplayerGameMode>(UGameplayStatics::GetGameMode(GetWorld()));
	if (MPGameMode)
	{
		MPGameMode->NotifyPlayersLeavingLevel();
	}

	// Get the server's player controller
	AODPlayerController* HostPC = Cast<AODPlayerController>(UGameplayStatics::GetPlayerController(GetWorld(), 0));
	if (HostPC)
	{
		GetWorld()->ServerTravel("/Game/ProMainMenu/Levels/MainMenu?listen");
	}
}
