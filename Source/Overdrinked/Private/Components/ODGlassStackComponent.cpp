// Copyright 2020 Space Gurus


#include "Components/ODGlassStackComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Net/UnrealNetwork.h"
#include "Actors/ODDrinkGlass.h"
#include "NiagaraFunctionLibrary.h"
#include "NiagaraComponent.h"

UODGlassStackComponent::UODGlassStackComponent()
{
	PrimaryComponentTick.bCanEverTick = false;

	ConstructorHelpers::FObjectFinder<UStaticMesh> StemmedSMObject(TEXT("/Game/DrinkGameHD/Artwork/Resources/Glasses/SM_StemGlass"));
	if (StemmedSMObject.Succeeded())
	{
		StemmedGlassMesh = StemmedSMObject.Object;
	}

	ConstructorHelpers::FObjectFinder<UStaticMesh> BrewSMObject(TEXT("/Game/DrinkGameHD/Artwork/Resources/Glasses/SM_BrewGlass"));
	if (BrewSMObject.Succeeded())
	{
		BrewGlassMesh = BrewSMObject.Object;
	}

	ConstructorHelpers::FObjectFinder<UStaticMesh> ShotSMObject(TEXT("/Game/DrinkGameHD/Artwork/Resources/Glasses/SM_ShotGlass"));
	if (ShotSMObject.Succeeded())
	{
		ShotGlassMesh = ShotSMObject.Object;
	}
}


void UODGlassStackComponent::BeginPlay()
{
	Super::BeginPlay();

	CurrentNumGlasses = StartingNumGlasses;

	if (StartingNumGlasses == 0)
	{
		// This is blank spot, so we're a table or something else that should just stack
		CalculateGlassPositions(false);
	}
	else
	{
		CalculateGlassPositions(true);
	}

	UE_LOG(LogTemp, Warning, TEXT("GlassStackInfo Size: %i"), GlassStackInfo.Num());
}

void UODGlassStackComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UODGlassStackComponent, CurrentNumGlasses);
	DOREPLIFETIME(UODGlassStackComponent, GlassStackInfo);
}

#if WITH_EDITOR
void UODGlassStackComponent::PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent)
{
	Super::PostEditChangeProperty(PropertyChangedEvent);

	FName PropertyName = (PropertyChangedEvent.Property != NULL) ? PropertyChangedEvent.Property->GetFName() : NAME_None;

	bool bShouldUpdateGlasses = false;

	if ((PropertyName == GET_MEMBER_NAME_CHECKED(UODGlassStackComponent, GlassType)))
	{
		AdjustGlassType();
		bShouldUpdateGlasses = true;
	}

	if ((PropertyName == GET_MEMBER_NAME_CHECKED(UODGlassStackComponent, StartingNumGlasses)))
	{
		bShouldUpdateGlasses = true;
	}

	if (bShouldUpdateGlasses)
	{
		CalculateGlassPositions(true);
		SpawnStartingGlassware();
	}
}
#endif

void UODGlassStackComponent::AdjustGlassType()
{
	switch (GlassType)
	{
	case EGlassType::GT_Brew:
		GlassWidth = 25.f;
		GlassHeight = 27.f;
		CurrentGlassMesh = BrewGlassMesh;
		break;
	case EGlassType::GT_Stemmed:
		GlassWidth = 20.f;
		GlassHeight = 24.f;
		CurrentGlassMesh = StemmedGlassMesh;
		break;
	case EGlassType::GT_Shot:
		GlassWidth = 15.f;
		GlassHeight = 15.f;
		CurrentGlassMesh = ShotGlassMesh;
		break;
	default:
		break;
	}
}

void UODGlassStackComponent::CalculateGlassPositions(bool bInEditor)
{
	// First we need to determine how many layers there will be
	int32 TempGlassNum = 0;
	int32 GlassesToSpawn = 0;
	float GWidth = 0.f;
	float GHeight = 0.f;
	float GScale = 0.f;
	if (bInEditor)
	{
		TempGlassNum = StartingNumGlasses;
		GWidth = GlassWidth;
		GHeight = GlassHeight;
		GScale = GlassMeshScale;
		GlassesToSpawn = StartingNumGlasses;
	}
	else
	{
		TempGlassNum = 14;
		GlassesToSpawn = 14;
		GWidth = 25.f;
		GHeight = 27.f;
		GScale = 1.75f;
	}

	int32 LayerCount = 1;
	float LayerDistAdder = 0.f; // Used to determine starting position for each first glass in a layer
	while (TempGlassNum > 0)
	{
		int32 GlassesThisLayer = FMath::Square(LayerCount);
		TempGlassNum -= GlassesThisLayer;

		if (TempGlassNum > 0)
		{
			LayerCount++;
			LayerDistAdder += GWidth / 2;
		}
	}

	// Clear the cache
	GlassPositions.Empty();

	// Edge cases for 1, 2, 3 and 4
	if (MaxGlasses == 1)
	{
		// Only one glass so we just need to spawn in the center
		GlassPositions.Add(GlassSpawnCenter);
	}
	else if (MaxGlasses == 2)
	{
		// Two glasses side by side
		float DistApart = GWidth / 2;

		FVector Pos1 = GlassSpawnCenter;
		FVector Pos2 = GlassSpawnCenter;
		Pos1.X += DistApart;
		Pos2.X -= DistApart;
		GlassPositions.Add(Pos1);
		GlassPositions.Add(Pos2);
	}
	else if (MaxGlasses == 3)
	{
		// Small triangle
		float DistApart = GWidth / 2;

		FVector Pos1 = GlassSpawnCenter;
		FVector Pos2 = GlassSpawnCenter;
		FVector Pos3 = GlassSpawnCenter;
		Pos1.X += DistApart;
		Pos2.X -= DistApart;
		Pos3.Z += GHeight;
		GlassPositions.Add(Pos1);
		GlassPositions.Add(Pos2);
		GlassPositions.Add(Pos3);
	}
	else if (MaxGlasses == 4)
	{
		// Small pyramid
		float DistApart = GWidth / 2;

		FVector Pos1 = GlassSpawnCenter;
		FVector Pos2 = GlassSpawnCenter;
		FVector Pos3 = GlassSpawnCenter;
		FVector Pos4 = GlassSpawnCenter;
		// Build a triangle out of the bottom later
		// Top of triangle
		Pos1.Y -= DistApart;
		// Corners
		float TriangleSide = (DistApart * 1.5f) / FMath::Sqrt(2);
		Pos2.X += TriangleSide;
		Pos2.Y += TriangleSide;
		Pos3.X -= TriangleSide;
		Pos3.Y += TriangleSide;
		// Top glass
		Pos4.Z += GHeight;
		GlassPositions.Add(Pos1);
		GlassPositions.Add(Pos2);
		GlassPositions.Add(Pos3);
		GlassPositions.Add(Pos4);
	}
	else
	{
		// 5 or more so build a pyramid
		int32 LayerNum = LayerCount;

		while (LayerNum > 0)
		{
			// Loop through each row and column to find spawn points
			for (float x = LayerDistAdder; x >= -LayerDistAdder; x -= GWidth)
			{
				for (float y = LayerDistAdder; y >= -LayerDistAdder; y -= GWidth)
				{
					if (GlassesToSpawn > 0)
					{
						float zVal = (LayerCount - LayerNum) * GHeight;

						if (GlassesToSpawn == 1 && LayerNum == 1)
						{
							FVector GlassPos = GlassSpawnCenter;
							GlassPos.Z += zVal;
							GlassPositions.Add(GlassPos);
						}
						else
						{
							FVector GlassPos = GlassSpawnCenter;
							GlassPos += FVector(x, y, zVal);
							GlassPositions.Add(GlassPos);
						}
					}

					GlassesToSpawn--;
				}
			}

			LayerNum--;
			LayerDistAdder -= LayerDistAdder / 2;
		}
	}
}

void UODGlassStackComponent::SpawnStartingGlassware()
{
	for (FGlassInfo GlassInfo : GlassStackInfo)
	{
		if (GlassInfo.GlassMesh)
		{
			GlassInfo.GlassMesh->DestroyComponent();
		}

		if (GlassInfo.DirtyEffect)
		{
			GlassInfo.DirtyEffect->DestroyComponent();
		}
	}

	GlassStackInfo.Empty();

	// Now spawn a glass mesh component at each of our points
	FName GlassName = FName("GlassName");
	int32 GlassCount = 1;
	for (FVector GlassPos : GlassPositions)
	{
		UStaticMeshComponent* GlassMeshComp = NewObject<UStaticMeshComponent>(this);

		if (GlassMeshComp)
		{
			GlassMeshComp->SetupAttachment(this);
			GlassMeshComp->RegisterComponent();
			GlassMeshComp->SetIsReplicated(true);
			GlassMeshComp->SetRelativeLocation(GlassPos);
			GlassMeshComp->SetRelativeRotation(FRotator(0.f, 45.f, 0.f));

			// For outlined look
			GlassMeshComp->SetRenderCustomDepth(true);
			GlassMeshComp->SetCustomDepthStencilValue(253);

			if (CurrentGlassMesh)
			{
				GlassMeshComp->SetStaticMesh(CurrentGlassMesh);
			}
			
			GlassMeshComp->SetVisibility(true);

			FGlassInfo NewGlassInfo;
			NewGlassInfo.GlassMesh = GlassMeshComp;
			NewGlassInfo.DirtyEffect = NULL;
			NewGlassInfo.GlassType = GlassType;
			NewGlassInfo.bIsDirty = false;

			GlassStackInfo.Add(NewGlassInfo);
		}
	}
}

UStaticMesh* UODGlassStackComponent::GetGlassMesh(EGlassType GType)
{
	switch (GType)
	{
	case EGlassType::GT_Brew:
		return BrewGlassMesh;
		break;
	case EGlassType::GT_Stemmed:
		return StemmedGlassMesh;
		break;
	case EGlassType::GT_Shot:
		return ShotGlassMesh;
		break;
	default:
		return NULL;
		break;
	}
}

AODDrinkGlass* UODGlassStackComponent::RemoveGlass(bool bShouldSpawnGlass)
{
	if (CurrentNumGlasses > 0)
	{
		int32 GlassIndex = CurrentNumGlasses - 1;
		if (GlassStackInfo.IsValidIndex(GlassIndex))
		{
			FGlassInfo GlassInfo = GlassStackInfo[GlassIndex];

			// Now handle the return
			if (bShouldSpawnGlass)
			{
				AODDrinkGlass* GlassToReturn = NULL;
				FActorSpawnParameters SpawnParams;

				switch (GlassInfo.GlassType)
				{
				case EGlassType::GT_Brew:
					GlassToReturn = GetWorld()->SpawnActor<AODDrinkGlass>(BrewGlassBP, FVector(0.f, 0.f, -100.f), FRotator(0.f, 0.f, 0.f), SpawnParams);
					break;
				case EGlassType::GT_Shot:
					GlassToReturn = GetWorld()->SpawnActor<AODDrinkGlass>(ShotGlassBP, FVector(0.f, 0.f, -100.f), FRotator(0.f, 0.f, 0.f), SpawnParams);
					break;
				case EGlassType::GT_Stemmed:
					GlassToReturn = GetWorld()->SpawnActor<AODDrinkGlass>(StemmedGlassBP, FVector(0.f, 0.f, -100.f), FRotator(0.f, 0.f, 0.f), SpawnParams);
					break;
				default:
					break;
				}

				return GlassToReturn;
			}
		}
	}

	return NULL;
}

void UODGlassStackComponent::RemoveTopGlassMesh_Implementation()
{
	if (CurrentNumGlasses > 0)
	{
		int32 GlassIndex = CurrentNumGlasses - 1;
		if (GlassStackInfo.IsValidIndex(GlassIndex))
		{
			FGlassInfo GlassInfo = GlassStackInfo[GlassIndex];

			if (GlassInfo.bIsDirty && GlassInfo.DirtyEffect)
			{
				GlassInfo.DirtyEffect->DestroyComponent();
			}

			if (GlassInfo.GlassMesh)
			{
				GlassInfo.GlassMesh->DestroyComponent();
			}

			CurrentNumGlasses--;

			GlassStackInfo.Remove(GlassInfo);
		}
	}
}

void UODGlassStackComponent::AddGlass(EGlassType GType, bool bIsDirty)
{
	if (CurrentNumGlasses < MaxGlasses)
	{
		FGlassInfo NewGlassInfo;

		// CurrentNumGlasses gives us the next index
		UStaticMeshComponent* GlassMeshComp = NewObject<UStaticMeshComponent>(this);

		if (GlassMeshComp)
		{
			GlassMeshComp->SetupAttachment(this);
			GlassMeshComp->RegisterComponent();
			GlassMeshComp->SetIsReplicated(true);
			GlassMeshComp->SetRelativeLocation(GlassPositions[CurrentNumGlasses]);
			GlassMeshComp->SetRelativeRotation(FRotator(0.f, 45.f, 0.f));

			UStaticMesh* GlassMesh = GetGlassMesh(GType);
			if (GlassMesh)
			{
				GlassMeshComp->SetStaticMesh(GlassMesh);
				GlassMeshComp->SetVisibility(true);
			}

			NewGlassInfo.GlassMesh = GlassMeshComp;
			NewGlassInfo.GlassType = GType;
			NewGlassInfo.bIsDirty = bIsDirty;
			
			// For outlined look
			GlassMeshComp->SetRenderCustomDepth(true);
			GlassMeshComp->SetCustomDepthStencilValue(253);

			// Spawn a Niagara system if needed
			UNiagaraComponent* NewDirtyEffect = NULL;
			if (bIsDirty)
			{
				NewDirtyEffect = UNiagaraFunctionLibrary::SpawnSystemAttached(DirtyEffect, GlassMeshComp, "", FVector(0.f, 0.f, 2.f), FRotator::ZeroRotator, FVector(1.f, 1.f, 1.f), EAttachLocation::SnapToTarget, false, ENCPoolMethod::None);

			}

			NewGlassInfo.DirtyEffect = NewDirtyEffect;

			GlassStackInfo.Add(NewGlassInfo);
		}

		CurrentNumGlasses++;
	}
}

void UODGlassStackComponent::UpdateNewGlassMesh_Implementation(FGlassInfo GlassInfo)
{
	
}

void UODGlassStackComponent::OnRep_GlassStackInfo()
{
	UE_LOG(LogTemp, Warning, TEXT("OnRep_GlassStackInfo"));
	for (FGlassInfo GlassInfo : GlassStackInfo)
	{
		if (GlassInfo.GlassMesh)
		{
			GlassInfo.GlassMesh->SetRenderCustomDepth(true);
			GlassInfo.GlassMesh->SetCustomDepthStencilValue(253);
		}

		// Does it have a niagara effect yet?
		if (GlassInfo.bIsDirty && !GlassInfo.DirtyEffect)
		{
			GlassInfo.DirtyEffect = UNiagaraFunctionLibrary::SpawnSystemAttached(DirtyEffect, GlassInfo.GlassMesh, "", FVector(0.f, 0.f, 2.f), FRotator::ZeroRotator, FVector(1.f, 1.f, 1.f), EAttachLocation::SnapToTarget, false, ENCPoolMethod::None);
		}
	}
}

int32 UODGlassStackComponent::GetNumGlasses()
{
	return CurrentNumGlasses;
}

