// Copyright 2020 Space Gurus


#include "ODMultiplayerGameMode.h"
#include "ODGameInstance.h"
#include "ODPlayerController.h"
#include "ODMultiplayerGameState.h"
#include "ODPlayerState.h"
#include "ODPlayerController.h"
#include "AI/ODAIManager.h"
#include "Environment/ODBlenderBase.h"
#include "ODMenuPawn.h"
#include "Kismet/GameplayStatics.h"
#include "Data/LevelConstraintData.h"

AODMultiplayerGameMode::AODMultiplayerGameMode()
{
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprints/Characters/BP_PlayerCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}

	PlayerControllerClass = AODPlayerController::StaticClass();
	GameStateClass = AODMultiplayerGameState::StaticClass();
	PlayerStateClass = AODPlayerState::StaticClass();

	bUseSeamlessTravel = true;

	// Get our data
	ConstructorHelpers::FObjectFinder<UDataTable> LevelDataObject(TEXT("/Game/DrinkGame/Data/DT_LevelConstraints"));
	if (LevelDataObject.Succeeded())
	{
		LevelConstraintDataTable = LevelDataObject.Object;
	}
}

void AODMultiplayerGameMode::PostLogin(APlayerController* NewPlayer)
{
	Super::PostLogin(NewPlayer);

	AODPlayerController* ODPlayerController = Cast<AODPlayerController>(NewPlayer);
	if (ODPlayerController)
	{
		PlayerControllerList.AddUnique(ODPlayerController);
		// Everyone starts as not ready

		// Set the BBB player num for some UI stuff
		AODPlayerState* NewPlayerState = Cast<AODPlayerState>(ODPlayerController->PlayerState);
		if (NewPlayerState)
		{
			UE_LOG(LogTemp, Warning, TEXT("Got the Player State in the MPGameMode"));
			NewPlayerState->SetPlayerNum(NumPlayers);
		}
	}
}

bool AODMultiplayerGameMode::ReadyToStartMatch_Implementation()
{
	UODGameInstance* ODGameInstance = Cast<UODGameInstance>(UGameplayStatics::GetGameInstance(GetWorld()));
	if (ODGameInstance)
	{
		if (bTraveledSeamlessly)
		{
			MaxNumPlayers = ODGameInstance->GetPartySize();
		}

		// Only start if all players have loaded locally
		if (NumReadyPlayers == MaxNumPlayers)
		{
			return true;
		}
	}
	return false;
}

void AODMultiplayerGameMode::HandleMatchHasStarted()
{
	Super::HandleMatchHasStarted();

	// Load in our data
	ReadInLevelData();

	FTimerHandle DelayStartHandle;

	if (GetWorld()->IsPlayInEditor())
	{
		GetWorldTimerManager().SetTimer(DelayStartHandle, this, &AODMultiplayerGameMode::HandleDelayedGameStart, 5.f, false);
	}
	else
	{
		GetWorldTimerManager().SetTimer(DelayStartHandle, this, &AODMultiplayerGameMode::HandleDelayedGameStart, 3.f, false);
	}
}

void AODMultiplayerGameMode::PostSeamlessTravel()
{
	Super::PostSeamlessTravel();

	bTraveledSeamlessly = true;
}

void AODMultiplayerGameMode::InitSeamlessTravelPlayer(AController* NewController)
{
	// We need to do this here because PostLogin isn't called on Seamless travel
	AODPlayerController* ODPlayerController = Cast<AODPlayerController>(NewController);
	if (ODPlayerController)
	{
		PlayerControllerList.AddUnique(ODPlayerController);
	}
}

void AODMultiplayerGameMode::NotifyPlayersLeavingLevel()
{
	for (AODPlayerController* ODPlayerController : PlayerControllerList)
	{
		ODPlayerController->HandleLeaveLevel();
	}
}

void AODMultiplayerGameMode::NotifyPlayersOfNewOrder(FDrinkOrder DrinkOrder)
{
	for (AODPlayerController* ODPlayerController : PlayerControllerList)
	{
		ODPlayerController->AddOrderToHUD(DrinkOrder);
	}
}

void AODMultiplayerGameMode::NotifyPlayersOfClosedOrder(FDrinkOrder DrinkOrder)
{
	for (AODPlayerController* ODPlayerController : PlayerControllerList)
	{
		ODPlayerController->RemoveOrderFromHUD(DrinkOrder);
	}
}

void AODMultiplayerGameMode::SetAIManagerReference(AODAIManager* Manager)
{
	if (Manager)
	{
		AIManager = Manager;
	}
}

int32 AODMultiplayerGameMode::GetLevelLengthInMinutes()
{
	return LevelLengthInMinutes;
}

void AODMultiplayerGameMode::SetGameSpeed(float val)
{
	GameMinuteSpeed = val;
}

float AODMultiplayerGameMode::GetGameSpeed()
{
	return GameMinuteSpeed;
}

void AODMultiplayerGameMode::SetPauseGameTime(bool bShouldPause)
{
	bIsTimePaused = bShouldPause;
}

bool AODMultiplayerGameMode::IsGameTimePaused()
{
	return bIsTimePaused;
}

void AODMultiplayerGameMode::HandlePlayerControllerIsReady(AODPlayerController* ReadyPlayerController)
{
	if (PlayerControllerList.Contains(ReadyPlayerController))
	{
		NumReadyPlayers++;
	}
}

void AODMultiplayerGameMode::HandleDelayedGameStart()
{
	// Start the match and stuff
	for (AODPlayerController* ODPlayerController : PlayerControllerList)
	{
		ODPlayerController->PrepareForGameStart();
	}

	AODMultiplayerGameState* MPGameState = Cast<AODMultiplayerGameState>(UGameplayStatics::GetGameState(GetWorld()));
	if (MPGameState)
	{
		MPGameState->StartLevelTimer(LevelLengthInMinutes);
	}

	if (AIManager)
	{
		AIManager->Server_BeginGameStartCountdown();
	}
}

TArray<int32> AODMultiplayerGameMode::GetStarThresholds()
{
	return StarThresholds;
}

void AODMultiplayerGameMode::HandleBrawlLevelUpdate(float BrawlPercentage)
{
	if (!bIsLevelOver)
	{
		for (AODPlayerController* ODPlayerController : PlayerControllerList)
		{
			// Level is over!
			if (BrawlPercentage >= BrawlThreshold)
			{
				UE_LOG(LogTemp, Warning, TEXT("AODMultiplayerGameMode::HandleBrawlLevelUpdate: BRAWL THRESHOLD REACHED"));
				if (ODPlayerController)
					ODPlayerController->HandleLevelFail();
			}

			// We need to normalize the value for the UI
			float UIBrawlPercent = BrawlPercentage / BrawlThreshold;
			if (ODPlayerController)
				ODPlayerController->UpdateBrawlLevelHUD(UIBrawlPercent);
		}

		if (BrawlPercentage >= BrawlThreshold)
		{
			bIsLevelOver = true;
		}
	}
}

void AODMultiplayerGameMode::HandleLevelEnd()
{
	for (AODPlayerController* ODPlayerController : PlayerControllerList)
	{
		ODPlayerController->HandleLevelEnd();
	}
}

float AODMultiplayerGameMode::GetIceMeltTime()
{
	return IceMeltTime;
}

void AODMultiplayerGameMode::SetBlendTimeGlobal(float NewBlendTime)
{
	TArray<AActor*> FoundActors;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), AODBlenderBase::StaticClass(), FoundActors);
	for (AActor* FoundActor : FoundActors)
	{
		AODBlenderBase* BlenderBase = Cast<AODBlenderBase>(FoundActor);
		if (BlenderBase)
		{
			BlenderBase->SetBlendSpeed(NewBlendTime);
		}
	}
}

void AODMultiplayerGameMode::SetBlenderOverflowTimeGlobal(float NewBlenderOverflowTime)
{
	TArray<AActor*> FoundActors;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), AODBlenderBase::StaticClass(), FoundActors);
	for (AActor* FoundActor : FoundActors)
	{
		AODBlenderBase* BlenderBase = Cast<AODBlenderBase>(FoundActor);
		if (BlenderBase)
		{
			BlenderBase->SetBlenderOverflowTime(NewBlenderOverflowTime);
		}
	}
}

void AODMultiplayerGameMode::SetBrawlThreshold(float NewThreshold)
{
	BrawlThreshold = NewThreshold;
}

void AODMultiplayerGameMode::ReadInLevelData()
{
	FLevelConstraintData* LevelConstraintData = LevelConstraintDataTable->FindRow<FLevelConstraintData>(FName(UGameplayStatics::GetCurrentLevelName(GetWorld())), FString(""));
	if (LevelConstraintData)
	{
		// Set our necessary variables for this level
		OneStarPoints = LevelConstraintData->OneStarPointThreshold;
		StarThresholds.Add(OneStarPoints);
		TwoStarPoints = LevelConstraintData->TwoStarPointThreshold;
		StarThresholds.Add(TwoStarPoints);
		ThreeStarPoints = LevelConstraintData->ThreeStarPointThreshold;
		StarThresholds.Add(ThreeStarPoints);

		BrawlThreshold = LevelConstraintData->BrawlThreshold;

		LevelLengthInMinutes = LevelConstraintData->LevelLengthInMinutes;
	}
}

