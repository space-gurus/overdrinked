// Copyright 2020 Space Gurus


#include "ODPlayerController.h"
#include "PlayerCharacter.h"

#include "UObject/ConstructorHelpers.h"
#include "Kismet/GameplayStatics.h"

#include "ODGameInstance.h"
#include "ODMultiplayerGameMode.h"
#include "ODMultiplayerGameState.h"
#include "ODLobbyGameMode.h"
#include "ODGameInstance.h"
#include "Actors/ODIngredientDispenser.h"
#include "Actors/ODDrinkMixer.h"
#include "Actors/ODDrinkGlass.h"
#include "Actors/ODInteractInterface.h"	
#include "Actors/ODGarnish.h"
#include "Environment/ODQTEStation.h"
#include "Environment/ODChopStation.h"
#include "Environment/ODCounter.h"
#include "AI/ODAICharacter.h"
#include "Gameplay/ODDrink.h"
#include "UI/ODUserWidget.h"
#include "UI/ODInGameHUD.h"
#include "UI/ODContainerWidget.h"
#include "UI/ODMixerWidget.h"
#include "UI/ODPourHUD.h"
#include "UI/ODQTEWidget.h"

AODPlayerController::AODPlayerController()
{
	ConstructorHelpers::FClassFinder<UODUserWidget> OptionsMenuBP(TEXT("/Game/Blueprints/UI/Menu/WBP_PauseMenu"));
	if (OptionsMenuBP.Class)
	{
		OptionsMenuBPClass = OptionsMenuBP.Class;
	}

	ConstructorHelpers::FClassFinder<UODUserWidget> InGameUIBP(TEXT("/Game/Blueprints/UI/WBP_InGameHUD"));
	if (InGameUIBP.Class)
	{
		InGameUIBPClass = InGameUIBP.Class;
	}

	ConstructorHelpers::FClassFinder<UODContainerWidget> ContainerWidgetBP(TEXT("/Game/Blueprints/UI/Widgets/WBP_ContainerWidget"));
	if (ContainerWidgetBP.Class)
	{
		ContainerWidgetBPClass = ContainerWidgetBP.Class;
	}

// 	ConstructorHelpers::FClassFinder<UUserWidget> LevelFailHostBP(TEXT("/Game/ProMainMenu/ODMenu/Game/WBP_CrashTest"));
// 	if (LevelFailHostBP.Class)
// 	{
// 		LevelFailHostBPClass = LevelFailHostBP.Class;
// 	}
}

void AODPlayerController::BeginPlay()
{
	Super::BeginPlay();

	APlayerCharacter* player = Cast<APlayerCharacter>(GetPawn());
	if (player)
	{
		PlayerCharacter = player;
		// Move where we hear sound
		SetAudioListenerOverride(PlayerCharacter->GetMesh(), FVector(0.f), FRotator(0.f));
	}

	// Load the proper UI
	// PS this is janky
	if (HasAuthority())
	{
		AODMultiplayerGameMode* MPGameMode = Cast<AODMultiplayerGameMode>(UGameplayStatics::GetGameMode(GetWorld()));

		if (MPGameMode)
		{
			LoadInGameUI();
		}
	}
	else
	{
		LoadInGameUI();
	}


	// Save a reference to the game state
	MPGameState = Cast<AODMultiplayerGameState>(GetWorld()->GetGameState());
	
	if (IsLocalPlayerController())
	{
		// Let the server know we're ready to go!
		Server_InformServerReadyStatus();
	}

	
}

void AODPlayerController::OnPossess(APawn* aPawn)
{
	Super::OnPossess(aPawn);

	if (HasAuthority())
	{
		UE_LOG(LogTemp, Error, TEXT("OnPossess ON THE SERVER %s"), *GetName());
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("OnPossess ON THE CLIENT %s"), *GetName());
	}

	APlayerCharacter* player = Cast<APlayerCharacter>(GetPawn());
	if (player)
	{
		PlayerCharacter = player;
	}

	//SetPlayerWidgetOnCharacter();
}

void AODPlayerController::OnRep_PlayerState()
{
	Super::OnRep_PlayerState();

	UE_LOG(LogTemp, Warning, TEXT("OnRep_PlayerState for %s"), *GetName());
}

void AODPlayerController::PrepareForGameStart_Implementation()
{
	// Make sure we're good
	APlayerCharacter* player = Cast<APlayerCharacter>(GetPawn());
	if (player)
	{
		PlayerCharacter = player;
	}

	if (InGameUIWidget && IsLocalPlayerController())
	{
		UE_LOG(LogTemp, Warning, TEXT("PrepareForGameStart() for %s"), *GetName());
		InGameUIWidget->FadeIn();
		StartGameCountdown();
	}

	if (PlayerCharacter)
	{
		PlayerCharacter->SetupPlayerIDWidget();
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("PlayerCharacter NOT valid in OnRep_PlayerState()"));
	}
}

void AODPlayerController::StartGameCountdown()
{
	if (IsLocalPlayerController())
	{
		GetWorldTimerManager().SetTimer(GameStartTimerHandle, this, &AODPlayerController::StartRoundSetup, 3.0f, false);

		if (InGameUIWidget)
		{
			InGameUIWidget->StartRoundCountdown();
		}
	}
}

void AODPlayerController::Server_InformServerReadyStatus_Implementation()
{
	// Let the game mode know
	AODMultiplayerGameMode* MPGameMode = Cast<AODMultiplayerGameMode>(UGameplayStatics::GetGameMode(GetWorld()));
	if (MPGameMode)
	{
		MPGameMode->HandlePlayerControllerIsReady(this);
	}
}

void AODPlayerController::StartRoundSetup()
{
	UE_LOG(LogTemp, Error, TEXT("START ROUND!!!!"));
}

void AODPlayerController::HandleLevelEnd_Implementation()
{
	if (IsLocalPlayerController())
	{
		bIsOptionsMenuOpen = true; // This is just to disable the player being able to do anything
		UODGameInstance* ODGameInstance = Cast<UODGameInstance>(UGameplayStatics::GetGameInstance(GetWorld()));
		if (ODGameInstance)
		{
			ODGameInstance->HandleLevelEnd();
		}

		if (InGameUIWidget)
		{
			InGameUIWidget->HandleLevelEnd();
		}
	}
}

void AODPlayerController::HandleLevelFail_Implementation()
{
	if (IsLocalPlayerController())
	{
		bIsOptionsMenuOpen = true; // This is just to disable the player being able to do anything
		UODGameInstance* ODGameInstance = Cast<UODGameInstance>(UGameplayStatics::GetGameInstance(GetWorld()));
		if (ODGameInstance)
		{
			ODGameInstance->HandleLevelFail();
		}

		if (InGameUIWidget)
		{
			InGameUIWidget->HandleLevelEnd();
		}
	}
}

void AODPlayerController::HandleLeaveLevel_Implementation()
{
	if (IsLocalPlayerController())
	{
		bIsOptionsMenuOpen = true; // This is just to disable the player being able to do anything
		if (InGameUIWidget)
		{
			//InGameUIWidget->HandleLeaveLevel();
		}
	}
}

void AODPlayerController::SetPlayerWidgetOnCharacter_Implementation()
{
	if (PlayerCharacter)
	{
		PlayerCharacter->SetupPlayerIDWidget();
	}
}

void AODPlayerController::LoadInGameUI()
{
	if (IsLocalPlayerController())
	{
		if (InGameUIBPClass)
		{
			InGameUIWidget = CreateWidget<UODInGameHUD>(this, InGameUIBPClass);
			if (InGameUIWidget)
			{
				InGameUIWidget->SetupWidget();

				// Setup the container widget for the player as well
				SetupContainerWidget();
			}
		}
	}
}

void AODPlayerController::LoadOptionsMenu()
{
	if (IsLocalPlayerController())
	{
		bIsOptionsMenuOpen = true;
		// Clear the UI first
		if (InGameUIWidget)
		{
			//InGameUIWidget->TearDownWidget();
		}

		if (OptionsMenuBPClass)
		{
			OptionsMenuWidget = CreateWidget<UODUserWidget>(this, OptionsMenuBPClass);
			if (OptionsMenuWidget)
			{
				OptionsMenuWidget->SetupWidget();
			}
		}
	}
}

void AODPlayerController::CloseOptionsMenu()
{
	if (IsLocalPlayerController())
	{
		bIsOptionsMenuOpen = false;

		if (InGameUIWidget)
		{
		}

		if (OptionsMenuWidget)
		{
			OptionsMenuWidget->TearDownWidget();
		}
	}
}

void AODPlayerController::SetupContainerWidget()
{
	if (IsLocalPlayerController())
	{
		if (ContainerWidgetBPClass)
		{
			ContainerWidget = CreateWidget<UODContainerWidget>(this, ContainerWidgetBPClass);
			if (ContainerWidget)
			{
				ContainerWidget->SetupWidget();
			}
		}
	}
}

UODInGameHUD* AODPlayerController::GetInGameHUD()
{
	return InGameUIWidget;
}

void AODPlayerController::ShowPourHUD(TArray<EIngredientType> Ingredients)
{

}

void AODPlayerController::HidePourHUD()
{

}

void AODPlayerController::ShowContainerWidget(AODBevContainer* AttachedContainer)
{
	if (ContainerWidget)
	{
		ContainerWidget->SetNewContainer(AttachedContainer);
	}
}

void AODPlayerController::HideContainerWidget()
{
	if (ContainerWidget)
	{
		ContainerWidget->HideContainer();
	}
}

void AODPlayerController::ClearContainerWidget()
{
	if (ContainerWidget)
	{
		ContainerWidget->ClearIngredients();
	}
}

void AODPlayerController::AddOrderToHUD_Implementation(FDrinkOrder DrinkOrder)
{
	if (IsLocalPlayerController())
	{
		InGameUIWidget->AddOrder(DrinkOrder);
	}
}

void AODPlayerController::RemoveOrderFromHUD_Implementation(FDrinkOrder DrinkOrder)
{
	if (IsLocalPlayerController())
	{
		InGameUIWidget->RemoveOrder(DrinkOrder);
	}
}

void AODPlayerController::UpdateBrawlLevelHUD_Implementation(float BrawlPercentage)
{
	if (IsLocalPlayerController())
	{
		if (InGameUIWidget)
		{
			InGameUIWidget->SetBrawlIndicatorLevel(BrawlPercentage);
		}
	}
}

void AODPlayerController::Client_FinishPickup_Implementation()
{
	if (PlayerCharacter && IsLocalPlayerController())
	{
		PlayerCharacter->PlayPickupSound();
	}
}

void AODPlayerController::Client_DropBevContainer_Implementation(AODBevContainer* ContainerToDrop)
{
}

bool AODPlayerController::GetCanFillAtDispenser()
{
	if (PlayerCharacter)
	{
		if (PlayerCharacter->GetIsInRangeOfDispenser() && PlayerCharacter->IsHoldingObject())
		{
			bCanFillAtDispenser = true;
		}
		else
		{
			bCanFillAtDispenser = false;
		}
	}
	return bCanFillAtDispenser;
}

void AODPlayerController::SetCanFillAtDispenser(bool val)
{
	bCanFillAtDispenser = val;
}

void AODPlayerController::RefreshMixerWidget()
{
	if (MixerWidget)
	{
		MixerWidget->AdjustFill();
	}
}

void AODPlayerController::Client_RefreshMixerWidget_Implementation()
{
	if (MixerWidget)
	{
		MixerWidget->AdjustFill();
	}
}

void AODPlayerController::Client_ClearMixerWidget_Implementation()
{
	if (MixerWidget)
	{
		MixerWidget->ClearIngredients();
	}
}

void AODPlayerController::SetDrinkInWidget()
{
}

void AODPlayerController::Client_SetDrinkInWidget_Implementation()
{
	AODBevContainer* BevContainer = Cast<AODBevContainer>(PlayerCharacter->GetObjectInHand());
	
	if (!MixerWidget)
	{
		UE_LOG(LogTemp, Warning, TEXT("MixerWidget NULL"));
	}

	if (!BevContainer)
	{
		UE_LOG(LogTemp, Warning, TEXT("BevContainer NULL"));
	}

}

void AODPlayerController::Client_StartQTE_Implementation(AActor* InteractingActor)
{
	bInQTE = true;
	ActiveQTEStation = Cast<AODQTEStation>(InteractingActor);
	if (ActiveQTEStation)
	{
		ActiveQTEStation->StartQTE();
	}
}

void AODPlayerController::FinishQTE(bool bQTEResult)
{
	Server_CloseQTEEffects(ActiveQTEStation);

	if (bQTEResult)
	{
		// We sucessfully completed the QTE, so get some kind of reward
		Server_RequestQTEReward(ActiveQTEStation);
	}
	else
	{
		// If it was a chop station we need to let the bug escape
		// TODO
	}

	bInQTE = false;
	ActiveQTEStation = NULL;
}

void AODPlayerController::Server_RequestQTEReward_Implementation(AODQTEStation* QTEActor)
{
	if (QTEActor)
	{
		QTEActor->HandleQTESuccess();
	}
}

void AODPlayerController::Server_CloseQTEEffects_Implementation(AODQTEStation* QTEActor)
{
	if (QTEActor)
	{
		QTEActor->FinishEffects();
	}
}

void AODPlayerController::Client_HandlePlayerError_Implementation()
{
	if (PlayerCharacter)
	{
		PlayerCharacter->PlayErrorSound();
	}
}

void AODPlayerController::Client_HandlePour_Implementation()
{
	if (PlayerCharacter)
	{
		PlayerCharacter->PlayPourSound();
	}
}

void AODPlayerController::Client_HandleAddGarnish_Implementation()
{
// 	if (PlayerCharacter && IsLocalPlayerController())
// 	{
// 		PlayerCharacter->PlayAddGarnishSound(TODO);
// 	}
}

void AODPlayerController::Client_HandleFill_Implementation(int32 FillLevel)
{
	if (PlayerCharacter && IsLocalPlayerController())
	{
		PlayerCharacter->PlayFillLevelSound(FillLevel);
	}
}

void AODPlayerController::CancelQTE()
{
	if (ActiveQTEStation)
	{
		ActiveQTEStation->CancelQTE();
	}
	
	FinishQTE(false);
}

void AODPlayerController::TravelToMainMenu(bool bHadNetworkError)
{
	this->ClientTravel("/Game/DrinkGame/Levels/Interface/MenuLevel", ETravelType::TRAVEL_Absolute, false);
}

bool AODPlayerController::IsPouring()
{
	return bIsPouringAtDispenser;
}

void AODPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();

	InputComponent->BindAxis("MoveForward", this, &AODPlayerController::HandleMoveForward);
	InputComponent->BindAxis("MoveRight", this, &AODPlayerController::HandleMoveRight);
	InputComponent->BindAxis("GamepadMixUp", this, &AODPlayerController::HandleGamepadMixUp);
	InputComponent->BindAxis("MouseMixUp", this, &AODPlayerController::HandleMouseMixUp);

	InputComponent->BindAction("Action1", IE_Pressed, this, &AODPlayerController::HandleAction1Pressed);
	InputComponent->BindAction("Action1", IE_Released, this, &AODPlayerController::HandleAction1Released);
	InputComponent->BindAction("Action2", IE_Pressed, this, &AODPlayerController::HandleAction2Pressed);
	InputComponent->BindAction("Action2", IE_Released, this, &AODPlayerController::HandleAction2Released);
	InputComponent->BindAction("Action3", IE_Pressed, this, &AODPlayerController::HandleAction3Pressed);
	InputComponent->BindAction("Action3", IE_Released, this, &AODPlayerController::HandleAction3Released);
	InputComponent->BindAction("Action4", IE_Pressed, this, &AODPlayerController::HandleAction4Pressed);
	InputComponent->BindAction("Action4", IE_Released, this, &AODPlayerController::HandleAction4Released);
	InputComponent->BindAction("OptionsMenu", IE_Pressed, this, &AODPlayerController::HandleOptions);
}

void AODPlayerController::HandleMoveForward(float val)
{
	// Disable movement if options is active
	if (!bIsOptionsMenuOpen)
	{
		if (PlayerCharacter && !bInQTE)
		{
			PlayerCharacter->MoveForward(val);
		}
	}
}

void AODPlayerController::HandleMoveRight(float val)
{
	// Disable movement if options is active
	if (!bIsOptionsMenuOpen)
	{
		if (PlayerCharacter && !bInQTE)
		{
			PlayerCharacter->MoveRight(val);
		}
	}
}

void AODPlayerController::HandleGamepadMixUp(float val)
{
	// Let the PlayerCharacter know how to animate the shake
	if (PlayerCharacter && val != 0.f)
	{
		PlayerCharacter->Server_SetShakeAxis(val / GamepadMixThreshold);
	}

	if (val > GamepadMixThreshold)
	{
		TryToMix(true, true, val);
	}
	else if (val < -GamepadMixThreshold)
	{
		TryToMix(true, false, val);
	}
}

void AODPlayerController::HandleMouseMixUp(float val)
{
	// Let the PlayerCharacter know how to animate the shake
	if (PlayerCharacter && val != 0.f)
	{
		PlayerCharacter->Server_SetShakeAxis(val / MouseMixThreshold);
	}

	if (val > MouseMixThreshold)
	{
		TryToMix(false, true, val);
	}
	else if (val < -MouseMixThreshold)
	{
		TryToMix(false, false, val);
	}
}

void AODPlayerController::HandleAction1Pressed()
{
	if (!PlayerCharacter)
	{
		UE_LOG(LogTemp, Error, TEXT("AODPlayerController::HandleAction1Pressed(): PlayerCharacter NULL"));
		return;
	}

	// Active QTEs take precedence
	if (bInQTE && ActiveQTEStation)
	{
		// Handle some effects
		AODChopStation* ChopStation = Cast<AODChopStation>(ActiveQTEStation);
		if (ChopStation)
		{
			PlayerCharacter->Server_PlayAttackAnim();
		}

		ActiveQTEStation->InteractWithQTE();
		return;
	}

// 	if (PlayerCharacter->IsInRangeOfCustomer())
// 	{
// 		// Taking orders should take precedence over everything
// 		AODAICharacter* AICharacter = PlayerCharacter->GetClosestCustomerInRange();
// 		if (AICharacter && AICharacter->IsCustomerWaitingToOrder())
// 		{
// 			AODMultiplayerGameState* ODGameState = Cast<AODMultiplayerGameState>(UGameplayStatics::GetGameState(GetWorld()));
// 			if (ODGameState && ODGameState->CanHandleNewOrder())
// 			{
// 				// Take their order
// 				Server_AcceptCustomerOrder(AICharacter);
// 				return;
// 			}
// 			else
// 			{
// 				// Orders are maxed right now
// 				PlayerCharacter->PlayErrorSound();
// 				return;
// 			}
// 		}
// 
// 		if (PlayerCharacter->IsHoldingDrinkGlass() && AICharacter && AICharacter->CanCustomerAcceptDrink())
// 		{
// 			AODDrinkGlass* DrinkToGive = Cast<AODDrinkGlass>(PlayerCharacter->GetObjectInHand());
// 
// 			// Drop what we're holding so it can attach to the customer
// 			PlayerCharacter->Server_DropObject();
// 
// 			// Try to hand whatever drink we're holding to the customer
// 			if (DrinkToGive)
// 			{
// 				Server_GiveDrinkToCustomer(AICharacter, DrinkToGive);
// 			}
// 			return;
// 		}
// 
// 		// TASE THEM
// 		if (PlayerCharacter->IsHoldingTaser() && AICharacter)
// 		{
// 			PlayerCharacter->Server_UseTaser();
// 			return;
// 		}
// 	}

	// Interacting with a counter
// 	if (PlayerCharacter->HasCounterInRange() && PlayerCharacter->GetClosestCounter())
// 	{
// 		AODCounter* InteractCounter = PlayerCharacter->GetClosestCounter();
// 		if (InteractCounter && InteractCounter->HasAttachedObject())
// 		{
// 			// Is it an interactable object?
// 			IODInteractInterface* InteractObj = Cast<IODInteractInterface>(InteractCounter->GetAttachedActor());
// 			if (InteractObj)
// 			{
// 				PlayerCharacter->Server_InteractSpecificObject(InteractCounter->GetAttachedActor());
// 				return;
// 			}
// 
// 			if (PlayerCharacter->IsHoldingPourableContainer())
// 			{
// 				AODIngredientDispenser* DispenserToUse = Cast<AODIngredientDispenser>(InteractCounter->GetAttachedActor());
// 				if (DispenserToUse)
// 				{
// 					// Interact with the dispenser attached
// 					currentHoldActionIndex = 1;
// 					StartPouring(DispenserToUse, 1);
// 					return;
// 				}
// 			}
// 		}
// 		else
// 		{
// 			// Just put it on the counter
// 			if (PlayerCharacter->IsHoldingObject() && InteractCounter->CanHoldItem())
// 			{
// 				// Set the object on the counter
// 				PlayerCharacter->Server_DropObject();
// 				//PlayerCharacter->PlayDropSound();
// 				return;
// 			}
// 		}
// 	}

	// Interactable Actor
	if (PlayerCharacter->CanInteract())
	{
		PlayerCharacter->Interact();
		return;
	}

	// Otherwise just drop whatever we're holding
	if (PlayerCharacter->IsHoldingObject())
	{
		PlayerCharacter->Server_DropObject(false, true);
		return;
	}

// 	if (PlayerCharacter->IsHoldingObject())
// 	{
// 		if (PlayerCharacter->GetWasteBinInRange() && PlayerCharacter->IsHoldingBevContainer())
// 		{
// 			AODBevContainer* BevContainer = Cast<AODBevContainer>(PlayerCharacter->GetObjectInHand());
// 			if (BevContainer->IsContainerEmpty())
// 			{
// 				PlayerCharacter->PlayErrorSound();
// 				return;
// 			}
// 			else
// 			{
// 				// Just dump into a trashcan
// 				PlayerCharacter->Server_PourContentsInHand();
// 				return;
// 			}
// 		}
// 		else if (PlayerCharacter->IsHoldingBevContainer() && PlayerCharacter->HasInteractableContainerInRange())
// 		{
// 			// The player is trying to pour the contents into another container
// 			AODDrinkGlass* GlassInRange = Cast<AODDrinkGlass>(PlayerCharacter->GetClosestInteractableBevContainer());
// 			AODBevContainer* ContainerInHand = Cast<AODBevContainer>(PlayerCharacter->GetObjectInHand());
// 			if ((GlassInRange && GlassInRange->IsGlassDirty()) || (ContainerInHand && ContainerInHand->IsContainerEmpty()))
// 			{
// 				// If we're trying to pour into a dirty glass OR we simply have nothing in our current container, we can't pour
// 				PlayerCharacter->PlayErrorSound();
// 				return;
// 			}
// 			else
// 			{
// 				PlayerCharacter->Server_PourContentsInHand();
// 				return;
// 			}
// 		}
// 		else if (PlayerCharacter->IsHoldingGarnish() && PlayerCharacter->HasInteractableContainerInRange())
// 		{
// 			PlayerCharacter->Server_AddGarnishToContainer();
// 			return;
// 		}
// 		else
// 			PlayerCharacter->Server_DropObject();
// 			//PlayerCharacter->PlayDropSound();
// 			return;
// 		}
// 	}

// 	bool bPickedUpObject = false;
// 	// Try to pickup a crawler first
// 	if (PlayerCharacter->GetCrawlerInRange())
// 	{
// 		PlayerCharacter->Server_PickupCrawler();
// 		bPickedUpObject = true;
// 	}
// 	else
// 	{
// 		// Just try and pick something up
// 		if (PlayerCharacter->HasObjectInRange())
// 		{
// 			PlayerCharacter->Server_PickupObjectInRange();
// 			bPickedUpObject = true;
// 		}
// 	}
// 		{
	
}

void AODPlayerController::HandleAction1Released()
{
	if (PlayerCharacter->IsWashing())
	{
		PlayerCharacter->Server_FinishWashing();
	}

	if (bIsPouringAtDispenser)
	{
		StopPouring();
	}
}

void AODPlayerController::HandleAction2Pressed()
{
	if (PlayerCharacter)
	{
		PlayerCharacter->Server_DashForward();
	}
}

void AODPlayerController::HandleAction2Released()
{
	
}

void AODPlayerController::HandleAction3Pressed()
{
	
}

void AODPlayerController::HandleAction3Released()
{
}

void AODPlayerController::HandleAction4Pressed()
{
	// Let the player get out of QTE
	if (bInQTE)
	{
		CancelQTE();
		return;
	}
}

void AODPlayerController::HandleAction4Released()
{
}

void AODPlayerController::HandleOptions()
{
	if (!bIsOptionsMenuOpen)
	{
		UODGameInstance* ODGameInstance = Cast<UODGameInstance>(UGameplayStatics::GetGameInstance(GetWorld()));
		if (ODGameInstance)
		{
			ODGameInstance->HandlePauseMenu(PlayerCharacter, true);
			bIsOptionsMenuOpen = true;
		}
	}
}

void AODPlayerController::HandleResume()
{
	UODGameInstance* ODGameInstance = Cast<UODGameInstance>(UGameplayStatics::GetGameInstance(GetWorld()));
	if (ODGameInstance)
	{
		ODGameInstance->HandlePauseMenu(PlayerCharacter, false);
		bIsOptionsMenuOpen = false;
	}
}

void AODPlayerController::HandleViewOrdersPressed()
{
	if (InGameUIWidget && MPGameState && MPGameState->GetNumActiveOrders() > 0)
	{
		InGameUIWidget->EnableViewOrders();
        
        PlayerCharacter->PlayShowOrdersSound();
	}
}

void AODPlayerController::HandleViewOrdersReleased()
{
	if (InGameUIWidget && InGameUIWidget->bIsOrderPanelActive)
	{
		InGameUIWidget->DisableViewOrders();
        
        PlayerCharacter->PlayHideOrdersSound();
	}
}

void AODPlayerController::StartPouring_Implementation(AODIngredientDispenser* Dispenser)
{
	GetWorldTimerManager().SetTimer(DispenseTimerHandle, this, &AODPlayerController::DispenseTimerUp, dispenseTickTime, false);
	bIsPouringAtDispenser = true;

	// Do the initial ingredient pour, so fast it probably won't be noticeable
	if (PlayerCharacter && Dispenser)
	{
		ActiveDispenser = Dispenser;
		// Subtract 1 because ingredients start at 0
		PlayerCharacter->Server_GetIngredientsFromDispenser(ActiveDispenser);
		PlayerCharacter->Server_SetIsPouring(true);
	}
}

void AODPlayerController::StopPouring()
{
	// Try to stop the sound
	if (PlayerCharacter)
	{
		PlayerCharacter->Server_StopDispensingIngredients();
		PlayerCharacter->Server_SetIsPouring(false);
	}

	bIsPouringAtDispenser = false;
	GetWorldTimerManager().ClearTimer(DispenseTimerHandle);
	currentHoldActionIndex = -1;
}

void AODPlayerController::OpenAdminMenu()
{
	if (HasAuthority())
	{
		SetupAdminMenu();
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("AODPlayerController::OpenAdminMenu(): You are not the host. Cannot open Admin Menu."));
	}
}

void AODPlayerController::DispenseTimerUp()
{
	if (bIsPouringAtDispenser)
	{
		// Pour again!
		if (PlayerCharacter && ActiveDispenser)
		{
			// Subtract 1 because ingredients start at 0
			PlayerCharacter->Server_GetIngredientsFromDispenser(ActiveDispenser);
		}
		GetWorldTimerManager().SetTimer(DispenseTimerHandle, this, &AODPlayerController::DispenseTimerUp, dispenseTickTime, false);
	}
}

bool AODPlayerController::TryToMix(bool bIsUsingGamepad, bool bIsMixActionUpwards, float AxisValue)
{
	// Check if what we're holding can be mixed
	if (PlayerCharacter && PlayerCharacter->IsHoldingDrinkMixer())
	{
		// First see if we're mid mix
		if (bIsMixing)
		{
			// Make sure we aren't shaking the same direction as last time
			if (bWasLastShakeUpward != bIsMixActionUpwards)
			{
				bWasLastShakeUpward = bIsMixActionUpwards;
				PlayerCharacter->Server_MixDrinkInHand();

				// Set a cool down timer
				GetWorldTimerManager().SetTimer(MixingTimerHandle, this, &AODPlayerController::MixingTimerUp, MixingResetTime, false);

				// This is for sound only
				CalculateShakeSpeed(bIsUsingGamepad, AxisValue);
// 				GetWorldTimerManager().ClearTimer(MixingSoundTimerHandle);
// 				GetWorldTimerManager().SetTimer(MixingSoundTimerHandle, this, &AODPlayerController::MixingSoundTimerUp, 0.1f, false);
			}
		}
		else
		{
			// This is our first shake, so get things started
			bIsMixing = true;
			PlayerCharacter->Server_SetIsShaking(bIsMixing);
			bWasLastShakeUpward = bIsMixActionUpwards ? true : false;
			PlayerCharacter->Server_MixDrinkInHand();

			// Sound
			CalculateShakeSpeed(bIsUsingGamepad, AxisValue);
			PlayerCharacter->ToggleShakeSound(bIsMixing);
		}
	}

	return true;
}

void AODPlayerController::MixingTimerUp()
{
	GetWorldTimerManager().ClearTimer(MixingTimerHandle);
	bIsMixing = false;
	PlayerCharacter->Server_SetIsShaking(bIsMixing);
	PlayerCharacter->Server_SetShakeAxis(0.f);
	PlayerCharacter->ToggleShakeSound(bIsMixing);
}

void AODPlayerController::MixingSoundTimerUp()
{
	
}

void AODPlayerController::Server_AcceptCustomerOrder_Implementation(AODAICharacter* Customer)
{
	// Add the customer order to the game state list, then let the AI know
	if (MPGameState && Customer)
	{
		FDrinkOrder NewOrder = Customer->GiveOrder();

		MPGameState->Server_AddOpenOrder(NewOrder);
	}
}

void AODPlayerController::Server_GiveDrinkToCustomer_Implementation(AODAICharacter* AICharacter, AODDrinkGlass* Drink)
{
	AICharacter->TakeDrinkFromPlayer(Drink);
}

float AODPlayerController::CalculateShakeSpeed(bool bIsUsingGamepad, float AxisValue)
{
	const float MinVal = 0.f;
	const float MaxVal = 3.0f;

	// Our shake speed is calculated different for each control scheme
	if (bIsUsingGamepad)
	{
		float DeltaMove = FMath::Abs(AxisValue - LastGamepadAxis);

	}
	else
	{
		// Using a mouse
	}

	return 0.f;
}
