// Copyright 2020 Space Gurus


#include "Environment/ODAISpawnArea.h"
#include "Components/BoxComponent.h"

AODAISpawnArea::AODAISpawnArea()
{
	PrimaryActorTick.bCanEverTick = false;

	SpawnArea = CreateDefaultSubobject<UBoxComponent>(TEXT("SpawnArea"));
	RootComponent = SpawnArea;
}

bool AODAISpawnArea::IsPointInSpawnArea(FVector PointToCheck)
{
	if (SpawnArea)
	{
		FVector BoxMinPoint = GetActorLocation() - SpawnArea->GetScaledBoxExtent();
		FVector BoxMaxPoint = GetActorLocation() + SpawnArea->GetScaledBoxExtent();

		FBox BoundingBox = FBox(BoxMinPoint, BoxMaxPoint);
		if (BoundingBox.IsInsideOrOn(PointToCheck))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	return false;
}

FVector AODAISpawnArea::GetRandomSpawnPoint()
{
	FVector RandomPoint;
	RandomPoint.X = FMath::RandRange(GetActorLocation().X - SpawnArea->GetScaledBoxExtent().X, GetActorLocation().X + SpawnArea->GetScaledBoxExtent().X);
	RandomPoint.Y = FMath::RandRange(GetActorLocation().Y - SpawnArea->GetScaledBoxExtent().Y, GetActorLocation().Y + SpawnArea->GetScaledBoxExtent().Y);
	RandomPoint.Z = 10.f; // Always spawn at this height
	return RandomPoint;
}

void AODAISpawnArea::BeginPlay()
{
	Super::BeginPlay();
	
}