// Copyright 2020 Space Gurus


#include "Environment/ODBlenderBase.h"
#include "Net/UnrealNetwork.h"
#include "Components/SkeletalMeshComponent.h"
#include "Components/WidgetComponent.h"
#include "Components/BoxComponent.h"
#include "PlayerCharacter.h"
#include "Actors/ODBlender.h"
#include "UI/ODProgressWidget.h"
#include "UI/ODFlashingWidget.h"
#include "NiagaraComponent.h"
#include "AkGameplayStatics.h"
#include "AkAudioEvent.h"

AODBlenderBase::AODBlenderBase(const FObjectInitializer& OI) : Super(OI)
{
	PrimaryActorTick.bCanEverTick = true;
	bReplicates = true;

	SkeletalMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("SkeletalMesh"));
	RootComponent = SkeletalMesh;
	SkeletalMesh->SetRenderCustomDepth(true);

	InteractCollider = CreateDefaultSubobject<UBoxComponent>(TEXT("InteractCollider"));
	InteractCollider->SetupAttachment(RootComponent);

	ProgressWidgetComponent = CreateDefaultSubobject<UWidgetComponent>(TEXT("ProgressWidgetComponent"));
	ProgressWidgetComponent->SetupAttachment(RootComponent);
	ProgressWidgetComponent->SetVisibility(false);

	WarningWidgetComponent = CreateDefaultSubobject<UWidgetComponent>(TEXT("WarningWidgetComponent"));
	WarningWidgetComponent->SetupAttachment(RootComponent);

	BlendEffectComponent = CreateDefaultSubobject<UNiagaraComponent>(TEXT("BlendEffectComponent"));
	BlendEffectComponent->SetupAttachment(RootComponent);
}


void AODBlenderBase::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AODBlenderBase, bIsBlending);
	DOREPLIFETIME(AODBlenderBase, bIsOverflowing);
	DOREPLIFETIME(AODBlenderBase, BlendProgress);
	DOREPLIFETIME(AODBlenderBase, OverflowProgress);
	DOREPLIFETIME(AODBlenderBase, AttachedBlender);
}

void AODBlenderBase::BeginPlay()
{
	Super::BeginPlay();
	
	BlendProgressWidget = Cast<UODProgressWidget>(ProgressWidgetComponent->GetUserWidgetObject());
	WarningWidget = Cast<UODFlashingWidget>(WarningWidgetComponent->GetUserWidgetObject());

	// Spawn the blender for this base
	if (HasAuthority())
	{
		FActorSpawnParameters SpawnParams;
		AODBlender* StartingBlender = GetWorld()->SpawnActor<AODBlender>(BlenderBP, GetActorLocation() + FVector(0.f, 0.f, 150.f), FRotator(0.f), SpawnParams);
		if (StartingBlender)
		{
			AttachedBlender = StartingBlender;
			AttachedBlender->AttachToActor(this, FAttachmentTransformRules::KeepWorldTransform, "JugPoint");
			AttachedBlender->SetActorRelativeLocation(FVector(0.f));
			AttachedBlender->SetIsAttached(true);
		}
	}
}

// Called every frame
void AODBlenderBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (bIsBlending && !bIsOverflowing)
	{
		BlendProgress += DeltaTime / BlendTime;

		if (BlendProgressWidget)
		{
			BlendProgressWidget->SetProgress(BlendProgress);
		}

		if (BlendProgress >= 1.f)
		{
			if (HasAuthority())
			{
				bIsOverflowing = true;
				OnRep_IsOverflowing();
			}
		}
	}
	else if (bIsBlending && bIsOverflowing)
	{
		OverflowProgress += DeltaTime / OverflowTime;

		if (OverflowProgress >= 1.f)
		{
			if (HasAuthority())
			{
				bIsOverflowing = false;
				bIsBlending = false;

				if (AttachedBlender)
				{
					AttachedBlender->SetOverflowProgress(OverflowProgress);				
				}

				HandleOverflowFinish();
			}
		}
	}
}

void AODBlenderBase::PlayerInteraction_Implementation(APlayerCharacter* Player)
{
	if (Player && Player->IsHoldingBlender() && !AttachedBlender)
	{
		// Take the blender from the player and start blending if possible
		AttachedBlender = Cast<AODBlender>(Player->GetObjectInHand());
		if (AttachedBlender)
		{
			Player->Server_DropObject(true, true);

			AttachedBlender->AttachToActor(this, FAttachmentTransformRules::KeepWorldTransform, "JugPoint");
			AttachedBlender->SetActorRelativeLocation(FVector(0.f));
			AttachedBlender->SetIsAttached(true);
			AttachedBlender->EndHighlight();
			
			if (AttachedBlender->CanBeBlended())
			{
				BlendProgress = AttachedBlender->GetBlendProgress();

				// This blender has already started to overflow from previous blending
				if (BlendProgress >= 1.f)
				{
					bIsOverflowing = true;
					bIsBlending = true;
					OnRep_IsOverflowing();
				}
				else
				{
					// Just blend like normal
					bIsBlending = true;
					Multi_StartBlend();
				}
			}
		}
	}
	else if (Player && !Player->IsHoldingObject() && AttachedBlender)
	{
		// Give the blender to the player
		AttachedBlender->SetBlendProgress(BlendProgress);
		AttachedBlender->DetachFromActor(FDetachmentTransformRules::KeepWorldTransform);
		AttachedBlender->EndHighlight();

		Player->Server_PickupSpecificObject(AttachedBlender, true);

		bool bShouldPlayStopSound = bIsBlending;

		AttachedBlender = NULL;
		bIsBlending = false;
		bIsOverflowing = false;
		Multi_StopBlend(bShouldPlayStopSound);
	}
	else
	{
		if (Player)
			Player->PlayErrorSound();
	}
}

void AODBlenderBase::BecomeRelevant_Implementation(APlayerCharacter* PlayerTriggered)
{
	ShowHighlight();

	if (AttachedBlender)
	{
		if (AttachedBlender->Implements<UODInteractInterface>())
		{
			IODInteractInterface::Execute_BecomeRelevant(AttachedBlender, PlayerTriggered);
		}
	}
}

void AODBlenderBase::LoseRelevance_Implementation()
{
	EndHighlight();

	if (AttachedBlender)
	{
		if (AttachedBlender->Implements<UODInteractInterface>())
		{
			IODInteractInterface::Execute_LoseRelevance(AttachedBlender);
		}
	}
}

void AODBlenderBase::ShowHighlight()
{
	SkeletalMesh->SetCustomDepthStencilValue(HighlightCustomDepth);
}

void AODBlenderBase::EndHighlight()
{
	SkeletalMesh->SetCustomDepthStencilValue(DefaultCustomDepth);
}

void AODBlenderBase::Multi_StartBlend_Implementation()
{
	ProgressWidgetComponent->SetVisibility(true);

	if (BlendSound)
	{
		FOnAkPostEventCallback NullCallback;
		UAkGameplayStatics::SetSwitch(nullptr, this, "ice", "with");
		BlendSoundID = UAkGameplayStatics::PostEvent(BlendSound, this, 0, NullCallback);
	}
}

void AODBlenderBase::Multi_StopBlend_Implementation(bool bWasBlending)
{
	if (bWasBlending)
	{
		UAkGameplayStatics::ExecuteActionOnPlayingID(AkActionOnEventType::Stop, BlendSoundID);
		if (BlendFinishSound)
		{
			FOnAkPostEventCallback NullCallback;
			UAkGameplayStatics::PostEvent(BlendFinishSound, this, 0, NullCallback);
		}
	}

	ProgressWidgetComponent->SetVisibility(false);
	WarningWidget->ToggleFlash(false);
	BlendEffectComponent->Deactivate();
}

bool AODBlenderBase::IsBlending()
{
	return bIsBlending;
}

void AODBlenderBase::OnRep_IsOverflowing()
{
	if (bIsOverflowing)
	{
		UE_LOG(LogTemp, Warning, TEXT("AODBlenderBase::OnRep_IsOverflowing(): Going into overdrive"));
		UAkGameplayStatics::SetSwitch(nullptr, this, "ice", "overdrive");

		ProgressWidgetComponent->SetVisibility(false);
	
		if (WarningWidget)
		{
			WarningWidget->ToggleFlash(true);
		}

		if (AttachedBlender)
		{
			BlendEffectComponent->SetNiagaraVariableLinearColor("DrinkColor", AttachedBlender->GetLiquidColor());
		}
		BlendEffectComponent->Activate();
	}
	else
	{
		if (OverflowProgress >= 1.f)
		{
			HandleOverflowFinish();
		}
	}
}

void AODBlenderBase::SetBlendSpeed(float val)
{
	BlendTime = val;
}

float AODBlenderBase::GetBlendTime()
{
	return BlendTime;
}

void AODBlenderBase::SetBlenderOverflowTime(float val)
{
	OverflowTime = val;
}

float AODBlenderBase::GetBlenderOverflowTime()
{
	return OverflowTime;
}

void AODBlenderBase::HandleOverflowFinish()
{
	WarningWidget->ToggleFlash(false);
	BlendEffectComponent->Deactivate();

	UAkGameplayStatics::ExecuteActionOnPlayingID(AkActionOnEventType::Stop, BlendSoundID);
	if (BlendFinishSound)
	{
		FOnAkPostEventCallback NullCallback;
		UAkGameplayStatics::PostEvent(BlendFinishSound, this, 0, NullCallback);
	}

	BlendProgress = 0.f;
	OverflowProgress = 0.f;
}

