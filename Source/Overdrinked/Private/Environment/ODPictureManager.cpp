// Copyright 2020 Space Gurus


#include "Environment/ODPictureManager.h"
#include "Components/SkeletalMeshComponent.h"
#include "Components/SceneCaptureComponent2D.h"
#include "Engine/TextureRenderTarget2D.h"
#include "Materials/Material.h"
#include "Materials/MaterialInstanceDynamic.h"
#include "UI/ODOrderCardWidget.h"
#include "AI/ODAICharacter.h"

AODPictureManager::AODPictureManager(const FObjectInitializer& OI) : Super(OI)
{
	PrimaryActorTick.bCanEverTick = true;

	ConstructorHelpers::FObjectFinder<UMaterial> FaceCaptureMaterialFinder(TEXT("/Game/DrinkGame/MaterialLibrary/Special/M_AIFaceCapture"));
	if (FaceCaptureMaterialFinder.Succeeded())
	{
		FaceCaptureMaterial = (UMaterial*)FaceCaptureMaterialFinder.Object;
	}

	SkeletalMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("SkeletalMesh"));
	RootComponent = SkeletalMesh;

	FaceCameraComponent = CreateDefaultSubobject<USceneCaptureComponent2D>(TEXT("FaceCameraComponent"));
	FaceCameraComponent->SetupAttachment(RootComponent);
	FaceCameraComponent->bCaptureOnMovement = false;
	FaceCameraComponent->bCaptureEveryFrame = false;
}

void AODPictureManager::BeginPlay()
{
	Super::BeginPlay();
	
}

void AODPictureManager::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (bIsTakingFaceCapture)
	{
		UTextureRenderTarget2D* RenderTarget2D = FaceCameraComponent->TextureTarget;
		if (RenderTarget2D && FaceCaptureMaterial)
		{
			UMaterialInstanceDynamic* DynamicMaterial = UMaterialInstanceDynamic::Create(FaceCaptureMaterial, this);
			DynamicMaterial->SetTextureParameterValue("FaceCapture", RenderTarget2D);

			if (ActiveOrderCard)
			{
				ActiveOrderCard->SetFaceCaptureMaterial(DynamicMaterial);

				bIsTakingFaceCapture = false;
			}
		}
	}
}

USkeletalMeshComponent* AODPictureManager::GetSkeletalMeshComponent()
{
	return SkeletalMesh;
}

void AODPictureManager::CapturePicture(UODOrderCardWidget* OrderWidget, AODAICharacter* CharacterToCapture)
{
	// Change our mesh to that of the character
	USkeletalMesh* CharacterMesh = CharacterToCapture->GetMesh()->SkeletalMesh;
	if (CharacterMesh)
	{
		SkeletalMesh->SetSkeletalMesh(CharacterMesh);
	}

	// Start taking a capture
	UTextureRenderTarget2D* RenderTarget2D = NewObject<UTextureRenderTarget2D>();
	if (RenderTarget2D)
	{
		RenderTarget2D->InitAutoFormat(512, 512);
		FaceCameraComponent->TextureTarget = RenderTarget2D;

		FaceCameraComponent->CaptureSceneDeferred();
		bIsTakingFaceCapture = true;

		ActiveOrderCard = OrderWidget;
	}
}

