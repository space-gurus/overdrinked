// Copyright 2020 Space Gurus


#include "Environment/ODAILocation.h"
#include "Components/BoxComponent.h"
#include "Components/ArrowComponent.h"
#include "Components/WidgetComponent.h"
#include "Environment/ODAIHub.h"
#include "AI/ODAICharacter.h"
#include "AI/ODAIController.h"

AODAILocation::AODAILocation(const FObjectInitializer& OI) : Super(OI)
{
	PrimaryActorTick.bCanEverTick = false;

	BoxCollider = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxCollider"));
	RootComponent = BoxCollider;

	FaceDirectionComponent = CreateDefaultSubobject<UArrowComponent>(TEXT("FaceDirectionComponent"));
	FaceDirectionComponent->SetupAttachment(RootComponent);
}

void AODAILocation::BoxColliderBeginOverlap(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	AODAICharacter* AICharacter = Cast<AODAICharacter>(OtherActor);
	if (AICharacter)
	{
		AODAIController* AIController = Cast<AODAIController>(AICharacter->GetController());
		if (AIController)
		{
			AIController->SetCurrentAILocation(this);
		}

		NumCharsInSpace++;
		bIsOccupied = true;
		SetIsAvailableForMove(false);
	}
}

void AODAILocation::BoxColliderEndOverlap(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	AODAICharacter* AICharacter = Cast<AODAICharacter>(OtherActor);
	if (AICharacter)
	{
		AODAIController* AIController = Cast<AODAIController>(AICharacter->GetController());
		if (AIController)
		{
			AIController->SetCurrentAILocation(NULL);
		}

		NumCharsInSpace--;
		
		if (NumCharsInSpace <= 0)
		{
			bIsOccupied = false;
			SetIsAvailableForMove(true);
		}
	}
}

void AODAILocation::StartWithCharacterInBounds()
{
	bIsOccupied = true;
	SetIsAvailableForMove(false);
	NumCharsInSpace = 1;

}

void AODAILocation::CheckForInitialOverlap()
{
	TArray<AActor*> OverlapActors;
	BoxCollider->GetOverlappingActors(OverlapActors);

	if (OverlapActors.Num() > 0)
	{
		AODAICharacter* AICharacter = Cast<AODAICharacter>(OverlapActors[0]);
		if (AICharacter)
		{
			StartWithCharacterInBounds();
		}
	}
}

AODAIHub* AODAILocation::GetConversationHub()
{
	return ConversationHub;
}

void AODAILocation::BeginPlay()
{
	Super::BeginPlay();
	
	BoxCollider->OnComponentBeginOverlap.AddDynamic(this, &AODAILocation::BoxColliderBeginOverlap);
	BoxCollider->OnComponentEndOverlap.AddDynamic(this, &AODAILocation::BoxColliderEndOverlap);

	CheckForInitialOverlap();

	//BoxCollider->SetHiddenInGame(false);

	// Let the hub know we exist for conversation
	if (ConversationHub)
	{
		ConversationHub->RegisterLocation(this);
	}
}

bool AODAILocation::IsLocationFree()
{
	return bIsOccupied ? false : true;
}

void AODAILocation::SetIsOccupied(bool val)
{
	bIsOccupied = val;

	if (bIsOccupied)
	{
		BoxCollider->SetHiddenInGame(false);
	}
}

bool AODAILocation::IsAvailableForMove()
{
	return bIsAvailableForMove;
}

void AODAILocation::SetIsAvailableForMove(bool val)
{
	bIsAvailableForMove = val;
}
