// Copyright 2020 Space Gurus


#include "Environment/ODGlobalSoundActor.h"
#include "AkGameplayStatics.h"
#include "AkAudioEvent.h"
#include "Kismet/GameplayStatics.h"
#include "Net/UnrealNetwork.h"
#include "ODMultiplayerGameState.h"

AODGlobalSoundActor::AODGlobalSoundActor()
{
	PrimaryActorTick.bCanEverTick = false;
	bReplicates = true;
}

void AODGlobalSoundActor::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
}

void AODGlobalSoundActor::BeginPlay()
{
	Super::BeginPlay();

	// Give the game state a reference to this for playing sounds
	AODMultiplayerGameState* MPGameState = Cast<AODMultiplayerGameState>(UGameplayStatics::GetGameState(GetWorld()));
	if (MPGameState)
	{
		MPGameState->SetGlobalSoundActor(this);
	}

	PlayLevelMusic();
}

void AODGlobalSoundActor::PlayLevelMusic()
{
	if (LevelMusic)
	{
		FOnAkPostEventCallback NullCallback;
		LevelMusicID = UAkGameplayStatics::PostEvent(LevelMusic, this, 0, NullCallback);
	}
}

void AODGlobalSoundActor::PlayDrinkDeliveryNoise_Implementation(EFeedbackType FeedbackLevel)
{
	switch (FeedbackLevel)
	{
		case EFeedbackType::FT_Great:
			if (PositiveDelivery)
			{
				FOnAkPostEventCallback NullCallback;
				PositiveDeliveryID = UAkGameplayStatics::PostEvent(PositiveDelivery, this, 0, NullCallback);
			}
			break;
		default:
			break;
	}
	
}

void AODGlobalSoundActor::PlayTakeOrder_Implementation()
{
	if (TakeOrder)
	{
		FOnAkPostEventCallback NullCallback;
		TakeOrderID = UAkGameplayStatics::PostEvent(TakeOrder, this, 0, NullCallback);
	}
}

void AODGlobalSoundActor::PlayDeliveryVoiceLine_Implementation(EFeedbackType FeedbackLevel)
{

}
