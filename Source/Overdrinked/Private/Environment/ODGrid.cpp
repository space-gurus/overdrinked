// Copyright 2020 Space Gurus


#include "Environment/ODGrid.h"
#include "Components/BoxComponent.h"

AODGrid::AODGrid()
{
	PrimaryActorTick.bCanEverTick = false;

	// Grid defaults
	gridHeight = 1;
	gridWidth = 1;

	testBox = CreateDefaultSubobject<UBoxComponent>(TEXT("Test Box"));
}

void AODGrid::BeginPlay()
{
	Super::BeginPlay();
	
}

void AODGrid::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AODGrid::RebuildGrid()
{
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Green, FString::Printf(TEXT("Blah blah blah")));

}

void AODGrid::ToggleVisibility()
{
	testBox->ToggleVisibility(true);
}

