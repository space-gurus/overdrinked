// Copyright 2020 Space Gurus


#include "Environment/ODGlassRack.h"
#include "Components/BoxComponent.h"
#include "Components/ODGlassStackComponent.h"
#include "PlayerCharacter.h"
#include "Actors/ODEnvGlass.h"
#include "Actors/ODDrinkGlass.h"
#include "DrawDebugHelpers.h"

AODGlassRack::AODGlassRack()
{
	PrimaryActorTick.bCanEverTick = false;
	bReplicates = true;

	DummyRoot = CreateDefaultSubobject<USceneComponent>(TEXT("DummyRoot"));
	RootComponent = DummyRoot;

	GlassStackComp = CreateDefaultSubobject<UODGlassStackComponent>(TEXT("GlassStack"));
	GlassStackComp->SetupAttachment(RootComponent);
	GlassStackComp->SetIsReplicated(true);
}

void AODGlassRack::BeginPlay()
{
	Super::BeginPlay();
}

void AODGlassRack::PlayerInteraction_Implementation(APlayerCharacter* Player)
{
	if (Player && !Player->IsHoldingObject())
	{
		// Tell the player to pickup a glass off the rock
		Player->Server_PickupSpecificObject(GetTopGlass(), true);
		GlassStackComp->RemoveTopGlassMesh();
	}
}

void AODGlassRack::BecomeRelevant_Implementation(APlayerCharacter* PlayerTriggered)
{
	// Highlight the glasses on the rack
}

void AODGlassRack::LoseRelevance_Implementation()
{
	// Stop highlighting the glasses on the rack
}

AODDrinkGlass* AODGlassRack::GetTopGlass()
{
	return GlassStackComp->RemoveGlass(true);
}

void AODGlassRack::AddGlass(EGlassType GlassType)
{
	GlassStackComp->AddGlass(GlassType, false);
}

