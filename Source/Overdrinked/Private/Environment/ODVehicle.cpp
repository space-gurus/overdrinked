// Copyright 2020 Space Gurus


#include "Environment/ODVehicle.h"
#include "Net/UnrealNetwork.h"
#include "Components/SplineComponent.h"
#include "Environment/ODVehicleSpawner.h"
#include "AkGameplayStatics.h"
#include "AkAudioEvent.h"

AODVehicle::AODVehicle(const FObjectInitializer& OI) : Super(OI)
{
	PrimaryActorTick.bCanEverTick = true;
	bReplicates = true;
	//SetReplicateMovement(true);
}

void AODVehicle::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AODVehicle, bShouldMove);
	DOREPLIFETIME(AODVehicle, MoveSpeed);
}

void AODVehicle::FollowPath(USplineComponent* PathToFollow, float TimeToEnd, FVector DopplerPos)
{	
	if (PathToFollow)
	{
		Path = PathToFollow;
	}

	if (HasAuthority())
	{
		bShouldMove = true;
		MoveSpeed = TimeToEnd;
	}

	DopplerPosition = DopplerPos;
	GetWorldTimerManager().SetTimer(DopplerTimerHandle, this, &AODVehicle::UpdateDopplerEffect, 0.1f, false);
}

void AODVehicle::BeginPlay()
{
	Super::BeginPlay();
	
	// Play the sound till we're gone
	UAkAudioEvent* CarNoise = AmbientCarNoises[FMath::RandRange(0, AmbientCarNoises.Num() - 1)];
	if (CarNoise)
	{

		FOnAkPostEventCallback NullCallback;
		UAkGameplayStatics::PostEvent(CarNoise, this, 0, NullCallback);
	}
}

void AODVehicle::OnRep_ShouldMove()
{
}

void AODVehicle::FinishPath()
{
	Destroy();
}

void AODVehicle::UpdateDopplerEffect()
{
	UAkGameplayStatics::SetRTPCValue(NULL, FVector::Dist(GetActorLocation(), DopplerPosition), 1, this, "vehicle_distance_player");
	GetWorldTimerManager().ClearTimer(DopplerTimerHandle);
	GetWorldTimerManager().SetTimer(DopplerTimerHandle, this, &AODVehicle::UpdateDopplerEffect, 0.1f, false);
}

void AODVehicle::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (bShouldMove && Path)
	{
		MoveProgress += DeltaTime / MoveSpeed;
		FVector MoveSpot = Path->GetWorldLocationAtTime(MoveProgress);
		SetActorLocation(MoveSpot);

		FRotator MoveRotation = Path->GetWorldRotationAtTime(MoveProgress);
		MoveRotation.Yaw = -MoveRotation.Yaw;
		SetActorRotation(MoveRotation);

		if (MoveProgress >= 1.f)
		{
			FinishPath();
		}
	}
}

