// Copyright 2020 Space Gurus


#include "Environment/ODDoor.h"
#include "Net/UnrealNetwork.h"
#include "Components/SkeletalMeshComponent.h"
#include "Components/BoxComponent.h"
#include "Components/ArrowComponent.h"
#include "AI/ODAICharacter.h"
#include "AI/ODBouncer.h"
#include "AkGameplayStatics.h"
#include "AkAudioEvent.h"

// Sets default values
AODDoor::AODDoor(const FObjectInitializer& OI) : Super(OI)
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
	bReplicates = true;

	// Components
	RangeCollider = CreateDefaultSubobject<UBoxComponent>(TEXT("RangeCollider"));
	RootComponent = RangeCollider;
	
	SkeletalMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("SkeletalMesh"));
	SkeletalMesh->SetupAttachment(RootComponent);
}

void AODDoor::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	
	DOREPLIFETIME(AODDoor, bIsOpen);
}

void AODDoor::RangeColliderBeginOverlap(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (HasAuthority())
	{
		AODAICharacter* AICharacter = Cast<AODAICharacter>(OtherActor);
		AODBouncer* Bouncer = Cast<AODBouncer>(OtherActor);
		if (AICharacter || Bouncer)
		{
			if (NumCustomersInRange < 1)
			{
				bIsOpen = true;
				OnRep_IsOpen();
			}

			NumCustomersInRange++;
		}
	}
}

void AODDoor::RangeColliderEndOverlap(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (HasAuthority())
	{
		AODAICharacter* AICharacter = Cast<AODAICharacter>(OtherActor);
		AODBouncer* Bouncer = Cast<AODBouncer>(OtherActor);
		if (AICharacter || Bouncer)
		{
			NumCustomersInRange--;

			if (NumCustomersInRange < 1)
			{
				bIsOpen = false;
				OnRep_IsOpen();
			}

			// Let the AI know
			if (AICharacter && bIsEntrance)
			{
				AICharacter->SetIsInBarArea(true);
			}
		}
	}
}

// Called when the game starts or when spawned
void AODDoor::BeginPlay()
{
	Super::BeginPlay();
	
	RangeCollider->OnComponentBeginOverlap.AddDynamic(this, &AODDoor::RangeColliderBeginOverlap);
	RangeCollider->OnComponentEndOverlap.AddDynamic(this, &AODDoor::RangeColliderEndOverlap);
}

bool AODDoor::IsOpen()
{
	return bIsOpen;
}

void AODDoor::OnRep_IsOpen()
{
	if (bIsOpen)
	{
		FOnAkPostEventCallback NullCallback;
		UAkGameplayStatics::PostEvent(CustomerEnterSound, this, 0, NullCallback);
	}
	else
	{
		FOnAkPostEventCallback NullCallback;
		UAkGameplayStatics::PostEvent(CustomerExitSound, this, 0, NullCallback);
	}
}

