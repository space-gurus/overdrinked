// Copyright 2020 Space Gurus


#include "Environment/ODWasteBin.h"
#include "Components/StaticMeshComponent.h"
#include "PlayerCharacter.h"
#include "Actors/ODBevContainer.h"

// Sets default values
AODWasteBin::AODWasteBin()
{
	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
	StaticMesh->SetSimulatePhysics(false);
	StaticMesh->SetRenderCustomDepth(true);
	RootComponent = StaticMesh;
}

void AODWasteBin::MeshColliderBeginOverlap(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{

}

void AODWasteBin::MeshColliderEndOverlap(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{

}

void AODWasteBin::PlayerInteraction_Implementation(APlayerCharacter* Player)
{
	if (Player->IsHoldingBevContainer())
	{
		AODBevContainer* BevContainer = Cast<AODBevContainer>(Player->GetObjectInHand());
		if (BevContainer)
		{
			if (BevContainer->IsContainerEmpty())
			{
				Player->PlayErrorSound();
			}
			else
			{
				Player->Server_DumpContainerInHand();
			}
		}
	}
}

void AODWasteBin::BecomeRelevant_Implementation(APlayerCharacter* PlayerTriggered)
{
	ShowHighlight();
}

void AODWasteBin::LoseRelevance_Implementation()
{
	HideHighlight();
}

void AODWasteBin::ShowHighlight()
{
	bIsHighlighted = true;
	StaticMesh->SetCustomDepthStencilValue(240);
}

void AODWasteBin::HideHighlight()
{
	bIsHighlighted = false;
	StaticMesh->SetCustomDepthStencilValue(0);
}

bool AODWasteBin::IsHighlighted()
{
	return bIsHighlighted;
}

void AODWasteBin::BeginPlay()
{
	Super::BeginPlay();
}

