// Copyright 2020 Space Gurus


#include "Environment/ODIceStation.h"
#include "Net/UnrealNetwork.h"
#include "Components/StaticMeshComponent.h"
#include "Components/BoxComponent.h"
#include "Components/WidgetComponent.h"
#include "NiagaraComponent.h"
#include "PlayerCharacter.h"
#include "ODPlayerController.h"
#include "Actors/ODGarnish.h"
#include "UI/ODSliderQTE.h"
#include "AkGameplayStatics.h"
#include "AkAudioEvent.h"
#include "AkComponent.h"

AODIceStation::AODIceStation(const FObjectInitializer& OI) : Super(OI)
{
	PrimaryActorTick.bCanEverTick = false;
	bReplicates = true;

	IceVentEffect = CreateDefaultSubobject<UNiagaraComponent>(TEXT("IceVentEffect"));
	IceVentEffect->SetupAttachment(RootComponent);
}

void AODIceStation::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
}

void AODIceStation::BeginPlay()
{
	Super::BeginPlay();
}

void AODIceStation::PlayerInteraction_Implementation(APlayerCharacter* Player)
{
	Super::PlayerInteraction_Implementation(Player);

}

void AODIceStation::InteractWithQTE()
{
	Super::InteractWithQTE();

	PlayIceHitSound();
}

void AODIceStation::HandleQTESuccess()
{
	Super::HandleQTESuccess();

	GivePlayerIce();
    
    PlayIceHitSound();
}

void AODIceStation::GivePlayerIce()
{
	FActorSpawnParameters SpawnParams;
	AODGarnish* SpawnedIce = GetWorld()->SpawnActor<AODGarnish>(IceBP, GetActorLocation(), GetActorRotation(), SpawnParams);

	if (SpawnedIce && PlayerInteracting)
	{
		PlayerInteracting->Server_PickupSpecificObject(SpawnedIce, false);
	}

	bIsAvailable = true;
    
    PlayIceReceiveSound();
}

void AODIceStation::PlayIceHitSound()
{
    if (IceHitSound)
    {
        FOnAkPostEventCallback NullCallback;
        UAkGameplayStatics::PostEvent(IceHitSound, this, 0, NullCallback);
    }
}

void AODIceStation::PlayIceReceiveSound()
{
    if (IceReceiveSound)
    {
        FOnAkPostEventCallback NullCallback;
        UAkGameplayStatics::PostEvent(IceReceiveSound, this, 0, NullCallback);
    }
}
