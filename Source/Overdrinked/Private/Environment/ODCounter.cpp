// Copyright 2020 Space Gurus


#include "Environment/ODCounter.h"
#include "Components/StaticMeshComponent.h"
#include "Components/BoxComponent.h"
#include "Components/ArrowComponent.h"
#include "Actors/ODPickupActor.h"
#include "PlayerCharacter.h"

// Sets default values
AODCounter::AODCounter(const FObjectInitializer& OI) : Super(OI)
{
	PrimaryActorTick.bCanEverTick = false;
	bReplicates = true;


	// Components
	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
	SetRootComponent(StaticMesh);
	StaticMesh->SetRenderCustomDepth(true);
	StaticMesh->SetCustomDepthStencilValue(0);
	
	AreaCollider = CreateDefaultSubobject<UBoxComponent>(TEXT("AreaCollider"));
	AreaCollider->SetupAttachment(RootComponent);
	AreaCollider->SetBoxExtent(FVector(50.f, 50.f, 25.f));
	DrinkAttachComponent = CreateDefaultSubobject<USceneComponent>(TEXT("DrinkAttachComponent"));

	ForwardArrow = CreateDefaultSubobject<UArrowComponent>(TEXT("ForwardArrow"));
	ForwardArrow->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void AODCounter::BeginPlay()
{
	Super::BeginPlay();
	
	AreaCollider->OnComponentBeginOverlap.AddDynamic(this, &AODCounter::AreaColliderBeginOverlap);
	AreaCollider->OnComponentEndOverlap.AddDynamic(this, &AODCounter::AreaColliderEndOverlap);

	if (AttachedObject)
	{
		bIsCounterFull = true;
		bCanHoldItem = false;

		AODPickupActor* PickupActor = Cast<AODPickupActor>(AttachedObject);
		if (PickupActor)
		{
			PickupActor->SetIsAttached(true);
		}
	}
}

// Called every frame
void AODCounter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AODCounter::AreaColliderBeginOverlap(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
}

void AODCounter::AreaColliderEndOverlap(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
}

void AODCounter::PlayerInteraction_Implementation(APlayerCharacter* Player)
{
	if (AttachedObject)
	{
		if (AttachedObject->Implements<UODInteractInterface>())
		{
			IODInteractInterface::Execute_PlayerInteraction(AttachedObject, Player);
		}
	}
	else if (Player && Player->IsHoldingObject() && bCanHoldItem && !bIsCounterFull) // Take the object in the player's hand
	{
		AActor* ObjectToAttach = Player->GetObjectInHand();
		// Tell the player
		Player->Server_DropObject(true, true);

		AttachObject(ObjectToAttach);
		IODInteractInterface::Execute_BecomeRelevant(ObjectToAttach, Player);
	}
}

void AODCounter::BecomeRelevant_Implementation(APlayerCharacter* PlayerTriggered)
{
	Highlight();

	if (AttachedObject)
	{
		if (AttachedObject->Implements<UODInteractInterface>())
		{
			IODInteractInterface::Execute_BecomeRelevant(AttachedObject, PlayerTriggered);
		}
	}
}

void AODCounter::LoseRelevance_Implementation()
{
	EndHighlight();

	if (AttachedObject)
	{
		if (AttachedObject->Implements<UODInteractInterface>())
		{
			IODInteractInterface::Execute_LoseRelevance(AttachedObject);
		}
	}
}

void AODCounter::Highlight()
{
	if (!bIsHighlighted)
	{
		bIsHighlighted = true;
		StaticMesh->SetCustomDepthStencilValue(240);
	}
}

void AODCounter::EndHighlight()
{
	if (bIsHighlighted)
	{
		bIsHighlighted = false;
		StaticMesh->SetCustomDepthStencilValue(0);
	}
}

USceneComponent* AODCounter::GetDrinkAttachComponent()
{
	return DrinkAttachComponent;
}

FVector AODCounter::GetDrinkAttachWorldLocation()
{
	FVector AttachWorldLocation = GetActorLocation();
	AttachWorldLocation.X -= DrinkAttachComponent->GetRelativeLocation().X;
	AttachWorldLocation.Y += DrinkAttachComponent->GetRelativeLocation().Y;
	AttachWorldLocation.Z += DrinkAttachComponent->GetRelativeLocation().Z;
	return AttachWorldLocation;
}

bool AODCounter::CanHoldItem()
{
	return bCanHoldItem && !bIsCounterFull;
}

bool AODCounter::GetIsCounterFull()
{
	return bIsCounterFull;
}

void AODCounter::SetIsCounterFull(bool val)
{
	bIsCounterFull = val;
}

bool AODCounter::IsHighlighted()
{
	return bIsHighlighted;
}

FVector AODCounter::GetCounterForwardVector()
{
	return ForwardArrow->GetForwardVector();
}

AActor* AODCounter::GetAttachedActor()
{
	return AttachedObject;
}

void AODCounter::AttachObject(AActor* ObjectToAttach)
{
	// Take the object from the player
	AttachedObject = ObjectToAttach;
	bIsCounterFull = true;
	bCanHoldItem = false;

	// Tell the object itself to attach
	AODPickupActor* PickupActor = Cast<AODPickupActor>(AttachedObject);
	if (PickupActor)
	{
		PickupActor->AttachToCounter(this);
	}
}

AActor* AODCounter::RemoveAttachedObject()
{
	AActor* RemovedActor = AttachedObject;
	AttachedObject = NULL;
	bIsCounterFull = false;

	AODPickupActor* PickupActor = Cast<AODPickupActor>(RemovedActor);
	if (PickupActor)
		bCanHoldItem = true;

	return RemovedActor;
}

bool AODCounter::HasAttachedObject()
{
	return AttachedObject ? true : false;
}

