// Copyright 2020 Space Gurus


#include "Environment/ODBarArea.h"
#include "Components/BoxComponent.h"

// Sets default values
AODBarArea::AODBarArea()
{
	PrimaryActorTick.bCanEverTick = false;
	bReplicates = true;

	BarAreaContainer = CreateDefaultSubobject<UBoxComponent>(TEXT("BarAreaContainer"));
	BarAreaContainer->SetBoxExtent(FVector(200.f, 200.f, 10.f));
}

// Called when the game starts or when spawned
void AODBarArea::BeginPlay()
{
	Super::BeginPlay();
	
}

