// Copyright 2020 Space Gurus


#include "Environment/ODChopStation.h"
#include "Net/UnrealNetwork.h"
#include "NiagaraComponent.h"
#include "NiagaraFunctionLibrary.h"
#include "Actors/ODGarnish.h"
#include "AI/ODCrawlerManager.h"
#include "PlayerCharacter.h"
#include "Environment/ODDummyPickup.h"
#include "UI/ODSliderQTE.h"
#include "AkGameplayStatics.h"
#include "AkAudioEvent.h"
#include "AkComponent.h"
#include "Kismet/GameplayStatics.h"

AODChopStation::AODChopStation(const FObjectInitializer& OI) : Super(OI)
{
	PrimaryActorTick.bCanEverTick = false;
	bReplicates = true;

	ChopEffectPosition = CreateDefaultSubobject<USceneComponent>(TEXT("ChopEffectPosition"));
	ChopEffectPosition->SetupAttachment(RootComponent);
}

void AODChopStation::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
}

void AODChopStation::BeginPlay()
{
	Super::BeginPlay();

	SliderQTE = Cast<UODSliderQTE>(QTEWidget);
}

void AODChopStation::PlayerInteraction_Implementation(APlayerCharacter* Player)
{
	Super::PlayerInteraction_Implementation(Player);

	// Get rid of the bug in the player's hand
	// This shouldn't be done here, refactor eventually
	if (PlayerInteracting && PlayerInteracting->IsHoldingCrawler())
	{
		PlayerInteracting->Server_DropObjectAndDestroy();

		FActorSpawnParameters SpawnParams;
		KnifeProp = GetWorld()->SpawnActor<AODDummyPickup>(KnifeBP, GetActorLocation(), GetActorRotation() + FRotator(0.f, 0.f, -90.f), SpawnParams);
		if (KnifeProp)
		{
			PlayerInteracting->Server_PickupSpecificObject(KnifeProp, false);
		}
	}

	StartChopEffect();
}

void AODChopStation::StartQTE()
{
	Super::StartQTE();

}

void AODChopStation::InteractWithQTE()
{
	Super::InteractWithQTE();

	if (SliderQTE && (LastSucessCount < SliderQTE->GetPlayerSuccessCount()))
	{
		PlaySquishSound();
		LastSucessCount++;
	}
	else
	{
		PlayChopSound();
	}
}

void AODChopStation::CancelQTE()
{
	Super::CancelQTE();
	FinishChopEffect();

	TArray<AActor*> FoundActors;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), AODCrawlerManager::StaticClass(), FoundActors);
	
	if (FoundActors.Num() > 0)
	{
		AODCrawlerManager* CrawlerManager = Cast<AODCrawlerManager>(FoundActors[0]);
		if (CrawlerManager)
		{
			CrawlerManager->Server_SpawnCrawlerAtPosition(GetActorLocation() - CrawlerSpawnSpot);
		}
	}
}

void AODChopStation::HandleQTESuccess()
{
	Super::HandleQTESuccess();

	PlaySquishSound();
	GivePlayerCrawler();

	LastSucessCount = 0;
}

void AODChopStation::HandleQTEFinish(bool bSuccess)
{
	Super::HandleQTEFinish(bSuccess);
	FinishChopEffect();
}

void AODChopStation::StartChopEffect_Implementation()
{
	ChopEffectComp = UNiagaraFunctionLibrary::SpawnSystemAttached(ChopEffect, ChopEffectPosition, "", FVector(0.f), FRotator(0.f), FVector(1.f), EAttachLocation::SnapToTarget, false, ENCPoolMethod::None);
	HandleKnife();
}

void AODChopStation::GivePlayerCrawler()
{
	FActorSpawnParameters SpawnParams;
	AODGarnish* SpawnedCrawler = GetWorld()->SpawnActor<AODGarnish>(CrawlerBP, GetActorLocation(), GetActorRotation(), SpawnParams);

	if (SpawnedCrawler && PlayerInteracting)
	{
		PlayerInteracting->Server_PickupSpecificGarnish(SpawnedCrawler);
	}

	bIsAvailable = true;
}

void AODChopStation::FinishChopEffect_Implementation()
{
	if (ChopEffectComp)
	{
		ChopEffectComp->DestroyComponent();
	}
}

void AODChopStation::FinishEffects_Implementation()
{
	Super::FinishEffects_Implementation();

	FinishChopEffect();

	HandleKnife();

	if (KnifeProp && PlayerInteracting)
	{
		PlayerInteracting->Server_DropObjectAndDestroy();
	}
}

void AODChopStation::PlayChopSound()
{
    if (ChopSound)
    {
        FOnAkPostEventCallback NullCallback;
        UAkGameplayStatics::PostEvent(ChopSound, this, 0, NullCallback);
    }
}

void AODChopStation::PlaySquishSound()
{
    if (SquishSound)
    {
        FOnAkPostEventCallback NullCallback;
        UAkGameplayStatics::PostEvent(SquishSound, this, 0, NullCallback);
    }
}
