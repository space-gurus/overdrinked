// Copyright 2020 Space Gurus


#include "Environment/ODGridUnit.h"
#include "Components/BoxComponent.h"

AODGridUnit::AODGridUnit()
{
	PrimaryActorTick.bCanEverTick = false;

	// Defaults
	UnitBox = CreateDefaultSubobject<UBoxComponent>(TEXT("UnitBox"));
	UnitBox->SetBoxExtent(FVector(50.f, 50.f, 50.f));
}

void AODGridUnit::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AODGridUnit::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

