// Copyright 2020 Space Gurus


#include "Environment/ODAIHub.h"
#include "Environment/ODAILocation.h"

AODAIHub::AODAIHub()
{
	PrimaryActorTick.bCanEverTick = false;

}

void AODAIHub::RegisterLocation(AODAILocation* HubLocation)
{
	HubLocations.AddUnique(HubLocation);
}

bool AODAIHub::CanHoldConversation()
{
	int32 FilledPositions = 0;

	for (AODAILocation* AILocation : HubLocations)
	{
		if (AILocation && !AILocation->IsLocationFree())
		{
			FilledPositions++;
		}
	}

	// If there's at least 2 characters here, a conversation can happen
	return FilledPositions >= 2;
}

void AODAIHub::BeginPlay()
{
	Super::BeginPlay();
	
}
