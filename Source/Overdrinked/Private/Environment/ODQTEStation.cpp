// Copyright 2020 Space Gurus


#include "Environment/ODQTEStation.h"
#include "Net/UnrealNetwork.h"
#include "Components/StaticMeshComponent.h"
#include "Components/BoxComponent.h"
#include "Components/WidgetComponent.h"
#include "UI/ODQTEWidget.h"
#include "Actors/ODSkelPickupActor.h"
#include "ODPlayerController.h"
#include "PlayerCharacter.h"

AODQTEStation::AODQTEStation(const FObjectInitializer& OI) : Super(OI)
{
	PrimaryActorTick.bCanEverTick = true;
	bReplicates = true;

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
	RootComponent = StaticMesh;
	StaticMesh->SetRenderCustomDepth(true);
	StaticMesh->SetCustomDepthStencilValue(DefaultCustomDepth);

	InteractCollider = CreateDefaultSubobject<UBoxComponent>(TEXT("InteractCollider"));
	InteractCollider->SetupAttachment(RootComponent);

	QTEComponent = CreateDefaultSubobject<UWidgetComponent>(TEXT("QTEComponent"));
	QTEComponent->SetupAttachment(RootComponent);
	QTEComponent->SetVisibility(false);

	HelperWidget = CreateDefaultSubobject<UWidgetComponent>(TEXT("HelperWidget"));
	HelperWidget->SetupAttachment(RootComponent);
}

void AODQTEStation::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AODQTEStation, bIsAvailable);
	DOREPLIFETIME(AODQTEStation, PlayerInteracting);
}

void AODQTEStation::BeginPlay()
{
	Super::BeginPlay();
	
	QTEWidget = Cast<UODQTEWidget>(QTEComponent->GetUserWidgetObject());
}

void AODQTEStation::Highlight_Implementation()
{
	StaticMesh->SetCustomDepthStencilValue(HighlightCustomDepth);
}

void AODQTEStation::EndHighlight_Implementation()
{
	StaticMesh->SetCustomDepthStencilValue(DefaultCustomDepth);
}

void AODQTEStation::PlayerInteraction_Implementation(APlayerCharacter* Player)
{
	PlayerInteracting = Player;

	if (bIsAvailable && PlayerInteracting)
	{
		if (PlayerInteracting->IsHoldingObject())
		{
			AODSkelPickupActor* Crawler = Cast<AODSkelPickupActor>(PlayerInteracting->GetObjectInHand());
			if (!Crawler)
			{
				// If we're holding an object and it's NOT a crawler, we can't do this QTE
				return;
			}
		}

		// Let the player know
		AODPlayerController* ODPlayerController = Cast<AODPlayerController>(PlayerInteracting->GetController());
		if (ODPlayerController)
		{
			ODPlayerController->Client_StartQTE(this);
			bIsAvailable = false;
		}
	}
}

void AODQTEStation::BecomeRelevant_Implementation(APlayerCharacter* PlayerTriggered)
{
	Highlight();
}

void AODQTEStation::LoseRelevance_Implementation()
{
	EndHighlight();
}

void AODQTEStation::StartQTE()
{
	if (QTEWidget)
	{
		QTEComponent->SetVisibility(true);
		QTEWidget->ActivateQTE(this);
	}
}

void AODQTEStation::CancelQTE()
{
	if (QTEWidget)
	{
		QTEWidget->DeactivateQTE();
	}
}

void AODQTEStation::InteractWithQTE()
{
	if (QTEWidget)
	{
		QTEWidget->InteractWithQTE();
	}
}

void AODQTEStation::HandleQTESuccess()
{
}

void AODQTEStation::HandleQTEFinish(bool bSuccess)
{
	// Let the player know we're done
	if (PlayerInteracting)
	{
		AODPlayerController* ODPlayerController = Cast<AODPlayerController>(PlayerInteracting->GetController());

		if (ODPlayerController)
		{
			ODPlayerController->FinishQTE(bSuccess);
		}
	}

	if (QTEComponent)
	{
		QTEComponent->SetVisibility(false);
	}

	bIsAvailable = true;
}

void AODQTEStation::FinishEffects_Implementation()
{

}
