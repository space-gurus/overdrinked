// Copyright 2020 Space Gurus


#include "Environment/ODWashStation.h"
#include "Net/UnrealNetwork.h"
#include "Components/StaticMeshComponent.h"
#include "Components/BoxComponent.h"
#include "Components/WidgetComponent.h"
#include "Components/ArrowComponent.h"
#include "Components/ODGlassStackComponent.h"
#include "Gameplay/ODGlassType.h"
#include "PlayerCharacter.h"
#include "Actors/ODDrinkGlass.h"
#include "Environment/ODGlassRack.h"
#include "UI/ODWashProgressWidget.h"
#include "NiagaraFunctionLibrary.h"
#include "NiagaraComponent.h"

AODWashStation::AODWashStation(const FObjectInitializer& OI) : Super(OI)
{
	PrimaryActorTick.bCanEverTick = true;
	bReplicates = true;

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
	RootComponent = StaticMesh;

	InteractCollider = CreateDefaultSubobject<UBoxComponent>(TEXT("InteractCollider"));
	InteractCollider->SetupAttachment(RootComponent);

	SinkInteractCollider = CreateDefaultSubobject<UBoxComponent>(TEXT("SinkInteractCollider"));
	SinkInteractCollider->SetupAttachment(RootComponent);

	ForwardArrow = CreateDefaultSubobject<UArrowComponent>(TEXT("ForwardArrow"));
	ForwardArrow->SetupAttachment(RootComponent);

	GlassStackComp = CreateDefaultSubobject<UODGlassStackComponent>(TEXT("GlassStack"));
	GlassStackComp->SetupAttachment(RootComponent);

	WashProgressWidgetComponent = CreateDefaultSubobject<UWidgetComponent>(TEXT("WashProgressWidget"));
	WashProgressWidgetComponent->SetupAttachment(RootComponent);

	WashEffectLocationComp = CreateDefaultSubobject<USceneComponent>(TEXT("WashEffectLocation"));
	WashEffectLocationComp->SetupAttachment(RootComponent);

	HelperWidget = CreateDefaultSubobject<UWidgetComponent>(TEXT("HelperWidget"));
	HelperWidget->SetupAttachment(RootComponent);
}

void AODWashStation::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (bIsPlayerWashing)
	{
		FinishWashCycle();
	}
}

void AODWashStation::BeginPlay()
{
	Super::BeginPlay();
	
	SinkInteractCollider->OnComponentBeginOverlap.AddDynamic(this, &AODWashStation::InteractColliderBeginOverlap);
	SinkInteractCollider->OnComponentEndOverlap.AddDynamic(this, &AODWashStation::InteractColliderEndOverlap);
}

void AODWashStation::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AODWashStation, WashProgress);
	DOREPLIFETIME(AODWashStation, bIsPlayerWashing);
}

void AODWashStation::InteractColliderBeginOverlap(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	APlayerCharacter* PlayerCharacter = Cast<APlayerCharacter>(OtherActor);
	if (PlayerCharacter)
	{
		bIsPlayerOverlappingSink = true;
	}
}

void AODWashStation::InteractColliderEndOverlap(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	APlayerCharacter* PlayerCharacter = Cast<APlayerCharacter>(OtherActor);
	if (PlayerCharacter)
	{
		bIsPlayerOverlappingSink = false;

		if (PlayerCharacter->IsWashing())
		{
			PlayerCharacter->Server_FinishWashing();
		}
	}
}

void AODWashStation::FinishWashCycle()
{
	WashProgress = FMath::Clamp(WashProgress + WashEfficiency, 0.f, 1.01f);

	if (WashProgress >= 1.f)
	{
		WashProgress = 0.f;

		// Finish this glass
		if (HasAuthority())
		{
			// Remove it from the sink's stack
			AODDrinkGlass* CleanGlass = GlassStackComp->RemoveGlass(true);
			GlassStackComp->RemoveTopGlassMesh(); // Uhm what?

			// And add it to the appropriate glass rack
			if (CleanGlass)
			{
				switch (CleanGlass->GetGlassType())
				{
					case EGlassType::GT_Brew:
						if (BrewGlassRack)
							BrewGlassRack->AddGlass(EGlassType::GT_Brew);
						break;
					case EGlassType::GT_Stemmed:
						if (StemmedGlassRack)
							StemmedGlassRack->AddGlass(EGlassType::GT_Stemmed);
						break;
					case EGlassType::GT_Shot:
						if (ShotGlassRack)
							ShotGlassRack->AddGlass(EGlassType::GT_Shot);
						break;
					default:
						break;
				}

				CleanGlass->Destroy();
			}

			// Are there more to wash?
			if (GlassStackComp->GetNumGlasses() <= 0)
			{
				if (PlayerWashing)
				{
					PlayerWashing->Server_FinishWashing();
					bIsPlayerWashing = false;
					WashProgressWidgetComponent->SetVisibility(false);
					
					if (WashEffect)
					{
						WashEffect->Deactivate();
					}
				}
			}
		}
	}

	UpdateWashWidget();

	// Keep going if needed
	if (PlayerWashing)
	{
		if (PlayerWashing->IsWashing())
		{
			GetWorldTimerManager().ClearTimer(WashTimerHandle);
			GetWorldTimerManager().SetTimer(WashTimerHandle, this, &AODWashStation::FinishWashCycle, WashSpeed, false);
		}
		else
		{
			// Handle finish
			WashProgressWidgetComponent->SetVisibility(false);
			bIsPlayerWashing = false;

			if (WashEffect)
			{
				WashEffect->Deactivate();
			}
		}
	}
}

void AODWashStation::OnRep_IsPlayerWashing()
{
	if (WashProgressWidgetComponent)
	{
		WashProgressWidgetComponent->SetVisibility(bIsPlayerWashing);
	}
}

void AODWashStation::OnRep_WashProgress()
{
	UpdateWashWidget();
}

void AODWashStation::UpdateWashWidget()
{
	if (WashProgressWidget)
	{
		WashProgressWidget->SetWashProgress(WashProgress);
	}
	else
	{
		// We don't have our reference yet
		WashProgressWidget = Cast<UODWashProgressWidget>(WashProgressWidgetComponent->GetUserWidgetObject());
		if (WashProgressWidget)
		{
			WashProgressWidget->SetWashProgress(WashProgress);
		}
	}
}

void AODWashStation::PlayerInteraction_Implementation(APlayerCharacter* Player)
{
	if (Player->IsHoldingDrinkGlass())
	{
		// Put the glass down on the station
		AODDrinkGlass* PlayerGlass = Cast<AODDrinkGlass>(Player->GetObjectInHand());
		if (PlayerGlass)
		{
			GlassStackComp->AddGlass(PlayerGlass->GetGlassType(), PlayerGlass->IsGlassDirty());
		}

		Player->Server_DropObjectAndDestroy();
	}
	else
	{
		// We can only wash if we're facing the front of the sink
		FVector StationForwardVector = ForwardArrow->GetForwardVector();
		FVector PlayerForwardVector = Player->GetActorForwardVector();
		float DirDot = FVector::DotProduct(StationForwardVector, PlayerForwardVector);

		if (DirDot < 0.2f)
		{
			// Wash some glasses
			if (!Player->IsWashing() && bIsPlayerOverlappingSink && (GlassStackComp->GetNumGlasses() > 0))
			{
				Player->StartWashing();
				PlayerWashing = Player;
				bIsPlayerWashing = true;

				// Set our initial timer
				GetWorldTimerManager().ClearTimer(WashTimerHandle);
				GetWorldTimerManager().SetTimer(WashTimerHandle, this, &AODWashStation::FinishWashCycle, WashSpeed, false);

				if (WashProgressWidgetComponent)
				{
					WashProgressWidgetComponent->SetVisibility(true);
				}

				// Get the effect going
				if (WashEffectSystem)
				{
					WashEffect = UNiagaraFunctionLibrary::SpawnSystemAttached(WashEffectSystem, WashEffectLocationComp, "", FVector::ZeroVector, FRotator::ZeroRotator, FVector(2.f, 2.f, 1.f), EAttachLocation::SnapToTarget, false, ENCPoolMethod::None);
				}
			}
		}
	}
}
