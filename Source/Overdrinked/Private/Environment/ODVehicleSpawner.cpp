// Copyright 2020 Space Gurus


#include "Environment/ODVehicleSpawner.h"
#include "Components/SplineComponent.h"
#include "Environment/ODVehicle.h"

AODVehicleSpawner::AODVehicleSpawner(const FObjectInitializer& OI) : Super(OI)
{
	PrimaryActorTick.bCanEverTick = false;

	DummyRoot = CreateDefaultSubobject<USceneComponent>(TEXT("DummyRoot"));
	RootComponent = DummyRoot;

	VehiclePath = CreateDefaultSubobject<USplineComponent>(TEXT("VehiclePath"));
	VehiclePath->SetupAttachment(RootComponent);
}

void AODVehicleSpawner::BeginPlay()
{
	Super::BeginPlay();
	
	SpawnVehicle();
}

void AODVehicleSpawner::SpawnVehicle()
{
	FActorSpawnParameters SpawnParams;
	FVector SpawnPosition = GetActorLocation();
	FRotator SpawnRotation = GetActorRotation();

	AODVehicle* NewVehicle = GetWorld()->SpawnActor<AODVehicle>(CarBP, SpawnPosition, FRotator(0.f), SpawnParams);

	if (NewVehicle)
	{
		NewVehicle->FollowPath(VehiclePath, VehicleMoveSpeed, DopplerPoint);
	}

	float TimeToNextSpawn = FMath::RandRange(MinWaitTimeToSpawn, MaxWaitTimeToSpawn);
	GetWorldTimerManager().SetTimer(SpawnTimerHandle, this, &AODVehicleSpawner::SpawnVehicle, TimeToNextSpawn, false);
}

