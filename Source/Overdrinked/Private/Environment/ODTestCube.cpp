// Copyright 2020 Space Gurus


#include "Environment/ODTestCube.h"

// Sets default values
AODTestCube::AODTestCube()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AODTestCube::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AODTestCube::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

