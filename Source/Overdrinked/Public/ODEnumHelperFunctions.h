// Copyright 2020 Space Gurus

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "IngredientsEnum.h"
#include "DrinksEnum.h"
#include "Gameplay/ODGlassType.h"
#include "CustomerEmotesEnum.h"
#include "Gameplay/ODMixMethod.h"
#include "ODEnumHelperFunctions.generated.h"

/**
 * 
 */
UCLASS()
class OVERDRINKED_API UODEnumHelperFunctions : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable, Category = "EnumHelperFunctions")
	static FName IngredientEnumToString(EIngredientType IngredientType);
	
	UFUNCTION(BlueprintCallable, Category = "EnumHelperFunctions")
	static EIngredientType IngredientStringToEnum(FName IngredientName);

	UFUNCTION(BlueprintCallable, Category = "EnumHelperFunctions")
	static FName DrinkEnumToString(EDrink DrinkType);

	UFUNCTION(BlueprintCallable, Category = "EnumHelperFunctions")
	static EDrink DrinkStringToEnum(FName DrinkName);

	UFUNCTION(BlueprintCallable, Category = "EnumHelperFunctions")
	static FName CustomerEmoteEnumToString(ECustomerEmote Emote);

	UFUNCTION(BlueprintCallable, Category = "EnumHelperFunctions")
	static ECustomerEmote CustomerEmoteStringToEnum(FName EmoteName);

	UFUNCTION(BlueprintCallable, Category = "EnumHelperFunctions")
	static float GetGlassShapeIndex(EGlassType GlassType);

	UFUNCTION(BlueprintCallable, Category = "RandomHelperFunctions")
	static FString ConvertTimeRemainingToClock(int32 TimeRemaining);

	UFUNCTION(BlueprintCallable, Category = "RandomHelperFunctions")
	static FString ConvertMapNameToURL(FName MapName);

	UFUNCTION(BlueprintCallable, Category = "EnumHelperFunctions")
	static FName MixMethodEnumToString(EMixMethod MixMethod);

	UFUNCTION(BlueprintCallable, Category = "EnumHelperFunctions")
	static EMixMethod MixMethodStringToEnum(FName MixMethodName);
};
