// Copyright 2020 Space Gurus

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerState.h"
#include "ODPlayerState.generated.h"

/**
 * 
 */
UCLASS()
class OVERDRINKED_API AODPlayerState : public APlayerState
{
	GENERATED_BODY()
	
public:
	UFUNCTION(BlueprintCallable)
	int32 GetPlayerNum();

	void SetPlayerNum(int32 PNum);

protected:
	virtual void BeginPlay() override;

	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	virtual void CopyProperties(APlayerState* PlayerState);

	virtual void OverrideWith(APlayerState* PlayerState);

protected:
	/** BB&B Player Num in this game (used for coloring, host is always 0) */
	UPROPERTY(BlueprintReadOnly, Replicated)
	int32 PlayerNum;

	UPROPERTY(Replicated)
	bool bIsPlayerReady = false;
};
