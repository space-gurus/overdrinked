// Copyright 2020 Space Gurus

#pragma once
UENUM(BlueprintType)
enum class EDrink : uint8
{
	D_None			UMETA(DisplayName = "None"),
	D_ZoorpLite		UMETA(DisplayName = "Zoorp Lite"),
	D_HeavyHopJPA	UMETA(DisplayName = "Heavy Hop JPA"),
	D_BlackHoleStout	UMETA(DisplayName = "Black Hole Stout"),
	D_FizzleBuzz	UMETA(DisplayName = "FizzleBuzz"),
	D_SexInTheCrater	UMETA(DisplayName = "Sex In The Crater"),
	D_NewFashioned	UMETA(DisplayName = "New Fashioned"),
	D_DinnoriaDomer	UMETA(DisplayName = "Dinnoria Domer")
};