// Copyright 2020 Space Gurus

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameMode.h"
#include "Gameplay/ODDrinkOrder.h"
#include "ODMultiplayerGameMode.generated.h"

class UDataTable;
class AODPlayerController;
class AODAIManager;

/**
 * 
 */
UCLASS()
class OVERDRINKED_API AODMultiplayerGameMode : public AGameMode
{
	GENERATED_BODY()
	
public:
	AODMultiplayerGameMode();

	virtual void PostLogin(APlayerController* NewPlayer) override;

	virtual bool ReadyToStartMatch_Implementation() override;

	virtual void HandleMatchHasStarted() override;

	virtual void PostSeamlessTravel() override;

	virtual void InitSeamlessTravelPlayer(AController* NewController) override;

	void NotifyPlayersLeavingLevel();

	void NotifyPlayersOfNewOrder(FDrinkOrder DrinkOrder);

	void NotifyPlayersOfClosedOrder(FDrinkOrder DrinkOrder);

	void SetAIManagerReference(AODAIManager* Manager);

	int32 GetLevelLengthInMinutes();

	UFUNCTION(BlueprintCallable)
	void SetGameSpeed(float val);

	UFUNCTION(BlueprintCallable)
	float GetGameSpeed();

	UFUNCTION(BlueprintCallable)
	void SetPauseGameTime(bool bShouldPause);

	UFUNCTION(BlueprintCallable)
	bool IsGameTimePaused();

	void HandlePlayerControllerIsReady(AODPlayerController* ReadyPlayerController);

	void HandleDelayedGameStart();

	TArray<int32> GetStarThresholds();

	void HandleBrawlLevelUpdate(float BrawlPercentage);

	void HandleLevelEnd();

	float GetIceMeltTime();

	UFUNCTION(BlueprintCallable)
	void SetBlendTimeGlobal(float NewBlendTime);

	UFUNCTION(BlueprintCallable)
	void SetBlenderOverflowTimeGlobal(float NewBlenderOverflowTime);

	UFUNCTION(BlueprintCallable)
	void SetBrawlThreshold(float NewThreshold);

protected:
	UDataTable* LevelConstraintDataTable;

	TArray<AODPlayerController*> PlayerControllerList;

	/** Hold statuses for player controllers on their local ready status */
	int32 NumReadyPlayers = 0;

	AODAIManager* AIManager;

	/** Did we come to this map seamlessly */
	bool bTraveledSeamlessly = false;

	/** Maximum number of players allowed in the game */
	UPROPERTY(EditAnywhere)
	int32 MaxNumPlayers = 1;

	/** Minimum amount of points for a one star rating */
	UPROPERTY(EditAnywhere, Category = "Points")
	int32 OneStarPoints;

	/** Minimum amount of points for a two star rating */
	UPROPERTY(EditAnywhere, Category = "Points")
	int32 TwoStarPoints;
	
	/** Minimum amount of points for a three star rating */
	UPROPERTY(EditAnywhere, Category = "Points")
	int32 ThreeStarPoints;

	UPROPERTY(VisibleAnywhere, Category = "Points")
	TArray<int32> StarThresholds;

	/** The length of this level */
	UPROPERTY(EditAnywhere, Category = "Level Specific")
	int32 LevelLengthInMinutes;

	/** Game speed (how long is 1 minute) */
	UPROPERTY(EditAnywhere, Category = "Level Specific")
	float GameMinuteSpeed = 1.f;

	int32 LevelLengthInSeconds;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Level Specific")
	float BrawlThreshold = 0.75f;

	/** How fast does ice melt? */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Difficulty Settings")
	float IceMeltTime = 5.f;

	bool bIsLevelOver = false;

	bool bIsTimePaused = false;

protected:
	void ReadInLevelData();
};
