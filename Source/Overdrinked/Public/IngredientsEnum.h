// Copyright 2020 Space Gurus

#pragma once
UENUM(BlueprintType)
enum class EIngredientType : uint8
{
	IT_None			UMETA(DisplayName = "None"),
	IT_Shlum		UMETA(DisplayName = "Shlum"),
	IT_Fitzky		UMETA(DisplayName = "Fitzky"),
	IT_Shocka		UMETA(DisplayName = "Shocka"),
	IT_Krin			UMETA(DisplayName = "Krin"),
	IT_PurpleDeath	UMETA(DisplayName = "PurpleDeath"),
	IT_RedStool		UMETA(DisplayName = "RedStoolEnergy"),
	IT_LiquidSun	UMETA(DisplayName = "LiquidDoubleSun"),
	IT_Sprixel		UMETA(DisplayName = "Sprixel"),
	IT_Zoorp		UMETA(DisplayName = "ZoorpLite"),
	IT_HeavyHop		UMETA(DisplayName = "HeavyHopJPA"),
	IT_BlackHoleStout	UMETA(DisplayName = "BlackHoleStout")
};