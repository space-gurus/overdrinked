// Copyright 2020 Space Gurus

#pragma once

#include "CoreMinimal.h"
#include "Components/SceneComponent.h"
#include "Gameplay/ODGlassType.h"
#include "ODGlassStackComponent.generated.h"

class UStaticMeshComponent;
class UNiagaraComponent;
class UNiagaraSystem;
class AODDrinkGlass;

USTRUCT()
struct FGlassInfo
{
	GENERATED_BODY()

	UPROPERTY()
	UStaticMeshComponent* GlassMesh;

	UPROPERTY()
	UNiagaraComponent* DirtyEffect;

	UPROPERTY()
	EGlassType GlassType;

	UPROPERTY()
	bool bIsDirty;

	bool operator==(const FGlassInfo& StructB)
	{
		return GlassMesh == StructB.GlassMesh;
	}
};

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class OVERDRINKED_API UODGlassStackComponent : public USceneComponent
{
	GENERATED_BODY()

public:	
	UODGlassStackComponent();

protected:
	virtual void BeginPlay() override;
	
	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

#if WITH_EDITOR
	virtual void PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent) override;
#endif

	void AdjustGlassType();

	void CalculateGlassPositions(bool bInEditor);

	void SpawnStartingGlassware();

	UStaticMesh* GetGlassMesh(EGlassType GType);

protected:
	UPROPERTY(EditAnywhere, Category = "Glassare", Meta = (MakeEditWidget = true))
	FVector GlassSpawnCenter;

	UPROPERTY(EditAnywhere, Category = "Glassware")
	TSubclassOf<AODDrinkGlass> StemmedGlassBP;

	UPROPERTY(EditAnywhere, Category = "Glassware")
	TSubclassOf<AODDrinkGlass> BrewGlassBP;

	UPROPERTY(EditAnywhere, Category = "Glassware")
	TSubclassOf<AODDrinkGlass> ShotGlassBP;

	UPROPERTY(EditAnywhere, Category = "Glassware")
	UNiagaraSystem* DirtyEffect;

	UPROPERTY(EditAnywhere, Category = "Glassware")
	UStaticMesh* StemmedGlassMesh;

	UPROPERTY(EditAnywhere, Category = "Glassware")
	UStaticMesh* BrewGlassMesh;

	UPROPERTY(EditAnywhere, Category = "Glassware")
	UStaticMesh* ShotGlassMesh;

	UPROPERTY(EditAnywhere, Category = "Glassware")
	EGlassType GlassType;

	UPROPERTY(EditAnywhere, Category = "Glassware", meta = (ClampMin = "0", ClampMax = "14"))
	int32 MaxGlasses;

	UPROPERTY(EditAnywhere, Category = "Glassware", meta = (ClampMin = "0", ClampMax = "14"))
	int32 StartingNumGlasses;

	UPROPERTY(Replicated, VisibleAnywhere, Category = "Glassware")
	int32 CurrentNumGlasses;

	UPROPERTY(ReplicatedUsing=OnRep_GlassStackInfo, VisibleAnywhere)
	TArray<FGlassInfo> GlassStackInfo;

	float GlassWidth = 25.f;
	float GlassHeight = 27.f;
	float GlassMeshScale = 1.75f;
	UStaticMesh* CurrentGlassMesh;

	TArray<FVector> GlassPositions;

public:
	/** Removes the glass from the top of the stack */
	AODDrinkGlass* RemoveGlass(bool bShouldSpawnGlass);

	UFUNCTION(NetMulticast, Reliable)
	void RemoveTopGlassMesh();

	void AddGlass(EGlassType GType, bool bIsDirty);

	UFUNCTION(NetMulticast, Reliable)
	void UpdateNewGlassMesh(FGlassInfo GlassInfo);

	UFUNCTION()
	void OnRep_GlassStackInfo();

	int32 GetNumGlasses();
};
