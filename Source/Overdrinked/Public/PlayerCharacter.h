// Fill out your copyright notice in the Description page of Project Settings.
#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "PlayerCharacter.generated.h"

class UBoxComponent;
class UCameraComponent;
class USpringArmComponent;
class UWidgetComponent;
class UNiagaraComponent;
class UAnimMontage;
class AODPlayerController;
class AODBevContainer;
class AODGarnish;
class AODCounter;
class AODIngredientDispenser;
class AODWasteBin;
class AODGlassRack;
class AODAICharacter;
class AODAICrawler;
class AODCrawlerManager;
class IPickupActor;
class IODInteractInterface;
class UAkAudioEvent;
class UAkComponent;
class UODPlayerIDWidget;
class AODSkelPickupActor;

UCLASS()
class OVERDRINKED_API APlayerCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	APlayerCharacter(const FObjectInitializer& OI);

	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

public:
	UPROPERTY(EditAnywhere)
	UBoxComponent* PickupCollider;

	UPROPERTY(EditAnywhere)
	UBoxComponent* CounterCollider;

	UPROPERTY(EditAnywhere)
	USpringArmComponent* CameraSpringArm;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UCameraComponent* PlayerCamera;

	UPROPERTY(EditAnywhere)
	UWidgetComponent* PlayerIdentifierWidgetComp;

	UODPlayerIDWidget* PlayerIDWidget;

	UPROPERTY(EditAnywhere)
	UWidgetComponent* ContainerWidgetComponent;

	UPROPERTY(EditAnywhere)
	UNiagaraComponent* TrailComponent;

	UPROPERTY(EditAnywhere)
	UAkComponent* AkComponent;

protected:
	virtual void BeginPlay() override;


	FString GetRoleEnumText(ENetRole role);

public:	
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UFUNCTION(NetMulticast, Reliable)
	void SetupPlayerIDWidget();

protected:
	AODPlayerController* ODPlayerController;

	UPROPERTY(EditAnywhere, Category="Movement")
	float DashAcceleration;

	UPROPERTY(EditAnywhere, Category = "Movement")
	float DashSpeed;

	UPROPERTY(EditAnywhere, Category = "Movement")
	float DashTime;

	UPROPERTY(EditAnywhere, Category = "Movement")
	float DashCooldownTime;

	UPROPERTY(EditAnywhere, Category = "Movement")
	float DashMultiplier;

	UPROPERTY(EditAnywhere, Category = "Animation")
	UAnimMontage* AttackMontage;

	UPROPERTY(EditAnywhere, Category = "Animation")
	UAnimMontage* WashMontage;

	UPROPERTY(Replicated)
	bool bCanDash = true;

	FTimerHandle DashCooldownTimerHandle;

	////////////////////////
	/// SOUND PROPERTIES ///
	////////////////////////
	UPROPERTY(EditAnywhere, Category = "Wwise")
	UAkAudioEvent* ShakeSound;

	UPROPERTY(EditAnywhere, Category = "Wwise")
	float ShakeSpeed;

	UPROPERTY(EditAnywhere, Category = "Wwise")
	UAkAudioEvent* PourDrinkSound;

	UPROPERTY(EditAnywhere, Category = "Wwise")
	UAkAudioEvent* PickupSound;

	UPROPERTY(EditAnywhere, Category = "Wwise")
	UAkAudioEvent* DropSound;
    
	UPROPERTY(EditAnywhere, Category = "Wwise")
	UAkAudioEvent* FillMeterSound;

    UPROPERTY(EditAnywhere, Category = "Wwise")
    UAkAudioEvent* ShowOrdersSound;
    
    UPROPERTY(EditAnywhere, Category = "Wwise")
    UAkAudioEvent* HideOrdersSound;

	UPROPERTY(EditAnywhere, Category = "Wwise")
	UAkAudioEvent* ErrorSound;
    
    UPROPERTY(EditAnywhere, Category = "Wwise")
    UAkAudioEvent* WasteSound;
    
    UPROPERTY(EditAnywhere, Category = "Wwise")
    UAkAudioEvent* WashSound;
    
    UPROPERTY(EditAnywhere, Category = "Wwise")
    UAkAudioEvent* WashSoundStop;

	UPROPERTY(EditAnywhere, Category = "Wwise")
	UAkAudioEvent* AddGarnishSound;

	UPROPERTY(EditAnywhere, Category = "Wwise")
	UAkAudioEvent* AddGarnishEmptyGlassSound;
    
	////////////////////

	UPROPERTY(EditAnywhere, Category = "Niagara Effect Parameters|Player Trail")
	float IdleSpawnRate = 20.f;

	UPROPERTY(EditAnywhere, Category = "Niagara Effect Parameters|Player Trail")
	float IdleScale = 1.f;

	UPROPERTY(EditAnywhere, Category = "Niagara Effect Parameters|Player Trail")
	float MovingSpawnRate = 30.f;

	UPROPERTY(EditAnywhere, Category = "Niagara Effect Parameters|Player Trail")
	float MovingScale = 1.f;

	UPROPERTY(EditAnywhere, Category = "Niagara Effect Parameters|Player Trail")
	float DashSpawnRate = 30.f;

	UPROPERTY(EditAnywhere, Category = "Niagara Effect Parameters|Player Trail")
	float DashScale = 1.5f;

	/** How long the niagara effect is scaled up for dashing (in seconds) */
	UPROPERTY(EditAnywhere, Category = "Niagara Effect Parameters|Player Trail")
	float DashEffectTime = 0.75f;

	UPROPERTY(VisibleAnywhere)
	TArray<AODAICharacter*> AICharactersInRange;

	UPROPERTY()
	TArray<AActor*> ObjectsInRange;

	UPROPERTY()
	AActor* ClosestInteractActor;

	AActor* ClosestPickupActor;

	UPROPERTY(VisibleAnywhere)
	TArray<AODBevContainer*> InteractableBevContainersInRange;

	AODBevContainer* ClosestInteractableContainer;

	UPROPERTY(VisibleAnywhere)
	TArray<AActor*> InteractActorsInRange;

	UPROPERTY(ReplicatedUsing=OnRep_ObjectInHand)
	AActor* ObjectInHand;

	UPROPERTY(EditAnywhere, Category = "Pickup")
	TSubclassOf<AODSkelPickupActor> CrawlerPickupBP;

	AODCrawlerManager* CrawlerManager;

	/** Value from -1 to 1 indicating angle of shake */
	UPROPERTY(Replicated)
	float ShakeAxis;

	/** Be sure to set this, used for animation! */
	UPROPERTY(Replicated, VisibleAnywhere)
	bool bIsHoldingObject = false;

	UPROPERTY(Replicated)
	bool bIsShaking = false;

	UPROPERTY(Replicated)
	bool bIsWashing = false;

	UPROPERTY(Replicated)
	bool bIsPouring = false;

	UPROPERTY(Replicated)
	bool bIsInRangeOfGlassRack = false;

	bool bIsPlayingShakeSound = false;

	UPROPERTY(Replicated)
	bool bIsDashing = false;

	uint32 ShakeSoundID;

	UPROPERTY(VisibleAnywhere) // TODO: Remove this as a UPROPERTY
	TArray<AODCounter*> CountersInRange;

	UPROPERTY(VisibleAnywhere) // TODO: Remove this as a UPROPERTY
	AODCounter* ClosestCounter;

	// CAMERA POSITIONING
	FVector CameraDefaultRelativeLocation;
	FVector CameraLockPosition;

	UPROPERTY(VisibleAnywhere)
	bool bIsInBarArea = false;

	/************************/
	// Drink system section //
	/************************/
	UPROPERTY(VisibleAnywhere)
	bool bIsInRangeOfDispenser = false;

	/** The dispenser closest to the player */
	AODIngredientDispenser* DispenserInRange;
	AODIngredientDispenser* DispenserBeingUsed;

	AODWasteBin* WasteBinInRange;

	AODGlassRack* GlassRackInRange;

	AODAICrawler* CrawlerInRange;

protected:
	UFUNCTION()
	void CapsuleColliderBeginOverlap(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
	void CapsuleColliderEndOverlap(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	UFUNCTION()
	void PickupColliderBeginOverlap(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
	void PickupColliderEndOverlap(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	UFUNCTION()
	void CounterColliderBeginOverlap(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
	void CounterColliderEndOverlap(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	void AddInteractActorInRange(AActor* InteractActor);

	void RemoveInteractActorInRange(AActor* InteractActor);

	void UpdateClosestInteractActor();

	void UpdateClosestPickupObject();

	void UpdateClosestInteractableContainer();

	void UpdateClosestCounter();

	UFUNCTION()
	void OnRep_ObjectInHand();

	void RefreshDash();

	UFUNCTION(BlueprintImplementableEvent)
	void HandleCrawlerInHand(bool bIsPickedUp);

public:
	/** Forward and backward movement */
	void MoveForward(float val);

	/** Side to side movement */
	void MoveRight(float val);

	/** Quickly move forward */
	UFUNCTION(Server, Reliable)
	void Server_StartDash();

	void EndDash();

	void SetClientDashSpeed();

	UFUNCTION(Client, Reliable)
	void Client_SetWalkSpeed();

	UFUNCTION(BlueprintCallable)
	bool HasObjectInRange();

	bool HasCounterInRange();

	AODCounter* GetClosestCounter();

	UFUNCTION(BlueprintCallable)
	bool CanCharacterDash();

	UFUNCTION(NetMulticast, Reliable)
	void NetPlayDashAnimation(int32 NumHandsInUse);

	void FinishDash();

	UFUNCTION(BlueprintImplementableEvent)
	void PlayDashAnimation(int32 NumHandsInUse);
	
	UFUNCTION(Server, Reliable)
	void Server_Interact();

	void Interact();

	UFUNCTION()
	void DashForward();

	UFUNCTION(Server, Reliable)
	void Server_DashForward();

	UFUNCTION(Server, Reliable)
	void Server_InteractSpecificObject(AActor* SpecificObject);

	void HandleObjectPickup(AActor* PickupObject);

	/** Try and pickup an object. Currently prioritizes closest object. Returns true if successful */
	UFUNCTION(Server, Reliable)
	void Server_PickupObjectInRange();

	/** Used by some objects to force the player to hold something */
	UFUNCTION(Server, Reliable)
	void Server_PickupSpecificObject(AODPickupActor* ActorToPickup, bool bShouldPlaySound);

	UFUNCTION(Server, Reliable)
	void Server_PickupSpecificGarnish(AODGarnish* GarnishToPickup);

	UFUNCTION(Server, Reliable)
	void Server_PickupCrawler(AODAICrawler* CrawlerToPickup);

	int32 GetCustomDepthStencilValue();

	/** Drop an object from the player */
	UFUNCTION(Server, Reliable)
	void Server_DropObject(bool bWillBeAttaching, bool bShouldPlaySound);

	/** Drop the object onto a stack */
	UFUNCTION(Server, Reliable)
	void Server_DropObjectAndDestroy();

	UFUNCTION(Client, Reliable)
	void Client_FinishDrop(bool bWillBeAttaching, AActor* ObjectBeingDropped, bool bShouldPlaySound);

	UFUNCTION(Server, Reliable)
	void Server_GetGlassFromRack();
	
	/** Attempt to pour ingredients into the currently held container from the ingredient dispenser 
	* @param ingredientIndex Which ingredient to pull from the dispenser, index maps to control scheme
	*/
	UFUNCTION(Server, Reliable)
	void Server_GetIngredientsFromDispenser(AODIngredientDispenser* DispenserToUse);

	UFUNCTION(Server, Reliable)
	void Server_StopDispensingIngredients();

	UFUNCTION(Server, Reliable)
	void Server_AddGarnishToContainer(AODGarnish* GarnishToAdd, AODDrinkGlass* ToGlass);

	/** Is this player holding something? */
	UFUNCTION(BlueprintCallable)
	bool IsHoldingObject();
	
	UFUNCTION(BlueprintCallable)
	bool IsHoldingBevContainer();

	/** Are we holding a drink mixer? */
	UFUNCTION(BlueprintCallable)
	bool IsHoldingDrinkMixer();

	UFUNCTION(BlueprintCallable)
	bool IsHoldingIce();

	UFUNCTION(BlueprintCallable)
	bool IsHoldingBlender();

	/** Can the thing we're holding receive ingredients from a dispenser? */
	bool IsHoldingPourableContainer();

	bool IsHoldingDrinkGlass();

	bool IsWashing();

	bool IsHoldingTaser();

	bool IsHoldingCrawler();

	bool GetIsInRangeOfDispenser();

	bool HasInteractableContainerInRange();

	bool IsHoldingGarnish();

	UFUNCTION(BlueprintCallable)
	bool IsHoldingAggressiveObject();

	bool HasDrinkMixerInRange();

	/** Get a reference to the dispenser the player is in range of. Returns NULL if there is no dispenser in range OR the player is not holding something to fill */
	AODIngredientDispenser* GetDispenserInRange();

	/** Get a reference to the dispenser the player is in range of. Returns NULL if there is no waste bin in range */
	AODWasteBin* GetWasteBinInRange();

	AODGlassRack* GetGlassRackInRange();

	AActor* GetObjectInHand();

	AActor* GetClosestInteractObject();

	AODBevContainer* GetClosestInteractableBevContainer();

	AODAICharacter* GetClosestCustomerInRange();

	AODAICrawler* GetCrawlerInRange();

	bool IsInRangeOfCustomer();

	bool CanInteract();

	/** This should only get called if it passed all checks in the PlayerController */
	UFUNCTION(Server, Reliable)
	void Server_MixDrinkInHand();

	/** Set the shake axis based on the player input for animation */
	UFUNCTION(Server, Reliable)
	void Server_SetShakeAxis(float val);

	/** Get the shake axis for shake animation */
	UFUNCTION(BlueprintCallable)
	float GetShakeAxis();

	UFUNCTION(BlueprintCallable)
	bool GetIsShaking();

	UFUNCTION(Server, Reliable)
	void Server_SetIsShaking(bool val);

	UFUNCTION(BlueprintCallable)
	bool IsPouring();

	UFUNCTION(Server, Reliable)
	void Server_SetIsPouring(bool val);

	/** Pour container in hand into something else if we have one & and we have a valid place to pour */
	UFUNCTION(Server, Reliable)
	void Server_PourContentsInHand();

	UFUNCTION(Server, Reliable)
	void Server_PourIntoContainer(AODBevContainer* OtherContainer);

	UFUNCTION(Server, Reliable)
	void Server_DumpContainerInHand();

	UFUNCTION(Client, Reliable)
	void PlayPourSound();

	UFUNCTION(Client, Reliable)
	void PlayPickupSound();

	UFUNCTION(Client, Reliable)
	void PlayDropSound();
    
    void PlayShowOrdersSound();
    
    void PlayHideOrdersSound();

	UFUNCTION(Client, Reliable)
	void PlayErrorSound();
    
	UFUNCTION(Client, Reliable)
    void PlayWasteSound();
    
    void PlayWashSound();
    
    void PlayWashSoundStop();

	UFUNCTION(Client, Reliable)
	void PlayAddGarnishSound(bool bIsGlassEmpty);

	void PlayFillLevelSound(int32 FillLevel);

	void ToggleShakeSound(bool bShouldBePlaying);
	
	void UpdateShakeSpeed(float speed);

	UFUNCTION(Server, Reliable)
	void Server_UseTaser(AODAICharacter* CharacterToTase);

	void StartWashing();

	UFUNCTION(Server, Reliable)
	void Server_FinishWashing();

	UFUNCTION(Server, Reliable)
	void Server_PlayAttackAnim();

	UFUNCTION(NetMulticast, Reliable)
	void Multi_PlayAttackAnim();

	UFUNCTION(NetMulticast, Reliable)
	void Multi_PlayWashMontage();

	UFUNCTION(NetMulticast, Reliable)
	void Multi_FinishWashMontage();

	UFUNCTION(NetMulticast, Reliable)
	void TaserDelay(AODAICharacter* CharacterToTase);
};
