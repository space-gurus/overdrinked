// Copyright 2020 Space Gurus

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "IngredientsEnum.h"
#include "Gameplay/ODDrinkScore.h"
#include "Gameplay/ODDrinkOrder.h"
#include "ODHelperFunctions.generated.h"

class AODDrinkGlass;

/**
 * 
 */
UCLASS()
class OVERDRINKED_API UODHelperFunctions : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
	
public:
	UFUNCTION(BlueprintCallable, Category = "ODHelperFunctions")
	static FLinearColor BuildDrinkColor(TArray<EIngredientType> IngredientTypes, TArray<int32> IngredientAmounts, UDataTable* IngredientDataTable);

	UFUNCTION(BlueprintCallable, Category = "ODHelperFunctions")
	static EDrinkScore ScoreDrink(FDrinkOrder DrinkOrder, AODDrinkGlass* Drink);

	UFUNCTION(BlueprintCallable, Category = "ODHelperFunctions")
	static TArray<int32> ConvertFloatArrayToInt(TArray<float> InArray);

	/** Find the degree of the angle between Actor A's forward vector and Actor B's position */
	UFUNCTION(BlueprintCallable, Category = "ODHelperFunctions")
	static float AngleBetweenActors(AActor* ActorA, AActor* ActorB);

	UFUNCTION(BlueprintCallable, Category = "ODHelperFunctions|Color")
	static FLinearColor GetComplementaryColor(FLinearColor InColor);
};
