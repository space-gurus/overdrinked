// Copyright 2020 Space Gurus

#pragma once
UENUM(BlueprintType)
enum class EAIMood : uint8
{
	AIM_Neutral		UMETA(DisplayName = "Neutral"),
	AIM_Happy		UMETA(DisplayName = "Happy"),
	AIM_Sad			UMETA(DisplayName = "Sad"),
	AIM_Angry		UMETA(DisplayName = "Angry")
};