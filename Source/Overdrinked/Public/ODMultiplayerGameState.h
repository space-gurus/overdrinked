// Copyright 2020 Space Gurus

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameState.h"
#include "Gameplay/ODDrinkOrder.h"
#include "Gameplay/ODOrderStatus.h"
#include "ODMultiplayerGameState.generated.h"

class AODGlobalSoundActor;

/**
 * 
 */
UCLASS()
class OVERDRINKED_API AODMultiplayerGameState : public AGameState
{
	GENERATED_BODY()
	
public:
	AODMultiplayerGameState();

	virtual void BeginPlay() override;

	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	UFUNCTION(BlueprintCallable)
	int32 GetTimeRemainingInSeconds();

	UFUNCTION(BlueprintCallable)
	float GetFractionTimeRemaining();

	bool CanHandleNewOrder();

protected:
	AODGlobalSoundActor* GlobalSoundActor;

	/** How many points the team has accrued in this level */
	UPROPERTY(ReplicatedUsing=OnRep_TeamPoints, BlueprintReadWrite, Category = "Points")
	int32 TeamPoints = 0;

	UPROPERTY(Replicated, VisibleAnywhere, BlueprintReadOnly, Category = "Points")
	TArray<int32> StarThresholds;

	UPROPERTY(Replicated, VisibleAnywhere, BlueprintReadOnly, Category = "Points")
	int32 ThreeStarThreshold;

	UPROPERTY(Replicated, BlueprintReadWrite)
	int32 TimeRemainingInSeconds = 0;

	UPROPERTY(Replicated, BlueprintReadWrite)
	int32 TotalLevelTime = 0;

	FTimerHandle ServerLevelTimerHandle;

	/** Number of currently active orders */
	UPROPERTY(ReplicatedUsing=OnRep_NumActiveOrders, BlueprintReadWrite, Category = "Drink System")
	int32 NumActiveOrders = 0;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Replicated, Category = "Drink System")
	int32 MaxNumOrders = 6;

	/** List of active orders the players are working on */
	UPROPERTY(Replicated, BlueprintReadWrite, Category = "Drink System")
	TArray<FDrinkOrder> OpenOrders;

	/** List of completed orders the players have finished in this level */
	UPROPERTY(Replicated, BlueprintReadWrite, Category = "Drink System")
	TArray<FDrinkOrder> CompletedOrders;

	/** List of failed orders the players have finished in this level */
	UPROPERTY(Replicated, BlueprintReadWrite, Category = "Drink System")
	TArray<FDrinkOrder> FailedOrders;

	/** What proportion of the bar is brawling */
	UPROPERTY(Replicated, BlueprintReadOnly)
	float BrawlPercentage = 0.f;

	bool bHasGameStarted = false;

public:
	void SetGlobalSoundActor(AODGlobalSoundActor* SoundActor);

	UFUNCTION(Server, Reliable)
	void StartLevelTimer(int32 LevelLengthInMinutes);

	UFUNCTION()
	void OnRep_NumActiveOrders();

	UFUNCTION()
	void OnRep_TeamPoints();

	int32 GetTeamPoints();

	int32 GetNumActiveOrders();

	TArray<FDrinkOrder> GetOpenOrders();

	UFUNCTION(Server, Reliable)
	void Server_AddOpenOrder(FDrinkOrder DrinkOrder);

	UFUNCTION(Server, Reliable)
	void Server_RemoveOpenOrder(FDrinkOrder DrinkOrder, bool bWasDelivered, AODAICharacter* Customer);

	bool HasGameStarted();

	void GameStarted();

	void SetBrawlPercentage(float BrawlPerc);

protected:
	void DecreaseTimeRemaining();

	TPair<int32, EOrderStatus> CalculatePoints(FDrinkOrder DrinkOrder, AODAICharacter* Customer);

	/** If parameter is false, an order was removed */
	void UpdateAIManager(bool bOrderAdded);
};
