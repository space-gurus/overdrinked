// Copyright 2020 Space Gurus

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Actors/ODInteractInterface.h"
#include "ODWashStation.generated.h"

class UStaticMeshComponent;
class UBoxComponent;
class UWidgetComponent;
class UArrowComponent;
class UNiagaraSystem;
class UNiagaraComponent;
class UODGlassStackComponent;
class APlayerCharacter;
class AODGlassRack;
class UODWashProgressWidget;

UCLASS()
class OVERDRINKED_API AODWashStation : public AActor, public IODInteractInterface
{
	GENERATED_BODY()
	
public:	
	AODWashStation(const FObjectInitializer& OI);

	virtual void Tick(float DeltaTime) override;
	
protected:
	UPROPERTY(EditAnywhere)
	UStaticMeshComponent* StaticMesh;

	UPROPERTY(EditAnywhere)
	UBoxComponent* InteractCollider;

	UPROPERTY(EditAnywhere)
	UBoxComponent* SinkInteractCollider;

	UPROPERTY(EditAnywhere)
	UArrowComponent* ForwardArrow;

	UPROPERTY(EditAnywhere)
	UODGlassStackComponent* GlassStackComp;

	UPROPERTY(EditAnywhere)
	USceneComponent* WashEffectLocationComp;

	UPROPERTY(EditAnywhere)
	UWidgetComponent* WashProgressWidgetComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UWidgetComponent* HelperWidget;

	UODWashProgressWidget* WashProgressWidget;

	bool bIsPlayerOverlappingSink = false;

	UPROPERTY(EditAnywhere, Category = "Washing Gameplay")
	AODGlassRack* BrewGlassRack;

	UPROPERTY(EditAnywhere, Category = "Washing Gameplay")
	AODGlassRack* StemmedGlassRack;

	UPROPERTY(EditAnywhere, Category = "Washing Gameplay")
	AODGlassRack* ShotGlassRack;

	/** This is how much progress is made each wash cycle*/
	UPROPERTY(EditAnywhere, Category = "Washing Gameplay")
	float WashEfficiency = 0.01f;

	/** How often WashEfficiency is added to the progress */
	UPROPERTY(EditAnywhere, Category = "Washing Gameplay")
	float WashSpeed = 0.1f;

	UPROPERTY(ReplicatedUsing=OnRep_WashProgress, VisibleAnywhere, Category = "Washing Gameplay")
	float WashProgress = 0.f;

	UPROPERTY(EditAnywhere, Category = "Effects")
	UNiagaraSystem* WashEffectSystem;

	UNiagaraComponent* WashEffect;

	FTimerHandle WashTimerHandle;

	APlayerCharacter* PlayerWashing;

	UPROPERTY(ReplicatedUsing=OnRep_IsPlayerWashing)
	bool bIsPlayerWashing = false;

protected:
	virtual void BeginPlay() override;

	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	UFUNCTION()
	virtual void InteractColliderBeginOverlap(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
	virtual void InteractColliderEndOverlap(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	void FinishWashCycle();

	UFUNCTION()
	void OnRep_IsPlayerWashing();

	UFUNCTION()
	void OnRep_WashProgress();

	void UpdateWashWidget();

public:	
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Interact Interface")
	void PlayerInteraction(APlayerCharacter* Player);
	virtual void PlayerInteraction_Implementation(APlayerCharacter* Player) override;
};
