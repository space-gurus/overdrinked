// Copyright 2020 Space Gurus

#pragma once

#include "CoreMinimal.h"
#include "Environment/ODQTEStation.h"
#include "ODChopStation.generated.h"

class UNiagaraSystem;
class UNiagaraComponent;
class AODGarnish;
class AODDummyPickup;
class UAkAudioEvent;
class UAkComponent;
class UODSliderQTE;

/**
 * 
 */
UCLASS()
class OVERDRINKED_API AODChopStation : public AODQTEStation
{
	GENERATED_BODY()

public:
	AODChopStation(const FObjectInitializer& OI);

	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

protected:
	virtual void BeginPlay() override;

protected:
	UPROPERTY(EditAnywhere)
	USceneComponent* ChopEffectPosition;

	UODSliderQTE* SliderQTE;

	UPROPERTY(EditAnywhere, Category = "Effects")
	UNiagaraSystem* ChopEffect;

	UNiagaraComponent* ChopEffectComp;

	UPROPERTY(EditAnywhere, Category = "Gameplay")
	TSubclassOf<AODGarnish> CrawlerBP;

	UPROPERTY(EditAnywhere, Category = "Effects")
	TSubclassOf<AODDummyPickup> KnifeBP;

	AODDummyPickup* KnifeProp;
    
    UPROPERTY(EditAnywhere, Category = "Wwise")
    UAkAudioEvent* ChopSound;
    
    UPROPERTY(EditAnywhere, Category = "Wwise")
    UAkAudioEvent* SquishSound;

	/* This is used for keeping track of which sound to play */
	int32 LastSucessCount = 0;

	UPROPERTY(EditAnywhere, Category = "Gameplay", Meta = (MakeEditWidget = true))
	FVector CrawlerSpawnSpot;

public:
	virtual void PlayerInteraction_Implementation(APlayerCharacter* Player) override;

	virtual void StartQTE() override;

	virtual void InteractWithQTE() override;

	virtual void CancelQTE() override;

	virtual void HandleQTESuccess() override;

	virtual void HandleQTEFinish(bool bSuccess) override;

protected:
	UFUNCTION(BlueprintImplementableEvent)
	void HandleKnife();

	UFUNCTION(NetMulticast, Reliable)
	void StartChopEffect();

	void GivePlayerCrawler();

	UFUNCTION(NetMulticast, Reliable)
	void FinishChopEffect();

	void FinishEffects_Implementation() override;
	
    void PlayChopSound();
    
    void PlaySquishSound();
};
