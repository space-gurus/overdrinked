// Copyright 2020 Space Gurus

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Actors/ODInteractInterface.h"
#include "Gameplay/ODGlassType.h"
#include "ODGlassRack.generated.h"

class UBoxComponent;
class UODGlassStackComponent;
class AODDrinkGlass;

UCLASS()
class OVERDRINKED_API AODGlassRack : public AActor, public IODInteractInterface
{
	GENERATED_BODY()
	
public:	
	AODGlassRack();

protected:
	UPROPERTY(EditAnywhere)
	USceneComponent* DummyRoot;

	UPROPERTY(EditAnywhere)
	UODGlassStackComponent* GlassStackComp;

protected:
	virtual void BeginPlay() override;

public:
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Interact Interface")
	void PlayerInteraction(APlayerCharacter* Player);
	virtual void PlayerInteraction_Implementation(APlayerCharacter* Player) override;

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Interact Interface")
	void BecomeRelevant(APlayerCharacter* PlayerTriggered);
	virtual void BecomeRelevant_Implementation(APlayerCharacter* PlayerTriggered) override;

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Interact Interface")
	void LoseRelevance();
	virtual void LoseRelevance_Implementation() override;

	AODDrinkGlass* GetTopGlass();

	void AddGlass(EGlassType GlassType);
};
