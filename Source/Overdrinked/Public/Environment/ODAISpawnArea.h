// Copyright 2020 Space Gurus

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ODAISpawnArea.generated.h"

class UBoxComponent;

UCLASS()
class OVERDRINKED_API AODAISpawnArea : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AODAISpawnArea();

protected:
	UPROPERTY(EditAnywhere)
	UBoxComponent* SpawnArea;

public:
	bool IsPointInSpawnArea(FVector PointToCheck);

	FVector GetRandomSpawnPoint();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
};
