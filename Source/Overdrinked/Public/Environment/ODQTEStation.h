// Copyright 2020 Space Gurus

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Actors/ODInteractInterface.h"
#include "ODQTEStation.generated.h"

class UStaticMeshComponent;
class UBoxComponent;
class UWidgetComponent;
class APlayerCharacter;
class UODQTEWidget;

/* 
* 
*/
UCLASS()
class OVERDRINKED_API AODQTEStation : public AActor, public IODInteractInterface
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AODQTEStation(const FObjectInitializer& OI);

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

protected:
	UPROPERTY(EditAnywhere)
	UStaticMeshComponent* StaticMesh;

	UPROPERTY(EditAnywhere)
	UBoxComponent* InteractCollider;

	UPROPERTY(EditAnywhere)
	UWidgetComponent* QTEComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UWidgetComponent* HelperWidget;

	/* The widget the player is interacting with */
	UODQTEWidget* QTEWidget;

	UPROPERTY(Replicated)
	bool bIsAvailable = true;

	UPROPERTY(Replicated)
	APlayerCharacter* PlayerInteracting;

	UPROPERTY(EditAnywhere)
	int32 DefaultCustomDepth = 253;

	UPROPERTY(EditAnywhere)
	int32 HighlightCustomDepth = 241;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

protected:
	UFUNCTION(Client, Reliable)
	void Highlight();

	UFUNCTION(Client, Reliable)
	void EndHighlight();

public:
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Interact Interface")
	void PlayerInteraction(APlayerCharacter* Player);
	virtual void PlayerInteraction_Implementation(APlayerCharacter* Player) override;

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Interact Interface")
	void BecomeRelevant(APlayerCharacter* PlayerTriggered);
	virtual void BecomeRelevant_Implementation(APlayerCharacter* PlayerTriggered) override;

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Interact Interface")
	void LoseRelevance();
	virtual void LoseRelevance_Implementation() override;
	
	virtual void StartQTE();

	virtual void CancelQTE();

	virtual void InteractWithQTE();

	virtual void HandleQTESuccess();

	virtual void HandleQTEFinish(bool bSuccess);

	UFUNCTION(NetMulticast, Reliable)
	virtual void FinishEffects();
};
