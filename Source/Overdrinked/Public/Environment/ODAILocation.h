// Copyright 2020 Space Gurus

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ODAILocation.generated.h"

class UBoxComponent;
class UArrowComponent;
class UWidgetComponent;
class AODAIController;
class AODAIHub;

UCLASS()
class OVERDRINKED_API AODAILocation : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AODAILocation(const FObjectInitializer& OI);

	UFUNCTION()
	void BoxColliderBeginOverlap(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
	void BoxColliderEndOverlap(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	void StartWithCharacterInBounds();

	/** This function exists because GetOverlappingActors returns 0 in BeginPlay still */
	void CheckForInitialOverlap();

	AODAIHub* GetConversationHub();

protected:
	UPROPERTY(EditAnywhere)
	UBoxComponent* BoxCollider;

	UPROPERTY(EditAnywhere)
	UArrowComponent* FaceDirectionComponent;

	UPROPERTY(EditAnywhere)
	AODAIHub* ConversationHub;

	/** Is there someone already standing here? */
	bool bIsOccupied;

	bool bIsAvailableForMove = true;

	int32 NumCharsInSpace = 0;

	UPROPERTY(EditAnywhere, Category = "AI Debug")
	bool bDrawDebugWidgets = false;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	bool IsLocationFree();

	void SetIsOccupied(bool val);

	bool IsAvailableForMove();

	void SetIsAvailableForMove(bool val);
};
