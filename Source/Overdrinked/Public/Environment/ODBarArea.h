// Copyright 2020 Space Gurus

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ODBarArea.generated.h"

class UBoxComponent;

/**
 * This actor is used mainly to specify where the camera should center
 */
UCLASS()
class OVERDRINKED_API AODBarArea : public AActor
{
	GENERATED_BODY()
	
public:
	UPROPERTY(EditAnywhere)
	UBoxComponent* BarAreaContainer;

public:	
	AODBarArea();

protected:
	virtual void BeginPlay() override;

};
