// Copyright 2020 Space Gurus

#pragma once

#include "CoreMinimal.h"
#include "Actors/ODPickupActor.h"
#include "ODDummyPickup.generated.h"

/**
 * 
 */
UCLASS()
class OVERDRINKED_API AODDummyPickup : public AODPickupActor
{
	GENERATED_BODY()

public:
	AODDummyPickup(const FObjectInitializer& OI);

};