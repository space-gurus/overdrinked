// Copyright 2020 Space Gurus

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Actors/ODInteractInterface.h"
#include "ODBlenderBase.generated.h"

class APlayerCharacter;
class AODBlender;
class UODProgressWidget;
class UODFlashingWidget;
class USkeletalMeshComponent;
class UWidgetComponent;
class UBoxComponent;
class UNiagaraComponent;
class UAkAudioEvent;

UCLASS()
class OVERDRINKED_API AODBlenderBase : public AActor, public IODInteractInterface
{
	GENERATED_BODY()
	
public:	
	AODBlenderBase(const FObjectInitializer& OI);

	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

protected:
	UPROPERTY(EditAnywhere)
	USkeletalMeshComponent* SkeletalMesh;

	UPROPERTY(EditAnywhere)
	UBoxComponent* InteractCollider;

	UPROPERTY(EditAnywhere)
	UWidgetComponent* ProgressWidgetComponent;

	UODProgressWidget* BlendProgressWidget;

	UPROPERTY(EditAnywhere)
	UWidgetComponent* WarningWidgetComponent;

	UODFlashingWidget* WarningWidget;

	UPROPERTY(EditAnywhere)
	UNiagaraComponent* BlendEffectComponent;

	UPROPERTY(EditAnywhere)
	TSubclassOf<AODBlender> BlenderBP;

	UPROPERTY(EditAnywhere, Category = "Wwise")
	UAkAudioEvent* BlendSound;

	uint32 BlendSoundID;

	UPROPERTY(EditAnywhere, Category = "Wwise")
	UAkAudioEvent* BlendFinishSound;

	UPROPERTY(Replicated)
	bool bIsBlending = false;

	/** How far along we've blended the drink */
	UPROPERTY(VisibleAnywhere, Replicated)
	float BlendProgress = 0.f;

	/** How fast a full blend should take (in seconds) */
	UPROPERTY(EditAnywhere)
	float BlendTime = 2.f;

	/** How long in seconds until this blender overflows after finishing a blend */
	UPROPERTY(EditAnywhere)
	float OverflowTime = 5.f;

	UPROPERTY(VisibleAnywhere, Replicated)
	float OverflowProgress = 0.f;

	UPROPERTY(ReplicatedUsing=OnRep_IsOverflowing)
	bool bIsOverflowing = false;
	
	UPROPERTY(Replicated)
	AODBlender* AttachedBlender;

	int32 DefaultCustomDepth = 255;
	int32 HighlightCustomDepth = 240;

protected:
	virtual void BeginPlay() override;

public:	
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Interact Interface")
	void PlayerInteraction(APlayerCharacter* Player);
	virtual void PlayerInteraction_Implementation(APlayerCharacter* Player) override;

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Interact Interface")
	void BecomeRelevant(APlayerCharacter* PlayerTriggered);
	virtual void BecomeRelevant_Implementation(APlayerCharacter* PlayerTriggered) override;

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Interact Interface")
	void LoseRelevance();
	virtual void LoseRelevance_Implementation() override;

	void ShowHighlight();

	void EndHighlight();

	UFUNCTION(NetMulticast, Reliable)
	void Multi_StartBlend();

	UFUNCTION(NetMulticast, Reliable)
	void Multi_StopBlend(bool bWasBlending);

	UFUNCTION(BlueprintCallable)
	bool IsBlending();

	UFUNCTION()
	void OnRep_IsOverflowing();

	UFUNCTION(BlueprintCallable)
	void SetBlendSpeed(float val);

	UFUNCTION(BlueprintCallable)
	float GetBlendTime();

	UFUNCTION(BlueprintCallable)
	void SetBlenderOverflowTime(float val);

	UFUNCTION(BlueprintCallable)
	float GetBlenderOverflowTime();

protected:
	void HandleOverflowFinish();
};
