// Copyright 2020 Space Gurus

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ODGridUnit.generated.h"

class UBoxComponent;

/**
 * This actor acts as a piece of a grid for the object placement system
 */
UCLASS()
class OVERDRINKED_API AODGridUnit : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AODGridUnit();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

protected:
	UPROPERTY(EditAnywhere)
	UBoxComponent* UnitBox;

};
