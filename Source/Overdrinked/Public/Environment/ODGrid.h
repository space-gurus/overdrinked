// Copyright 2020 Space Gurus

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ODGrid.generated.h"

class UBoxComponent;

UCLASS()
class OVERDRINKED_API AODGrid : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AODGrid();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable, CallInEditor, Category="Grid Builder")
	void RebuildGrid();

	UFUNCTION(BlueprintCallable, CallInEditor, Category = "Grid Builder")
	void ToggleVisibility();

protected:
	UPROPERTY(EditAnywhere)
	UBoxComponent* testBox;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Grid Builder")
	int32 gridWidth;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Grid Builder")
	int32 gridHeight;

	TArray<TArray<UBoxComponent*>> GridBoxes;

};
