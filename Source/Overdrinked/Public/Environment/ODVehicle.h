// Copyright 2020 Space Gurus

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ODVehicle.generated.h"

class USplineComponent;
class UAkAudioEvent;
class AODVehicleSpawner;

UCLASS()
class OVERDRINKED_API AODVehicle : public AActor
{
	GENERATED_BODY()
	
public:	
	AODVehicle(const FObjectInitializer& OI);

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	void FollowPath(USplineComponent* PathToFollow, float TimeToEnd, FVector DopplerPos);

protected:
	USplineComponent* Path;

	UPROPERTY(ReplicatedUsing=OnRep_ShouldMove)
	bool bShouldMove = false;

	UPROPERTY(Replicated, EditAnywhere)
	float MoveSpeed = 1.f;

	float MoveProgress = 0.f;

	FVector DopplerPosition;

	FTimerHandle DopplerTimerHandle;

	UPROPERTY(EditAnywhere, Category = "Wwise")
	TArray<UAkAudioEvent*> AmbientCarNoises;

protected:
	virtual void BeginPlay() override;

	UFUNCTION()
	void OnRep_ShouldMove();

	void FinishPath();

	void UpdateDopplerEffect();

public:	
	virtual void Tick(float DeltaTime) override;

};
