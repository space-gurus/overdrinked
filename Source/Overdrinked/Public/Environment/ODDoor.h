// Copyright 2020 Space Gurus

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ODDoor.generated.h"

class USkeletalMeshComponent;
class UBoxComponent;
class UArrowComponent;
class UAkAudioEvent;

UCLASS()
class OVERDRINKED_API AODDoor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AODDoor(const FObjectInitializer& OI);

	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	UFUNCTION()
	void RangeColliderBeginOverlap(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
	void RangeColliderEndOverlap(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

protected:
	UPROPERTY(EditAnywhere)
	USkeletalMeshComponent* SkeletalMesh;

	UPROPERTY(EditAnywhere)
	UBoxComponent* RangeCollider;

	/// SOUND PROPERTIES
	UPROPERTY(EditAnywhere, Category = "Wwise")
	UAkAudioEvent* CustomerEnterSound;
    
    UPROPERTY(EditAnywhere, Category = "Wwise")
    UAkAudioEvent* CustomerExitSound;

	int32 NumCustomersInRange = 0;

	UPROPERTY(EditAnywhere)
	float DoorSpeed = 2.f;

	UPROPERTY(EditAnywhere)
	bool bIsEntrance = true;

	UPROPERTY(ReplicatedUsing=OnRep_IsOpen)
	bool bIsOpen = false;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION(BlueprintCallable)
	bool IsOpen();

	UFUNCTION()
	void OnRep_IsOpen();
};
