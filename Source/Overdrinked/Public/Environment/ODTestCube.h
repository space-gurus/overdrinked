// Copyright 2020 Space Gurus

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ODTestCube.generated.h"

UCLASS()
class OVERDRINKED_API AODTestCube : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AODTestCube();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
