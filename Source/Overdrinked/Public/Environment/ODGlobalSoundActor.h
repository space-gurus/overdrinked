// Copyright 2020 Space Gurus

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ODDataTypes.h"
#include "ODGlobalSoundActor.generated.h"

class UAkAudioEvent;
class UDataTable;

UCLASS()
class OVERDRINKED_API AODGlobalSoundActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AODGlobalSoundActor();

	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

protected:
	UPROPERTY(EditAnywhere, Category = "Wwise|Music")
	UAkAudioEvent* LevelMusic;

	int32 LevelMusicID;

	UPROPERTY(EditAnywhere, Category = "Wwise|Sound Effects")
	UAkAudioEvent* PositiveDelivery;

	int32 PositiveDeliveryID;

	UPROPERTY(EditAnywhere, Category = "Wwise|Sound Effects")
	UAkAudioEvent* TakeOrder;

	UPROPERTY(EditAnywhere, Category = "WWise|Voice Lines")
	UDataTable* LevelStartLineData;

	UPROPERTY(EditAnywhere, Category = "WWise|Voice Lines")
	UDataTable* PositiveFeedbackLineData;

	UPROPERTY(EditAnywhere, Category = "WWise|Voice Lines")
	UDataTable* NegativeFeedbackLineData;

	int32 TakeOrderID;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION(BlueprintCallable)
	void PlayLevelMusic();

public:
	UFUNCTION(NetMulticast, Reliable, BlueprintCallable)
	void PlayDrinkDeliveryNoise(EFeedbackType FeedbackLevel);

	UFUNCTION(NetMulticast, Reliable, BlueprintCallable)
	void PlayTakeOrder();

	UFUNCTION(NetMulticast, Reliable, BlueprintCallable)
	void PlayDeliveryVoiceLine(EFeedbackType FeedbackLevel);
};
