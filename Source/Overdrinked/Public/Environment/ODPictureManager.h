// Copyright 2020 Space Gurus

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ODPictureManager.generated.h"

class USkeletalMeshComponent;
class USceneCaptureComponent2D;
class UODOrderCardWidget;
class AODAICharacter;

UCLASS()
class OVERDRINKED_API AODPictureManager : public AActor
{
	GENERATED_BODY()
	
public:
	AODPictureManager(const FObjectInitializer& OI);

protected:
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	USkeletalMeshComponent* GetSkeletalMeshComponent();

	UFUNCTION(BlueprintCallable)
	void CapturePicture(UODOrderCardWidget* OrderWidget, AODAICharacter* CharacterToCapture);

protected:
	UPROPERTY(EditAnywhere)
	USkeletalMeshComponent* SkeletalMesh;

	UPROPERTY(EditAnywhere)
	USceneCaptureComponent2D* FaceCameraComponent;

	UMaterial* FaceCaptureMaterial;

	UODOrderCardWidget* ActiveOrderCard;
	
	bool bIsTakingFaceCapture = false;
};
