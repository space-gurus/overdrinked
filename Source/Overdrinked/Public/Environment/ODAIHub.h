// Copyright 2020 Space Gurus

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ODAIHub.generated.h"

class AODAILocation;

UCLASS()
class OVERDRINKED_API AODAIHub : public AActor
{
	GENERATED_BODY()
	
public:	
	AODAIHub();

	void RegisterLocation(AODAILocation* HubLocation);

	bool CanHoldConversation();

protected:
	virtual void BeginPlay() override;

protected:
	UPROPERTY(VisibleAnywhere)
	TArray<AODAILocation*> HubLocations;
};
