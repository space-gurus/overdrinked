// Copyright 2020 Space Gurus

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Actors/ODInteractInterface.h"
#include "ODWasteBin.generated.h"

class UStaticMeshComponent;

UCLASS()
class OVERDRINKED_API AODWasteBin : public AActor, public IODInteractInterface
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AODWasteBin();

	UFUNCTION()
	virtual void MeshColliderBeginOverlap(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
	virtual void MeshColliderEndOverlap(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Interact Interface")
	void PlayerInteraction(APlayerCharacter* Player);
	virtual void PlayerInteraction_Implementation(APlayerCharacter* Player) override;

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Interact Interface")
	void BecomeRelevant(APlayerCharacter* PlayerTriggered);
	virtual void BecomeRelevant_Implementation(APlayerCharacter* PlayerTriggered) override;

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Interact Interface")
	void LoseRelevance();
	virtual void LoseRelevance_Implementation() override;

	void ShowHighlight();

	void HideHighlight();

	bool IsHighlighted();

protected:
	UPROPERTY(EditAnywhere)
	UStaticMeshComponent* StaticMesh;

	bool bIsHighlighted = false;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
};
