// Copyright 2020 Space Gurus

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ODVehicleSpawner.generated.h"

class USplineComponent;
class AODVehicle;

UCLASS()
class OVERDRINKED_API AODVehicleSpawner : public AActor
{
	GENERATED_BODY()
	
public:	
	AODVehicleSpawner(const FObjectInitializer& OI);

protected:
	UPROPERTY(EditAnywhere)
	USceneComponent* DummyRoot;
	
	UPROPERTY(EditAnywhere)
	USplineComponent* VehiclePath;

	UPROPERTY(EditAnywhere, Meta = (MakeEditWidget = true))
	FVector EndPoint;

	UPROPERTY(EditAnywhere)
	float MinWaitTimeToSpawn = 3.f;

	UPROPERTY(EditAnywhere)
	float MaxWaitTimeToSpawn = 12.f;

	UPROPERTY(EditAnywhere)
	float VehicleMoveSpeed = 5.f;

	UPROPERTY(EditAnywhere)
	TSubclassOf<AODVehicle> CarBP;

	FTimerHandle SpawnTimerHandle;

	UPROPERTY(EditAnywhere, Meta = (MakeEditWidget = true))
	FVector DopplerPoint;

protected:
	virtual void BeginPlay() override;

	void SpawnVehicle();
};
