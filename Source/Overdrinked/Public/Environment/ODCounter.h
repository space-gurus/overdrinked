// Copyright 2020 Space Gurus

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Actors/ODInteractInterface.h"
#include "ODCounter.generated.h"

class UBoxComponent;
class UArrowComponent;
class APlayerCharacter;

UCLASS()
class OVERDRINKED_API AODCounter : public AActor, public IODInteractInterface
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AODCounter(const FObjectInitializer& OI);

protected:
	UPROPERTY(EditAnywhere)
	UStaticMeshComponent* StaticMesh;

	UPROPERTY(EditAnywhere)
	UBoxComponent* AreaCollider;

	UPROPERTY(EditAnywhere)
	UArrowComponent* ForwardArrow;

	UPROPERTY(EditAnywhere)
	USceneComponent* DrinkAttachComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Materials")
	UMaterialInterface* RegularMaterial;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Materials")
	UMaterialInterface* PlayerHighlightMaterial;

	UPROPERTY(VisibleAnywhere)
	bool bCanHoldItem = true;

	UPROPERTY(VisibleAnywhere)
	bool bIsCounterFull = false;

	bool bIsHighlighted = false;

	/** The attached object that the player will interact with when interacting with this counter */
	UPROPERTY(EditAnywhere)
	AActor* AttachedObject;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
	void AreaColliderBeginOverlap(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
	void AreaColliderEndOverlap(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Interact Interface")
	void PlayerInteraction(APlayerCharacter* Player);
	virtual void PlayerInteraction_Implementation(APlayerCharacter* Player) override;

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Interact Interface")
	void BecomeRelevant(APlayerCharacter* PlayerTriggered);
	virtual void BecomeRelevant_Implementation(APlayerCharacter* PlayerTriggered) override;

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Interact Interface")
	void LoseRelevance();
	virtual void LoseRelevance_Implementation() override;

	/** Highlight this object to the player to tell them they are targeting this counter */
	void Highlight();

	/** End the highlight effect (if the player is out of range */
	void EndHighlight();

	USceneComponent* GetDrinkAttachComponent();

	FVector GetDrinkAttachWorldLocation();

	bool CanHoldItem();

	bool GetIsCounterFull();

	void SetIsCounterFull(bool val);

	bool IsHighlighted();

	FVector GetCounterForwardVector();

	AActor* GetAttachedActor();

	void AttachObject(AActor* ObjectToAttach);

	AActor* RemoveAttachedObject();

	bool HasAttachedObject();
};
