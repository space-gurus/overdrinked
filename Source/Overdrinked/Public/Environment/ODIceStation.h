// Copyright 2020 Space Gurus

#pragma once

#include "CoreMinimal.h"
#include "Environment/ODQTEStation.h"
#include "Actors/ODInteractInterface.h"
#include "ODIceStation.generated.h"

class UNiagaraComponent;
class AODGarnish;
class UODSliderQTE;
class UAkAudioEvent;
class UAkComponent;

UCLASS()
class OVERDRINKED_API AODIceStation : public AODQTEStation
{
	GENERATED_BODY()

public:	
	AODIceStation(const FObjectInitializer& OI);

	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

protected:
	UPROPERTY(EditAnywhere)
	UNiagaraComponent* IceVentEffect;

	UPROPERTY(EditAnywhere, Category = "Gameplay")
	TSubclassOf<AODGarnish> IceBP;
    
    UPROPERTY(EditAnywhere, Category = "Wwise")
    UAkAudioEvent* IceHitSound;
    
    UPROPERTY(EditAnywhere, Category = "Wwise")
    UAkAudioEvent* IceReceiveSound;

protected:
	virtual void BeginPlay() override;

public:
	virtual void PlayerInteraction_Implementation(APlayerCharacter* Player) override;

	virtual void InteractWithQTE() override;
	
	virtual void HandleQTESuccess() override;

	void GivePlayerIce();
    
    void PlayIceHitSound();
    
    void PlayIceReceiveSound();
};
