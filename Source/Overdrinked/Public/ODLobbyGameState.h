// Copyright 2020 Space Gurus

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameState.h"
#include "ODLobbyGameState.generated.h"

class AODMenuPlayerController;
class AODLobbyManager;

/**
 * 
 */
UCLASS()
class OVERDRINKED_API AODLobbyGameState : public AGameState
{
	GENERATED_BODY()

public:
	AODLobbyGameState();

	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	virtual void BeginPlay() override;

	void SetNumPlayers(int32 NumOfPlayers);

	UFUNCTION(BlueprintCallable)
	int32 GetNumPlayers();

	void SetNumReadyPlayers();

	UFUNCTION(BlueprintCallable)
	int32 GetNumReadyPlayers();

	bool AreAllPlayersReady();

	UFUNCTION(BlueprintCallable)
	void SetChosenMapName(FName MapName);

	UFUNCTION(Server, Reliable, BlueprintCallable)
	void Server_SetPlayerReadyStatus(APlayerController* PlayerController);

	UFUNCTION(Server, Reliable)
	void Server_AddPlayer(AODMenuPlayerController* MenuPlayerController, int32 PlayerIndex);

	UFUNCTION(Server, Reliable)
	void Server_RemovePlayer(int32 PlayerIndex);

	void SetLobbyManagerReference(AODLobbyManager* NewManager);

	UFUNCTION(BlueprintCallable)
	FName GetChosenMapName();

	UFUNCTION(BlueprintCallable)
	bool GetPlayerReadyStatus(int32 PlayerIndex);

protected:
	UPROPERTY(ReplicatedUsing=OnRep_NumPlayers)
	int32 NumPlayers = 0;

	UPROPERTY(ReplicatedUsing=OnRep_NumReadyPlayers)
	int32 NumReadyPlayers = 0;

	TMap<AODMenuPlayerController*, int32> PlayerIndexMap;

	UPROPERTY(Replicated)
	TArray<bool> ReadyStatuses;

	AODLobbyManager* LobbyManager;

	UPROPERTY(ReplicatedUsing=OnRep_ChosenMapName)
	FName ChosenMapName;

protected:
	UFUNCTION()
	void OnRep_NumPlayers();

	UFUNCTION()
	void OnRep_NumReadyPlayers();

	UFUNCTION()
	void OnRep_ChosenMapName();
};
