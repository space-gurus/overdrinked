// Copyright 2020 Space Gurus

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameMode.h"
#include "ODLobbyGameMode.generated.h"

class AODMenuPlayerController;

/**
 * 
 */
UCLASS()
class OVERDRINKED_API AODLobbyGameMode : public AGameMode
{
	GENERATED_BODY()
	
public:
	AODLobbyGameMode();

	virtual void PostLogin(APlayerController* NewPlayer) override;
	virtual void Logout(AController* Exiting) override;

	AODMenuPlayerController* GetHostPlayerController();

	void NotifyPlayersOfMapTravel();

protected:
	TArray<AODMenuPlayerController*> PlayerControllerList;

	uint32 NumberOfPlayers = 0;
};
