// Copyright 2020 Space Gurus

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "OnlineSubsystem.h"
#include "Interfaces/OnlineSessionInterface.h"
#include "Interfaces/OnlineFriendsInterface.h"
#include "ISwitcherooModule.h"
#include "SwitcherooTypes.h"
#include "ODGameInstance.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnSearchBeginDelegate);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnSearchSucceedDelegate);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnSearchFailDelegate);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnInviteAcceptedCompleteDelegate);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnFriendsUpdateComplete);

USTRUCT(Blueprintable)
struct FServerData
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(BlueprintReadOnly)
	FName Name;

	uint16 CurrentPlayers;
	uint16 MaxPlayers;
	FString HostUsername;
};

class FOnlineSessionSearch;
class UODMenuWidget;
class UODMainMenuWidget;

/**
 * 
 */
UCLASS()
class OVERDRINKED_API UODGameInstance : public UGameInstance
{
	GENERATED_BODY()
	
public:
	UODGameInstance(const FObjectInitializer& ObjectInitializer);

	virtual void Init();

	void OnDetectedInputDeviceChanged(ESwitcherooInputDevice ActiveDevice);

	UFUNCTION(Exec)
	void ToggleRoleCards();

	bool GetDrawRoleCards();

	UFUNCTION(Exec, BlueprintCallable)
	void Host();

	/** Try to join this lobby name. Returns true if successful */
	UFUNCTION(Exec, BlueprintCallable)
	bool Join(int32 LobbyIndex);

	UFUNCTION(BlueprintCallable)
	FString GetTrueLobbyname();

	UFUNCTION(BlueprintCallable)
	FServerData GetLobbyData();

	UFUNCTION(Exec)
	void PrintCurrentSessionState();

	/** Does what it says! */
	FName GenerateRandomLobbyName();

	void OnCreateSessionComplete(FName SessionName, bool Success);
	void OnDestroySessionComplete(FName SessionName, bool Success);
	void OnEndSessionComplete(FName InSessionName, bool bWasSuccessful);
	void OnFindSessionsComplete(bool Success);
	void OnJoinSessionComplete(FName SessionName, EOnJoinSessionCompleteResult::Type Result);
	void OnReadFriendsListComplete(int32 LocalUserNum, bool bWasSuccessful, const FString& ListName, const FString& ErrorStr);
	void OnSessionUserInviteAccepted(const bool bWasSuccess, const int32 ControllerId, TSharedPtr<const FUniqueNetId> UserId, const FOnlineSessionSearchResult& InviteResult);

	void HandleNetworkFailure(UWorld* World, UNetDriver* NetDriver, ENetworkFailure::Type FailureType, const FString& ErrorString);

	void HandleNetworkConnectionStatusChanged(const FString& ServiceName, EOnlineServerConnectionStatus::Type LastConnectionStatus, EOnlineServerConnectionStatus::Type ConnectionStatus);

	void HandleSessionFailure(const FUniqueNetId& NetId, ESessionFailure::Type FailureType);

	UFUNCTION(BlueprintCallable)
	void TryJoinLobby(FString LobbyToSearchFor);

	void CreateSession();

	UFUNCTION(BlueprintCallable)
	void CloseExistingSession();

	UFUNCTION(BlueprintCallable)
	void ClientCloseSession();

	UFUNCTION(BlueprintCallable)
	void HostCloseSession();

	void SessionCleanup();

	void ReturnToMainMenu() override;

	void KickPlayer(int32 PlayerToKick);

	UFUNCTION(BlueprintCallable)
	void RefreshFriendsList();

	UFUNCTION(BlueprintCallable)
	void InviteFriend(FString FriendName);

	void SetPartySize(int32 PartySize);

	int32 GetPartySize();

	bool HasVisitedMainMenu();

	bool IsInSession();

	bool IsHost();

	UFUNCTION(BlueprintImplementableEvent)
	void HandlePauseMenu(APlayerCharacter* PlayerCharacter, bool bSetGamePaused);

	UFUNCTION(BlueprintImplementableEvent)
	void HandleLevelEnd();

	UFUNCTION(BlueprintImplementableEvent)
	void HandleLevelFail();

	UFUNCTION(BlueprintCallable)
	TArray<FString> GetFriendNames();

	UFUNCTION(BlueprintCallable)
	void HostReloadLevel();

	UFUNCTION(BlueprintCallable)
	void HostMovePartyToLobby();

protected:
	// Debug variables
	bool bDrawRoleCards = false;

	/// MATCHMAKING VARS ///
	IOnlineSessionPtr SessionInterface;
	TSharedPtr<FOnlineSessionSearch> SessionSearch;

	int32 FoundLobbyIndex = -1;
	
	FServerData LobbyData;
	int32 LobbyNameLength = 5;

	/** Used for searching */
	FName DesiredLobbyName;

	/** Only used once you've created a lobby or found one */
	FName TrueLobbyName;

	/// FRIENDS INTERFACE VARS ///
	IOnlineFriendsPtr FriendsInterface;
	
 	FOnReadFriendsListComplete OnReadFriendsListCompleteDelegate;
	FOnSessionUserInviteAcceptedDelegate OnSessionUserInviteAcceptedDelegate;
	FDelegateHandle OnSessionUserInviteAcceptedDelegateHandle;

	TArray<TSharedRef<FOnlineFriend>> FriendsList;
	TArray<FString> FriendNames;

	///  ONLINE IDENTITY INTERFACE VARS ///
	IOnlineIdentityPtr OnlineIdentityInterface;

	UPROPERTY(BlueprintReadOnly)
	bool bIsHost = false;

	/** This is used for designating how many players are needed to start a map */
	int32 PartyMemberCount = 1;

	UPROPERTY(BlueprintReadWrite)
	bool bHasVisitedMainMenu = false;

	UPROPERTY(BlueprintReadOnly)
	bool bIsInSession = false;

	UPROPERTY(BlueprintReadWrite)
	bool bWasDisconnected = false;

	UPROPERTY(BlueprintAssignable)
	FOnSearchBeginDelegate SearchBeginDelegate;
	
	UPROPERTY(BlueprintAssignable)
	FOnSearchFailDelegate SearchFailDelegate;

	UPROPERTY(BlueprintAssignable)
	FOnSearchSucceedDelegate SearchSucceedDelegate;

	UPROPERTY(BlueprintAssignable)
	FOnFriendsUpdateComplete FriendsUpdateDelegate;

	UPROPERTY(BlueprintAssignable)
	FOnInviteAcceptedCompleteDelegate InviteAcceptedCompleteDelegate;
};
