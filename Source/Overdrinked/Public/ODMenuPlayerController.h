// Copyright 2020 Space Gurus

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "ISwitcherooModule.h"
#include "SwitcherooTypes.h"
#include "ODMenuPlayerController.generated.h"

class UODUserWidget;
class UODMainMenuWidget;
class UODLobbyMenuWidget;
class AODLobbyPlayer;
class UODWidgetPage;
class UODMenuWidget;
class UODSwitcherMenu;
class UDataTable;
class UAkAudioEvent;

/**
 * 
 */
UCLASS()
class OVERDRINKED_API AODMenuPlayerController : public APlayerController
{
	GENERATED_BODY()
	
public:
	AODMenuPlayerController();

	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	virtual void BeginPlay() override;

	void OnDetectedInputDeviceChanged(ESwitcherooInputDevice ActiveDevice);

	// Menu Functions
	UFUNCTION(BlueprintCallable)
	void LoadMainMenu();

	UFUNCTION(BlueprintCallable)
	void LoadLobbyMenu();

	UFUNCTION(BlueprintCallable)
	void TransitionToLobbyMenu();

	UFUNCTION(BlueprintCallable)
	void TransitionFromLobbyToMainMenu();

	UFUNCTION(NetMulticast, Reliable)
	void PrepareForMapTravel();

	UFUNCTION(Server, Reliable, BlueprintCallable)
	void Server_TravelToSelectedMap();

	/** Handle if the server suddenly disconnected */
	UFUNCTION(Client, Reliable)
	void Client_HandleHostDisconnect();

	UODSwitcherMenu* GetMainMenuWidget();

	UODLobbyMenuWidget* GetLobbyMenuWidget();

	UFUNCTION(BlueprintCallable)
	void SetActivePageWidget(UODWidgetPage* NewPage, bool bTriggerShowPage = true);

	UFUNCTION(BlueprintCallable)
	bool IsPlayerReady();

	UFUNCTION(Server, Reliable, BlueprintCallable)
	void Server_ToggleIsPlayerReady();

	void SetLobbyAvatarReference(AODLobbyPlayer* LobbyPlayer);

	AODLobbyPlayer* GetLobbyAvatar();

	FName GetPlayerName();

	UFUNCTION(Client, Reliable)
	void SetLobbyChosenMapName(FName MapName);

	void UpdateLobbyFriendsList(TArray<FString> PlayerNames);

	UFUNCTION(Client, Reliable)
	void HandleNewLobbyPlayer(int32 PlayerIndex, FName PlayerName, int32 PlayerLevel);

	UFUNCTION(Client, Reliable)
	void HandleLobbyPlayerLeave(int32 PlayerIdx);

	UFUNCTION(Client, Reliable)
	void HandlePlayerReadyStatusUpdate(int32 PlayerIndex, bool bIsReady);

	UODWidgetPage* GetActiveWidgetPage();

	UFUNCTION(NetMulticast, Reliable, BlueprintCallable)
	void SetPlayerIndex(int32 Index);

	UFUNCTION(BlueprintCallable)
	int32 GetPlayerIndex();

	UFUNCTION(Client, Reliable)
	void HandleCloseLobby();

protected:
	// Called to bind functionality to input
	void SetupInputComponent() override;

	void HandleMenuUp();
	void HandleMenuDown();
	void HandleTabLeft();
	void HandleTabRight();
	void HandleMenuBack();

	UFUNCTION()
	void OnRep_IsPlayerReady();

	void StopMenuMusic();

protected:
	UDataTable* LevelDataTable;

	// Menu variables
	TSubclassOf<UODUserWidget> LobbyMenuBPClass;
	UODLobbyMenuWidget* LobbyMenuWidget;

	TSubclassOf<UODSwitcherMenu> MainMenuBPClass;
	UODSwitcherMenu* MainMenuWidget;

	TSubclassOf<UODMenuWidget> StartPageBPClass;
	UODMenuWidget* StartPage;

	UODWidgetPage* ActiveParentPage;

	bool bIsNavigatingWithController = false;

	// Player related variables
	UPROPERTY(ReplicatedUsing=OnRep_IsPlayerReady)
	bool bIsPlayerReady = false;

	// Network index of the player (-1 until set)
	int32 MPPlayerIndex = -1;

	// Lobby stuff
	/** Lobby player associated with this player controller */
	AODLobbyPlayer* LobbyAvatar;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UAkAudioEvent* StopMenuMusicEvent;
};
