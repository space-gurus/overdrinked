// Copyright 2020 Space Gurus

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "ODPlayerIDWidget.generated.h"

/**
 * 
 */
UCLASS()
class OVERDRINKED_API UODPlayerIDWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:
	UFUNCTION(BlueprintImplementableEvent)
	void SetPlayer(const FString& PlayerName, int32 PlayerID);

};
