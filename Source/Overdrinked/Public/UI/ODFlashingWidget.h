// Copyright 2020 Space Gurus

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "ODFlashingWidget.generated.h"

/**
 * 
 */
UCLASS()
class OVERDRINKED_API UODFlashingWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:
	UFUNCTION(BlueprintImplementableEvent)
	void SetFlashRate(float FlashRate);

	UFUNCTION(BlueprintImplementableEvent)
	void ToggleFlash(bool ShouldPlay);
};
