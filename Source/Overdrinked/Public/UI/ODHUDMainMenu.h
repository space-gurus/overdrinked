// Copyright 2020 Space Gurus

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "ODHUDMainMenu.generated.h"

class UODMainMenu;

/**
 * 
 */
UCLASS()
class OVERDRINKED_API AODHUDMainMenu : public AHUD
{
	GENERATED_BODY()
	
protected:
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	UODMainMenu* MainMenu;

public:
	UFUNCTION(BlueprintCallable)
	UODMainMenu* GetMainMenu();
};
