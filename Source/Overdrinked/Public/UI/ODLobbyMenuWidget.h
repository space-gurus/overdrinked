// Copyright 2020 Space Gurus

#pragma once

#include "CoreMinimal.h"
#include "UI/ODMenuWidget.h"
#include "ODLobbyMenuWidget.generated.h"

/**
 * 
 */
UCLASS()
class OVERDRINKED_API UODLobbyMenuWidget : public UODMenuWidget
{
	GENERATED_BODY()
	
public:
	UFUNCTION(BlueprintImplementableEvent)
	void UpdateReadyStatus();

	UFUNCTION(BlueprintImplementableEvent)
	void UpdateChosenMapName(FName MapName);

	UFUNCTION(BlueprintImplementableEvent)
	void UpdateFriendsList(const TArray<FString>& PlayerNames);
};
