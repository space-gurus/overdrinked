// Copyright 2020 Space Gurus

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Gameplay/ODGlassType.h"
#include "IngredientsEnum.h"
#include "ODMixerWidget.generated.h"

class UMaterialInstanceDynamic;
class UImage;
class UDataTable;
class AODIngredient;
class AODBevContainer;
class AODDrink;

/**
 * 
 */
UCLASS()
class OVERDRINKED_API UODMixerWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:
	UODMixerWidget(const FObjectInitializer& ObjectInitializer);

	void InitialSetup(AODBevContainer* DrinkMixer);

	void AdjustFill();

	void ClearIngredients();

	UFUNCTION(BlueprintImplementableEvent)
	void PrintVectorParameter(UMaterialInstanceDynamic* MaterialInstance, FName ParamName);

	UFUNCTION(BlueprintImplementableEvent)
	void SetDrinkString(FName DrinkName);

	UFUNCTION(BlueprintImplementableEvent)
	void SetGlassOverlayImage(EGlassType GlassType);

	void DisplayFinishedDrink();

protected:
	void ResetMixerMaterial();

protected:
	UPROPERTY(meta = (BindWidget))
	UImage* MixerImage;

	UMaterialInterface* MixerMaterialInterface;
	UMaterialInstanceDynamic* DynamicMixerMaterial;

	uint32 CurrentMaterialIndex;

	TMap<EIngredientType, uint32> IngredientToMaterialIndex;

	UPROPERTY(BlueprintReadOnly)
	AODBevContainer* DrinkContainerReference;

	AODDrink* CurrentDrinkReference;

	float MixerMaxVolume;

	// Constants for referencing material params
	FString BaseIngredientString = "Ingredient";
	FString ColorString = "Color";
	FString LevelString = "Level";

	UPROPERTY(BlueprintReadOnly)
	UDataTable* DrinkDataTable;

	UDataTable* IngredientDataTable;

	void SetMaterialShape(UMaterialInstanceDynamic* MaterialToSet);
};
