// Copyright 2020 Space Gurus

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "ODOnlineJoinPage.generated.h"

/**
 * 
 */
UCLASS()
class OVERDRINKED_API UODOnlineJoinPage : public UUserWidget
{
	GENERATED_BODY()
	
public:
	UFUNCTION(BlueprintImplementableEvent)
	void SearchBegin();

	UFUNCTION(BlueprintImplementableEvent)
	void SearchEnd(bool bSuccess);
};
