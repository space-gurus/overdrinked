// Copyright 2020 Space Gurus

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "ODMainMenu.generated.h"

class UODLobbyPage;

/**
 * 
 */
UCLASS()
class OVERDRINKED_API UODMainMenu : public UUserWidget
{
	GENERATED_BODY()
	
protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UODLobbyPage* HostLobbyPage;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UODLobbyPage* ClientLobbyPage;

public:
	UFUNCTION(BlueprintCallable)
	UODLobbyPage* GetHostLobby();

	UFUNCTION(BlueprintCallable)
	UODLobbyPage* GetClientLobby();

	UFUNCTION(BlueprintCallable)
	UODLobbyPage* GetActiveLobbyPage();

	UFUNCTION(BlueprintImplementableEvent)
	void PrepareForMapTravel();
};
