// Copyright 2020 Space Gurus

#pragma once

#include "CoreMinimal.h"
#include "UI/ODQTEWidget.h"
#include "ODSmashQTE.generated.h"

class UMaterialInstanceDynamic;

/**
 * 
 */
UCLASS()
class OVERDRINKED_API UODSmashQTE : public UODQTEWidget
{
	GENERATED_BODY()
	
public:
	virtual void NativeTick(const FGeometry& MyGeometry, float InDeltaTime) override;

	virtual void InteractWithQTE() override;

	virtual void DeactivateQTE() override;

	UFUNCTION(BlueprintImplementableEvent)
	void SetProgress(float ProgressPercent);

protected:
	void PlayerHit();

protected:
	/** How fast to decrase the bar towards 0 */
	UPROPERTY(EditAnywhere)
	float ProgressDecreaseSpeed = 2.f;

	/** How much to fill the bar each time the player hits the QTE */
	UPROPERTY(EditAnywhere)
	float PlayerHitProgress = 0.1f;
	
	float Progress = 0.f;

	bool bIsProgressBarFull = false;
};
