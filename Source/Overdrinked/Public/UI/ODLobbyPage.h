// Copyright 2020 Space Gurus

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "ODLobbyPage.generated.h"

/**
 * 
 */
UCLASS()
class OVERDRINKED_API UODLobbyPage : public UUserWidget
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintImplementableEvent)
	void SetNewPlayer(int32 PlayerIndex, FName PlayerName, int32 PlayerLevel);

	UFUNCTION(BlueprintImplementableEvent)
	void SetPlayerReadyStatus(int32 PlayerIndex, bool bReadyStatus);

	UFUNCTION(BlueprintImplementableEvent)
	void RemovePlayer(int32 PlayerIndex);
	
	UFUNCTION(BlueprintImplementableEvent)
	void UpdateChosenMapName(FName MapName);

	UFUNCTION(BlueprintImplementableEvent)
	void HandleCloseLobby();
};
