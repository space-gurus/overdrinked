// Copyright 2020 Space Gurus

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "ODProgressWidget.generated.h"

class UProgressBar;

/**
 * 
 */
UCLASS()
class OVERDRINKED_API UODProgressWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:
	UFUNCTION(BlueprintCallable)
	void SetProgress(float val);

protected:
	UPROPERTY(meta=(BindWidget))
	UProgressBar* Progress;
};
