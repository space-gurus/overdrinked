// Copyright 2020 Space Gurus

#pragma once

#include "CoreMinimal.h"
#include "UI/ODMenuWidget.h"
#include "ODMainMenuWidget.generated.h"

class UWidgetSwitcher;
class UWidget;
class UButton;
class UODMixerWidget;

/**
 * 
 */
UCLASS()
class OVERDRINKED_API UODMainMenuWidget : public UODMenuWidget
{
	GENERATED_BODY()
	
public:
	UFUNCTION(BlueprintCallable)
	void LoadMainMenu();

	UFUNCTION(BlueprintCallable)
	void LoadHostMenu();

	UFUNCTION(BlueprintCallable)
	void LoadJoinMenu();

	UFUNCTION(BlueprintImplementableEvent)
	void SetSearchingTextVisibility(bool value);

	UFUNCTION(BlueprintImplementableEvent)
	void ShowLobbyNotFound();

	void UpdateVisibleButtons();

	void StartNavigatingWithController();

	void MoveMenuSelectionUp();

	void MoveMenuSelectionDown();

	void HighlightCurrentButtonForControllerSelection();
	
protected:
	UPROPERTY(meta = (BindWidget))
	UWidgetSwitcher* MainMenuSwitcher;

	UPROPERTY(meta = (BindWidget))
	UWidget* MainMenuWidget;

	UPROPERTY(meta = (BindWidget))
	UWidget* HostMenuWidget;

	UPROPERTY(meta = (BindWidget))
	UWidget* JoinMenuWidget;

	UPROPERTY(BlueprintReadWrite)
	TArray<UButton*> VisibleButtons;

	int32 ButtonHighlightIndex = -1;
};
