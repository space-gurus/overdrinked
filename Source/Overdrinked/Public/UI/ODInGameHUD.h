// Copyright 2020 Space Gurus

#pragma once

#include "CoreMinimal.h"
#include "UI/ODUIWidget.h"
#include "Gameplay/ODDrinkOrder.h"
#include "ODInGameHUD.generated.h"

class UCanvasPanel;
class UODMixerWidget;
class UODPourHUD;

/**
 * 
 */
UCLASS()
class OVERDRINKED_API UODInGameHUD : public UODUIWidget
{
	GENERATED_BODY()

public:
	UODInGameHUD();

	UFUNCTION(BlueprintImplementableEvent)
	void FadeIn();

	UFUNCTION(BlueprintImplementableEvent)
	void StartRoundCountdown();

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void HandleLevelEnd();

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void HandleLeaveLevel();

	UFUNCTION(BlueprintImplementableEvent)
	void EnableViewOrders();

	UFUNCTION(BlueprintImplementableEvent)
	void DisableViewOrders();

	UFUNCTION(BlueprintImplementableEvent)
	void AddOrder(FDrinkOrder DrinkOrder);

	UFUNCTION(BlueprintImplementableEvent)
	void RemoveOrder(FDrinkOrder DrinkOrder);

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void SetTips(int32 TipAmount);

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void SetBrawlIndicatorLevel(float BrawlLevel);

	UFUNCTION(BlueprintCallable)
	void AddMixerWidgetToViewport(AODDrinkMixer* DrinkMixer);

	UFUNCTION(BlueprintCallable)
	void RemoveMixerWidgetFromViewport();

	UODMixerWidget* CreateMixerWidget();

	UODPourHUD* GetPourHUD();

	UPROPERTY(BlueprintReadWrite)
	bool bIsOrderPanelActive = false;

protected:

	TSubclassOf<UUserWidget> MixerWidgetBPClass;
	UODMixerWidget* MixerWidget;
};
