// Copyright 2020 Space Gurus

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "ODLobbyPlayerWidget.generated.h"

/**
 * 
 */
UCLASS()
class OVERDRINKED_API UODLobbyPlayerWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:
	UFUNCTION(BlueprintImplementableEvent)
	void SetPlayerName(FName PlayerName);

	UFUNCTION(BlueprintImplementableEvent)
	void SetReadyStatus(bool ReadyStatus);
};
