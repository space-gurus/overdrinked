// Copyright 2020 Space Gurus

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Gameplay/ODGarnishType.h"
#include "IngredientsEnum.h"
#include "ODResourceHexWidget.generated.h"

/**
 * 
 */
UCLASS()
class OVERDRINKED_API UODResourceHexWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:
	UFUNCTION(BlueprintImplementableEvent)
	void SetResourceAsGarnish(EGarnishType NewGarnishType);

	UFUNCTION(BlueprintImplementableEvent)
	void SetResourceAsIngredient(EIngredientType NewIngredientType);

	UFUNCTION(BlueprintImplementableEvent)
	void SetResourceAmount(int32 Amount);

	UFUNCTION(BlueprintCallable)
	EGarnishType GetGarnishType();

	UFUNCTION(BlueprintCallable)
	EIngredientType GetIngredientType();

	UFUNCTION(BlueprintCallable)
	bool IsBeingUsed();

	UFUNCTION(BlueprintCallable)
	void SetIsBeingUsed(bool val);

	void HideAndReset();

protected:
	UPROPERTY(BlueprintReadWrite)
	EGarnishType GarnishType = EGarnishType::GT_None;

	UPROPERTY(BlueprintReadWrite)
	EIngredientType IngredientType = EIngredientType::IT_None;

	UPROPERTY(BlueprintReadWrite)
	bool bIsBeingUsed = false;
};
