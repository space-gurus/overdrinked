// Copyright 2020 Space Gurus

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "ODPourHUDSimple.generated.h"

class UImage;

/**
 * 
 */
UCLASS()
class OVERDRINKED_API UODPourHUDSimple : public UUserWidget
{
	GENERATED_BODY()
	
protected:
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	UImage* IngredientOne;
	
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	UImage* IngredientTwo;
	
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	UImage* IngredientThree;
	
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	UImage* IngredientFour;

	TArray<UImage*> IngredientImages;

	UPROPERTY(BlueprintReadWrite)
	bool bIsWidgetOpen = false;

public:
	UFUNCTION(BlueprintImplementableEvent)
	void OpenWidget();

	UFUNCTION(BlueprintImplementableEvent)
	void CloseWidget();

	void InitialSetup(TArray<FColor> IngredientColors);

	bool IsWidgetOpen();
};
