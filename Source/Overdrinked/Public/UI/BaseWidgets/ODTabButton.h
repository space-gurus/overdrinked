// Copyright 2020 Space Gurus

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "ODTabButton.generated.h"

/**
 * 
 */
UCLASS()
class OVERDRINKED_API UODTabButton : public UUserWidget
{
	GENERATED_BODY()
	
public:
	UFUNCTION(BlueprintCallable)
	void SetTabActiveStatus(bool bActive);

	UFUNCTION(BlueprintCallable)
	bool IsStartingTab();

	int32 GetSwitcherIndex();

protected:
	/** Switcher index this tab attached to */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 SwitcherIndex = 0;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool bIsStartingTab = false;

	UPROPERTY(BlueprintReadWrite)
	bool bIsActive = false;

	UPROPERTY(BlueprintReadWrite)
	bool bIsHovered = false;
};
