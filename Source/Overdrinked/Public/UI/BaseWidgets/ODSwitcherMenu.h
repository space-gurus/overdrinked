// Copyright 2020 Space Gurus

#pragma once

#include "CoreMinimal.h"
#include "UI/ODMenuWidget.h"
#include "ODSwitcherMenu.generated.h"

class UWidgetSwitcher;
class UODWidgetPage;
class UODTabButton;

/**
 * 
 */
UCLASS()
class OVERDRINKED_API UODSwitcherMenu : public UODMenuWidget
{
	GENERATED_BODY()

public:
	void NativeOnInitialized() override;

	UFUNCTION(BlueprintCallable)
	UODWidgetPage* GetActiveWidgetPage();

	bool IsTabMenu();

	void ChangeTabDirectional(bool bIsRight);

	UFUNCTION(BlueprintCallable)
	void ChangeTab(int32 TabIndex);

protected:
	UPROPERTY(meta = (BindWidget), BlueprintReadOnly)
	UWidgetSwitcher* MenuSwitcher;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool bIsTabMenu;

	TArray<UODTabButton*> SwitcherTabs;

	UODTabButton* ActiveTabButton;

	int32 ActiveTabIndex = 0;

protected:
	void ClearInactiveTabButtons();
};
