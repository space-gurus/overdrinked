// Copyright 2020 Space Gurus

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "ODButton.generated.h"

class UButton;
class UODWidgetPage;

/**
 * 
 */
UCLASS()
class OVERDRINKED_API UODButton : public UUserWidget
{
	GENERATED_BODY()
	
public:
	virtual void NativePreConstruct() override;

	UFUNCTION(BlueprintCallable)
	void SetParentPageReference(UODWidgetPage* NewParent);

	UFUNCTION(BlueprintCallable)
	void SetActiveFocus();

	UFUNCTION(BlueprintCallable)
	void LoseActiveFocus();

	bool IsStartingFocus();

protected:
	UPROPERTY(meta=(BindWidget))
	UButton* MenuButton;

	UPROPERTY(BlueprintReadWrite)
	UODWidgetPage* ParentPage;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool bIsSelected = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool bIsHovered = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool bIsFocused = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool bIsStartingFocus = false;
};
