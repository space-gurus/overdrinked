// Copyright 2020 Space Gurus

#pragma once

#include "CoreMinimal.h"
#include "UI/ODMenuWidget.h"
#include "SwitcherooTypes.h"
#include "ODWidgetPage.generated.h"

class UCanvasPanel;
class UODButton;
class UODSwitcherMenu;

/**
 * 
 */
UCLASS()
class OVERDRINKED_API UODWidgetPage : public UODMenuWidget
{
	GENERATED_BODY()

public:
	virtual void NativeOnInitialized() override;

	UFUNCTION(BlueprintCallable)
	void BecomeActive(bool bTriggerShowPage);

	UFUNCTION(BlueprintImplementableEvent)
	void DelayActivate();

	UFUNCTION(BlueprintImplementableEvent)
	void HandleBack();

	UFUNCTION(BlueprintImplementableEvent)
	void ShowPage();

	void MoveSelectionForward();

	void MoveSelectionBackward();

	void HandleDeviceChanged(ESwitcherooInputDevice ActiveDevice);

	UFUNCTION(BlueprintCallable)
	void SetFocusedButton(UODButton* NewFocus);

	UFUNCTION(BlueprintCallable)
	bool HasNestedSwitcherMenu();

	UFUNCTION(BlueprintCallable)
	UODSwitcherMenu* GetNestedSwitcherMenu();

	/** Use this if this page is nested inside of a SwitcherMenu */
	UFUNCTION(BlueprintImplementableEvent)
	void HandleTabSwitch(bool bIsArriving);

	UFUNCTION(BlueprintImplementableEvent)
	void HandlePageSwitch(bool bIsArriving);

protected:
	void ClearUnfocusedButtons();

	void ClearAllFocus();
	
protected:
	UPROPERTY(meta= (BindWidget))
	UCanvasPanel* MainPanel;

	UPROPERTY(BlueprintReadWrite)
	TArray<UODButton*> ButtonList;
	
	UPROPERTY(BlueprintReadWrite)
	UODButton* StartingFocusButton;

	UPROPERTY(BlueprintReadWrite)
	int32 FocusButtonIndex = 0;

	UPROPERTY(BlueprintReadWrite)
	UODSwitcherMenu* NestedSwitcherMenu;
};
