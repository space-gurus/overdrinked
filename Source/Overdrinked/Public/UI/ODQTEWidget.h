// Copyright 2020 Space Gurus

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "ODQTEWidget.generated.h"

class AODQTEStation;

/**
 * 
 */
UCLASS()
class OVERDRINKED_API UODQTEWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:
	virtual void NativeTick(const FGeometry& MyGeometry, float InDeltaTime) override;

	virtual void InteractWithQTE();

	virtual void ActivateQTE(AODQTEStation* NewQTEStation);

	virtual void DeactivateQTE();

protected:
	UPROPERTY(BlueprintReadWrite)
	bool bIsQTEActive = false;

	bool bPlayerSuccess = false;

	AODQTEStation* QTEStation;
};
