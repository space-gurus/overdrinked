// Copyright 2020 Space Gurus

#pragma once

#include "CoreMinimal.h"
#include "UI/ODQTEWidget.h"
#include "ODSliderQTE.generated.h"

class UProgressBar;
class AODQTEStation;

/**
 * 
 */
UCLASS()
class OVERDRINKED_API UODSliderQTE : public UODQTEWidget
{
	GENERATED_BODY()
	
public:
	virtual void NativeTick(const FGeometry& MyGeometry, float InDeltaTime) override;

	virtual void InteractWithQTE() override;

	UFUNCTION(BlueprintImplementableEvent)
	void SetProgress(float ProgressPercent);

protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float QTESpeed = 1.f;

	UPROPERTY(EditAnywhere)
	int32 SuccessesNeeded = 3;

	float Progress = 0.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float SuccessRangeStart = 0.75f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float SuccessRangeSize = 0.1f; // How much to add to the min for success range

	bool bIsProgressBarFinished = false;

	int32 PlayerSuccessCount = 0;

public:
	void ActivateQTE(AODQTEStation* NewQTEStation) override;

	virtual void DeactivateQTE() override;

	void TryPlayerHit();

	bool IsInSuccessRange();

	bool IsQTEActive();

	int32 GetPlayerSuccessCount();
};
