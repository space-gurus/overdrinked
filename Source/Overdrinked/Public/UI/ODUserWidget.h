// Copyright 2020 Space Gurus
// colton
#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "ODUserWidget.generated.h"

/**
 * 
 */
UCLASS()
class OVERDRINKED_API UODUserWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:
	/** Setup this widget, MUST BE OVERRIDDEN */
	UFUNCTION(BlueprintCallable)
	virtual void SetupWidget();

	/** Tear down this widget, MUST BE OVERRIDDEN */
	UFUNCTION(BlueprintCallable)
	virtual void TearDownWidget();

	bool IsWidgetActive();

protected:
	UPROPERTY(BlueprintReadWrite)
	bool bIsWidgetActive = false;
};
