// Copyright 2020 Space Gurus

#pragma once

#include "CoreMinimal.h"
#include "UI/ODUserWidget.h"
#include "ODMenuWidget.generated.h"

/**
 * 
 */
UCLASS()
class OVERDRINKED_API UODMenuWidget : public UODUserWidget
{
	GENERATED_BODY()
	
public:
	virtual void SetupWidget() override;

	virtual void TearDownWidget() override;
};
