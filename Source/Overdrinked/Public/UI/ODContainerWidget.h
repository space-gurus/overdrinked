// Copyright 2020 Space Gurus

#pragma once

#include "CoreMinimal.h"
#include "UI/ODUIWidget.h"
#include "Gameplay/ODGlassType.h"
#include "IngredientsEnum.h"
#include "Gameplay/ODContainerContents.h"
#include "ODContainerWidget.generated.h"

class UMaterialInstanceDynamic;
class UImage;
class UDataTable;
class AODBevContainer;
class UODResourceHexWidget;

/**
 * 
 */
UCLASS()
class OVERDRINKED_API UODContainerWidget : public UODUIWidget
{
	GENERATED_BODY()

public:
	UODContainerWidget(const FObjectInitializer& OI);

	virtual void NativeTick(const FGeometry& MyGeometry, float InDeltaTime) override;

protected:
	UPROPERTY(meta = (BindWidget))
	UODResourceHexWidget* ResourceHex1;

	UPROPERTY(meta = (BindWidget))
	UODResourceHexWidget* ResourceHex2;

	UPROPERTY(meta = (BindWidget))
	UODResourceHexWidget* ResourceHex3;

	UPROPERTY(meta = (BindWidget))
	UODResourceHexWidget* ResourceHex4;

	UPROPERTY(meta = (BindWidget))
	UODResourceHexWidget* ResourceHex5;

	TArray<UODResourceHexWidget*> ResourceHexes;

	UMaterialInterface* MixerMaterialInterface;

	UPROPERTY(BlueprintReadWrite)
	UMaterialInstanceDynamic* DynamicMixerMaterial;

	uint32 CurrentMaterialIndex;

	TMap<EIngredientType, uint32> IngredientToMaterialIndex;

	UPROPERTY(BlueprintReadOnly)
	AODBevContainer* DrinkContainerReference;

	UDataTable* IngredientDataTable;

	float ContainerMaxVolume;

	// Constants for referencing material params
	const FString BaseIngredientString = "Ingredient";
	const FString ColorString = "Color";
	const FString LevelString = "Level";

	UPROPERTY(EditAnywhere)
	FVector2D ViewportOffset;

public:
	void SetNewContainer(AODBevContainer* NewContainer);

	void AdjustContents();

	void ClearIngredients();

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void ShowContainer();

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void HideContainer();

protected:
	UFUNCTION(BlueprintImplementableEvent)
	void SetGlassOverlayImage(EGlassType GlassType);

	UFUNCTION(BlueprintImplementableEvent)
	void SetResourceIcons(FContainerContents ContainerContents);

	void SetFillMaterialShape(UMaterialInstanceDynamic* MaterialToSet);

	void SetupMixerMaterial();

	UFUNCTION(BlueprintImplementableEvent)
	void SetFillMaterial(EGlassType GlassType);

};