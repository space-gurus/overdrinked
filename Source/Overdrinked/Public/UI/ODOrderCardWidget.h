// Copyright 2020 Space Gurus

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "ODOrderCardWidget.generated.h"

class UImage;
class UProgressBar;
class UMaterialInstanceDynamic;

/**
 * 
 */
UCLASS()
class OVERDRINKED_API UODOrderCardWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:
	UODOrderCardWidget(const FObjectInitializer& ObjectInitializer);

	UFUNCTION(BlueprintImplementableEvent)
	void SetFaceCaptureMaterial(UMaterialInstanceDynamic* DynamicMaterial);

protected:
	UPROPERTY(meta = (BindWidget))
	UProgressBar* OrderTimerBar;
};
