// Copyright 2020 Space Gurus

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "CustomerEmotesEnum.h"
#include "ODCustomerEmoteWidget.generated.h"

/**
 * 
 */
UCLASS()
class OVERDRINKED_API UODCustomerEmoteWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void ShowEmote(ECustomerEmote Emote);

	UFUNCTION(BlueprintImplementableEvent)
	void ShowEmoteForTime(ECustomerEmote Emote, int32 TimeInSeconds);

	UFUNCTION(BlueprintImplementableEvent)
	void ChangeEmote(ECustomerEmote Emote);

	UFUNCTION(BlueprintImplementableEvent)
	void HideEmote();

	UFUNCTION(BlueprintImplementableEvent)
	void ShowGlow();

	UFUNCTION(BlueprintImplementableEvent)
	void HideGlow();

	bool IsGlowShowing();

	bool IsEmoteShowing();

protected:
	UPROPERTY(BlueprintReadWrite)
	bool bIsGlowShowing = false;

	UPROPERTY(BlueprintReadWrite)
	bool bIsEmoteShowing = false;
};
