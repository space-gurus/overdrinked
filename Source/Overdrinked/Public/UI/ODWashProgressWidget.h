// Copyright 2020 Space Gurus

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "ODWashProgressWidget.generated.h"

/**
 * 
 */
UCLASS()
class OVERDRINKED_API UODWashProgressWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:
	UFUNCTION(BlueprintImplementableEvent)
	void SetWashProgress(float Amount);

};
