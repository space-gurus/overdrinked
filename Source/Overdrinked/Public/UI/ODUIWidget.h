// Copyright 2020 Space Gurus

#pragma once

#include "CoreMinimal.h"
#include "UI/ODUserWidget.h"
#include "ODUIWidget.generated.h"

/**
 * 
 */
UCLASS()
class OVERDRINKED_API UODUIWidget : public UODUserWidget
{
	GENERATED_BODY()
	
public:
	virtual void SetupWidget() override;

	virtual void TearDownWidget() override;
};
