// Copyright 2020 Space Gurus

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "IngredientsEnum.h"
#include "ODPourHUD.generated.h"

class UImage;
class UTextBlock;
class UDataTable;

/**
 * 
 */
UCLASS()
class OVERDRINKED_API UODPourHUD : public UUserWidget
{
	GENERATED_BODY()
	
public:
	UODPourHUD(const FObjectInitializer& ObjectInitializer);

	UFUNCTION(BlueprintImplementableEvent)
	void OpenWidget();

	UFUNCTION(BlueprintImplementableEvent)
	void CloseWidget();

	void SetIngredients(TArray<EIngredientType> Ingredients);

protected:
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	UImage* IngredientOne;

	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	UImage* IngredientTwo;

	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	UImage* IngredientThree;

	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	UImage* IngredientFour;

	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	UTextBlock* IngredientOneText;

	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	UTextBlock* IngredientTwoText;

	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	UTextBlock* IngredientThreeText;

	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	UTextBlock* IngredientFourText;

	TArray<UImage*> IngredientImages;
	TArray<UMaterialInstanceDynamic*> DynamicIngredientMaterials;
	TMap<UImage*, UTextBlock*> IngredientImageToText;

	UDataTable* IngredientData;
};
