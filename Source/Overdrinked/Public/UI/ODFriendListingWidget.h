// Copyright 2020 Space Gurus

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "ODFriendListingWidget.generated.h"

/**
 * 
 */
UCLASS()
class OVERDRINKED_API UODFriendListingWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:
	UFUNCTION(BlueprintImplementableEvent)
	void SetupPlayerName(FName PlayerName);

};
