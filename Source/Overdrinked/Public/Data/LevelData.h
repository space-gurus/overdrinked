// Copyright 2020 Space Gurus
#pragma once

#include "CoreMinimal.h"
#include "Engine/DataTable.h"
#include "LevelData.generated.h"

/** Struct to store recipe data for drinks */
USTRUCT(BlueprintType)
struct FLevelData : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString LevelName;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString LevelReferencePath;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UTexture* LevelImage;
};