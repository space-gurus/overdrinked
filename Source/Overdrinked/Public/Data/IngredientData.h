// Copyright 2020 Space Gurus

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataTable.h"
#include "IngredientsEnum.h"
#include "IngredientData.generated.h"

/** Struct to store recipe data for drinks */
USTRUCT(BlueprintType)
struct FIngredientData : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	EIngredientType IngredientType;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FColor Color;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UTexture2D* Icon;
};