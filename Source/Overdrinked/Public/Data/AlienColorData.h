#pragma once

#include "CoreMinimal.h"
#include "Engine/DataTable.h"
#include "AlienColorData.generated.h"

/** Struct to store recipe data for drinks */
USTRUCT(BlueprintType)
struct FAlienColorData : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FLinearColor ColorOne;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FLinearColor ColorTwo;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FLinearColor ColorThree;
};