// Copyright 2020 Space Gurus

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataTable.h"
#include "IngredientsEnum.h"
#include "Gameplay/ODGlassType.h"
#include "DrinkData.generated.h"

class UNiagaraSystem;

USTRUCT(BlueprintType)
struct F2DAesthetic : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UMaterialInstance* MaterialInstance;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TAssetPtr<UMaterialInstance> Garnish;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TAssetPtr<UMaterialInstance> Effect;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FVector2D GarnishOffset;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FVector2D EffectOffset;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float FoamPercent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FColor FoamColor;
};

USTRUCT(BlueprintType)
struct F3DAesthetic : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UMaterialInstance* MaterialInstance;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TAssetPtr<UStaticMesh> Garnish;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TAssetPtr<UNiagaraSystem> Effect;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FVector GarnishOffset;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FVector EffectOffset;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float FoamPercent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FColor FoamColor;
};

/** Struct to store recipe data for drinks */
USTRUCT(BlueprintType)
struct FDrinkData : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TMap<EIngredientType, float> IngredientList; 

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FColor BaseColor;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<EGlassType> PossibleGlasses;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	F2DAesthetic Aesthetic2D;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	F3DAesthetic Aesthetic3D;
};