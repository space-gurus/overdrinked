// Copyright 2020 Space Gurus
#pragma once

#include "CoreMinimal.h"
#include "Engine/DataTable.h"
#include "AlienNameData.generated.h"

/** Struct to store recipe data for drinks */
USTRUCT(BlueprintType)
struct FAlienNameData : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString AlienName;
};