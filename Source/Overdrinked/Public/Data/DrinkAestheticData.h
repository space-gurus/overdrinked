// Copyright 2020 Space Gurus

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataTable.h"
#include "DrinkAestheticData.generated.h"

class UTexture2D;

/** Struct to store recipe data for drinks */
USTRUCT(BlueprintType)
struct FDrinkAestheticData : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FColor Color;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TAssetPtr<UTexture2D> UIImage;
};