// Copyright 2020 Space Gurus

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataTable.h"
#include "CustomerEmotesEnum.h"
#include "CustomerEmoteData.generated.h"

/** Struct to store recipe data for drinks */
USTRUCT(BlueprintType)
struct FCustomerEmoteData : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	ECustomerEmote Emote;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UTexture2D* Icon;
};