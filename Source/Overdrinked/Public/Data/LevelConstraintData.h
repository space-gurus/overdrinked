// Copyright 2020 Space Gurus

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataTable.h"
#include "Gameplay/ODGarnishType.h"
#include "Gameplay/ODMixMethod.h"
#include "Gameplay/ODGlassType.h"
#include "IngredientsEnum.h"
#include "LevelConstraintData.generated.h"

/** Struct to store recipe data for drinks */
USTRUCT(BlueprintType)
struct FLevelConstraintData : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()

public:
	/** How long is this level */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 LevelLengthInMinutes;

	/** What percentage of the bar can break out into a brawl before the level is lost? */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float BrawlThreshold;

	/** How many points needed this level to get 1 star */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 OneStarPointThreshold;

	/** How many points needed this level to get 2 stars */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 TwoStarPointThreshold;

	/** How many points needed this level to get 3 stars */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 ThreeStarPointThreshold;
	
	/** What drinks can be ordered by AI this level */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<EIngredientType> AvailableIngredients;

	/** What garnishes can be added by AI this level */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<EGarnishType> AvailableGarnishes;

	/** What glasses are available for this level */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<EGlassType> AvailableGlasses;

	/** What mix methods are available for this level */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<EMixMethod> AvailableMixMethods;

	/** Max AI capacity in bar area */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 MaxAICapacity;

	/** Num AI we can flex while spawning */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 AICapacityFlex;

};