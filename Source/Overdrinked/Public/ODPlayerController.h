// Copyright 2020 Space Gurus

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "IngredientsEnum.h"
#include "Gameplay/ODDrinkOrder.h"
#include "ODPlayerController.generated.h"

class APlayerCharacter;
class AODAICharacter;
class AODQTEStation;
class UODMenuWidget;
class UODUIWidget;
class UODUserWidget;
class UODContainerWidget;
class UODInGameHUD;
class UODQTEWidget;
class UODMixerWidget;
class AODBevContainer;
class AODCounter;
class AODIngredientDispenser;
class AODMultiplayerGameState;

/**
 * 
 */
UCLASS()
class OVERDRINKED_API AODPlayerController : public APlayerController
{
	GENERATED_BODY()
	
public:
	AODPlayerController();

	virtual void BeginPlay() override;

	virtual void OnPossess(APawn* aPawn) override;

	void OnRep_PlayerState() override;

protected:
	APlayerCharacter* PlayerCharacter;

	AODMultiplayerGameState* MPGameState;

	FTimerHandle GameStartTimerHandle;

	bool bCanFillAtDispenser = false;

	FTimerHandle DispenseTimerHandle;

	UPROPERTY(EditAnywhere, Category = "Drink System")
	float dispenseTickTime = 0.1f;

	bool bIsPouringAtDispenser = false;
	int32 currentHoldActionIndex = -1;

	/** This is used to reset the mixing action */
	FTimerHandle MixingTimerHandle;
	FTimerHandle MixingSoundTimerHandle;

	UPROPERTY(EditAnywhere, Category = "Drink System")
	float MixingResetTime = 0.2f;

	UPROPERTY(EditAnywhere, Category = "Drink System")
	float GamepadMixThreshold = 0.9f;

	UPROPERTY(EditAnywhere, Category = "Drink System")
	float MouseMixThreshold = 5.f;

	float LastMouseAxis = 0.f;
	float LastGamepadAxis = 0.f;

	bool bIsMixing = false;
	bool bWasLastShakeUpward = false;

	bool bInQTE = false;

	// UI variables
	TSubclassOf<UODUserWidget> LobbyUIBPClass;
	UODUserWidget* LobbyUIWidget;
	TSubclassOf<UODUserWidget> InGameUIBPClass;
	UODInGameHUD* InGameUIWidget;
	TSubclassOf<UODUserWidget> OptionsMenuBPClass;
	UODUserWidget* OptionsMenuWidget;

	TSubclassOf<UODContainerWidget> ContainerWidgetBPClass;
	UODContainerWidget* ContainerWidget;

	TSubclassOf<UUserWidget> LevelFailHostBPClass;

	// TODO: Get rid of this
	UODMixerWidget* MixerWidget;

	/* The actor associated with the current active QTE */
	AODQTEStation* ActiveQTEStation;

	AODIngredientDispenser* ActiveDispenser;

	bool bIsOptionsMenuOpen = false;

public:
	UFUNCTION(NetMulticast, Reliable)
	void PrepareForGameStart();

	UFUNCTION()
	void StartGameCountdown();

	/** Tell the server this player controller is initialized and fully ready to play */
	UFUNCTION(Server, Reliable)
	void Server_InformServerReadyStatus();

	void StartRoundSetup();

	UFUNCTION(NetMulticast, Reliable)
	void HandleLevelEnd();

	UFUNCTION(NetMulticast, Reliable)
	void HandleLevelFail();

	UFUNCTION(NetMulticast, Reliable)
	void HandleLeaveLevel();

	UFUNCTION(Server, Reliable)
	void SetPlayerWidgetOnCharacter();

	UFUNCTION(BlueprintCallable)
	void LoadInGameUI();

	UFUNCTION(BlueprintCallable)
	void LoadOptionsMenu();

	UFUNCTION(BlueprintCallable)
	void CloseOptionsMenu();

	void SetupContainerWidget();

	UODInGameHUD* GetInGameHUD();

	void ShowPourHUD(TArray<EIngredientType> Ingredients);

	void HidePourHUD();

	void ShowContainerWidget(AODBevContainer* AttachedContainer);

	void HideContainerWidget();

	void ClearContainerWidget();

	UFUNCTION(NetMulticast, Reliable)
	void AddOrderToHUD(FDrinkOrder DrinkOrder);

	UFUNCTION(NetMulticast, Reliable)
	void RemoveOrderFromHUD(FDrinkOrder DrinkOrder);

	UFUNCTION(NetMulticast, Reliable)
	void UpdateBrawlLevelHUD(float BrawlPercentage);
	
	/** Handle logic for UI and other things on pickup */
	UFUNCTION(Client, Reliable)
	void Client_FinishPickup();

	/** Handle logic for UI and other things on drop */
	UFUNCTION(Client, Reliable)
	void Client_DropBevContainer(AODBevContainer* ContainerToDrop);

	UFUNCTION(BlueprintCallable)
	bool GetCanFillAtDispenser();

	void SetCanFillAtDispenser(bool val);

	void RefreshMixerWidget();

	UFUNCTION(Client, Reliable)
	void Client_RefreshMixerWidget();

	UFUNCTION(Client, Reliable)
	void Client_ClearMixerWidget();

	void SetDrinkInWidget();

	UFUNCTION(Client, Reliable)
	void Client_SetDrinkInWidget();

	/** Start a QTE on the player. Pass a reference to the object being interacted with */
	UFUNCTION(Client, Reliable)
	void Client_StartQTE(AActor* InteractingActor);
	
	void FinishQTE(bool bQTEResult);

	UFUNCTION(Server, Reliable)
	void Server_RequestQTEReward(AODQTEStation* QTEActor);

	UFUNCTION(Server, Reliable)
	void Server_CloseQTEEffects(AODQTEStation* QTEActor);

	UFUNCTION(Client, Reliable)
	void Client_HandlePlayerError();

	UFUNCTION(Client, Reliable)
	void Client_HandlePour();

	UFUNCTION(Client, Reliable)
	void Client_HandleAddGarnish();

	UFUNCTION(Client, Reliable)
	void Client_HandleFill(int32 FillLevel);

	void CancelQTE();

	/** Handle traveling back to main menu if there was an error */
	void TravelToMainMenu(bool bHadNetworkError);

	bool IsPouring();

	void StopPouring();

	/** ADMIN SECTION **/
public:
	UFUNCTION(Exec, BlueprintCallable)
	void OpenAdminMenu();

protected:
	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
	void SetupAdminMenu();

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
	void CloseAdminMenu();

protected:
	// Called to bind functionality to input
	void SetupInputComponent() override;

	void HandleMoveForward(float val);

	void HandleMoveRight(float val);

	void HandleGamepadMixUp(float val);

	void HandleMouseMixUp(float val);

	/** Initial call for action mapping Action 1 */
	void HandleAction1Pressed();

	/** Release call for action mapping Action 1 */
	void HandleAction1Released();

	/** Initial call for action mapping Action 2 */
	void HandleAction2Pressed();

	/** Release call for action mapping Action 2 */
	void HandleAction2Released();

	/** Initial call for action mapping Action 3 */
	void HandleAction3Pressed();

	/** Release call for action mapping Action 3 */
	void HandleAction3Released();

	/** Initial call for action mapping Action 4 */
	void HandleAction4Pressed();

	/** Release call for action mapping Action 4 */
	void HandleAction4Released();

	void HandleOptions();

	UFUNCTION(BlueprintCallable)
	void HandleResume();

	void HandleViewOrdersPressed();

	void HandleViewOrdersReleased();

public:
	UFUNCTION(Client, Reliable)
	void StartPouring(AODIngredientDispenser* Dispenser);

protected:
	UFUNCTION()
	void DispenseTimerUp();

	bool TryToMix(bool bIsUsingGamepad, bool bIsMixActionUpwards, float AxisValue);

	void MixingTimerUp();

	void MixingSoundTimerUp();

public:
	UFUNCTION(Server, Reliable)
	void Server_AcceptCustomerOrder(AODAICharacter* Customer);

	UFUNCTION(Server, Reliable)
	void Server_GiveDrinkToCustomer(AODAICharacter* AICharacter, AODDrinkGlass* Drink);

protected:
	/** Calculate shake speed for Wwise RTPC. If false we assume the player is using a mouse */
	float CalculateShakeSpeed(bool bIsUsingGamepad, float AxisValue);
};
