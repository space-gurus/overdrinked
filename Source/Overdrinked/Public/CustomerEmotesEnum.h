// Copyright 2020 Space Gurus

#pragma once
UENUM(BlueprintType)
enum class ECustomerEmote : uint8
{
	CE_Happy	UMETA(DisplayName = "Happy"),
	CE_Sad		UMETA(DisplayName = "Sad"),
	CE_Angry	UMETA(DisplayName = "Angry"),
	CE_Confused UMETA(DisplayName = "Confused"),
	CE_Order	UMETA(Displayname = "Order")
};
