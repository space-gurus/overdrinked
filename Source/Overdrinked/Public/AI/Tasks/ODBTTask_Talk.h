// Copyright 2020 Space Gurus

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/Tasks/BTTask_BlackboardBase.h"
#include "ODBTTask_Talk.generated.h"

/**
 * 
 */
UCLASS()
class OVERDRINKED_API UODBTTask_Talk : public UBTTask_BlackboardBase
{
	GENERATED_BODY()
	
public:
	UODBTTask_Talk();

	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;
};
