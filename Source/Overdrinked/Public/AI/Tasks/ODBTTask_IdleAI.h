// Copyright 2020 Space Gurus

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/Tasks/BTTask_BlackboardBase.h"
#include "ODBTTask_IdleAI.generated.h"

/**
 * 
 */
UCLASS()
class OVERDRINKED_API UODBTTask_IdleAI : public UBTTask_BlackboardBase
{
	GENERATED_BODY()
	
public:
	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;

public:
	UPROPERTY(EditAnywhere)
	float IdleAnimChance = 0.5f;
};
