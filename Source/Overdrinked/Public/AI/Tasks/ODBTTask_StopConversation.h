// Copyright 2020 Space Gurus

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/Tasks/BTTask_BlackboardBase.h"
#include "ODBTTask_StopConversation.generated.h"

/**
 * 
 */
UCLASS()
class OVERDRINKED_API UODBTTask_StopConversation : public UBTTask_BlackboardBase
{
	GENERATED_BODY()

public:
	UODBTTask_StopConversation();

	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;
};
