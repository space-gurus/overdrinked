// Copyright 2020 Space Gurus

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/Tasks/BTTask_BlackboardBase.h"
#include "ODBTTask_StartConversation.generated.h"

class AODAIController;
class AODAICharacter;

/**
 * 
 */
UCLASS()
class OVERDRINKED_API UODBTTask_StartConversation : public UBTTask_BlackboardBase
{
	GENERATED_BODY()
	
public:
	UODBTTask_StartConversation();

	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;

protected:
	AODAICharacter* AICharacter;
	AODAIController* AIController;
};
