// Copyright 2020 Space Gurus

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/Tasks/BTTask_BlackboardBase.h"
#include "ODBTTask_FindRandomSpot.generated.h"

/**
 * 
 */
UCLASS()
class OVERDRINKED_API UODBTTask_FindRandomSpot : public UBTTask_BlackboardBase
{
	GENERATED_BODY()

	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Task Parameters")
	float SearchRadius = 1000.f;

};
