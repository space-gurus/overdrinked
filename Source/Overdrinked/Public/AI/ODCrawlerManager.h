// Copyright 2020 Space Gurus

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ODCrawlerManager.generated.h"

class UBoxComponent;
class AODAICrawler;

UCLASS()
class OVERDRINKED_API AODCrawlerManager : public AActor
{
	GENERATED_BODY()
	
public:	
	AODCrawlerManager(const FObjectInitializer& OI);

protected:
	UPROPERTY(EditAnywhere, Category = "Spawning System")
	TSubclassOf<AODAICrawler> CrawlerBP;

	UPROPERTY(EditAnywhere, Category = "Spawning System", Meta = (MakeEditWidget = true))
	TArray<FVector> CrawlerSpawnPositions;

	UPROPERTY(VisibleAnywhere)
	int32 NumActiveCrawlers = 0;

	TArray<AODAICrawler*> ActiveCrawlers;

protected:
	virtual void BeginPlay() override;

public:	
	virtual void Tick(float DeltaTime) override;

	int32 GetNumActiveCrawlers();

	UFUNCTION(Server, Reliable)
	void Server_SpawnCrawler();

	UFUNCTION(Server, Reliable)
	void Server_SpawnCrawlerAtPosition(FVector SpawnPos);

};
