// Copyright 2020 Space Gurus

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Actors/ODInteractInterface.h"
#include "ODAICrawler.generated.h"

class USceneComponent;
class APlayerCharacter;
class AODCrawlerController;
class AODCrawlerManager;
class UAkAudioEvent;

UCLASS()
class OVERDRINKED_API AODAICrawler : public ACharacter, public IODInteractInterface
{
	GENERATED_BODY()

public:
	AODAICrawler(const FObjectInitializer& OI);

	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

protected:
	virtual void BeginPlay() override;

	virtual void Tick(float DeltaSeconds) override;

protected:
	UPROPERTY(EditAnywhere)
	USceneComponent* PickupAttachPoint;

	AODCrawlerController* CrawlerController;

	AODCrawlerManager* CrawlerManager;

	UPROPERTY(Replicated, BlueprintReadOnly)
	bool bIsPickedUp = false;

	UPROPERTY(EditAnywhere, Category = "Wwise")
	UAkAudioEvent* PickupSound;

	APlayerCharacter* PlayerHolding;

	UPROPERTY(EditAnywhere)
	int32 DefaultCustomDepth = 253;

	UPROPERTY(EditAnywhere)
	int32 HighlightCustomDepth = 241;

public:
	// INTERACT INTERFACE
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Interact Interface")
	void PlayerInteraction(APlayerCharacter* Player);
	virtual void PlayerInteraction_Implementation(APlayerCharacter* Player) override;

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Interact Interface")
	void BecomeRelevant(APlayerCharacter* PlayerTriggered);
	virtual void BecomeRelevant_Implementation(APlayerCharacter* PlayerTriggered) override;

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Interact Interface")
	void LoseRelevance();
	virtual void LoseRelevance_Implementation() override;

protected:
	UFUNCTION(Client, Reliable)
	void Highlight();

	UFUNCTION(Client, Reliable)
	void EndHighlight();
	//////////////////////

public:
	void SetManagerReference(AODCrawlerManager* Manager);

	AODCrawlerManager* GetManagerReference();

	void HandlePickup(APlayerCharacter* Player);

	UFUNCTION(NetMulticast, Reliable)
	void PlayPickupSound();
};
