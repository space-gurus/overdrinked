// Copyright 2020 Space Gurus

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/Decorators/BTDecorator_BlackboardBase.h"
#include "ODBTDec_HoldConversation.generated.h"

/**
 * 
 */
UCLASS()
class OVERDRINKED_API UODBTDec_HoldConversation : public UBTDecorator_BlackboardBase
{
	GENERATED_BODY()

	UODBTDec_HoldConversation();

	virtual bool CalculateRawConditionValue(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) const override;
};
