// Copyright 2020 Space Gurus

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "ODCrawlerController.generated.h"

class UBehaviorTree;
class AODAICrawler;

/**
 * 
 */
UCLASS()
class OVERDRINKED_API AODCrawlerController : public AAIController
{
	GENERATED_BODY()
	
public:
	AODCrawlerController();

	virtual void OnPossess(APawn* InPawn) override;

	void RespondToPickup();

	void RespondToDrop();

protected:
	UPROPERTY(EditDefaultsOnly, Category = "AI")
	UBehaviorTree* BehaviorTree;

	AODAICrawler* Crawler;
};
