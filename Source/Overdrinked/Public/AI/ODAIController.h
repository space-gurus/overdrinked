// Copyright 2020 Space Gurus

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "DrinksEnum.h"
#include "Gameplay/ODGlassType.h"
#include "Gameplay/ODDrinkOrder.h"
#include "ODAIController.generated.h"

class AODAICharacter;
class UBlackboardComponent;
class UAIPerceptionComponent;
class UAISenseConfig_Sight;
class UDataTable;
class AODAIManager;
class AODAILocation;

/**
 * 
 */
UCLASS()
class OVERDRINKED_API AODAIController : public AAIController
{
	GENERATED_BODY()
	
public:
	AODAIController(const class FObjectInitializer& ObjectInitializer);

	virtual void OnPossess(APawn* InPawn) override;

	void HandleGameStart();

	UFUNCTION(Server, Reliable)
	void Server_PlaceNewOrder();

	UFUNCTION(Server, Reliable)
	void Server_TakeDrink();

	UFUNCTION(Server, Reliable)
	void Server_DropGlass();

	UFUNCTION(Server, Reliable)
	void Server_FinishLife();

	void SetBoolBlackboardKey(FName KeyName, bool val);

	void SetVectorBlackboardKey(FName KeyName, FVector val);

	void SetBrawlTargetKey(AODAICharacter* TargetCharacter);

	bool CanTakeDrink();

	bool CanDropItem();

	void SetAIManagerReference(AODAIManager* Manager);

	AODAIManager* GetAIManager();

	FDrinkOrder MakeNewDrinkOrder();

	EGlassType PickRandomGlass();

	/** Pick random ingredients with a volume that will fit in the provided glass */
	TMap<EIngredientType, int32> PickRandomIngredients(EGlassType GlassType);

	int32 RandomNumBetween(int32 Min, int32 Max);

	TMap<EGarnishType, int32> PickRandomGarnishes();

	EMixMethod PickRandomMixMethod();

	void StartConversation();

	void FinishConversation();

	void SetCurrentAILocation(AODAILocation* NewLocation);

	AODAILocation* GetCurrentAILocation();

	void SetFillerStatus(bool val);

	bool IsFiller();

	void HandleTaserLogic();

	void StartAttack();

	void StartTalk();

protected:
	/** Owning character */
	AODAICharacter* AICharacter;

	/** Manager for this controller */
	AODAIManager* AIManager;

	UPROPERTY(EditDefaultsOnly, Category = "AI")
	UBehaviorTree* ActiveBehaviorTree;

	UPROPERTY(EditDefaultsOnly, Category = "AI")
	UBehaviorTree* FillerBehaviorTree;

	UBlackboardComponent* BlackboardComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="AI")
	UAIPerceptionComponent* AIPerceptionComponent;

	UPROPERTY(BlueprintReadWrite, Category="AI")
	UAISenseConfig_Sight* SightConfiguration;

	UPROPERTY(BlueprintReadOnly)
	UDataTable* LevelConstraintsTable;

	UPROPERTY(BlueprintReadOnly)
	UDataTable* DrinksTable;

	FTimerHandle ConversationTimerHandle;

	AODAILocation* CurrentAILocation;

	/** Should this AI generate thirst and try to order? */
	UPROPERTY(VisibleAnywhere)
	bool bIsFiller = false;
};
