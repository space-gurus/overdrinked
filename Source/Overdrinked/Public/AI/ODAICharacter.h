// Copyright 2020 Space Gurus

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Actors/ODInteractInterface.h"
#include "Gameplay/ODDrinkOrder.h"
#include "Gameplay/ODGlassType.h"
#include "ODMeshMergeFunctionLibrary.h"
#include "CustomerEmotesEnum.h"
#include "AIMoodEnum.h"
#include "ODAICharacter.generated.h"

class UWidgetComponent;
class UMaterialInstanceDynamic;
class UODCustomerEmoteWidget;
class USceneCaptureComponent2D;
class USkeletalMeshComponent;
class USpotLightComponent;
class UAnimMontage;
class UAnimInstance;
class UAnimSequence;
class UPhysicsAsset;
class UDataTable;
class AODAIController;
class AODDrinkGlass;
class AODAIManager;
class UODOrderCardWidget;
class UAkAudioEvent;
class UAkComponent;
class UNiagaraSystem;
class UNiagaraComponent;

UCLASS()
class OVERDRINKED_API AODAICharacter : public ACharacter, public IODInteractInterface
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AODAICharacter(const FObjectInitializer& OI);

	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	UFUNCTION()
	void CapsuleColliderBeginOverlap(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
	void CapsuleColliderEndOverlap(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	UFUNCTION()
	void SkeletalMeshBeginOverlap(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
	void SkeletalMeshEndOverlap(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

protected:
	UDataTable* AlienColorTable;

	UMaterialInstanceDynamic* DynamicHeadMaterial;
	UMaterialInstanceDynamic* DynamicTorsoMaterial;
	UMaterialInstanceDynamic* DynamicLegMaterial;

	UPROPERTY(EditAnywhere)
	int32 DefaultCustomDepth = 253;

	UPROPERTY(EditAnywhere)
	int32 HighlightCustomDepth = 243;

	UPROPERTY(EditAnywhere)
	UWidgetComponent* EmoteWidgetComponent;

	float EmoteDisplayTime = 7.f;

	UPROPERTY(EditAnywhere)
	USceneCaptureComponent2D* FaceCameraComponent;

	UPROPERTY(VisibleAnywhere)
	USpotLightComponent* Spotlight;

	UPROPERTY(EditAnywhere)
	FLinearColor SpotlightColor;

	UPROPERTY(VisibleAnywhere, Replicated)
	int32 HeadMeshIndex = 0;

	UPROPERTY(VisibleAnywhere, Replicated)
	int32 LegMeshIndex = 0;

	UPROPERTY(VisibleAnywhere, Replicated)
	int32 TorsoMeshIndex = 0;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Character Generation")
	FSkeletalMeshMergeParams MeshMergeParams;

	UPROPERTY(EditAnywhere, Category = "Character Generation")
	TArray<USkeletalMesh*> HeadMeshOptions;

	UPROPERTY(EditAnywhere, Category = "Character Generation")
	TArray<USkeletalMesh*> TorsoMeshOptions;

	UPROPERTY(EditAnywhere, Category = "Character Generation")
	TArray<USkeletalMesh*> LegMeshOptions;

	UPROPERTY(EditAnywhere, Category = "Character Generation")
	TArray<FName> GrabbableSocketNames;

	UPROPERTY(EditAnywhere, Category = "Animation")
	UPhysicsAsset* NPCPhysicsAsset;

	UPROPERTY(EditAnywhere, Category = "Animation")
	TSubclassOf<UAnimInstance> NPCAnimBPClass;
	
	UPROPERTY(EditAnywhere, Category = "Animation")
	TArray<UAnimMontage*> TalkAnimations;

	UPROPERTY(EditAnywhere, Category = "Animation")
	TArray<UAnimMontage*> DrinkTalkAnimations;

	UPROPERTY(EditAnywhere, Category = "Animation")
	TArray<UAnimMontage*> AttackAnimations;

	UPROPERTY(EditAnywhere, Category = "Animation")
	TArray<UAnimMontage*> HitReactAnimations;

	UPROPERTY(EditAnywhere, Category = "Animation")
	TArray<UAnimMontage*> ParryAnimations;

	UPROPERTY(EditAnywhere, Category = "Animation")
	TArray<UAnimMontage*> IdleAnimations;

	UPROPERTY(EditAnywhere, Category = "Animation")
	UAnimMontage* NPCOrderMontage;

	UPROPERTY(EditAnywhere, Category = "Animation")
	UAnimMontage* NPCTakeDrinkMontage;

	UPROPERTY(EditAnywhere, Category = "Animation")
	UAnimMontage* ElectrocuteMontage;
    
    UPROPERTY(EditAnywhere, Category = "Wwise")
    UAkAudioEvent* TaserElectrocuteSound;

	UPROPERTY(EditAnywhere, Category = "Wwise")
	UAkAudioEvent* PunchSound;

	UPROPERTY(EditAnywhere, Category = "Wwise")
	TArray<UAkAudioEvent*> GruntSounds;

	UPROPERTY(EditAnywhere, Category = "Effects")
	UNiagaraSystem* ElectrocuteEffectSystem;

	UPROPERTY(EditAnywhere, Category = "Effects")
	UNiagaraSystem* AsleepEffectSystem;

	/* How long the material effect plays before fading */
	UPROPERTY(EditAnywhere, Category = "Effects")
	float ElectrocuteMaterialEffectLength = 2.f;

	/* How it takes for the material effect to fade */
	UPROPERTY(EditAnywhere, Category = "Effects")
	float ElectrocuteMaterialEffectFadeSpeed = 1.f;

	float ElectrocuteLerpTime = 0.f;

	bool bShouldFinishElectrocuteEffect = false;
	float ElectrocuteMaterialEffectValue = 0.f;

	UNiagaraComponent* AsleepEffectComp;

	UNiagaraComponent* ElectrocuteEffectComp;

	TSubclassOf<UODCustomerEmoteWidget> EmoteWidgetBPClass;
	UODCustomerEmoteWidget* CustomerEmoteWidget;

	AODAIController* MyController;

	UPROPERTY(Replicated, VisibleAnywhere)
	FDrinkOrder DrinkOrder;

	FTimerHandle OrderTimerHandle;

	AODDrinkGlass* DrinkInHand;

	UPROPERTY(Replicated)
	bool bIsActive = true;

	UPROPERTY(Replicated)
	bool bIsInBarArea = false;

	UPROPERTY(ReplicatedUsing=OnRep_IsWaitingToOrder)
	bool bIsWaitingToOrder = false;

	UPROPERTY(Replicated)
	bool bIsWaitingForOrderDelivery = false;

	UPROPERTY(Replicated)
	bool bIsHoldingDrink = false;

	UPROPERTY(Replicated)
	bool bIsFinishedWithDrink = false;

	UPROPERTY(Replicated, BlueprintReadOnly)
	bool bIsInConversation = false;

	UPROPERTY(Replicated, BlueprintReadOnly)
	bool bRecentlyFinishedConversation = false;

	UPROPERTY(ReplicatedUsing=OnRep_WantsToFight, EditAnywhere, BlueprintReadOnly)
	bool bWantsToFight = false;

	UPROPERTY(EditAnywhere, Category = "AI Properties")
	float AggressiveSpeed = 200.f;

	// BEHAVIOR VARIABLES
	UPROPERTY(ReplicatedUsing=OnRep_Mood, VisibleAnywhere, BlueprintReadOnly, Category = "Behavior")
	EAIMood Mood;

	UPROPERTY(Replicated, VisibleAnywhere, BlueprintReadOnly, Category = "Behavior")
	float IntoxicationLevel = 0.f;

	UPROPERTY(Replicated, VisibleAnywhere, BlueprintReadOnly, Category = "Behavior")
	float IntoxicationThreshold = 60.f;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Behavior")
	float AggressionLevel = 0.f;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Behavior")
	float AggressionThreshold = 0.f;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Behavior")
	float ThirstLevel = 0.f;

	/** Thirst level at which this character will try to order a drink */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Behavior")
	float ThirstThreshold = 50.f;

	UPROPERTY(ReplicatedUsing=OnRep_IsSad)
	bool bIsSad = false;

	bool bIsTakingFaceCapture = false;

	bool bIsAttacking = false;

	bool bIsBeingHit = false;

	UMaterial* FaceCaptureMaterial;

	UODOrderCardWidget* OrderCardWidget;

	/** Who we're trying to fight */
	AODAICharacter* BrawlTarget;

	FTimerHandle HitTimerHandle;
	FTimerHandle AttackTimerHandle;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION()
	void OnRep_Mood();

	UFUNCTION()
	void OnRep_IsWaitingToOrder();

	UFUNCTION()
	void OnRep_IsSad();

	UFUNCTION()
	void OnRep_WantsToFight();

	void PrintCurrentDrinkOrder();

	void GenerateCharacterVisuals();

	void HandleOrderPrepTimerFinished();

	void Client_HandleOrderTimerFinished();

	void RagdollAllMeshes();

	void TryToRefreshOrdering();

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// INTERACT INTERFACE
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Interact Interface")
	void PlayerInteraction(APlayerCharacter* Player);
	virtual void PlayerInteraction_Implementation(APlayerCharacter* Player) override;

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Interact Interface")
	void BecomeRelevant(APlayerCharacter* PlayerTriggered);
	virtual void BecomeRelevant_Implementation(APlayerCharacter* PlayerTriggered) override;

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Interact Interface")
	void LoseRelevance();
	virtual void LoseRelevance_Implementation() override;

protected:
	UFUNCTION(Client, Reliable)
	void Highlight();

	UFUNCTION(Client, Reliable)
	void EndHighlight();
	//////////////////////

public:
	void SetIsInBarArea(bool val);

	UFUNCTION(BlueprintCallable)
	AODAIController* GetODAIController();

	UFUNCTION(BlueprintCallable)
	USceneCaptureComponent2D* GetFaceCameraComponent();

	UFUNCTION(BlueprintCallable)
	AODDrinkGlass* GetDrinkInHand();

	int32 GetCustomDepthStencilValue();

	bool IsCustomerActive();

	UFUNCTION(BlueprintCallable)
	bool IsHoldingDrink();

	UFUNCTION(BlueprintCallable)
	float GetOrderTimeElapsed();

	UFUNCTION(BlueprintCallable)
	void UpdateWalkSpeed(float NewSpeed);

	UFUNCTION(BlueprintCallable)
	bool IsCustomerWaitingToOrder();

	UFUNCTION(BlueprintCallable)
	bool IsWaitingForOrderDelivery();

	UFUNCTION(BlueprintCallable)
	bool IsInBarArea();

	void SetBrawlTarget(AODAICharacter* TargetCharacter);

	AODAICharacter* GetBrawlTarget();

	bool CanCustomerAcceptDrink();

	void GenerateDrinkOrder();

	void ClearCurrentOrder();

	FDrinkOrder GiveOrder();

	UFUNCTION(Server, Reliable)
	void Server_StartOrderTimer();

	UFUNCTION(NetMulticast, Reliable)
	void Multicast_StartOrderTimer();

	void PlayElectrocute();

	UFUNCTION(NetMulticast, Reliable)
	void PlayDrinkAnim();

	UFUNCTION(NetMulticast, Reliable)
	void PlayAttackAnim();

	UFUNCTION(NetMulticast, Reliable)
	void PlayTalkAnim();

	UFUNCTION(Server, Reliable)
	void PlayIdleAnim();

	UFUNCTION(NetMulticast, Reliable)
	void Multi_PlayIdleAnim(int32 IdleAnimIndex);

	void FinishAttack();

	void TakeDrinkFromPlayer(AODDrinkGlass* DrinkToTake);

	void DropItem();

	void EvaluateDrinkOrder();

	void HandleWaitTimeExpired();

	void HandleSuccessfulDelivery();

	UFUNCTION(NetMulticast, Reliable)
	void ChangeEmote(ECustomerEmote EmoteToShow);

	UFUNCTION(BlueprintCallable)
	void TakeFaceCapture(UODOrderCardWidget* OrderWidget);

	void SetThirstThreshold(float val);

	void IncreaseThirst(float val);

	void DecreaseThirst(float val);

	void IncreaseIntoxicationLevel(float val);

	UFUNCTION(BlueprintCallable)
	bool IsIntoxicated();

	void SetAggressionThreshold(float val);

	void IncreaseAggression(float val);

	void ConsumeDrinkInHand();

	bool WantsToOrderDrink();

	void SetInConversation(bool val);

	bool IsInConversation();

	UFUNCTION(BlueprintCallable)
	bool WantsToFight();

	bool IsAttacking();

	bool IsBeingHit();

	void SetRecentlyFinishedConversation(bool val);

	bool RecentlyFinishedConversation();

	UFUNCTION(NetMulticast, Reliable)
	void HandleTaser();

	UFUNCTION(NetMulticast, Reliable)
	void HandleBouncerGrab();

	void PlayOrderAnim();

	void FinishElectrocute();

	void FinishElectrocuteEffect();

	TArray<FName> GetGrabbableSocketNames();

	void HandleHit(AODAICharacter* CharacterHitting);

	void FinishHandleHit();

	void CancelOrder();

	void PlayPunchSound();

protected:
	void SetupCustomerSpotlight();

	void DestroyCustomerSpotlight();
};
