// Copyright 2020 Space Gurus

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "ODBouncerController.generated.h"

class AODBouncer;
class AODAICharacter;
class AODAIManager;

/**
 * 
 */
UCLASS()
class OVERDRINKED_API AODBouncerController : public AAIController
{
	GENERATED_BODY()

public:
	AODBouncerController();

	virtual void OnPossess(APawn* InPawn) override;

	void StartGrabCustomer();

	void SetBoolBlackboardKey(FName KeyName, bool val);

	bool GetBoolBlackboardKey(FName KeyName);

	void SetVectorBlackboardKey(FName KeyName, FVector val);

	void SetObjectBlackboardKey(FName KeyName, UObject* obj);

	UObject* GetObjectBlackboardKey(FName KeyName);

	void TryToGrabAI(AODAICharacter* AIToGrab);

	void TryTossAI(AODAICharacter* AIToToss);

protected:
	UPROPERTY(EditDefaultsOnly, Category = "AI")
	UBehaviorTree* BehaviorTree;

	AODBouncer* Bouncer;

	AODAIManager* AIManager;
};
