// Copyright 2020 Space Gurus

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "ODBouncer.generated.h"

class AODBouncerController;
class AODAICharacter;
class UAkComponent;
class UPhysicsConstraintComponent;
class UPhysicsHandleComponent;

UCLASS()
class OVERDRINKED_API AODBouncer : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AODBouncer();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

public:
	bool IsAvailable();

	void AddCustomerToGrabList(AODAICharacter* AICharacter);

	UFUNCTION(NetMulticast, Reliable)
	void GrabAI(AODAICharacter* AIToGrab);

	void TossAI(AODAICharacter* AIToGrab);

	void TriggerPhysicsGrab();

	void FinishGrab();

	FVector GetPostLocation();
	
	int32 GetAIQueueSize();

	void DelayStart();

protected:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UPhysicsConstraintComponent* GrabConstraintComponent;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UStaticMeshComponent* GrabCubeMesh;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UPhysicsHandleComponent* GrabHandleComponent;

	AODBouncerController* BouncerController;

	TQueue<AODAICharacter*> CustomersToGrab;

	UPROPERTY(Replicated, VisibleAnywhere)
	bool bIsAvailable = true;

	UPROPERTY(Replicated, BlueprintReadOnly)
	bool bIsDragging = false;

	bool bIsQueueEmpty = true;

	int32 QueueSize = 0;

	UPROPERTY(EditAnywhere, Category = "Animation")
	UAnimMontage* GrabMontage;

	UPROPERTY(VisibleAnywhere)
	FVector PostLocation;

	AODAICharacter* GrabbedAI;
};