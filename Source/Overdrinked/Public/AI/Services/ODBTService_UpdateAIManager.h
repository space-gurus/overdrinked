// Copyright 2020 Space Gurus

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTService.h"
#include "ODBTService_UpdateAIManager.generated.h"

/**
 * 
 */
UCLASS()
class OVERDRINKED_API UODBTService_UpdateAIManager : public UBTService
{
	GENERATED_BODY()
	
	UODBTService_UpdateAIManager(const FObjectInitializer& ObjectInitializer);

public:
	virtual void OnBecomeRelevant(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;

	virtual void TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds) override;
};
