// Copyright 2020 Space Gurus

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTService.h"
#include "ODBTService_UpdateBehavior.generated.h"

/**
 * 
 */
UCLASS()
class OVERDRINKED_API UODBTService_UpdateBehavior : public UBTService
{
	GENERATED_BODY()

	UODBTService_UpdateBehavior(const FObjectInitializer& ObjectInitializer);

public:
	virtual void OnBecomeRelevant(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;

	virtual void TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds) override;

protected:
	float MinThirstRange = 1.5f;
	float MaxThirstRange = 4.f;
};
