// Copyright 2020 Space Gurus

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Gameplay/ODDrinkOrder.h"
#include "ODAIManager.generated.h"

class AODAICharacter;
class AODAILocation;
class AODAISpawnArea;
class UBoxComponent;
class AODBouncer;
class UAkRtpc;

USTRUCT(BlueprintType)
struct FAISpawnProperties
{
	GENERATED_USTRUCT_BODY()

	FVector SpawnLocation;

	FRotator SpawnRotation;

	AODAILocation* AILocation = NULL;
};

UCLASS()
class OVERDRINKED_API AODAIManager : public ACharacter
{
	GENERATED_BODY()

public:	
	AODAIManager(const FObjectInitializer& OI);

	UFUNCTION(Server, Reliable)
	void Server_StartRound();

	UFUNCTION(Server, Reliable)
	void Server_BeginGameStartCountdown();

	UFUNCTION(Server, Reliable)
	void Server_SpawnStartingCustomers();

	FVector GetBarEntrance();

	FVector GetBarExit();

	UBoxComponent* GetBarBoundingArea();

	void SetupAISpawnAreas();

	bool IsPointInBarArea(FVector PointToCheck);

	void FinishCustomerActivity(AODAICharacter* AICharacter);

	bool CanHandleNewOrder();

	int32 GiveOrderPrepTime();

	int32 GiveOrderWaitTime();

	/** Spawn a single AI customer */
	void SpawnAICustomer();

	void SpawnAICustomerAtLocation(FAISpawnProperties SpawnProperties);

	void GenerateAIAttributes(AODAICharacter* NewCharacter, bool bIsStarterAI);

	FAISpawnProperties GetRandomSpawnLocation();

	FVector FindPointInAISpawnArea();

	/** Only use this method to check for startup spawns otherwise it will almost always return true */
	bool IsStartingSpawnPointValid(FVector SpawnPoint);

	void NotifyOfLeaving(AODAICharacter* CharacterLeaving);

	void HandleAngry();

	void NotifyOfTaser(AODAICharacter* CharacterDisabled);

	void AddOrder();

	void RemoveOrder();

	TArray<AODAICharacter*> GetActiveAICharacters();

	bool HasGameStarted();

	void UpdateBrawlLevel();

	/* Determines if there's enough crawlers for the current orders */
	void HandleCrawlerOrder();

protected:
	virtual void BeginPlay() override;

	void IncrementSpawnIndex();

	void UpdateAICount();

protected:
	FTimerHandle GameStartTimerHandle;

	UPROPERTY(VisibleAnywhere)
	bool bGameStarted = false;

	/** Specifies the area in the bar the AI can move */
	UPROPERTY(EditAnywhere, Category = "AI Navigation")
	UBoxComponent* BarBoundingArea;

	/** First place AI will navigate to */
	UPROPERTY(EditAnywhere, Category = "AI Navigation", Meta = (MakeEditWidget = true))
	FVector BarEntrance;

	/** Last place AI will navigate to */
	UPROPERTY(EditAnywhere, Category = "AI Navigation", Meta = (MakeEditWidget = true))
	FVector BarExit;

	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "Order System", meta = (ClampMin = "0", ClampMax = "6"))
	int32 MaxNumOrders = 6;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Order System")
	int32 CurrentNumOrders;

	/** How many aliens can try to order while the orders are maxed out? */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Order System")
	int32 NumFlexOrders = 2;

	UPROPERTY(EditAnywhere, Category = "Spawning System")
	TSubclassOf<AODAICharacter> AIBlueprint;

	/** How many AI should spawn at the start of this level? */
	UPROPERTY(EditAnywhere, Category = "Spawning System")
	int32 NumStartingAI = 3;

	/** What's the max number of AI this bar can hold? */
	UPROPERTY(VisibleAnywhere, Category = "Spawning System")
	int32 CurrentAICapacity;

	/** What's the max number of AI this bar can hold? */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Spawning System")
	int32 MaxAICapacity = 20;

	/** This number specifies how far out of range we can get from our max capacity for spawning */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Spawning System")
	int32 CapacityFlexValue = 5;

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere, Category = "Spawning System")
	float AICapacityProportion = 0.f;

	/** List of positions AI can spawn in outside of the bar */
	UPROPERTY(EditAnywhere, Category = "Spawning System", Meta = (MakeEditWidget = true))
	TArray<FVector> AISpawnPositions;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI Attributes")
	float MinThirstThreshold = 30.f;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI Attributes")
	float MaxThirstThreshold = 65.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI Attributes")
	float MinAggroThreshold = 30.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI Attributes")
	float MaxAggroThreshold = 100.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI Attributes")
	int32 MinOrderWaitTime = 20;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI Attributes")
	int32 MaxOrderWaitTime = 40;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI Attributes")
	int32 MinOrderPrepTime = 20;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI Attributes")
	int32 MaxOrderPrepTime = 50;

	UPROPERTY(EditAnywhere, Category = "Wwise")
	UAkRtpc* BarPopuplationRTPC;

	int32 CurrentSpawnIndex = 0;

	/** List of currently active AI agents spawned by this manager */
	UPROPERTY()
	TArray<AODAICharacter*> ActiveAICustomers;

	UPROPERTY()
	TArray<AODAISpawnArea*> AISpawnAreas;

	TArray<FVector> PastStartupSpawnLocations;

	// This is used so we can't spawn inside of another capsule
	float CapsuleRadius = 34.f;
};
