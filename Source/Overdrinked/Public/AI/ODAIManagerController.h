// Copyright 2020 Space Gurus

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "ODAIManagerController.generated.h"

class AODAIManager;

/**
 * 
 */
UCLASS()
class OVERDRINKED_API AODAIManagerController : public AAIController
{
	GENERATED_BODY()
	
public:
	AODAIManagerController(const class FObjectInitializer& ObjectInitializer);

	virtual void OnPossess(APawn* InPawn) override;

	UFUNCTION(Server, Reliable)
	void Server_CreateNewCustomer();

protected:
	/** The manager we're controlling */
	AODAIManager* AIManager;

	UPROPERTY(EditDefaultsOnly, Category = "AI")
	UBehaviorTree* BehaviorTree;

	UBlackboardComponent* BlackboardComponent;
};
