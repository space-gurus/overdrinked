// Copyright 2020 Space Gurus

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "ODDataTypes.generated.h"

UCLASS()
class OVERDRINKED_API UODDataTypes : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
	
};

#pragma region ENUMS

UENUM(BlueprintType)
enum class EFeedbackType : uint8
{
	FT_Poor		UMETA(DisplayName = "Poor"),
	FT_Fair		UMETA(DisplayName = "Fair"),
	FT_Good		UMETA(DisplayName = "Good"),
	FT_Great	UMETA(DisplayName = "Great")
};

#pragma endregion ENUMS