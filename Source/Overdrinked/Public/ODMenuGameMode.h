// Copyright 2020 Space Gurus

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameMode.h"
#include "ODMenuGameMode.generated.h"

class AODMenuPlayerController;

/**
 * 
 */
UCLASS()
class OVERDRINKED_API AODMenuGameMode : public AGameMode
{
	GENERATED_BODY()
	
public:
	AODMenuGameMode();

	virtual void PostLogin(APlayerController* NewPlayer) override;
	virtual void Logout(AController* Exiting) override;

	virtual void HandleMatchHasStarted() override;

	virtual void PostSeamlessTravel() override;

	virtual void HandleStartingNewPlayer_Implementation(APlayerController* NewPlayer) override;

	void HandleNewPlayer(APlayerController* NewPlayer);

	UFUNCTION(BlueprintCallable)
	void CloseLobby();

	AODMenuPlayerController* GetHostPlayerController();

	void NotifyPlayersOfMapTravel();

	void NotifyPlayersOfReadyStatus(int32 PlayerIndex, bool bIsReady);

	UFUNCTION(BlueprintCallable)
	void NotifyPlayersOfNewMapName(FName MapName);

protected:
	TArray<AODMenuPlayerController*> PlayerControllerList;

	uint32 NumberOfPlayers = 0;

	UPROPERTY(BlueprintReadOnly)
	bool bArrivedSeamless = false;

	bool bHandledHostSeamless = false;
};
