// Copyright 2020 Space Gurus

#pragma once

#include "CoreMinimal.h"
#include "Actors/ODBevContainer.h"
#include "ODBlender.generated.h"

class UMaterialInstanceDynamic;

/**
 * 
 */
UCLASS()
class OVERDRINKED_API AODBlender : public AODBevContainer
{
	GENERATED_BODY()
	
public:
	AODBlender(const FObjectInitializer& OI);

	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

protected:
	virtual void BeginPlay() override;

public:
	virtual void PlayerInteraction_Implementation(APlayerCharacter* Player) override;
	virtual void BecomeRelevant_Implementation(APlayerCharacter* PlayerTriggered) override;

	virtual void Server_AddIngredient_Implementation(EIngredientType IngredientToAdd, float Amount) override;

	void Server_AddContainerContents_Implementation(FContainerContents NewContents, bool bIsAddition) override;

	bool CanBeBlended();

	UFUNCTION(BlueprintCallable)
	float GetBlendProgress();

	UFUNCTION(BlueprintCallable)
	void SetBlendProgress(float val);

	UFUNCTION(BlueprintCallable)
	float GetOverflowProgress();

	UFUNCTION(BlueprintCallable)
	void SetOverflowProgress(float val);

	void AdjustLiquidMaterial();

	void OnRep_CurrentVolume() override;

protected:
	/** The material representing the liquid in the glass */
	UMaterialInstanceDynamic* FillMaterialDynamic;

	/** The material representing the actual glass of the blender */
	UMaterialInstanceDynamic* GlassMaterialDynamic;

	/* When this reaches 1.0, the drink is fully blended. Adding ingredients resets this */
	UPROPERTY(Replicated, VisibleAnywhere)
	float BlendProgress = 0.f;

	/* If this reaches 1.0, the drink is lost */
	UPROPERTY(Replicated, VisibleAnywhere)
	float OverflowProgress = 0.f;
};
