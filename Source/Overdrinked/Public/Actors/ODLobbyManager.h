// Copyright 2020 Space Gurus

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ODLobbyManager.generated.h"

class AODLobbyPlayer;
class AODMenuPlayerController;

UCLASS()
class OVERDRINKED_API AODLobbyManager : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AODLobbyManager();

	/** Returns a reference to the new player avatar */
	void SpawnPlayerAvatar(AODMenuPlayerController* NewPlayerController);

	void RemovePlayerAvatar(AODMenuPlayerController* NewPlayerController);

	AActor* GetPlayerViewCamera();

protected:
	UPROPERTY(EditAnywhere)
	TSubclassOf<AODLobbyPlayer> LobbyPlayerBlueprint;

	/** List of positions player representations will spawn */
	UPROPERTY(EditAnywhere, Meta = (MakeEditWidget = true))
	TArray<FVector> PlayerPositions; 

	UPROPERTY(EditAnywhere)
	AActor* PlayerViewCamera;

	int32 NumOfPlayers = 0;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

};
