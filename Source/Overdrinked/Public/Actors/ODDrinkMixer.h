// Copyright 2020 Space Gurus

#pragma once

#include "CoreMinimal.h"
#include "Actors/ODBevContainer.h"
#include "ODDrinkMixer.generated.h"

class UODMixerWidget;

/**
 * A BevContainer that has mixing functionality. Mixed drinks must be made first in this container.
 */
UCLASS()
class OVERDRINKED_API AODDrinkMixer : public AODBevContainer
{
	GENERATED_BODY()
	
public:
	AODDrinkMixer(const FObjectInitializer& OI);

	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

protected:
	/** Has this mixer received enough input to be fully shaken? Adding an ingredient resets this */
	UPROPERTY(BlueprintReadOnly)
	bool bHasBeenFullyShaken = false;

	int32 ShakeAmount = 0;

	/** When the mixer reaches this amount, the drink is fully shaken */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Drink System")
	int32 FullyShakenThreshold = 10;

	UPROPERTY(BlueprintReadOnly)
	bool bHasBeenFullyStirred = false;

protected:
	virtual void BeginPlay() override;

public:
	/** Add an ingredient to the container. */
	virtual void Server_AddIngredient_Implementation(EIngredientType IngredientToAdd, float Amount) override;

	/** Add a point of shaking to the mixer. Adding an ingredient resets if it's been shaken 
	* Returns true if a drink was made, false otherwise
 	*/
	UFUNCTION(BlueprintCallable, Category = "Drink System")
	bool ShakeMixer();

	bool HasBeenFullyShaken();

	void UpdateMixerWidget();

protected:
	void ResetShake();

	UFUNCTION(BlueprintCallable, Category = "Drink System")
	void StirMixer();

	void PrintIngredients();


};
