// Copyright 2020 Space Gurus

#pragma once

#include "CoreMinimal.h"
#include "Actors/ODGarnish.h"
#include "ODIce.generated.h"

class UNiagaraComponent;

/**
 * 
 */
UCLASS()
class OVERDRINKED_API AODIce : public AODGarnish
{
	GENERATED_BODY()

public:
	AODIce(const FObjectInitializer& OI);

protected:
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
	
	virtual void BeginPlay() override;

public:
	virtual void Tick(float DeltaTime) override;
	
	void StartMelting();

	void FinishMelting();

	void HandlePickup_Implementation(AActor* ActorPickingUp) override;

	void HandleDrop_Implementation() override;

protected:
	UPROPERTY(EditAnywhere)
	UNiagaraComponent* IceMistEffect;
	
protected:
	UPROPERTY(Replicated, VisibleAnywhere)
	float IceMeltTime = 0.f;

	UPROPERTY(Replicated, EditAnywhere)
	float IceMeltSpeed = 5.f;

	UPROPERTY(Replicated)
	bool bShouldMelt = false;

	FVector OriginalScale;
};
