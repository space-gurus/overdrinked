// Copyright 2020 Space Gurus

#pragma once

#include "CoreMinimal.h"
#include "Actors/ODPickupActor.h"
#include "IngredientsEnum.h"
#include "Gameplay/ODMixMethod.h"
#include "Gameplay/ODGarnishType.h"
#include "Gameplay/ODGlassType.h"
#include "Gameplay/ODContainerContents.h"
#include "ODBevContainer.generated.h"

class UStaticMeshComponent;
class UTextRenderComponent;
class UWidgetComponent;
class UMaterialInterface;
class UMaterialInstanceDynamic;
class UDataTable;
class UODDrinkComponent;
class UODMixerWidget;
class UODContainerWidget;
class AODBevIngredient;
class AODCounter;
class AODDrink;

UCLASS()
class OVERDRINKED_API AODBevContainer : public AODPickupActor
{
	GENERATED_BODY()
	
public:	
	AODBevContainer(const FObjectInitializer& OI);

	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

public:
	/** This is used for editing the fill level of the mesh material */
	UPROPERTY()
	UMaterialInstanceDynamic* DynamicDrinkMaterial;

protected:
	/** The stuff actually inside this container */
	UPROPERTY(Replicated, VisibleAnywhere, BlueprintReadOnly, Category = "Drink System")
	FContainerContents ContainerContents;

	UPROPERTY(EditAnywhere, Category = "Drink System")
	EGlassType GlassType;

	/** The widget representing this container */
	UODContainerWidget* ContainerWidget;

	bool bIsBeingHeldByAI = false;

	AODCounter* ClosestCounter;

	UPROPERTY(BlueprintReadOnly)
	UDataTable* IngredientDataTable;

	UPROPERTY(Replicated)
	FLinearColor LiquidColor;

	/** Max amount of liquid this container can hold */
	UPROPERTY(Replicated, EditAnywhere, Category = "Drink System")
	float MaxVolume = 10.f;

	/** Current amount of liquid in this container */
	UPROPERTY(ReplicatedUsing=OnRep_CurrentVolume, EditAnywhere, Category = "Drink System")
	float CurrentVolume = 0.f;

	/** Property used to set fill height of material. Proportionate to the maxVolume */
	UPROPERTY(Replicated)
	float MaterialFillAmount = 0.f;

protected:
	virtual void BeginPlay() override;

	UFUNCTION()
	virtual void OnRep_CurrentVolume();

	void PrintIngredients();

	FString GetRoleEnumText(ENetRole role);

public:	
	virtual void Tick(float DeltaTime) override;

	virtual void MeshColliderBeginOverlap(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) override;

	virtual void MeshColliderEndOverlap(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex) override;

	/// Interact interface
	virtual void PlayerInteraction_Implementation(APlayerCharacter* Player) override;
	virtual void BecomeRelevant_Implementation(APlayerCharacter* PlayerTriggered) override;

	virtual void ShowPickupHighlight() override;

	virtual void EndHighlight() override;

	FContainerContents GetContainerContents();

	EGlassType GetGlassType();

	UFUNCTION(BlueprintCallable)
	bool IsContainerEmpty();

	/** Set the reverse reference for updating the widget attached to this container */
	void SetWidgetReference(UODContainerWidget* NewContainerWidget);

	UODContainerWidget* GetMixerWidget();

	/** Add an ingredient to the container. Returns true if successful, must be overridden */
	UFUNCTION(Server, Reliable)
	virtual void Server_AddIngredient(EIngredientType IngredientToAdd, float Amount);

	/** Remove an ingredient from the container. A player probably shouldn't be able to do this, must be overridden */
	UFUNCTION(Server, Reliable)
	virtual void Server_RemoveIngredient(EIngredientType IngredientToRemove);

	UFUNCTION(Server, Reliable)
	virtual void Server_AddGarnish(EGarnishType GarnishToAdd, int32 Amount);

	UFUNCTION(Server, Reliable)
	virtual void Server_RemoveGarnish(EGarnishType GarnishToRemove);

	UFUNCTION(Server, Reliable)
	void Server_TransferContents(AODBevContainer* OtherContainer);

	UFUNCTION(Server, Reliable)
	virtual void Server_AddContainerContents(FContainerContents NewContents, bool bIsAddition);

	void Dump();

	UFUNCTION(Server, Reliable)
	void RemoveAllContents();

	virtual void HandlePickup(AActor* ActorPickingUp) override;

	virtual void HandleDrop();

	UFUNCTION(NetMulticast, Reliable)
	void ScaleMesh(float NewScale);

	bool GetIsBeingHeld();

	bool IsBeingHeldByPlayer();

	bool IsBeingHeldByAI();

	bool GetIsLockedOnGridSpace();

	float GetMaxVolume();

	float GetCurrentVolume();

	FLinearColor GetLiquidColor();

	/** Try to pour the contents of this container into something else */
	void TryToPourContents(AODBevContainer* OtherContainer);

};