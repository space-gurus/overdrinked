// Copyright 2020 Space Gurus

#pragma once

#include "CoreMinimal.h"
#include "Actors/ODPickupActor.h"
#include "ODTaser.generated.h"

class AODCounter;
class UNiagaraSystem;
class UNiagaraComponent;
class UWidgetComponent;
class UAkAudioEvent;

/**
 * 
 */
UCLASS()
class OVERDRINKED_API AODTaser : public AODPickupActor
{
	GENERATED_BODY()
	
public:
	AODTaser(const FObjectInitializer& OI);

	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	virtual void HandlePickup(AActor* ActorPickingUp) override;

	virtual void HandleDrop();

	UFUNCTION(NetMulticast, Reliable)
	void UseTaser();

	UFUNCTION()
	void OnRep_IsBeingHeld();

protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UWidgetComponent* HelperWidget;

	UPROPERTY(EditAnywhere, Category = "Wwise")
	UAkAudioEvent* TaserFireSound;

	UPROPERTY(EditAnywhere, Category = "Effects")
	UNiagaraSystem* TaserNS;

	UPROPERTY(EditAnywhere)
	USceneComponent* TaserEffectSpawnLoc;

	UPROPERTY(ReplicatedUsing=OnRep_IsBeingHeld)
	bool bIsBeingHeld = false;
};
