// Copyright 2020 Space Gurus

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "ODInteractInterface.generated.h"

// This class does not need to be modified.
UINTERFACE(BlueprintType)
class UODInteractInterface : public UInterface
{
	GENERATED_BODY()
};

/**
 *  This interface is used when a player can interact with the actor
 */
class OVERDRINKED_API IODInteractInterface
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Interact Interface")
	void PlayerInteraction(APlayerCharacter* Player);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Interact Interface")
	void BecomeRelevant(APlayerCharacter* PlayerTriggered);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Interact Interface")
	void LoseRelevance();
};
