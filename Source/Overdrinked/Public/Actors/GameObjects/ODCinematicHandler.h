// Copyright 2020 Space Gurus

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ODCinematicHandler.generated.h"

class ALevelSequenceActor;
class ULevelSequencePlayer;
class ULevelSequence;

UCLASS()
class OVERDRINKED_API AODCinematicHandler : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AODCinematicHandler();

protected:
	UPROPERTY(EditAnywhere)
	ULevelSequencePlayer* FadeInSequencePlayer;

	ALevelSequenceActor* LevelSequenceActor;

	UPROPERTY(EditAnywhere)
	ULevelSequence* FadeInSequence;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

};
