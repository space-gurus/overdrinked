// Copyright 2020 Space Gurus

#pragma once

#include "CoreMinimal.h"
#include "Actors/ODBevContainer.h"
#include "ODDrinkGlass.generated.h"

class UNiagaraSystem;
class UNiagaraComponent;

/**
 * 
 */
UCLASS()
class OVERDRINKED_API AODDrinkGlass : public AODBevContainer
{
	GENERATED_BODY()

public:
	AODDrinkGlass(const FObjectInitializer& OI);

	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	virtual void BeginPlay() override;

	void AdjustLiquidMaterial();

	void SetFillMaterialProperties();

	virtual void OnRep_CurrentVolume() override;

	// Decrease the current volume without affecting the ingredients in the glass, returns the actual amount decreased
	float DecreaseVolume(float Amount);

	void ClearDrink();

	virtual void PlayerInteraction_Implementation(APlayerCharacter* Player) override;

	virtual void Server_AddIngredient_Implementation(EIngredientType IngredientToAdd, float Amount) override;

protected:
	UPROPERTY(EditAnywhere)
	UMaterialInstance* DirtyGlassMaterial;

	UPROPERTY(EditAnywhere)
	UMaterialInstance* CleanGlassMaterial;

	/** The material representing the liquid in the glass */
	UMaterialInstanceDynamic* FillMaterialDynamic;

	/** The material representing the actual glass */
	UMaterialInstanceDynamic* GlassMaterialDynamic;

	UPROPERTY(EditAnywhere)
	UNiagaraSystem* DirtyEffect;

	UNiagaraComponent* DirtyNiagara;

	/** Does this glass need to be washed before being used again? */
	UPROPERTY(Replicated)
	bool bIsGlassDirty = false;

protected:
	void OnRep_IsHoldingDrink();

public:
	bool IsGlassDirty();

	void MakeDirty();

	void MakeClean();
};
