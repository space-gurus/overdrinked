// Copyright 2020 Space Gurus

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ODSkelPickupActor.generated.h"

class USkeletalMeshComponent;
class UBoxComponent;

UCLASS()
class OVERDRINKED_API AODSkelPickupActor : public AActor
{
	GENERATED_BODY()
	
public:	
	AODSkelPickupActor(const FObjectInitializer& OI);

protected:
	virtual void BeginPlay() override;

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

protected:
	UPROPERTY(EditAnywhere)
	USkeletalMeshComponent* SkeletalMesh;

	UPROPERTY(EditAnywhere, Category = "Pickup Properties")
	FVector LocationOffset;

	UPROPERTY(EditAnywhere, Category = "Pickup Properties")
	FRotator RotationOffset;

	int32 DefaultCustomDepth = 255;

public:
	UFUNCTION(NetMulticast, Reliable)
	virtual void HandlePickup(AActor* ActorPickingUp);

	UFUNCTION(NetMulticast, Reliable)
	virtual void HandleDrop();
};
