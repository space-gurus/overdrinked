// Copyright 2020 Space Gurus

#pragma once

#include "CoreMinimal.h"
#include "Actors/ODGarnish.h"
#include "ODCrawler.generated.h"

class UNiagaraComponent;

/**
 * 
 */
UCLASS()
class OVERDRINKED_API AODCrawler : public AODGarnish
{
	GENERATED_BODY()
	
public:
	AODCrawler(const FObjectInitializer& OI);

	void HandlePickup_Implementation(AActor* ActorPickingUp) override;

	void HandleDrop_Implementation() override;

protected:
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	virtual void BeginPlay() override;

};
