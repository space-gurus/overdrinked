// Copyright 2020 Space Gurus

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Gameplay/ODGlassType.h"
#include "ODEnvGlass.generated.h"

UCLASS()
class OVERDRINKED_API AODEnvGlass : public AActor
{
	GENERATED_BODY()
	
public:	
	AODEnvGlass();

protected:
	UPROPERTY(EditAnywhere)
	UStaticMeshComponent* StaticMesh;

	UPROPERTY(EditAnywhere, Category = "Glassware")
	UStaticMesh* StemmedGlassMesh;

	UPROPERTY(EditAnywhere, Category = "Glassware")
	UStaticMesh* BrewGlassMesh;

	UPROPERTY(EditAnywhere, Category = "Glassware")
	UStaticMesh* ShotGlassMesh;

protected:
	virtual void BeginPlay() override;

public:
	void SetGlassMesh(EGlassType GlassType);

};
