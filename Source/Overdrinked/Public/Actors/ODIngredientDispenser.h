// Copyright 2020 Space Gurus

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Actors/ODInteractInterface.h"
#include "IngredientsEnum.h"
#include "ODIngredientDispenser.generated.h"

class UDataTable;
class UWidgetComponent;
class UAkAudioEvent;
class UArrowComponent;

UCLASS()
class OVERDRINKED_API AODIngredientDispenser : public AActor, public IODInteractInterface
{
	GENERATED_BODY()
	
public:
	AODIngredientDispenser(const FObjectInitializer& OI);

	virtual void BeginPlay() override;

protected:
	UPROPERTY(EditAnywhere)
	UStaticMeshComponent* StaticMesh;

	UPROPERTY(EditAnywhere)
	UArrowComponent* ForwardArrow;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UWidgetComponent* WidgetComponent;

	UPROPERTY(EditAnywhere, Category = "Drink System")
	EIngredientType IngredientType;

	UDataTable* IngredientDataTable;

	/** The amount this dispenser sends out each time it's asked for ingredients */
	UPROPERTY(EditAnywhere, Category = "Drink System")
	float DispenseAmount = 0.1f;

	// SOUNDS
	UPROPERTY(EditAnywhere, Category = "Wwise")
	UAkAudioEvent* DispenseSound;

	int32 DispensePlayingID;

	FTimerHandle DispenseSoundTimerHandle;

	UPROPERTY(EditAnywhere, Category = "Wwise")
	UAkAudioEvent* DispenseEndSound;

	bool bIsPlayingDispenseLoopSound = false;

	int32 DefaultCustomDepth = 255;
	int32 HighlightCustomDepth = 240;

public:
	UFUNCTION()
	void MeshBeginOverlap(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
	void MeshEndOverlap(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	/// Interact interface
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Interact Interface")
	void PlayerInteraction(APlayerCharacter* Player);
	virtual void PlayerInteraction_Implementation(APlayerCharacter* Player) override;

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Interact Interface")
	void BecomeRelevant(APlayerCharacter* PlayerTriggered);
	virtual void BecomeRelevant_Implementation(APlayerCharacter* PlayerTriggered) override;

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Interact Interface")
	void LoseRelevance();
	virtual void LoseRelevance_Implementation() override;
	/////////////

	void ShowHighlight();

	void EndHighlight();

	TPair<EIngredientType, float> DispenseIngredient();

	/** Get the amount of ingredient should be pulled */
	float GetDispenseAmount();

	EIngredientType GetIngredientType();

	bool IsPlayingDispenseLoopSound();

	UFUNCTION(NetMulticast, Reliable)
	void StartDispenseSound();

	UFUNCTION(NetMulticast, Reliable)
	void FinishDispenseSound();

protected:
	void PourWidgetSetup();

	void DispenseSoundTimerUp();

};
