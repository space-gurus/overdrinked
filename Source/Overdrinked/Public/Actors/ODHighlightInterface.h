// Copyright 2020 Space Gurus

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "ODHighlightInterface.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UODHighlightInterface : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class OVERDRINKED_API IODHighlightInterface
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	void ShowHighlight();

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	void EndHighlight();

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	bool IsHighlighted();
};
