// Copyright 2020 Space Gurus

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Actors/ODInteractInterface.h"
#include "ODPickupActor.generated.h"

class UMaterialInstanceDynamic;
class AODCounter;

UCLASS()
class OVERDRINKED_API AODPickupActor : public AActor, public IODInteractInterface
{
	GENERATED_BODY()
	
public:	
	AODPickupActor(const FObjectInitializer& OI);

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

protected:
	/** This is used for editing the highlight of the mesh material */
	UPROPERTY(Replicated)
	UMaterialInstanceDynamic* ExteriorDynamicMaterial;

	UPROPERTY(EditAnywhere)
	UStaticMeshComponent* StaticMesh;

	UPROPERTY(EditAnywhere)
	FColor PickupHighlightColor;

	UPROPERTY(EditAnywhere)
	FColor InteractHighlightColor;

	UPROPERTY(EditAnywhere)
	FRotator PickupRotationOffset;

	UPROPERTY(EditAnywhere);
	FVector PickupPositionOffset;

	UPROPERTY(EditAnywhere)
	FRotator DropRotationOffset;

	UPROPERTY(EditAnywhere);
	FVector DropPositionOffset;

	UPROPERTY(EditAnywhere)
	FRotator CounterAttachRotationOffset;

	UPROPERTY(EditAnywhere);
	FVector CounterAttachPositionOffset;

protected:
	virtual void BeginPlay() override;

	UFUNCTION()
	virtual void MeshColliderBeginOverlap(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
	virtual void MeshColliderEndOverlap(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(NetMulticast, Reliable)
	virtual void HandlePickup(AActor* ActorPickingUp);

	UFUNCTION(NetMulticast, Reliable)
	virtual void HandleDrop();

	/// Interact interface
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Interact Interface")
	void PlayerInteraction(APlayerCharacter* Player);
	virtual void PlayerInteraction_Implementation(APlayerCharacter* Player) override;

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Interact Interface")
	void BecomeRelevant(APlayerCharacter* PlayerTriggered);
	virtual void BecomeRelevant_Implementation(APlayerCharacter* PlayerTriggered) override;

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Interact Interface")
	void LoseRelevance();
	virtual void LoseRelevance_Implementation() override;
	/////////////

	/** Try to attach to the given actor (counter) */
	virtual void AttachToCounter(AODCounter* counter);

	virtual void ShowPickupHighlight();

	virtual void ShowInteractHighlight();

	virtual void EndHighlight();

	bool IsHighlighted();

	/** Is this actor already attached to something else? */
	bool IsAttached();

	void SetIsAttached(bool val);

protected:
	int32 DefaultCustomDepth = 255;

	bool bIsPickupHighlighted = false;

	bool bIsInteractHighlighted = false;

	/** Is a player holding this item? */
	bool bIsBeingHeldByPlayer = false;

	/** Is this item locked on a grid spot? */
	UPROPERTY(VisibleAnywhere, Replicated)
	bool bIsAttached = false;

	UPROPERTY(EditAnywhere)
	AODCounter* ParentCounter;
};
