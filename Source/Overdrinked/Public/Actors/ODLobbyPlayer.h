// Copyright 2020 Space Gurus

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ODLobbyPlayer.generated.h"

class USkeletalMeshComponent;
class UWidgetComponent;
class UODLobbyPlayerWidget;
class AODMenuPlayerController;
class AODLobbyManager;

UCLASS()
class OVERDRINKED_API AODLobbyPlayer : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AODLobbyPlayer(const FObjectInitializer& OI);

	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	void SetPlayerControllerReference(AODMenuPlayerController* NewPlayerController);

	void SetLobbyManagerReference(AODLobbyManager* NewManager);

	UFUNCTION(NetMulticast, Reliable)
	void UpdateReadyStatus(bool val);

	UFUNCTION()
	void UpdatePlayerName(FName PlayerName);

	void SetPlayerCameraView(AODMenuPlayerController* PlayerController);

	UFUNCTION()
	void OnRep_AttachedPlayerName();

	void SetAttachedPlayerName(FName PlayerName);

protected:
	UPROPERTY(EditAnywhere)
	USkeletalMeshComponent* SkeletalMesh;

	UPROPERTY(EditAnywhere)
	UWidgetComponent* LobbyPlayerWidgetComponent;

	TSubclassOf<UODLobbyPlayerWidget> LobbyWidgetBPClass;
	UODLobbyPlayerWidget* LobbyPlayerWidget;

	AODLobbyManager* LobbyManager;

	AODMenuPlayerController* AttachedPlayerController;

	UPROPERTY(ReplicatedUsing=OnRep_AttachedPlayerName)
	FName AttachedPlayerName;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
};
