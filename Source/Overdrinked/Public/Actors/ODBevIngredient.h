// Copyright 2020 Space Gurus

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ODBevIngredient.generated.h"

UCLASS()
class OVERDRINKED_API AODBevIngredient : public AActor
{
	GENERATED_BODY()
	
public:	
	AODBevIngredient();

protected:
	virtual void BeginPlay() override;

public:	
	virtual void Tick(float DeltaTime) override;

	FString GetIngredientType();

protected:
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Ingredient Properties")
	FString IngredientType;

};
