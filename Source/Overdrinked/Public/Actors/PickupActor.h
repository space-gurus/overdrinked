// Copyright 2020 Space Gurus

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "PickupActor.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UPickupActor : public UInterface
{
	GENERATED_BODY()
};

/**
 * Actors should implement this interface if the player can pick them up and carry them around.
 */
class OVERDRINKED_API IPickupActor
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Pickup Actions")
	void HandlePickup(AActor* playerActor);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Pickup Actions")
	void HandleDrop();
};
