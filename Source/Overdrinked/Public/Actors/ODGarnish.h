// Copyright 2020 Space Gurus

#pragma once

#include "CoreMinimal.h"
#include "Actors/ODPickupActor.h"
#include "Gameplay/ODGarnishType.h"
#include "ODGarnish.generated.h"

class UNiagaraComponent;
class UBoxComponent;
class AODCounter;

UCLASS()
class OVERDRINKED_API AODGarnish : public AODPickupActor
{
	GENERATED_BODY()
	
public:	
	AODGarnish(const FObjectInitializer& OI);

protected:
	virtual void BeginPlay() override;

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

public:	
	virtual void Tick(float DeltaTime) override;

	EGarnishType GetGarnishType();

protected:
	UPROPERTY(EditAnywhere)
	UBoxComponent* PickupCollider;

	UPROPERTY(EditAnywhere, Category = "Gameplay")
	EGarnishType GarnishType;

protected:
	bool bIsHighlighted = false;

	/** Is a player holding this item? */
	bool bIsBeingHeldByPlayer = false;

	/** Is this item locked on a grid spot? */
	bool bIsLockedOnGridSpace = false;
};
