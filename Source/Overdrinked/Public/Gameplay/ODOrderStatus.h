// Copyright 2020 Space Gurus

#pragma once
UENUM(BlueprintType)
enum class EOrderStatus : uint8
{
	OS_Prep	UMETA(DisplayName = "Prep"),
	OS_Expired UMETA(DisplayName = "Expired"),
	OS_Tasered UMETA(DisplayName = "Tasered"),
	OS_WrongCustomer UMETA(DisplayName = "Wrong Customer"),
	OS_DeliveredBad UMETA(DisplayName = "Delivered Bad"),
	OS_DeliveredMediocre UMETA(DisplayName = "Delivered Mediocre"),
	OS_DeliveredGreat UMETA(DisplayName = "Delivered Great"),
	OS_DeliveredPerfect UMETA(DisplayName = "Delivered Perfect")
};