#pragma once

#include "CoreMinimal.h"
#include "IngredientsEnum.h"
#include "Gameplay/ODGarnishType.h"
#include "Gameplay/ODMixMethod.h"
#include "ODContainerContents.generated.h"

USTRUCT(BlueprintType)
struct FContainerContents
{
	GENERATED_BODY()

	/** Paired with IngredientAmounts to act as a map that can be replicated */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	TArray<EIngredientType> IngredientTypes;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	TArray<float> IngredientAmounts;

	/** Paired with GarnishAmounts to act as a map that can be replicated */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	TArray<EGarnishType> GarnishTypes;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	TArray<int32> GarnishAmounts;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	EMixMethod MixMethod;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	bool bIsMixed;
};