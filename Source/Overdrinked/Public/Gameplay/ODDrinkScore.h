// Copyright 2020 Space Gurus

#pragma once
UENUM(BlueprintType)
enum class EDrinkScore : uint8
{
	DS_A	UMETA(DisplayName = "A"),
	DS_B	UMETA(DisplayName = "B"),
	DS_C	UMETA(DisplayName = "C"),
	DS_D	UMETA(DisplayName = "D"),
	DS_F	UMETA(DisplayName = "F")
};