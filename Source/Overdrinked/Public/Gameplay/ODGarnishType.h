// Copyright 2020 Space Gurus

#pragma once
UENUM(BlueprintType)
enum class EGarnishType : uint8
{
	GT_None		UMETA(DisplayName = "None"),
	GT_Ice		UMETA(DisplayName = "Ice"),
	GT_Crawler		UMETA(DisplayName = "Crawler")
};