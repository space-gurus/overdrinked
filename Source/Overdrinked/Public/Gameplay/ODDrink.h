// Copyright 2020 Space Gurus

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "IngredientsEnum.h"
#include "Gameplay/ODMixMethod.h"
#include "Gameplay/ODDrinkScore.h"
#include "ODDrink.generated.h"

class UDataTable;
class AODIngredient;

/**
 *  A non-spawnable class representing a drink for gameplay purposes.
 */
UCLASS()
class OVERDRINKED_API AODDrink : public AActor
{
	GENERATED_BODY()
	
public:
	AODDrink();

	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	/** Get the name of this drink if it's completed */
	FString GetDrinkName();

	float GetRecipeVariance();

	float GetTargetRecipeVolume();

	EDrinkScore GetDrinkRating();
	
	void BuildDrink(TArray<AODIngredient*> ProvidedIngredients);

protected:
	void AssignDrinkRating();

protected:
	UPROPERTY(BlueprintReadOnly)
	UDataTable* RecipeData;
	
	UPROPERTY(Replicated)
	FString DrinkName;

	UPROPERTY(Replicated)
	EDrinkScore DrinkRating;

	UPROPERTY(Replicated)
	EMixMethod MixMethod;

	UPROPERTY(Replicated)
	float RecipeVariance;

	UPROPERTY(Replicated)
	float TargetRecipeVolume;

	UPROPERTY(Replicated)
	float ActualDrinkVolume;

	float IngredientVarianceThreshold = 0.1f;
	float RecipeVarianceThreshold = 2.0f;

	float GradeAThreshold = 0.f;
	float GradeBThreshold = 1.f;
	float GradeCThreshold = 2.f;
	float GradeDThreshold = 3.f;
	float GradeFThreshold = 4.f;
};
