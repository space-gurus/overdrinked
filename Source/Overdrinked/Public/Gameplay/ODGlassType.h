// Copyright 2020 Space Gurus

#pragma once
UENUM(BlueprintType)
enum class EGlassType : uint8
{
	GT_Mixer	UMETA(DisplayName = "Mixer"),
	GT_Brew		UMETA(DisplayName = "Brew"),
	GT_Stemmed	UMETA(DisplayName = "Stemmed"),
	GT_Blender	UMETA(DisplayName = "Blender"),
	GT_Shot		UMETA(DisplayName = "Shot")
};