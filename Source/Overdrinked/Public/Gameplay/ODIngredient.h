// Copyright 2020 Space Gurus

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ODIngredient.generated.h"

UCLASS()
class OVERDRINKED_API AODIngredient : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AODIngredient();

	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

public:
	UPROPERTY(Replicated, EditAnywhere)
	FName Name;

	UPROPERTY(Replicated, EditAnywhere)
	float Amount;
	
	UPROPERTY(Replicated, EditAnywhere)
	FColor Color;

};
