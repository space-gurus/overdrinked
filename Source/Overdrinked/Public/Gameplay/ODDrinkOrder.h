// Copyright 2020 Space Gurus

#pragma once

#include "CoreMinimal.h"
#include "IngredientsEnum.h"
#include "DrinksEnum.h"
#include "Gameplay/ODGlassType.h"
#include "Gameplay/ODGarnishType.h"
#include "Gameplay/ODMixMethod.h"
#include "Gameplay/ODOrderStatus.h"
#include "ODDrinkOrder.generated.h"

class AODAICharacter;

USTRUCT(BlueprintType)
struct FDrinkOrder
{
	GENERATED_USTRUCT_BODY()

	FDrinkOrder() : OrderOwner(NULL), DesiredMixMethod(EMixMethod::MM_None), DesiredGlass(EGlassType::GT_Brew), OrderTimer(0), OrderPrepTimer(0), OrderNumber(-1) {}

	/** Pointer to the actor who placed this order */
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
	AODAICharacter* OrderOwner;

	/** What ingredients the customer wants */
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
	TArray<EIngredientType> DesiredIngredients;

	/** What amounts of ingredients the customer wants */
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
	TArray<int32> DesiredIngredientAmounts;

	/** What garnishes the customer wants (in what amounts) */
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
	TArray<EGarnishType> DesiredGarnishes;

	/** What amount of garnishes the customer wants */
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
	TArray<int32> DesiredGarnishAmounts;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
	EMixMethod DesiredMixMethod;

	/** Glass the customer wants the drink served in */
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
	EGlassType DesiredGlass;

	/** How long (in seconds) this customer is willing to wait for their order to be taken */
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
	int32 OrderTimer;

	/** How long (in seconds) this customer is willing to wait for their drink after their order has been taken*/
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
	int32 OrderPrepTimer;

	/** Unique ID for this order */
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
	int32 OrderNumber;

	/** Status of this order */
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
	EOrderStatus OrderStatus;

	/** Tips earned from this order */
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
	int32 Tip = 0;

public:
	FORCEINLINE bool operator==(const FDrinkOrder& Other) const { return OrderNumber == Other.OrderNumber; }
};