// Copyright 2020 Space Gurus

#pragma once
UENUM(BlueprintType)
enum class EMixMethod : uint8
{
	MM_None			UMETA(DisplayName = "None"),
	MM_Shake		UMETA(DisplayName = "Shake"),
	MM_Blend		UMETA(DisplayName = "Blend"),
	MM_Straight		UMETA(DisplayName = "Straight")
};