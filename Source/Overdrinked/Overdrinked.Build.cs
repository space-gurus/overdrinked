// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class Overdrinked : ModuleRules
{
	public Overdrinked(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay", "Slate", "SlateCore", "OnlineSubsystem", "OnlineSubsystemSteam", "OnlineSubsystemUtils", "Niagara", "GameplayTasks", "NavigationSystem", "AkAudio", "LevelSequence", "MovieScene", "Switcheroo" });
    }
}
