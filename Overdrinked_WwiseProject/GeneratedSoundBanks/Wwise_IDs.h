/////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Audiokinetic Wwise generated include file. Do not edit.
//
/////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __WWISE_IDS_H__
#define __WWISE_IDS_H__

#include <AK/SoundEngine/Common/AkTypes.h>

namespace AK
{
    namespace EVENTS
    {
        static const AkUniqueID ACTION_ERROR = 73972840U;
        static const AkUniqueID DOOR_OPEN_SHANTY = 3287288246U;
        static const AkUniqueID DRINK_DELIVERED = 3953974484U;
        static const AkUniqueID DRINK_ELECTRIFIED = 1803676722U;
        static const AkUniqueID DRINK_ELECTRIFIED_STOP = 2588458897U;
        static const AkUniqueID DRINK_FIZZING = 1667013953U;
        static const AkUniqueID DRINK_FIZZING_STOP = 1141111940U;
        static const AkUniqueID DRINK_POURING = 2122706554U;
        static const AkUniqueID DRINK_SHAKING = 3107248431U;
        static const AkUniqueID DRINK_SMOKING = 3747201778U;
        static const AkUniqueID DRINK_SMOKING_STOP = 662586961U;
        static const AkUniqueID GLASS = 2449969375U;
        static const AkUniqueID MENU_BACK = 3063554414U;
        static const AkUniqueID MENU_ERROR = 4238832851U;
        static const AkUniqueID MENU_HIGHLIGHT = 29030485U;
        static const AkUniqueID MENU_QUIT = 2776193354U;
        static const AkUniqueID MENU_SELECT = 4203375351U;
        static const AkUniqueID PICKUP_OBJECT = 1174059869U;
        static const AkUniqueID PLAYER_DASH = 2394582229U;
        static const AkUniqueID PUTDOWN_OBJECT = 1911282022U;
        static const AkUniqueID WALLA = 2538584666U;
    } // namespace EVENTS

    namespace STATES
    {
        namespace FINAL_MINUTE
        {
            static const AkUniqueID GROUP = 3338430254U;

            namespace STATE
            {
                static const AkUniqueID NO = 1668749452U;
                static const AkUniqueID NONE = 748895195U;
                static const AkUniqueID YES = 979470758U;
            } // namespace STATE
        } // namespace FINAL_MINUTE

        namespace MAIN_MENU
        {
            static const AkUniqueID GROUP = 2005704188U;

            namespace STATE
            {
                static const AkUniqueID ACTIVE = 58138747U;
                static const AkUniqueID IDLE = 1874288895U;
                static const AkUniqueID NONE = 748895195U;
            } // namespace STATE
        } // namespace MAIN_MENU

        namespace MIX_STATE
        {
            static const AkUniqueID GROUP = 4154157223U;

            namespace STATE
            {
                static const AkUniqueID LOCAL = 4098848842U;
                static const AkUniqueID NETWORK = 731245997U;
                static const AkUniqueID NONE = 748895195U;
            } // namespace STATE
        } // namespace MIX_STATE

        namespace MUSIC_BREAKDOWN
        {
            static const AkUniqueID GROUP = 3589492710U;

            namespace STATE
            {
                static const AkUniqueID NO = 1668749452U;
                static const AkUniqueID NONE = 748895195U;
                static const AkUniqueID YES = 979470758U;
            } // namespace STATE
        } // namespace MUSIC_BREAKDOWN

        namespace MUSIC_CHOICE
        {
            static const AkUniqueID GROUP = 2794192044U;

            namespace STATE
            {
                static const AkUniqueID NONE = 748895195U;
                static const AkUniqueID ONE = 1064933119U;
                static const AkUniqueID THREE = 912956111U;
                static const AkUniqueID TWO = 678209053U;
            } // namespace STATE
        } // namespace MUSIC_CHOICE

        namespace PAUSE
        {
            static const AkUniqueID GROUP = 3092587493U;

            namespace STATE
            {
                static const AkUniqueID NONE = 748895195U;
                static const AkUniqueID PAUSE = 3092587493U;
                static const AkUniqueID UNPAUSED = 1365518790U;
            } // namespace STATE
        } // namespace PAUSE

        namespace PIPE_IN_USE
        {
            static const AkUniqueID GROUP = 2068050237U;

            namespace STATE
            {
                static const AkUniqueID NO = 1668749452U;
                static const AkUniqueID NONE = 748895195U;
                static const AkUniqueID YES = 979470758U;
            } // namespace STATE
        } // namespace PIPE_IN_USE

    } // namespace STATES

    namespace SWITCHES
    {
        namespace CRAWLER_TYPE
        {
            static const AkUniqueID GROUP = 306037130U;

            namespace SWITCH
            {
                static const AkUniqueID BUG = 714721617U;
                static const AkUniqueID RAT = 1047714102U;
            } // namespace SWITCH
        } // namespace CRAWLER_TYPE

        namespace ICE
        {
            static const AkUniqueID GROUP = 344481046U;

            namespace SWITCH
            {
                static const AkUniqueID OVERDRIVE = 2988571309U;
                static const AkUniqueID WITH = 1704837181U;
                static const AkUniqueID WITHOUT = 190872515U;
            } // namespace SWITCH
        } // namespace ICE

    } // namespace SWITCHES

    namespace GAME_PARAMETERS
    {
        static const AkUniqueID BAR_BUSINESS = 3730107905U;
        static const AkUniqueID BAR_POPULATION = 1228110158U;
        static const AkUniqueID BAR_TIME = 2302825700U;
        static const AkUniqueID CHAOS = 4242684915U;
        static const AkUniqueID DIALOGUE_BUS = 573466488U;
        static const AkUniqueID DIALOGUE_SC = 137050204U;
        static const AkUniqueID FILL_COUNTER_PITCH = 1777411164U;
        static const AkUniqueID MASTER_VOLUME = 4179668880U;
        static const AkUniqueID MUSIC_BUS = 2680856269U;
        static const AkUniqueID PLAYER_SC = 3923732995U;
        static const AkUniqueID SFX_BUS = 213475909U;
        static const AkUniqueID SHAKE_SPEED = 2342370205U;
        static const AkUniqueID STINGER_SC = 1475575706U;
        static const AkUniqueID VEHICLE_DISTANCE_PLAYER = 1557689533U;
    } // namespace GAME_PARAMETERS

    namespace TRIGGERS
    {
        static const AkUniqueID FINAL_MINUTE = 3338430254U;
    } // namespace TRIGGERS

    namespace BANKS
    {
        static const AkUniqueID INIT = 1355168291U;
        static const AkUniqueID AMBIENT = 77978275U;
        static const AkUniqueID DRINK = 2201969025U;
        static const AkUniqueID MUSIC = 3991942870U;
        static const AkUniqueID PLAYER = 1069431850U;
        static const AkUniqueID UI = 1551306167U;
    } // namespace BANKS

    namespace BUSSES
    {
        static const AkUniqueID AMBIENT_BUS = 3148666284U;
        static const AkUniqueID DIALOGUE_BUS = 573466488U;
        static const AkUniqueID MASTER_AUDIO_BUS = 3803692087U;
        static const AkUniqueID MENU_BUS = 3022218555U;
        static const AkUniqueID MUSIC_BUS = 2680856269U;
        static const AkUniqueID SFX_BUS = 213475909U;
    } // namespace BUSSES

    namespace AUX_BUSSES
    {
        static const AkUniqueID CYCLE_1_REVERB = 2102949344U;
        static const AkUniqueID MAIN_MENU_REVERB = 2362224979U;
    } // namespace AUX_BUSSES

    namespace AUDIO_DEVICES
    {
        static const AkUniqueID NO_OUTPUT = 2317455096U;
        static const AkUniqueID SYSTEM = 3859886410U;
    } // namespace AUDIO_DEVICES

}// namespace AK

#endif // __WWISE_IDS_H__
