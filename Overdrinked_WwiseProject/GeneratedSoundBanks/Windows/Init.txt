State Group	ID	Name			Wwise Object Path	Notes
	2068050237	pipe_in_use			\Default Work Unit\pipe_in_use	
	4154157223	mix_state			\Default Work Unit\mix_state	

State	ID	Name	State Group			Notes
	748895195	None	pipe_in_use			
	979470758	yes	pipe_in_use			
	1668749452	no	pipe_in_use			
	731245997	network	mix_state			
	748895195	None	mix_state			
	4098848842	local	mix_state			

Game Parameter	ID	Name			Wwise Object Path	Notes
	137050204	dialogue_sc			\Mix\dialogue_sc	
	213475909	sfx_bus			\Mix\sfx_bus	
	573466488	dialogue_bus			\Mix\dialogue_bus	
	2680856269	music_bus			\Mix\music_bus	
	3923732995	player_sc			\Mix\player_sc	
	4179668880	master_volume			\Mix\master_volume	

Audio Bus	ID	Name			Wwise Object Path	Notes
	213475909	SFX_Bus			\Default Work Unit\Master Audio Bus\SFX_Bus	
	573466488	Dialogue_Bus			\Default Work Unit\Master Audio Bus\Dialogue_Bus	
	2680856269	Music_Bus			\Default Work Unit\Master Audio Bus\Music_Bus	
	3022218555	Menu_Bus			\Default Work Unit\Master Audio Bus\Menu_Bus	
	3148666284	Ambient_Bus			\Default Work Unit\Master Audio Bus\Ambient_Bus	
	3803692087	Master Audio Bus			\Default Work Unit\Master Audio Bus	

Auxiliary Bus	ID	Name			Wwise Object Path	Notes
	2102949344	Cycle_1_Reverb			\Default Work Unit\Master Audio Bus\Cycle_1_Reverb	
	2362224979	Main_Menu_Reverb			\Default Work Unit\Master Audio Bus\Main_Menu_Reverb	

Effect plug-ins	ID	Name	Type				Notes
	137050204	Dialogue_SC	Wwise Meter			
	213475909	SFX_bus	Wwise Compressor			
	506891147	Plate_Small	Wwise RoomVerb			
	1759944409	Cut_3dB_100Hz	Wwise Parametric EQ			
	2891462749	Room_Medium_High_Absorbtion	Wwise RoomVerb			
	3923732995	player_sc	Wwise Meter			

Audio Devices	ID	Name	Type				Notes
	2317455096	No_Output	No Output			
	3859886410	System	System			

