Event	ID	Name			Wwise Object Path	Notes
	662586961	drink_smoking_stop			\Default Work Unit\Drink_Sounds\drink_smoking_stop	
	1141111940	drink_fizzing_stop			\Default Work Unit\Drink_Sounds\drink_fizzing_stop	
	1667013953	drink_fizzing			\Default Work Unit\Drink_Sounds\drink_fizzing	
	1803676722	drink_electrified			\Default Work Unit\Drink_Sounds\drink_electrified	
	2122706554	drink_pouring			\Default Work Unit\Drink_Sounds\drink_pouring	
	2588458897	drink_electrified_stop			\Default Work Unit\Drink_Sounds\drink_electrified_stop	
	3107248431	drink_shaking			\Default Work Unit\Drink_Sounds\drink_shaking	
	3747201778	drink_smoking			\Default Work Unit\Drink_Sounds\drink_smoking	
	3953974484	drink_delivered			\Default Work Unit\Drink_Sounds\drink_delivered	

Game Parameter	ID	Name			Wwise Object Path	Notes
	2605305585	ice_amount			\Default Work Unit\ice_amount	

Effect plug-ins	ID	Name	Type				Notes
	3948262040	Octave_Down	Wwise Pitch Shifter			

In Memory Audio	ID	Name	Audio source file		Wwise Object Path	Notes	Data Size
	43285523	S_DrinkSmoking_End_Mono	Z:\Volumes\Orico\Overdrinked\Overdrinked_WwiseProject\.cache\Windows\SFX\S_DrinkSmoking_End_Mono_10C4C929.wem		\Actor-Mixer Hierarchy\Drink Sounds\Drink Effects\Smoking\S_DrinkSmoking_End_Mono		171342
	123495595	S_DrinkElectrified_Start_Mono	Z:\Volumes\Orico\Overdrinked\Overdrinked_WwiseProject\.cache\Windows\SFX\S_DrinkElectrified_Start_Mono_10C4C929.wem		\Actor-Mixer Hierarchy\Drink Sounds\Drink Effects\Electrified\S_DrinkElectrified_Start_Mono		162004
	143583664	S_DrinkDelivered_Mono_4	Z:\Volumes\Orico\Overdrinked\Overdrinked_WwiseProject\.cache\Windows\SFX\S_DrinkDelivered_Mono_4_10C4C929.wem		\Actor-Mixer Hierarchy\Drink Sounds\Drink Delivered\S_DrinkDelivered_Mono_4		163792
	200069082	S_DrinkShaking_WithIce_Mono_4	Z:\Volumes\Orico\Overdrinked\Overdrinked_WwiseProject\.cache\Windows\SFX\S_DrinkShaking_WithIce_Mono_4_D79FB2B7.wem		\Actor-Mixer Hierarchy\Drink Sounds\Drink Shaking\4\S_DrinkShaking_WithIce_Mono_4		441144
	229130055	S_DrinkFizzing_End_Mono	Z:\Volumes\Orico\Overdrinked\Overdrinked_WwiseProject\.cache\Windows\SFX\S_DrinkFizzing_End_Mono_10C4C929.wem		\Actor-Mixer Hierarchy\Drink Sounds\Drink Effects\Fizzing\S_DrinkFizzing_End_Mono		426856
	244465050	S_DrinkPouring_Mono_6	Z:\Volumes\Orico\Overdrinked\Overdrinked_WwiseProject\.cache\Windows\SFX\S_DrinkPouring_Mono_6_10C4C929.wem		\Actor-Mixer Hierarchy\Drink Sounds\Drink Pouring\S_DrinkPouring_Mono_6		59900
	245142758	S_DrinkDelivered_Mono_3	Z:\Volumes\Orico\Overdrinked\Overdrinked_WwiseProject\.cache\Windows\SFX\S_DrinkDelivered_Mono_3_10C4C929.wem		\Actor-Mixer Hierarchy\Drink Sounds\Drink Delivered\S_DrinkDelivered_Mono_3		156874
	266856117	S_DrinkPouring_Mono_4	Z:\Volumes\Orico\Overdrinked\Overdrinked_WwiseProject\.cache\Windows\SFX\S_DrinkPouring_Mono_4_10C4C929.wem		\Actor-Mixer Hierarchy\Drink Sounds\Drink Pouring\S_DrinkPouring_Mono_4		130230
	272594352	S_DrinkShaking_WithIce_Mono_3	Z:\Volumes\Orico\Overdrinked\Overdrinked_WwiseProject\.cache\Windows\SFX\S_DrinkShaking_WithIce_Mono_3_D79FB2B7.wem		\Actor-Mixer Hierarchy\Drink Sounds\Drink Shaking\3\S_DrinkShaking_WithIce_Mono_3		227994
	350605720	S_DrinkPouring_Mono_3	Z:\Volumes\Orico\Overdrinked\Overdrinked_WwiseProject\.cache\Windows\SFX\S_DrinkPouring_Mono_3_10C4C929.wem		\Actor-Mixer Hierarchy\Drink Sounds\Drink Pouring\S_DrinkPouring_Mono_3		121684
	402435983	S_DrinkPouring_Mono_2	Z:\Volumes\Orico\Overdrinked\Overdrinked_WwiseProject\.cache\Windows\SFX\S_DrinkPouring_Mono_2_10C4C929.wem		\Actor-Mixer Hierarchy\Drink Sounds\Drink Pouring\S_DrinkPouring_Mono_2		104088
	418504067	S_DrinkPouring_Mono_5	Z:\Volumes\Orico\Overdrinked\Overdrinked_WwiseProject\.cache\Windows\SFX\S_DrinkPouring_Mono_5_10C4C929.wem		\Actor-Mixer Hierarchy\Drink Sounds\Drink Pouring\S_DrinkPouring_Mono_5		93428
	450023182	S_DrinkDelivered_Mono_1	Z:\Volumes\Orico\Overdrinked\Overdrinked_WwiseProject\.cache\Windows\SFX\S_DrinkDelivered_Mono_1_10C4C929.wem		\Actor-Mixer Hierarchy\Drink Sounds\Drink Delivered\S_DrinkDelivered_Mono_1		144044
	466929166	S_DrinkDelivered_Mono_2	Z:\Volumes\Orico\Overdrinked\Overdrinked_WwiseProject\.cache\Windows\SFX\S_DrinkDelivered_Mono_2_10C4C929.wem		\Actor-Mixer Hierarchy\Drink Sounds\Drink Delivered\S_DrinkDelivered_Mono_2		143354
	472474403	S_DrinkPouring_Mono_1	Z:\Volumes\Orico\Overdrinked\Overdrinked_WwiseProject\.cache\Windows\SFX\S_DrinkPouring_Mono_1_10C4C929.wem		\Actor-Mixer Hierarchy\Drink Sounds\Drink Pouring\S_DrinkPouring_Mono_1		74806
	523119816	S_DrinkSmoking_Loop_Mono	Z:\Volumes\Orico\Overdrinked\Overdrinked_WwiseProject\.cache\Windows\SFX\S_DrinkSmoking_Loop_Mono_D79FB2B7.wem		\Actor-Mixer Hierarchy\Drink Sounds\Drink Effects\Smoking\S_DrinkSmoking_Loop_Mono		178882
	605794721	S_DrinkShaking_WithIce_Mono_1	Z:\Volumes\Orico\Overdrinked\Overdrinked_WwiseProject\.cache\Windows\SFX\S_DrinkShaking_WithIce_Mono_1_D79FB2B7.wem		\Actor-Mixer Hierarchy\Drink Sounds\Drink Shaking\1\S_DrinkShaking_WithIce_Mono_1		236308
	617151990	S_DrinkFizzing_Loop_Mono	Z:\Volumes\Orico\Overdrinked\Overdrinked_WwiseProject\.cache\Windows\SFX\S_DrinkFizzing_Loop_Mono_D79FB2B7.wem		\Actor-Mixer Hierarchy\Drink Sounds\Drink Effects\Fizzing\S_DrinkFizzing_Loop_Mono		362118
	677905694	S_DrinkFizzing_Start_Mono	Z:\Volumes\Orico\Overdrinked\Overdrinked_WwiseProject\.cache\Windows\SFX\S_DrinkFizzing_Start_Mono_10C4C929.wem		\Actor-Mixer Hierarchy\Drink Sounds\Drink Effects\Fizzing\S_DrinkFizzing_Start_Mono		342284
	716248758	S_DrinkSmoking_Start_Mono	Z:\Volumes\Orico\Overdrinked\Overdrinked_WwiseProject\.cache\Windows\SFX\S_DrinkSmoking_Start_Mono_10C4C929.wem		\Actor-Mixer Hierarchy\Drink Sounds\Drink Effects\Smoking\S_DrinkSmoking_Start_Mono		316210
	726020057	S_DrinkShaking_NoIce_Mono_4	Z:\Volumes\Orico\Overdrinked\Overdrinked_WwiseProject\.cache\Windows\SFX\S_DrinkShaking_NoIce_Mono_4_D79FB2B7.wem		\Actor-Mixer Hierarchy\Drink Sounds\Drink Shaking\4\S_DrinkShaking_NoIce_Mono_4		529344
	735293303	S_DrinkShaking_NoIce_Mono_3	Z:\Volumes\Orico\Overdrinked\Overdrinked_WwiseProject\.cache\Windows\SFX\S_DrinkShaking_NoIce_Mono_3_D79FB2B7.wem		\Actor-Mixer Hierarchy\Drink Sounds\Drink Shaking\3\S_DrinkShaking_NoIce_Mono_3		597516
	803925834	S_DrinkElectrified_Loop_Mono	Z:\Volumes\Orico\Overdrinked\Overdrinked_WwiseProject\.cache\Windows\SFX\S_DrinkElectrified_Loop_Mono_D79FB2B7.wem		\Actor-Mixer Hierarchy\Drink Sounds\Drink Effects\Electrified\S_DrinkElectrified_Loop_Mono		309110
	962025713	S_DrinkShaking_NoIce_Mono_2	Z:\Volumes\Orico\Overdrinked\Overdrinked_WwiseProject\.cache\Windows\SFX\S_DrinkShaking_NoIce_Mono_2_D79FB2B7.wem		\Actor-Mixer Hierarchy\Drink Sounds\Drink Shaking\2\S_DrinkShaking_NoIce_Mono_2		416936
	998748172	S_DrinkShaking_WithIce_Mono_2	Z:\Volumes\Orico\Overdrinked\Overdrinked_WwiseProject\.cache\Windows\SFX\S_DrinkShaking_WithIce_Mono_2_D79FB2B7.wem		\Actor-Mixer Hierarchy\Drink Sounds\Drink Shaking\2\S_DrinkShaking_WithIce_Mono_2		422768
	1013724638	S_DrinkShaking_NoIce_Mono_1	Z:\Volumes\Orico\Overdrinked\Overdrinked_WwiseProject\.cache\Windows\SFX\S_DrinkShaking_NoIce_Mono_1_D79FB2B7.wem		\Actor-Mixer Hierarchy\Drink Sounds\Drink Shaking\1\S_DrinkShaking_NoIce_Mono_1		529344
	1030055471	S_DrinkDelivered_Mono_5	Z:\Volumes\Orico\Overdrinked\Overdrinked_WwiseProject\.cache\Windows\SFX\S_DrinkDelivered_Mono_5_10C4C929.wem		\Actor-Mixer Hierarchy\Drink Sounds\Drink Delivered\S_DrinkDelivered_Mono_5		165652

