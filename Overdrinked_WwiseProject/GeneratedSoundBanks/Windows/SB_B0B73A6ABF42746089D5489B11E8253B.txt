Event	ID	Name			Wwise Object Path	Notes
	335636031	freekus_closing_status			\Default Work Unit\Dialogue\freekus_closing_status	
	2089248438	freekus_general_bar_status			\Default Work Unit\Dialogue\freekus_general_bar_status	
	2957636878	freekus_delivery_status			\Default Work Unit\Dialogue\freekus_delivery_status	
	3908074552	freekus_opening_status			\Default Work Unit\Dialogue\freekus_opening_status	

Switch Group	ID	Name			Wwise Object Path	Notes
	1310233351	closing_status			\Default Work Unit\closing_status	
	1831982039	opening			\Default Work Unit\opening	
	2644002326	delivery_status			\Default Work Unit\delivery_status	
	4201395053	bar_status			\Default Work Unit\bar_status	

Switch	ID	Name	Switch Group			Notes
	513390134	bad	closing_status			
	668632890	good	closing_status			
	998496889	first	opening			
	2376466361	other	opening			
	1961009801	poor	delivery_status			
	2161557176	perfect	delivery_status			
	1427264549	happy	bar_status			
	2684142446	upset	bar_status			

